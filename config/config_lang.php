<?php
define("_MULTI_LANG",true);
define("_DEFAULT_LANG","en");
$_lngVars=array();
if(_MULTI_LANG)
{
    if($cookie=\Classes\Cookie::get("lang"))
    {
        include realpath("config/lang_files/config_".$cookie.".php");
    }
    else
    {
        include realpath("config/lang_files/config_"._DEFAULT_LANG.".php");
    }
}
