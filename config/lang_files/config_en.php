<?php
define("_MONTHS_ARR",serialize(array("Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec")));

$_lngVars=array("_lngName"=>"Name",
      "_lngSurname"=>"Surname",
      "_lngCity"=>"City",
      "_lngEmail"=>"Email",
      "_lngPhoto"=>"Photo",
      "_lngDateBirth"=>"Date of birth",
      "_lngMale"=>"Male",
      "_lngFemale"=>"Female",
      "_lngMethodologyWork"=>"Methodology of work",
      "_lngCategory"=>"Category",
      "_lngPassword"=>"Password",
      "_lngRePassword"=>"Retype password",
      "_lngDescription"=>"Description",
      "_lngStudents"=>"Students",
      "_lngProfessors"=>"Professors",
      "_lngRegistration"=>"Registration",
      "_lngListing"=>"Listing",
      "_lngSignIn"=>"Sign In",
      "_lngLanguage"=>"Language",
      "_lngEnglish"=>"English",
      "_lngSerbian"=>"Serbian",
      "_lngHomePg"=>"Home",
    "_lngPsihoIntroNaslov"=>"Intro",
      "_lngPsihoIntro"=>"<p>This is a psychological test that can offer you a fun pastime, to find out some indications of hidden personality aspects, or perhaps even more than that. Discover it for yourself.
                <br><br>
The test consists of twelve different requirements (items). While solving it, it is necessary to follow the question order and choose one of more proffered answers that fits your attitudes and personality profile the most.
  <br><br>
The test taking time is not limited, but do try to make the most of it in order to make the answers, as well as the test results themselves - as accurate as possible.
  <br><br>
In the end, check your test results.</p>",
    "_lngPsiho2IntroNaslov"=>"Professional affinity test",
     "_lngPsiho2Intro"=>"<p>This test will help you find out which kind of job is best suited for you, regardless of the profession you are dealing with. You will find out what your role in a team is, as well as how you can further improve. There are no correct and incorrect answers.
Please reply to each claim by choosing one of the offered answers, in accordance to the degree of your agreement with the claim.<br><br>
Answers 1 to 4 are offered, and they have the following meaning:<br><br>
I completely disagree / I mostly disagree / I mostly agree / I completely agree<br><br>
In the end, check your test results.</p>",
    "_lngPsihoMladjiIntro"=>"<p>This test is intended for high school students, students and adults, so you can not do it.
                <br><br>
However, we will be happy to get back to you when we place tests for your age, to discover your talents and to check your English language skills.
  <br><br>
We're waiting for you, bye! <i class=\"em em-wink\"></i>
  </p>"
    );


