<?php
define("_MONTHS_ARR",serialize(array("Jan","Feb","Mar","Apr","Maj","Jun","Jul","Avg","Sep","Okt","Nov","Dec")));

$_lngVars=array("_lngName"=>"Ime",
      "_lngSurname"=>"Prezime",
      "_lngCity"=>"Grad",
      "_lngEmail"=>"E - pošta",
      "_lngPhoto"=>"Fotografija",
      "_lngDateBirth"=>"Datum rodjenja",
      "_lngMale"=>"Muško",
      "_lngFemale"=>"Žensko",
      "_lngMethodologyWork"=>"Metodologija rada",
      "_lngCategory"=>"Kategorija",
      "_lngPassword"=>"Lozinka",
      "_lngRePassword"=>"Ponovite lozinku",
      "_lngDescription"=>"Opis",
      "_lngStudents"=>"Studenti",
      "_lngProfessors"=>"Profesori",
      "_lngRegistration"=>"Registracija",
      "_lngListing"=>"Listing",
      "_lngSignIn"=>"Prijavi se",
      "_lngLanguage"=>"Jezik",
      "_lngEnglish"=>"Engleski",
      "_lngSerbian"=>"Srpski",
      "_lngHomePg"=>"Početna",
    "_lngPsihoIntroNaslov"=>"Uputstvo za rešavanje testa.",
    "_lngPsihoIntro"=>"<p>Pred Vama se nalazi psiho-test koji Vam nudi mogućnost razonode, ili otkrivanja nekih naznaka
skrivenih aspekata ličnosti, ali možda i nešto više od toga. Proverite sami!
                <br><br>
Test je konstruisan od 12 različitih zahteva (ajtema). Pri rešavanju Testa neophodno je da po redosledu
pitanja odaberete jedan od više! ponuđenih odgovora, koji se najviše uklapa u Vaš stav i profil ličnosti.
  <br><br>
Vreme za rešavanje testa nije ograničeno, ali iskoristite Vašu prisutnost što adekvatnije, kako bi odgovori
bili što verodostojniji, kao i rezultat samog testiranja.
  <br><br>
Na kraju, pogledajte rezultate testa.</p>",
    "_lngPsiho2IntroNaslov"=>"Test profesionalnih afiniteta",
     "_lngPsiho2Intro"=>"<p>Ovaj test će Vam pomoći da saznate koja vrsta poslova Vam najviše odgovara bez obzira na
profesiju kojom se bavite. Saznaćete koja je vaša uloga u timu, kao i koje možete dalje
usavršavati. Ne postoje tačni i pogrešni odgovori.<br><br>
Molimo Vas da na svaku tvrdju odgovorite tako što ćete izabrati jedno od ponuđenih polja koje
je u skladu sa stepenom vašeg slaganja sa tvrdnjom. Ponuđeni su odgovori od 1 do 4 i imaju
sledeće značenje:<br><br>
Uopšte se ne slažem / Uglavnom se ne slažem / Uglavnom se slažem / U potpunosti se slažem<br><br>
Na kraju, pogledajte rezultate testa.</p>",
     "_lngPsihoMladjiIntro"=>"<p>Ovaj test je namenjen srednjoškolcima, studentima i odraslima, tako da ga ne možeš raditi.
                <br><br>

Ali, sa zadovoljstvom ćemo ti se javiti kada budu postavljeni i testovi za tvoj uzrast, da otkriješ svoje talente i proveriš znanje engleskog jezika.
  <br><br>
Čekamo te, ćaos! <i class=\"em em-wink\"></i>
  </p>"
    );


