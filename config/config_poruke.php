<?php
$config['Poruke']=array
    ("error404"=>"Ne postoji trazena stranica","noModel"=>"Trazeni model ne postoji","noView"=>"Trazeni view ne postoji",
    "noMethod"=>"Pozivate nepostojecu funkciju","noParams"=>"Nedostaju parametri za trazenu funkciju",
    "noBaseCont"=>"Morate naslediti klasu baseController","noBaseMod"=>"Morate naslediti klasu baseModel",
    "noClass"=>"Ucitavate nepostojecu biblioteku");

