<?php
$_val_added_rules_stud=["alphautf8"=>"AlphaUtf8","emailexists"=>"EmailExists","fileajax"=>"FileAjax"];
$_val_rules_stud=[
        'name'         => 'required|alphautf8',
        'surname'         => 'required|alphautf8',
        'city'         => 'required|alphautf8',
        'email'         => 'required|email|emailexists',
        'months'         => 'required|number',
        'days'         => 'required|number',
        'years'         => 'required|number',
        'sex'         => 'required|number',
        'photo'         => 'required|fileajax:image',
        'password'         => 'required|password',
        'retype-password'         => 'required|same:password'
        ];
$_val_added_rules_reg=["alphautf8"=>"AlphaUtf8"];
$_val_rules_reg=[
        'name'         => 'required|alphautf8',
        'email'         => 'required|email',
       /* 'months'         => 'required|number',
        'days'         => 'required|number',
        'years'         => 'required|number',*/
        'sex'         => 'required|alpha',
        ];
$_val_rules_log=[
        'email'         => 'required|email',
        'user'         => 'required|number',
        'password'         => 'required|password',

        ];

