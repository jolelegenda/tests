<?php

error_reporting(-1);
include_once "config_poruke.php";
include_once "config_db.php";
include_once "config_module.php";
include_once "config_lang.php";
include_once realpath("config/validate/validate_rules.php");

define('_ENVIROMENT', 'DEVELOPMENT'); // DEVELOPMENT or PRODUCTION
define("_DEFAULT_CONTROLLER", "home");

define('_WEB_PATH',"http://".$_SERVER['HTTP_HOST'].str_replace(basename($_SERVER['SCRIPT_NAME']),"",$_SERVER['SCRIPT_NAME']));

define("_NEW_PRODUCT_HOME_PAGE",6);

define("_VIEWS_FOLDER", "views");
define("_FOLDER_CLASSES", "classes");

define("_VIEWS_PATH", $_SERVER['DOCUMENT_ROOT']."/all_shine_out/"._VIEWS_FOLDER);
define("_CACHE_FOLDER", $_SERVER['DOCUMENT_ROOT']."/all_shine_out/views/cache/");

//define("_FROM_MAIL", "office@kornetiorijent.com");//savicdragan2707@gmail.com
define("_FROM_MAIL", "savicdragan2707@gmail.com");

define("_DEFAULT_THEME", "testsHome");

//define("_API_URL",'http://138.197.191.236:8081/');
define("_API_URL",'http://api.testyourenglish.ha.rs/');
//define("_API_URL",'http://utest.me:8081/');

/***    TEST CONFIG  ***/
$imaagesConnectNewbie=array("q1"=>"1.png","q2"=>"2.png","q3"=>"3.png");
$imaagesConnectNewbie2=array("q1"=>"Laura was going to arrive even earlier than planned","q2"=>"Adam was supposed to be the best man","q3"=>"The bridesmaids were supposed to carry the flowers","q4"=>"The reception was supposed to be outside","q5"=>"Laura &#39;s ex-boyfriend wasn&#39;t supposed to be there","q6"=>"The couple were going to fly to Bali");
$imagesRadioImageNewbie=array("q1"=>"1.png","q2"=>"2.png","q3"=>"3.png");

$testids=array("Newbie"=>4,"Novice"=>5,"Apprentice"=>12,"Freshman"=>13);
$nextlevel=array("Newbie"=>'Novice',"Novice"=>'Apprentice',"Apprentice"=>"Freshman");

$test_stories=array("skolarci6"=>"Lana’s day<br><br>
My day during the week begins at 7:30, when my mother wakes me up. I get up at 7:35 and I take a shower so
I can wake up properly. Then I get dressed and I have breakfast with my family. That’s at about 7:45.
Sometimes we have cereal, sometimes my mom makes scrambled eggs or pancakes but I prefer simple
peanut butter sandwiches. I get ready and leave home at 8 o’clock to catch the bus. I get to school at about
8:20. Our lessons begin at 8:30, and I always arrive on time. My teachers don’t like when we are late because
late arrivals disrupt classes. At 10 o’clock we have a 20-minute break and after that, lessons continue till our
longer break at 11:50, when we also have a meal. We have about 30 minutes to eat, and we do it in the school
canteen. I usually have some steamed vegetables and fish or meatballs. It’s healthy and tasty. We finish
school at 15:15 and then I go back home.<br><br>
I arrive home at about a quarter to four. When I get home, I do my homework first because I want to be free
as soon as possible. But before that I drink something, usually it’s a glass of milk. Then I have some time to
chat with my family, listen to music, or see some of my friends. But I’m always back before 7:30, because
that’s when we always have dinner. After it, I usually help my mother with the washing up. My mother is a
maths teacher and after dinner, she’s always busy correcting her students’ work and preparing her lessons.
Then I watch TV or play some video games, and after that go to bed.<br><br><b>Choose the correct answer:</b>",
    "skolarci8"=>"example: Molly’s neighbour is watering the plants while she is away. (ARRANGED) Molly ARRANGED HER NEIGHBOUR TO WATER THE PLANTS_ while she is away.<br><br>",
    "tinejdzeri5"=>"Leaves, flowers and seeds<br><br>
One way of recognising a particular plant or a tree is by its leaves. A plant breathes through its
leaves. It also uses them to absorb energy from the sunlight, and to give off excess moisture. The
green colour of most plants is due to chlorophyll, a chemical in the plant which enables it to make
food in its leaves through the action of sunlight.<br>
Examine a leaf carefully. Down the centre is the midrib, which is the continuation of the stem. Veins
branch off from the midrib and from a network which feeds the whole leaf with the nourishment
brought up by the stem from the roots.<br><br>
<img src='"._WEB_PATH."views/testsHome/images/tinejdzeri/1.jpg'/>
    <br><br>
    Each kind of flower has its own shape and colour and sometimes its own scent. As well as being
beautiful a flower has a practical purpose in the life of a plant, for it produces the fruit and seed
from which new plants grow.<br>
Insects are attracted to a flower by its colour, by its scent and by the nectar stored at the base of the
petals. You may have seen a bee busy among the flowers, taking the nectar. As it does this, it picks
up a little of the pollen in the hairs of its body. Pollen is a very fine powder or dust. As the bee flies
about from one flower to another it rubs some of the pollen it has picked up from one flower on to
the sticky pistil of another. The pollen travels down the inside of the pistil to the ovary where the
seeds are formed. In due course the seeds grow and, when they are ripe, if they fall on suitable
ground they will become new plants.<br>
Four conditions are necessary for a seed to begin to grow. There must be enough moisture to enable
young plant to burst its hard shell. It must be warm enough. It must have air to grow. Finally it
needs darkness – seeds begin to grow quicker when they are in the dark.
<br><br>
-from PLANTS AND HOW THEY GROW by F .E. Newing and R. Bowood
",
    "tinejdzeri8"=>"example: You needn’t do it all by yourself. I’ll help you. (HAVE) You DON’T HAVE TO DO IT all by yourself. I’ll help you.<br><br>",
    'stduenti5'=>'Story time<br>
Five minutes east of Amsterdam&#39;s central station is a cinema unlike any other. Members of the
audience are scattered around on swivelling chairs. The viewing areas—two small rooms
with 25 seats each—are bathed in natural light, which pours through large plate-glass
windows. Passers-by peer in. Yet none of this troubles the audience; they are cut off from all
sound and light by their Samsung Gear virtual-reality (VR) kit. Each person is in a world of
their own.<br>The Virtual Reality Cinema opened in March, and runs eight 30-minute shows five days a
week. It is self-sustaining as a business, according to its boss, Jip Samhoud, who has also made
short VR films for his cinema-goers. Finding an audience is not a problem for Mr Samhoud;
the challenge is finding the content. His cinema employs two people full-time to travel the
world in search of fresh VR experiences.<br>Their task should soon become easier. If 2016 was the year of VR gaming, with the release of
PlayStation VR and with Pokemon Go capturing the attention of millions, 2017 will be the
year of VR storytelling.<br>Hollywood studios are pouring money into VR. The Tribeca Film Festival will allow VR films
to compete for awards in 2017. Tencent, a Chinese gaming and messaging giant, has
commissioned VR movies. Other big Chinese firms are watching closely. And the topic will go
mainstream with the release in the spring of &quot;Ghost in the Shell&quot;, a live-action Hollywood
version of the cult Japanese anime series, which features virtual reality as a key part of the
narrative. In 2017, Mr Samhoud expects, &quot;cinematic VR will get to the next level&quot;.<br>Yet it remains unclear what exactly that will be. When cinema was pioneered, early film-
makers either dabbled in spectacle—a train heading towards the screen—or replicated
conventions from theatre, which involved large doses of shouting and melodrama. The first
television shows were in essence radio with pictures. It took until the 1950s for the jump-cut
to come into vogue. Only with time and experimentation did a language develop for
storytelling on screens large and small. A new grammar book is now needed for VR.<br>The first rules are already being drafted, starting with perhaps the most important: the role of
the audience. Oculus, which set off the current VR hype cycle when Facebook bought it for
$2bn in 2014, has created Story Studio, which makes original films. Saschka Unseld, who runs
Story Studio, thinks about VR viewers in a way that is closer to theatre than to cinema or
gaming. The audience is very much present and participating but unable to change or guide a
story, he says.<br>Theatre provides lessons in making movies in VR too, says Mr Samhoud. Actors need to be in
character for the duration of the take, not just when they are in frame; the audience is
omnipresent. Staging and setup must be similarly precise. Likewise, far more time goes into
pre-production than actual performance.<br>Gradually, through a process of trial and error, some rules will become golden. But the first
step is to start putting films out in the public realm for viewers to decide what works and
what doesn&#39;t. More cinemas like the one in Amsterdam are planned, which will allow people
who don&#39;t own the necessary gadgets to experience VR. &quot;Our brains still think in the film
way,&quot; says Mr Unseld. &quot;It will take a while to separate ourselves from that”. That separation
will start to become a (virtual) reality in 2017.<br><strong>- Virtual reality will give rise to new forms of narrative, Leo Mirani - The Economist</strong>'
    );

$_premiumResults=["Skolarci"=>[["Vocabulary"=>[["points"=>[0,5],"interpretation"=>"Vocabulary is the body of words known to an each individual person. It is essential to have a wide
vocabulary in order to be able to communicate in a particular language. Your English vocabulary is
quite poor and you need to work more on enriching it. Reading English texts and doing vocabulary
exercises (like answering questions, translating, writing essays and word building exercises) will
help you accomplish this goal."],["points"=>[6,9],"interpretation"=>"Your English vocabulary is quite limited and you need to work more on enriching it. It is essential to
have a wide vocabulary in order to be able to communicate in a particular language. We
recommend you to read more English texts, do vocabulary exercises (like answering questions,
translating, writing essays and word building exercises) and have more oral practice."],["points"=>[10,13],"interpretation"=>"You do have general English vocabulary knowledge but you need to work more on enriching and
expanding it. Reading English texts, doing vocabulary exercises (like answering questions,
translating, writing essays and doing word building exercises) and oral practice will help you
accomplish this goal."],["points"=>[14,16],"interpretation"=>"You have a wide English language vocabulary needed on this level. Reading English texts,
vocabulary exercises, learning phrasal verbs and oral practice will help you enrich and expand both
your passive (words and phrases you recognise and understand when you hear or read them, but
don’t use them yourself when you speak or write) and active vocabulary (words and phrases you
use actively both when writing and speaking) even more. Keep up the good work!"]]],["Tenses"=>[["points"=>[0,3],"interpretation"=>"You lack basic knowledge about tenses and their use. When expressing one’s thoughts, rich
vocabulary is not enough to be understood properly, it’s very important to know how to use tenses,
too. You need to work harder on their forming as well as on contexts in which to use each particular
tense."],["points"=>[4,7],"interpretation"=>"You need to work on forming and use of tenses. It’s not only important what you say, but also how
you say it. Correct use of tenses will help you be understood properly. Also, mastering tenses is
necessary in order to use more complex grammatical constructions like reported speech and
passive voice."],["points"=>[8,11],"interpretation"=>"You have basic knowledge about tenses and their use, but you need to work more on them. Pay
more attention to the use of past continuous, past simple and present perfect and to all individual
contexts of their usage."],["points"=>[12,13],"interpretation"=>"You are very good at forming and using present simple, continuous and perfect, as well as past
simple and continuous. There are still more tenses you are going to learn as you continue to
improve your English, which will allow you to express yourself better as well as use more complex
grammatical constructions such as passive voice, conditional sentences or reported speech."]]],["Text understanding"=>[["points"=>[0,2],"interpretation"=>"To be able to understand text properly, you need to understand both the meaning of the words and
the contexts of sentences. You need to work on enriching your vocabulary in order to be able to
understand the text."],["points"=>[3,4],"interpretation"=>"Every sentence conveys information which is not always easy to recognise. To be able to fully
understand the meaning and find the information, you need to pay more attention to details while
reading the text."],["points"=>[5,6],"interpretation"=>"You are very good at understanding contexts of sentences and finding the necessary information in
them, as well as recognising fine differences in meaning when the context is changed. You’re on the
right path towards higher vocabulary and semantics levels."]]],["Prepositions"=>[["points"=>[0,2],"interpretation"=>"You need to work more on prepositions. Prepositions are words used to link nouns, pronouns, or
phrases to other words in sentences. There are three types of prepositions: of time, place and
direction, all of them very important for the context of the sentence and general expression."],["points"=>[3,4],"interpretation"=>"You are familiar with basic prepositions of time, place and direction, but you haven’t quite learned
how to use them properly. All of them are very important for the context of the sentence and
general expression."],["points"=>[5,5],"interpretation"=>"You have mastered the basic prepositions of time, place and direction and their use. However, there
are about 150 prepositions in English language which you are going to learn while mastering your
English."]]],["Reported speech"=>[["points"=>[0,1],"interpretation"=>"You need to work on the reported speech. It is used when one is talking about what someone stated
or asked and there are strict rules that must be followed when reporting orders/requests,
statements and questions. These rules include using proper conjunctions and word order."],["points"=>[2,2],"interpretation"=>"You are familiar with the basic rules of reported speech. Still, you need to pay more attention to the
word order when reporting questions (you must know that there is no inversion in the sentence
unless it&#39;s direct question) as well as to appropriate conjunctions."],["points"=>[3,3],"interpretation"=>"You have mastered basic reported speech rules. The next step is to master the concord of tenses
which you will have to apply in reported speech when your English gets to the next level, as well as
the backshift of not only tenses but also prepositions of time and place that have to be applied
accordingly."]]],["Modal verbs"=>[["points"=>[0,1],"interpretation"=>"You need to work more on the modal verbs can/could, must and should. They are used to express
ability, permission, obligation, as well as various other manners to indicate the action. The modal
verbs can and must need substitute verbs to express obligation or ability in past and future."],["points"=>[2,2],"interpretation"=>"You are familiar with the modal verbs can/could, must and should and their use. You need to work
more on substitute verbs which are used to express obligation or ability in past and future."],["points"=>[3,3],"interpretation"=>"You are very good at using the modal verbs can/could, must and should and the substitute verbs
which are used to express obligation or ability in past and future. You are ready for the next level,
on which you will learn how their past/future forms also depend on whether they are used to
express ability, permission, advice, possibility or probability."]]]],
    "Tinejdzeri"=>[["Vocabulary"=>[["points"=>[0,5],"interpretation"=>"Vocabulary is the body of words known to an individual person. It is essential to have a wide
vocabulary to be able to communicate in a particular language. Your English vocabulary is quite
poor for this level and you need to work more on enriching it. Reading English texts and doing
vocabulary exercises (word building exercises, translating and writing essays) as well as learning
phrasal verbs will help you accomplish this goal."],["points"=>[6,9],"interpretation"=>"Your English vocabulary is quite limited for this level and you need to work more on enriching and
expanding it. It is essential to have a wide vocabulary in order to be able to communicate in a
particular language. You are recommended to read more English texts do vocabulary exercises (like
translating, writing essays and doing word building exercises) and learn phrasal verbs."],["points"=>[10,13],"interpretation"=>"You do have general English vocabulary knowledge but you need to work more on enriching and
expanding it. Reading English texts and books, oral practice, doing vocabulary exercises (word
building exercises, translating and writing various types of essays) as well as learning phrasal verbs
will help you accomplish this goal."],["points"=>[14,16],"interpretation"=>"You have a considerable English language vocabulary needed on this level. Reading English texts
and books, doing vocabulary exercises, learning phrasal verbs as well as having active
conversations with native speakers will help you enrich and expand both your passive (words and
phrases you recognise and understand when hearing or reading them, but don’t use them yourself)
and active vocabulary (words and phrases you use actively both in written and spoken form) even
more. Keep up the good work!"]]],
        ["Text understanding"=>[["points"=>[0,1],"interpretation"=>"To be able to understand text properly, you need to understand both the meaning of the words and
the given context. You need to work on enriching your vocabulary in order to be able to understand
the text."],["points"=>[2,5],"interpretation"=>"Text understanding requires not only being familiar with the basic word meaning, but also with its
meanings in various contexts. To understand the text properly, you need to work more on enriching
your vocabulary as well as on understanding the given context."],["points"=>[6,9],"interpretation"=>"Every sentence conveys a subcontext which is not always easy to recognise. To be able to fully
understand subtle differences in various contexts, you need to pay more attention to details while
reading the text."],["points"=>[10,11],"interpretation"=>"You are very good at understanding sentence context as well as recognising subtle differences in
meaning when the context is changed. You’re on the right path towards higher vocabulary and
semantics levels."]]],
        ["Concord of tenses"=>[["points"=>[0,3],"interpretation"=>"You lack basic knowledge about tenses and their use. When expressing one’s thoughts, rich
vocabulary is not enough to be understood properly, it’s very important to know how to use tenses,
too. You need to work harder on their individual use, as well as on concord of tenses."],["points"=>[4,6],"interpretation"=>"You need to work on use and concord of tenses. It’s not only important what you say, but also how
you say it. Correct use of tenses will help you be understood properly. Also, mastering tenses is
necessary in order to use more complex grammatical constructions like passive voice, reported
speech and conditional sentences."],["points"=>[7,9],"interpretation"=>"You have basic knowledge about use and concord of tenses, but you need to work more on them.
Pay more attention to the use of past perfect/past perfect continuous and present perfect/present
perfect continuous, as well as on ways of expressing future in the past."],["points"=>[10,11],"interpretation"=>"You are good at forming, using and concord of tenses which are necessary for mastering more
complex grammatical constructions like reported speech, passive voice and subjunctive mood."]]],
        ["Passive voice"=>[["points"=>[0,3],"interpretation"=>"You lack basic knowledge about the passive voice. It is often used in English language, even more
often than the active voice since English sentence must contain a grammatical subject. Therefore, in
the passive voice the role/function of the subject in the sentence is assumed by an object which is
acted upon by someone or something, or in case when the conveyor of the action in the sentence is
unknown, irrelevant, or omitted."],["points"=>[4,5],"interpretation"=>"You have basic knowledge about the passive voice, but you haven’t mastered its forming and use
yet. Pay attention to its use in progressive tenses, after modal verbs, in infinitive constructions, and
especially to its forming in concord of tenses."],["points"=>[6,6],"interpretation"=>"You are very good at forming and using the passive voice in various syntaxes and grammatical
constructions. You are ready to move on to the next level."]]],
        ["Expressing future"=>[["points"=>[0,1],"interpretation"=>"You lack basic knowledge about how to express the future in English language. Referring to future
time situations can be done in several ways, by using present continuous, be going to construction,
modal verb will etc. The main difference between them is what exactly is being expressed (plan,
intention, prediction and so on) by choosing to use each of them."],["points"=>[2,2],"interpretation"=>"You are familiar with the ways of expressing future in English language. However, you need to pay
attention to concord of tenses as well when referring to future time situations."],["points"=>[3,3],"interpretation"=>"You have mastered the ways of expressing future. You are ready for higher level future tenses such
as future continuous, future perfect and future perfect continuous."]]],
        ["Conjunctions"=>[["points"=>[0,1],"interpretation"=>"You need to work more on conjunctions. Conjunctions are very important for constructing
sentences. They join words, phrases and clauses together."],["points"=>[2,2],"interpretation"=>"You are familiar with conjunctions, but you haven’t mastered them yet. Conjunctions are very
important because they link together different parts of sentences to help emphasizing ideas or
forming more complex sentences."],["points"=>[3,3],"interpretation"=>"You have mastered the types of conjunctions and their use. Conjunctions are very important
because they link together different parts of sentences to help emphasize ideas or form more
complex sentences, for which you seem to be ready."]]]
        ],
       "Studenti"=>[["Vocabulary"=>[["points"=>[0,6],"interpretation"=>"Vocabulary is the body of words known to an each individual person. It is essential to have a wide
vocabulary to be able to communicate in a particular language. Your English vocabulary is quite
poor for this level and you need to work more on enriching and expanding it. Reading English texts,
oral practice and doing vocabulary exercises (word building exercises, translating and writing
essays) covering various fields of topics (general, science, environment, culture, technology,
engineering, business etc.), learning phrasal verbs, as well as various idiomatic phrases that allow
one to get into the spirit of the language, will help you accomplish this goal."],["points"=>[7,11],"interpretation"=>"Your English vocabulary is quite limited for this level and you need to work more on enriching and
expanding it. It is essential to have a wide vocabulary in order to be able to communicate in a
particular language. You are recommended to read more English texts, have oral practice (speaking
as well as having dialogues) and do vocabulary exercises (translating, writing essays and word
building exercises) covering various fields of topics (general, science, environment, culture,
technology, engineering, business etc.), learn phrasal verbs, as well as various idiomatic phrases
that allow one to get into the spirit of the language, to accomplish this goal."],["points"=>[12,16],"interpretation"=>"You do have general English vocabulary knowledge needed for this level but you need to work
more on enriching and expanding it. Reading English texts and books and doing vocabulary
exercises (translating, writing different types of essays and word building exercises) covering
various fields of topics (general, science, environment, culture, technology, engineering, business
etc.), learning phrasal verbs, as well as various idiomatic phrases that allow one to get into the
spirit of the language, and also having active conversations (discussions or debates) with native
speakers will help you accomplish this goal."],["points"=>[17,19],"interpretation"=>"You have a broad English language vocabulary, but you can always do even better. Reading English
texts and books and doing vocabulary exercises (translating, writing different types of essays and
word building exercises) covering various fields of topics (general, science, environment, culture,
technology, engineering, business etc.), learning phrasal verbs, as well as various idiomatic phrases
that allow one to get into the spirit of the language, and also having active conversations
(discussions or debates) with native speakers will get you there in no time. Keep up the good work!"]]],
         ["Concord of tenses"=>[["points"=>[0,5],"interpretation"=>"You lack the firm elemental knowledge about tenses and their use. When expressing one’s thoughts,
rich vocabulary is not enough to be understood properly, it’s also very important to know how to
use all tenses. Therefore you need to work much more on use and concord of tenses, their
formation as well as their more complex grammatical constructions."],["points"=>[6,10],"interpretation"=>"You need to work more on use and concord of tenses. It’s not only important what you say, but also
how you say it. Correct use of tenses will help you be understood properly. Also, mastering tenses is
necessary in order to use more complex grammatical constructions like reported speech,
conditional sentences and subjunctive mood."],["points"=>[11,14],"interpretation"=>"You do possess quite a bit of knowledge about use and concord of tenses, but you need to work
more on them. Pay more attention to the use of past perfect/past perfect continuous and present
perfect/present perfect continuous, as well as on passive forms of verbs, and to their use in more
complex grammatical constructions like reported speech, conditional sentences and subjunctive
mood."],["points"=>[15,17],"interpretation"=>"You are very good at forming, using and concord of tenses which are necessary for mastering more
complex grammatical constructions like reported speech, passive voice and subjunctive mood.
You’ve made an insignificantly small number of mistakes, but you are almost at the top of your
game!"]]],
          ["Text understanding"=>[["points"=>[0,4],"interpretation"=>"Text understanding requires not just being familiar with the basic word meaning, but also with a
whole wide range of its meanings in various contexts or in different (often idiomatic) phrases and
syntactic meanings. To understand the text properly, you need to work more on enriching and
expanding your vocabulary as well as on understanding the given context."],["points"=>[5,6],"interpretation"=>"Every sentence conveys a subcontext which is not always easy to recognise. To be able to fully
understand subtle differences in various contexts, you need to pay more attention to details while
reading the text, avoid ambiguities and pinpoint finer layers in meaning."],["points"=>[7,8],"interpretation"=>"You are very good at understanding contexts of sentences and recognising subtle differences in
meaning when the context is changed. You’re on the right track toward highest levels."]]],
           ["Passive voice"=>[["points"=>[0,2],"interpretation"=>"You lack basic knowledge about the passive voice. Its use is even more common in English language
than the use of the active voice, hence the need to master its forming and use. As you probably
know by now, in passive voice the object assumes the function of the subject when the actual
grammatical subject (the conveyor of the action) acts upon it, but it is also often used when the
above mentioned subject of the sentence (actual conveyor of the action or state) is unknown,
irrelevant, or omitted."],["points"=>[3,4],"interpretation"=>"You do have basic knowledge about the passive voice, but you haven’t mastered its forming and use
yet. Pay attention to its use after modal verbs, in infinitive and progressive constructions, and
especially to its forming in concord of tenses."],["points"=>[5,6],"interpretation"=>"You are quite good at forming and using the passive voice in various syntactic and grammatical
constructions.."]]],
           ["Perfect infinitive"=>[["points"=>[0,1],"interpretation"=>"You need to work more on the perfect infinitive. It is often used after certain verbs, modals or in
conditional sentences to express things that could/may/might have happened in the past or will be
completed at a point in the future."],["points"=>[2,2],"interpretation"=>"You are familiar with the perfect infinitive, but you need to learn more about its forming and use,
especially in negative sentences and with passive voice."],["points"=>[3,3],"interpretation"=>"You have mastered the forming and use of the perfect infinitive."]]],
           ["Participles"=>[["points"=>[0,1],"interpretation"=>"You need to work more on participles. They are formed from a verb and modify a noun, noun
phrase, verb or verb phrase. There are three types of participles in English language. You need to
know the difference as well as contexts in which to use each of them."],["points"=>[2,2],"interpretation"=>"You are familiar with participles, but you haven’t mastered them yet. Bear in mind there are
present, past and perfect participle in English language. You need to know the difference between
contexts in which to use each of them."],["points"=>[3,3],"interpretation"=>"You have mastered all three types of participles as well as different contexts in which you should
use them. Well done."]]],
           ["Conditional sentences"=>[["points"=>[0,1],"interpretation"=>"You need to work more on conditional sentences. They are used to express the action which can
only take place if a certain condition is fulfilled, with varying levels of probability. You should learn
more about the types and their use."],["points"=>[2,2],"interpretation"=>"You have learned how to use conditional sentences. However, you need to work more on the use of
all four types of conditional sentences with various tense combinations, modal verbs and omitting
if, as well as in cases when other conjunctions (positive or negative) are being used."],["points"=>[3,3],"interpretation"=>"You have mastered the types and use of conditional sentences. You are ready to tackle the most
complex grammatical and syntactic structures."]]]
           ]
    ];

$_additionalUnits=["Skolarci"=>[["1"=>["Articles","Possessive adjectives/pronouns","Relative clauses","Indefinite pronouns","Genitive (Saxon)","Genitive (Norman)"]],["2"=>["Quantifiers","Plural of nouns","Subject and object questions","Comparison of adjectives/adverbs"]]],
    "Tinejdzeri"=>[["1"=>["Bare infinitive","Articles","Quantifiers","Relative pronouns","Indefinite pronouns"]],["2"=>["Gerund","Participles","Plural of nouns","Conditional sentences","Comparison of adjectives/adverbs","Reported speech"]]],
    "Studenti"=>[["1"=>["(be/get) used to","Causative have/get","Expressing future","Plural of nouns","Modal verbs","Inversion"]],["2"=>["Gerund vs infinitive","Subjunctive mood","Reported speech"]]]
    ];

$_finalText=["Skolarci"=>[["levo"=>"These grammar units are part of word building and semantics, such as irregular plural forms,
Saxon and Norman genitive (the ‘s or of form), comparison of adjectives and adverbs (with
appropriate suffixes), articles (definite, indefinite and zero article), indefinite pronouns
(referring to one or more unspecified objects, beings, or places) or quantifiers (words or phrases
used before nouns to indicate the amount or quantity). Also, while both possessive pronouns and
possessive adjectives show ownership and therefore are often confused, possessive pronouns are
used to replace the noun and possessive adjectives are used to describe the noun."],["desno"=>"On the sentence structure level, relative clauses are used to define or identify the preceding noun
or to give extra information about it. They can be defining or non-defining. Also, in English language
questions are formed by using auxiliary verbs and inverting the word order. In subject questions,
do, does and did - are not used and the word order is not inverted.<br><br>
All of these grammar units are vital for ability to express meaning, form and correct sentence
structure, so it is very important to master and use them properly."]]];

$_psihoResults=["Otkačeni moler"=>"Životnom prostoru dajete svoj pečat. Vaši zidovi odišu energijom. Vaš korak je lagan i siguran. Uživate u
različitostima. Ostavljate utisak, neizbrisiv trag prilikom susreta sa drugima. Drugi pamte Vaš stisak ruke.
Dovoljno ste svoji, ali i neizostavni deo jedne celine.<br><br>
Životni prostor je Vaš svet, Vaša kreativnost... životni prostor su Vaši prijatelji, Vaša porodica.<br><br>
Ne zaboravite, kada završite sa molerajem, obavezno očistite prostor, vratite stvari na svoje mesto. Tada
će Vaš trud i rad zasijati, dobiti oblik koji im pripada. Kreativnost ne isključuje red u prostoru, red u
životu!","Tihi vetar"=>"Vaš korak je tih, ali pripada samo Vama! Ne rizikujete, oprezni ste. Drugi se sa Vama osećaju sigurno.
Privlači Vas lepota jednoga cveta u polju cvetova. Ne zaboravite, uživajte u mirisima celog polja.
Iskoristite taj lagani, spori korak .. i pogledajte u ono što Vam dolazi u susret ... a ne samo u ono što iza
Vas ostaje.<br><br>
Svaki od cvetova u polju je deo Vas. Prigrlite sve cvetove, volite ih bez obzira na lepotu onog jednog koji
Vam odvlači pažnju. Zaslužujete da sebi poklonite ljubav.<br><br>
Ne odustajte od gledanja u daljinu, zraci svetlosti će Vam se ukazati!","Nedodirljivi dirigent"=>"Tako ste umešni u vođenju orkestra. Svi su hipnotisani Vašom sigurnošću, i budnošću. Publika upija
svaki Vaš pokret ruku. Vama to ne smeta, to Vam daje veću energiju. Svaki Vaš pokret je živ, pun
energije. Uživate na pozornici.<br><br>
Nekada zaboravite da postoji publika. Ne čujete kašljucanje ili komešanje iz poslednjih redova. Ne
zaboravite, da nema publike, Vaša izvrsnost ne bi imala toliku snagu!<br><br>
Kada se svetla pozornice ugase, i ostanete sami, mislite o kompozicijama koje ste izvodili.
Analizirajte ih. Nije svaka kompozicija perfektno izvedena, bez obzira što Vi to umete. Čuvajte
članove Vašeg orkestra, i oni su neizostavni deo vaše ogromne snage. Vi ne sumnjate u svoje
mogućnosti, kao što ni mi ne sumnjamo u Vas!","Ulični borac"=>"Nije svaka pobeda uspeh. Uspeh se ne ogleda samo u pobedi. Uspeh je mnogo više od toga. Ne
odustajte od pobede, ali dok vodite bitku, pazite da Vam uspeh ne izmakne. Preuzmite odgovornost,
to će vas učiniti jačim. Gledajte svoga protivnika, mnogo je verovatno da se nalazite u sličnoj
situaciji.<br><br>
Rođeni ste pobednik, ali u borbi koju sa sobom vodite. Nemojte drugima dokazivati da ste najbolji,
dokažite sebi. Pronađite prijatelja među svojim protivnicima, i svet je Vaš.<br><br>
Kada se borba završi, ne čekajte samo proglašenje pobednika ... osvrnite se oko sebe, proverite da li
je borba bila vredna uspeha koji ste postigli."];

$_psihoResultsEng=["Wacky wall-painter"=>"You give your own mark to the living space. Your walls are energy-exuding. Your step is easy and confident. You enjoy diversities. You leave an impression, an indelible mark when encountering others. They remember the strength of your handshake. You are independent enough, but also an indispensable part of the whole.<br><br>
The living space is your world, your creativity... the living space is your friends, your family.<br><br>
Remember, when you're done with your painting, make sure to clear out the space, and put things back in their place. Then your work and effort will shine, and get the shape that they deserve. Creativity does not exclude order in space, and order in life!",
    "Silent wind"=>"Your step is quiet, but it belongs only to you! You don’t take risks, you're cautious. The others feel safe with you. You’re attracted to the beauty of that single flower in the field filled with other flowers. Do not forget, enjoy the smells of the entire field. Take advantage of that light, slow step .. and look at what is coming towards you ... not only what remains behind you.<br><br>
Each of the flowers in the field is a part of you. Embrace all the flowers, love them regardless of the beauty of that one that draws your attention. You deserve to give yourself love.<br><br>
Do not give up looking at the distance, the rays of light will show themselves to you!",
    "Untouchable conductor"=>"You are so adept at running the orchestra. Everyone is hypnotized by your confidence and alertness. The audience absorbs every movement of your hands. It does not bother you, it gives you more energy. Each of your movements is alive, full of energy. You enjoy the stage.<br><br>
Sometimes you forget that the audience is there. You do not hear cough, or stirring from the back rows. Do not forget that if there was no audience, your excellence would not have that much power!<br><br>
When the bright stage lights are turned off and you remain alone, think of the compositions you performed. Analyze them. Not every composition was perfectly performed, regardless of the fact that you can do that. Take care of the members of your orchestra, they are also an indispensable part of your immense power. You do not doubt your abilities, just like we do not have any doubts in you!",
    "Street fighter"=>"Not every victory means success. Success is not seen only in victory. Success is so much more than that. Do not give up on winning, but as you are battling, be careful that the success does not elude you. Take responsibility, that will make you stronger. Look at your opponent, it is very likely that you are in a similar situation.<br><br>
You were born as a winner, but in the battle you are having against yourself. Do not try proving to the others that you are the best, prove that to yourself. Find a friend among your opponents, and the world will be yours.<br><br>
When the battle is over, do not wait only for the announcement of the winner ... look around, make sure that the fight was worth the success you have achieved. "];

$_psiho2Results=["Preduzimač"=>"Lako preuzimate inicijativu I odgovornost za svoj tim. Umete da prenesete drugima svoju energiju I da ih vodite. Ne bojite se da pokrenete sopstveni biznis I da organizujete ljude da vas prate. Dobro se snalazite u preduzetničkim vodama I da se nosite sa rizicima koje takvi poslovi donose. Spremni ste da uđete u poslove o kojima ne znate mnogo, ali ćete zato umeti da odaberete tim ljudi koji će posao umeti da iznesu. Vi ste vođa tima, vođa projekta, menadzer, samostalni preduzetnik. Umete da se borite za svoje ideje. U ovim poslovima je veoma važna veština asertivne komunikacije koja podrazumeva I slušanje drugih, pa je važno da je negujete I usavršavate.",
  "Izvršilac/Praktičar"=>"Vi ste paktičar, bez obzira na posao kojim se bavite. Važno vam je da pravila I procedure budu jasno definisane, jer se tako najbolje snalazite. Budite slobodni da insistirate na njima u situacijama kada one nisu jasne, jer ćete tako biti najproduktivniji. Ne volite da se puno rasplinjavate I da od svačega pravite filozofiju, vaš moto je “više rada, manje priče”. Važno vam je da vidite rezultate svog rada, jer imate utisak  da niste dovoljno produktivni kada oni nisu vidljivi. Vi ste odlični za sve poslove koji podrazumevaju efikasnost I produktivnost.Važno je da I drugima u timu budu jasni razlozi vašeg insistiranja na praktičnom, kako vas ne bi pogrešno razumeli. Zato se ne ustručavajte da kažete šta vam je I zbog čega potrebno. ",
    "Prenosilac znanja/Mentor"=>"Vi ste odličan mentor, prenosilac znanja, nastavnik, trener. Leže vam poslovi podučavanja, čime god da se bavite. Umete da slušate druge I da vodite računa o njihovim potrebama. Umete da odredite pravu meru I način na koji neko može nešto da razume. Ne ustručavate se da isprobavate nove načine rada I da dalje neprestano učite. Sve su ovo karakteristike ljudi koji umeju da prenose znanja drugima. Da nije takvih kao što ste vi, nebismo imali od koga da učimo. Mogli biste da razmišljate o izradi nekih uputstava, priručnika, vodiča, koji bi sardžali neophodne korake u vezi sa poslom kojim se bavite. Zato je važno da tokom svog profesionalnog rada razvijate I ovu veštinu pisane komunikacije.",
    "PR Menadzer/Team worker"=>"Vi ste od onih koji sa “svima mogu”.  Važna vam je atmosfera u timu I kako se ljudi osećaju, jer smatrate da je to neophodno da bi se posao dobro obavio. Pravi ste timski radnik, jer je vaš najveći doprinos radu funkcionisanje tima. Vi ste najbolji saradnik vođe tima. Dobro vam idu poslovi zagovaranja, lobiranja I predstavljanja svog tima. Imate razvijene komunikacijske veštine, pa se možete baviti odnosima sa javnošću, prodajom,  pregovaranjem, medijacijom I svim poslovima u kojima su neophodne dobro razvijene vestine komunikacije.",
    "Inovator, Kreator"=>"Vi cesto imate orignalne ideje. Imate potencijal da kreirate nove poduhvate ili stare organiyujete na originalan nacin. Ne volite utabane staze, one vam brzo postanu dosadne, jer smatrate da vas doprinos moze da bude mnogo veci. Dobro cete se snaci u svim poslovima koji podrazumevaju kreativnost I inovativnost. Osmisljavanje novih projekata, novih metoda I procedura su poslovi za vas. Vazno je samo da tokom svog profesionalnog angazovanja usavrsavate vestine komunikacije u timu, kako ne biste ostali neshvaceni.",
    "Ekonomista, Pravnik, Revizor, Evaluator"=>"Vaša posvećenost detaljima i briga o tome da se posao uradi na najbolji mogući način, vas dobro opredeljuje ka svim onim poslovima koji zahtevaju pregled, reviziju, evaluaciju. Dobro se snalazite sa papirima i administracijom. Da nije vas mnogi poslovi bi na izgled bili završeni, a zapravo bi ostali sa puno grešaka. Vodite računa o pravu i pravilima, uopšte o svim detaljima, pa ćete se dobro snaći i u poslovima koji zahtevaju poznavanje prava. Važno je samo da uvek imate na umu koji je cilj nekog zadatka i da ne gubite puno energije na detalje koji možda nisu najvažniji za efikasnost i produktivnost."];

$_psiho2ResultsEng=["Entrepreneur"=>"It is easy for you to take over the initiative and responsibility for your team. You can transfer your energy to others, and guide them well. Do not be afraid to start your own business and to organize people to follow you. You find your way quite well in entrepreneurial waters, and deal easily with the risks that such jobs bring. You are ready to get into the jobs that you do not know much about, but you will on the other hand be able to choose a team of people who will be able to carry it out. You are the team leader, the project leader, manager, independent entrepreneur. You know how to fight for your ideas. In these kinds of jobs, the skill of assertive communication that involves listening to others is very important, so it is therefore important that you nurture and perfect it.",
  "Executor/Practitioner"=>
    "You are practical, regardless of the job you are dealing with. It is important for you that rules and procedures are clearly defined, because in that case you manage everything better. Feel free to insist on them in situations where they are not clear, because that is how you are going to be your most productive self. You do not like to digress and diverge a lot and create a philosophy out of everything, your motto is 'more work, less talk'. It is important for you to see the results of your work, because you are under impression that you are not productive enough when they are not visible. You are excellent for all jobs that involve efficiency and productivity. It is important that your reasons for insistence on the practical are also clear to the others in the team, so that you are not misunderstood. Therefore, do not hesitate to say what you need and why.",
    "Knowledge Transferor/Mentor"=>
    "You are an excellent mentor, knowledge transferor, teacher, trainer. Teaching is something you’re natural with, whatever it is that you are doing. You can listen to others and take care of their needs. You can determine the right extent and the way someone can understand something. You do not hesitate to try new ways of working, and you continually learn. These are all the characteristics of people who are good at transferring knowledge to others. If there were not those like you, we would not have anyone to learn from. You might want to think about creating some instructions manuals, guides that would complement the necessary steps in relation to the work you are dealing with. It is therefore important that, during your professional work, you should also develop this skill of written communication.",
    "PR Manager/Team worker"=>"4. You are one of those who can get along with everyone. The vibe and atmosphere in the team as well as how everyone feels are important to you, because you consider it necessary for any work to be carried out well. You are a true team player, because your greatest contribution is the smooth functioning of the team. You are the best associate to the team leader. You are good at jobs of advocacy, lobbying, and representation of your team. Your communication skills are very well developed, so you can deal with public relations, sales, negotiation, mediation and all other types of jobs that require well-developed communication skills.",
    "Innovator, Creator"=>"5. You often have original ideas. You have the potential to create new ventures, or to reorganize the old ones in an original way. You do not like the paths that are already paved, they quickly become boring because you feel that your contribution may be much larger. You will manage quite well in all kinds of jobs that involve creativity and innovation. Designing new projects, new methods and procedures, those are the jobs for you. It is only important that, during your professional engagement, you should perfect the team communication skills, so that you do not remain misunderstood.",
    "Economist, Lawyer, Auditor, Evaluator"=>"6. Your dedication to detail and the concern that the job is being carried out in the best possible way defines you rather well for all those kinds of jobs that require review, revision, evaluation. You deal well working with paperwork and administration. If it was not for you, many tasks would be seemingly finished, while in fact they would be left with a lot of mistakes. The legal and rules are important to you, just like all details in general, and therefore you will also manage to do well at all jobs that require knowledge of the law. It's only important to always keep in mind what the goal of an assignment is, and do not waste a lot of energy on details that may not be the most important for efficiency and productivity."];

$nizBespl=[4,5,12,13];
$nizPrem=[14,15,22];
$nizPsiho=[17,21,24,25];

$psiho2Pred=[1,3,5,10,16,17,22,26];
$psiho2Izvrs=[4,12,13,18,21,23,27,29];
$psiho2Mentor=[7,9,11,19,25,31,32,22];
$psiho2Pr=[6,7,11,25,19,33,34,35];
$psiho2Inovator=[3,8,10,22,28,31,32,14];
$psiho2Ekonom=[2,15,20,24,36,38,39,40];

$ogConfig=[
    "newbie"=>["type"=>"website","title"=>"Engleski jezik znam kao početnik | A koliko dobro ti poznaješ jezik – saznaj odmah","description"=>"Na dobrom si putu. Ali ako hoćeš da se snalaziš kao turista ili započneš razgovor, treba ti još vežbe.","image"=>"http://testyourenglish.online/views/testsHome/images/result-score1.png","url"=>_WEB_PATH."result/Newbie/1"],
    "novice"=>["type"=>"website","title"=>"Imam osnovno znanje engleskog | A koliko dobro ti poznaješ jezik – saznaj odmah","description"=>"Nije loše, definitivno znaš osnovnu gramatiku i imaš fond reči dovoljan za sporazumevanje.","image"=>"http://testyourenglish.online/views/testsHome/images/result-score2.png","url"=>_WEB_PATH."result/Novice/1"],
    "apprentice"=>["type"=>"website","title"=>"Engleski jezik mi je ok | A koliko ti dobro poznaješ engleski – saznaj odmah","description"=>"Veoma dobro! Lepo se snalaziš sa engleskim, od kafića do sastanka. Samo napred!","image"=>"http://testyourenglish.online/views/testsHome/images/result-score3.png","url"=>_WEB_PATH."result/Apprentice/1"],
    "freshman"=>["type"=>"website","title"=>"Engleski jezik znam sasvim solidno | A koliko ti dobro poznaješ engleski – saznaj odmah.","description"=>"Svaka čast. Engleskim se lepo služiš čak i u poslovne svrhe. Još malo truda i eto te na sledećem nivou.","image"=>"http://testyourenglish.online/views/testsHome/images/result-score4.png","url"=>_WEB_PATH."result/Freshman/1"],
    "student"=>["type"=>"website","title"=>"Engleski jezik znam veoma dobro | A koliko ti dobro poznaješ engleski – saznaj odmah","description"=>"Sjajno! Ovo je već visok nivo poznavanja jezika. Malo ti fali za sledeći nivo. Čestitamo!","image"=>"http://testyourenglish.online/views/testsHome/images/result-score5.png","url"=>_WEB_PATH."result/Student/1"],
    "academic"=>["type"=>"website","title"=>"Engleski jezik znam perfektno | A koliko ti dobro poznaješ engleski – saznaj odmah","description"=>"Odlično! Mali broj njih je dogurao do ovog nivoa – tvoje poznavanje jezika je izvrsno! Slobodno se hvali svima ;)","image"=>"http://testyourenglish.online/views/testsHome/images/result-score6.png","url"=>_WEB_PATH."result/Academic/1"]
    ];
/***  ***/



