<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Classes;

/**
 * Description of Translate
 *
 * @author Sonor Smart Force
 */
class Translate 
{
    public static function prepareLangVars($template)
    {
        global $_lngVars;
        
        foreach($_lngVars as $k=>$v)
        {
            $template->setData($k,$v);
        }
    }

}
