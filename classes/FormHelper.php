<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Classes;

/**
 * Description of FormHelper
 *
 * @author Sonor Smart Force
 */
class FormHelper 
{
    public static function yearsCombo($year="")
    {
        $years_string="";
        $years_string.="<select name='years'>";
          //$year=1982;
          for($i=date('Y');$i>=1900;$i--)
          {
              if($year!=="" && ($year==$i))
              {
                  $years_string.="<option value='".$i."' selected>{$i}</option>";
              }
              else
              {
            $years_string.="<option value='".$i."'>{$i}</option>";
              }
          }
                                                                                                                                     
       $years_string.="</select>";
       
       return $years_string;
    }
    public static function monthsCombo($month="")
    {
        $months_string="";
        $months_string.="<select name='months'>";
           $i=1;
          foreach(unserialize(_MONTHS_ARR) as $mnth)
            {
                if($month!=="" && ($month==$i))
                {
                    $months_string.="<option value='".$i."' selected>{$mnth}</option>";
                }
                else
                {
                $months_string.="<option value='".$i."'>{$mnth}</option>";
                }
                $i++;
            }
                                                                                                                                     
       $months_string.="</select>";
       
       return $months_string;
    }
    public static function daysCombo($day="")
    {
        $days_string="";
        $days_string.="<select name='days'>";
        
          for($i=1;$i<32;$i++)
            {
              if($day!=="" && ($day==$i))
              {
                  $days_string.="<option value='".$i."' selected>{$i}</option>";
              }
              else
              {
                  $days_string.="<option value='".$i."'>{$i}</option>";
              }
                
            }
                                                                      
                                                                                                                                     
       $days_string.="</select>";
       
       return $days_string;
    }
    public static function csrfHiddenFiled($csrf_token)
    {
       return'<input type="hidden" name="csrf_token" value="'.$csrf_token.'" id="csrf_token"/>';
    }
    public static function selectFromArray($array)
    {
        
        $final_string="";
        $final_string.="<select  class='form-control'>";
            foreach($array as $a)
            {
                $final_string.="<option value={$a}>{$a}</option>";
            }
        $final_string.="</select>";
        
        return $final_string;
    }
    public static function disabledField($class="",$border="")
    {
        return '<input disabled type="text" class="form-control '.$class.'" id="'.$class.'" style="color:#f0ede6 !important;'.$border.'">';
    }
    public static function textField($qid,$length)
    {
        $lngthArr=array();
        if(is_array($length))
        {
            foreach($length as $l)
            {
                $lngthArr[]=strlen($l);
            }
            rsort($lngthArr);
            $length=$lngthArr[0];
        }
        else 
        {
            $length=strlen($length);
        }
        
        $width=(10*$length)+20;
        return '<input type="text" class="form-control '.$qid.'" style="width:'.$width.'px !important">';
    }
}
