<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Classes;
use azi\Arguments;
use azi\Rules\Contracts\RuleInterface;
use Models\Professor;
/**
 * Description of EmailExists
 *
 * @author Sonor Smart Force
 */
class EmailExistsProf implements RuleInterface

{
    public function validate( $field, $value, Arguments $args )
    {
           $this->value=$value;
           $profesorsWithMail =  Professor::Email($value)->get();
       
            if(count($profesorsWithMail)==0)
            {
                return true;
            }
              return false; 
    }

    /**
     * @return mixed
     */
    public function message()
    {
        return "Professor with email address {$this->value} already exists";
    } //put your code here
}
