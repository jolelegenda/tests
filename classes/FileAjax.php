<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Classes;
use azi\Arguments;
use azi\Rules\File;
/**
 * Description of AlphaUtf8
 *
 * @author Sonor Smart Force
 */
class FileAjax extends File
{
    public function validate( $field, $file, Arguments $args )
    {
       
        if(empty($file))
            return false;
        
        return parent::validate($field, $file, $args);
    }

    /**
     * @return mixed
     */
    public function message()
    {
        return parent::message();
    }
}
