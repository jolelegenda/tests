<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Classes;

/**
 * Description of QuestionData
 *
 * @author Sonor Smart Force
 */
class QuestionData 
{
    public $question;
    public $hint;
    public $sentences;
    public $answers;
    public $options;
    public $additionalData;
    public $questionType;
    public $correctAnswers;
}
