<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Classes;

/**
 * Description of TetsHelper
 *
 * @author Sonor Smart Force
 */
class TestHelper {
    //put your code here
    public static function forSentence($options)
    {
        $replaced=array();
        $replace=array();
        $returnArray=array();
        $i=0;
        foreach($options as $o)
        {
            $replaced[]="#replace{$i}#";
            $replace[]=\Classes\FormHelper::selectFromArray($o);
            $i++;
        }
        $returnArray[0]=$replaced;
        $returnArray[1]=$replace;
        return $returnArray;
    }
}
