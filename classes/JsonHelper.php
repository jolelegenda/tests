<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Classes;

/**
 * Description of JsonHelper
 *
 * @author Sonor Smart Force
 */
class JsonHelper {
    
    public static function response($data)
    {
        echo json_encode($data);
    }
    
    public static function setResponseData($hasErrors,$message='',$errors_array=array(),$data=array())
    {
        return [
            'status'  => 'validation-failed',
            'message' => $message,
            'errors'  =>$errors_array,
            'hasErrors'  =>$hasErrors,
            'data'  =>$data
            ];
    }
}
