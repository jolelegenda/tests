<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Classes;

/**
 * Description of RESTClient
 *
 * @author Sonor Smart Force
 */
class RESTClient extends \GuzzleHttp\Client
{
    protected $data=[];
    protected $headers=[];
    protected $url='';
    public function __construct($data,$url,$headers=[]) 
    {
        
        parent::__construct();
        $this->setData($data);
        $this->setHeaders($headers);
        $this->setUrl($url);
    }
    
    public function get()
    {
        
        $response =  $this->request('GET', $this->url,['headers' => $this->headers]);
        return json_decode($response->getBody()->getContents());
    }
    
    public function post($dataType='json')
    {
        $response = $this->request('POST',$this->url, [
        $dataType => $this->data,'headers' => $this->headers
        ]);
       return json_decode($response->getBody()->getContents());
    }
    public function put($dataType='json')
    {
        $response = $this->request('PUT',$this->url, [
        $dataType => $this->data,'headers' => $this->headers
        ]);
       return json_decode($response->getBody()->getContents());
    }
    private function setData($data)
    {
        $this->data=$data;
    }
    
    private function setHeaders($headers)
    {
        $this->headers=$headers;
    }
    private function setUrl($url)
    {
        $this->url=$url;
    }
}
