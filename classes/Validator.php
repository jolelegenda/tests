<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Classes;

/**
 * Description of Validator
 *
 * @author Sonor Smart Force
 */
class Validator extends \azi\Envalid
{

    protected $added_rules=array();
  


    public function __construct($added=array()) 
    {
        parent::__construct();
        $this->added_rules=$added;
         
    }
    public function isValid($data,$rules)
    {
        //$validator = new \azi\Envalid();
        foreach($this->added_rules as $k=>$v)
        {
            $class="\\Classes\\".$v;
            $this->addRule($k, new $class());
        }
      // dd($data);
        $this->validate($data, $rules);
        
        if($this->failed())
        {
            return false;
        }
        else
        {
            return true;
        }
      //  return 
    }
    
}
