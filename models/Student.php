<?php

namespace Models;


class Student extends EloquentBaseModel{
    public $primaryKey = 'ID';
    public $table = 'students';


    protected $fillable = ['name', 'surname', 'city', 'email', 'dateofbirth','sex', 'password','salt'];
 
    public function scopeEmail($query, $param)
    {
        return $query->where('email', $param);
    }
    public function scopeSalt($query, $param)
    {
        return $query->where('salt', $param);
    }
    public function images()
    {
        return $this->hasMany('Models\Image', 'student_id', 'ID');
    }
}

