<?php

namespace Models;


class Professor extends EloquentBaseModel{
    public $primaryKey = 'ID';
    public $table = 'professors';


    protected $fillable = ['name', 'surname', 'city', 'email', 'dateofbirth','sex','password','salt','method_id','category_id','description'];
 
    public function scopeEmail($query, $param)
    {
        return $query->where('email', $param);
    }
    public function scopeStatus($query, $param)
    {
        return $query->where('status', $param);
    }
    public function scopeSalt($query, $param)
    {
        return $query->where('salt', $param);
    }
    public function images()
    {
        return $this->hasMany('Models\Image', 'professor_id', 'ID');
    }
}

