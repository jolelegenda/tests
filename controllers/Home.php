<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Controllers;

class Home extends frontendController 
{
    public function __construct() {
        parent::__construct();
        
    }
    
    public function index()
    {
        $this->template->setData("name","");
           $this->template->setData("birth","");
           $this->template->setData("email","");
           $this->template->setData("sex","");
           if(\Classes\Cookie::get("lang")===false || \Classes\Cookie::get("lang")=="en")
           {
               $this->template->setData("psiho1",24);
               $this->template->setData("psiho2",25);
              
               $this->template->setData("lng_suffix","sr");
               $this->template->setData("lng_link",_WEB_PATH."views/testsHome/images/serbia.png");
           }
           else
           {
               $this->template->setData("psiho1",17);
               $this->template->setData("psiho2",21);
               $this->template->setData("lng_suffix","en");
               $this->template->setData("lng_link",_WEB_PATH."views/testsHome/images/united-kingdom.png");
           }
        if (\Classes\Cookie::get("registered")!==false) {
           
             $apiAuth=new \Classes\RESTClient([],_API_URL."/participants/getbyemail/".\Classes\Cookie::get("registered"),['X-JWT-Auth'=>$_SESSION['jwt']]);
  
            $res=$apiAuth->get();
          // dump($res->status);
          // dump($res->data->name);
           foreach($res->data as $k=>$v)
           {
                $name=$v->name;
                $birth=$v->birth;
                $email=$v->email;
                $sex=$v->sex;
                $idParticipant=$k;
                
                //$sex=$v->sex;
           }
           $_SESSION['participant_id']=$idParticipant;
           $this->template->setData("name",$name);
           $this->template->setData("birth",$birth);
           $this->template->setData("email",$email);
           $this->template->setData("sex",$sex);
        }
         echo $this->template->render('index',true);
    }
    public function submitReg()
    {
        global $_val_added_rules_reg, $_val_rules_reg;
        
        $validator = new \Classes\Validator($_val_added_rules_reg);

        $data = $_POST;

        if (!$validator->isValid($data, $_val_rules_reg)) {
            $resp_data = \Classes\JsonHelper::setResponseData(1, '', $validator->getErrors());
            $this->response($resp_data);
            // dump($validator->getErrors());
        } else {
                            //$_SESSION['participant_id']=$res->data->id;
//                $resp_data = \Classes\JsonHelper::setResponseData(0, 'Sucess');
//                $this->response($resp_data);
           /* dump($_POST);
            die;*/
          
          $tstampNow=time();
          /*  if (\Classes\Cookie::get("registered")==$_POST['email']) {
                $d=["tstamp"=>$tstamp,"tstampnow"=>$tstampNow];
                   $resp_data = \Classes\JsonHelper::setResponseData(0, 'Sucesssss',[],$d);
                   $this->response($resp_data);
            }
            else
            {*/
            $apiAuth=new \Classes\RESTClient([],_API_URL."/participants/getbyemail/".$_POST['email'],['X-JWT-Auth'=>$_SESSION['jwt']]);
  
            $res=$apiAuth->get();
            if($res->status==="Success")
            {
                 foreach($res->data as $k=>$v)
                 {
                    $name=$v->name;
                    $birth=$v->birth;
                    $email=$v->email;
                    $sex=$v->sex;
                    $idParticipant=$k;
                    //$sex=$v->sex;
                 }
                 
                 $birthArr=explode(".",$birth);
                 $year= \Classes\FormHelper::yearsCombo($birthArr[2]);
                 $month= \Classes\FormHelper::monthsCombo($birthArr[1]);
                 $day= \Classes\FormHelper::daysCombo($birthArr[0]);
                 $tstamp=strtotime("{$birthArr[1]}/{$birthArr[0]}/{$birthArr[2]}");
                 $data=[
                'year'=>$year,
                "month"=>$month,
                "day"=>$day,
                'sex'=>$sex,
                'name'=>$name,
                'email'=>$email,
                "tstamp"=>$tstamp,
                "tstampnow"=>$tstampNow
                 ];
                 $_SESSION['participant_id']=$idParticipant;
                 \Classes\Cookie::set("registered",$email, 30, "/");
                 $resp_data = \Classes\JsonHelper::setResponseData(0, 'Exists', [],$data);
            $this->response($resp_data);
            }
            else
            {
            $birthdate=$_POST['days'].".".$_POST['months'].".".$_POST['years'];
            $tstamp=strtotime("{$_POST['months']}/{$_POST['days']}/{$_POST['years']}");
            $data=[
            'birth'=>$birthdate,
            'sex'=>$_POST['sex'],
            'name'=>$_POST['name'],
           'email'=>$_POST['email'],
           'fb_id'=>$_POST['fb_id']
             ];
            
            $apiAuth=new \Classes\RESTClient($data,_API_URL."participants/create",['X-JWT-Auth'=>$_SESSION['jwt']]);
           // dump($apiAuth->post()->data->id);
            $res=$apiAuth->post();
           // dump($res);
            if($res->status=='Success')
            {
                $_SESSION['participant_id']=$res->data->id;
//                if (\Classes\Cookie::get("congrats") == $_SESSION['testname']) {
//                    echo "";
//                } else {
//                    echo $_SESSION['trenutnorez'];
//                    \Classes\Cookie::set("congrats", $_SESSION['testname'], 30, "/");
//                }
               $d=["tstamp"=>$tstamp,"tstampnow"=>$tstampNow];
                  
                \Classes\Cookie::set("registered",$_POST['email'], 30, "/");
                $resp_data = \Classes\JsonHelper::setResponseData(0, 'Sucess',[],$d);
                $this->response($resp_data);
            }
            
            else
            {
                $resp_data = \Classes\JsonHelper::setResponseData(1, '', array("reg" => array("Your registration was not successful.")));
                $this->response($resp_data);
            }
            }
          //  }
           
        }
    }
    public function setLanguage($lng)
    {
        \Classes\Cookie::set("lang", $lng, 30,"/");
       // $this->redirect(_WEB_PATH);
        $this->redirect($_SERVER['HTTP_REFERER']);
    }
    public function confirmRegistration($salt,$model_name="Student")
    {
        
        $salt=$this->filter_input($salt);
        $instance= new \Modules\register\Register();
        $model="\\Models\\".$model_name;
        //$model= urlencode($model);
        $student=$instance->confirm($model,$salt);
       
    }
}
