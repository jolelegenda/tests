<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Controllers;

/**
 * Description of Student
 *
 * @author Sonor Smart Force
 */
class Student extends frontendController 
{
    public function __construct() {
        $this->inst_stud= new \Modules\students\Students();
        parent::__construct();
        
    }
    public function index()
    { 
    }
    public function registracijaStudenata()
    {
        
        $this->template->setData('contentHead','Registration - students');
        $this->template->setData('days', \Classes\FormHelper::daysCombo());
        $this->template->setData('months', \Classes\FormHelper::monthsCombo());
        $this->template->setData('years', \Classes\FormHelper::yearsCombo());
        $this->template->render('registracijaStudenata');
    }
    public function registrujStudenta()
    {
        global $_val_added_rules_stud, $_val_rules_stud;

        $validator = new \Classes\Validator($_val_added_rules_stud);

        $data = array_merge($_POST, $_FILES);

        if (!$validator->isValid($data, $_val_rules_stud)) {
            $resp_data = \Classes\JsonHelper::setResponseData(1, '', $validator->getErrors());
            $this->response($resp_data);
            // dump($validator->getErrors());
        } else {
            $student = $this->inst_stud->register();
            $file = $this->uploadFile("\\Models\\Image", $student);

            if ($student && $file) {
                $resp_data = \Classes\JsonHelper::setResponseData(0, 'Your registration is successful, we sent you confirmation mail.');
                $this->response($resp_data);
            } else {
                $resp_data = \Classes\JsonHelper::setResponseData(1, '', array("reg" => array("Your registration was not successful.")));
                $this->response($resp_data);
            }
        }
    }
    public function studentProfile($name,$id)
    { 
        if($_SESSION['user']['user_type']!=1 || !isset($_SESSION['user']))
        {
            $this->redirect(_WEB_PATH);
            die;
        }
        $prof=$this->inst_stud->info($id);
        $this->template->setData('prof',$prof);
        $this->template->setData('contentHead','Student - profile');
        $this->template->render('studentProfile');
    }
}
