<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Controllers;

/**
 * Description of Test
 *
 * @author Sonor Smart Force
 */
class Test extends \Controllers\frontendController 
{
    public function __construct() {
        $this->test_inst=new \Modules\tests\Tests();
        $this->test_res=new \Modules\tests\Results();
        $this->test_prem_inst=new \Modules\premiumtest\Tests();
        $this->test_prem_res=new \Modules\premiumtest\Results();
       $this->test_psiho_inst=new \Modules\psihotest\Tests();
       $this->test_psiho_res=new \Modules\psihotest\Results();
        parent::__construct();
    }
    public function displayTest($id)
    {
      global $nizBespl,$nizPrem,$nizPsiho;  
       
      $_SESSION['idTesta']=$id;
     // var_dump($_SESSION['idTesta']);//die;
      if(in_array($id,$nizBespl))
      {
      $this->test_inst->displayTest($id);
      }
      if(in_array($id,$nizPrem))
      {
      $this->test_prem_inst->displayTest($id);
      }
       if(in_array($id,$nizPsiho))
      {
       $this->test_psiho_inst->displayTest($id);
      }
        
    }
    public function saveTest($id)
    {
        $this->test_inst->saveTest($id);
    }
    public function results()
    {
      global $nizBespl,$nizPrem,$nizPsiho; 
        //var_dump($nizBespl);die;
      if(in_array($_SESSION['idTesta'],$nizBespl))
      {
      // var_dump($_SESSION['idTesta']);die;
       $this->test_res->CalculateResults();
      }
      if(in_array($_SESSION['idTesta'],$nizPrem))
      {
        $this->test_prem_res->CalculateResults();
      }
      if(in_array($_SESSION['idTesta'],$nizPsiho))
      {
         if($_SESSION['idTesta']==17 || $_SESSION['idTesta']==24)
        $this->test_psiho_res->CalculateResults();
         if($_SESSION['idTesta']==21 || $_SESSION['idTesta']==25)
        $this->test_psiho_res->CalculateResults2();
      }
    }
    public function ukupno()
    {
        global $nizBespl,$nizPrem,$nizPsiho; 
      
      if(in_array($_SESSION['idTesta'],$nizBespl))
      {
        $this->test_res->Sum();
      }
      if(in_array($_SESSION['idTesta'],$nizPrem))
      {
       $this->test_prem_res->Sum();
      }
      if(in_array($_SESSION['idTesta'],$nizPsiho))
      {
         $this->test_psiho_res->Sum();
      }
    }
    public function trenutno()
    {
        global $nizBespl,$nizPrem,$nizPsiho; 
      
      if(in_array($_SESSION['idTesta'],$nizBespl))
      { 
       $this->test_res->RealTime();
      }
    }
    public function calculating()
    {
        global $nizBespl,$nizPrem,$nizPsiho; 
      
      if(in_array($_SESSION['idTesta'],$nizBespl))
      { 
        $this->test_inst->calculating();
      }
      if(in_array($_SESSION['idTesta'],$nizPrem))
      {
        $this->test_prem_inst->calculating();
      }
      if(in_array($_SESSION['idTesta'],$nizPsiho))
      {
          $this->test_psiho_inst->calculating();
      }
    }
    public function result($name,$share)
    {
       
         global $nizBespl,$nizPrem,$nizPsiho; 
      if(isset($_SESSION['idTesta']))
      {    
      if(in_array($_SESSION['idTesta'],$nizBespl))
      { 
        $this->test_inst->result($name,$share);
      }
       if(in_array($_SESSION['idTesta'],$nizPrem))
      { 
        $this->test_prem_inst->result($name,0);
      }
       if(in_array($_SESSION['idTesta'],$nizPsiho))
      {
         $this->test_psiho_inst->result($name,0);
      }
      }
      else
      {
          $this->test_inst->result($name,$share);
      }
    }
    public function deleteParticipants()
    {
        $this->test_inst->deleteParticipants();
    }
}
