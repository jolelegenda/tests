<?php
namespace Controllers;
use Views\Template;
use Views\ModuleTemplate;
use Classes\Translate;
class frontendController extends baseController{
    
    public function __construct($module_name=false) 
    {
      // echo $module_name;
       // echo phpinfo();
        $this->template = new Template(_DEFAULT_THEME);
        
        if($module_name)
        {
          $this->template = new ModuleTemplate(_DEFAULT_THEME,$module_name);
        }
        
        Translate::prepareLangVars($this->template);
      
        if (!isset($_SESSION['csrf_token'])) {

              \Classes\Session::set('csrf_token', base64_encode(openssl_random_pseudo_bytes(32)));
        }
        if ($_SERVER['REQUEST_METHOD'] === 'POST')
        {
            //echo 123;
        /*  var_dump($_POST['csrf_token']);echo "<br>";
            var_dump($_SESSION['csrf_token']);*/
            if (!isset($_POST['csrf_token']) || ( isset($_POST['csrf_token']) && $_POST['csrf_token'] !== $_SESSION['csrf_token'])) 
            {
            /*  echo $_SESSION['csrf_token'];echo "post".$_POST['csrf_token'];
              die;*/
//            dump(urldecode($_POST['csrf_token']));
//                dump($_POST['csrf_token'] !== $_SESSION['csrf_token']);
                 $resp_data=\Classes\JsonHelper::setResponseData(1, '', array("reg"=>array("For security reasons refresh the page and try again.")));
                   $this->response($resp_data);
                   die;
            }
        }
       /* dump($_SESSION['csrf_token']);echo "<br><br>";
        dump($_POST['csrf_token']);*/
        //echo $a=\Classes\Session::get('csrf_token');
       $this->template->setData('csrf_field', \Classes\FormHelper::csrfHiddenFiled(\Classes\Session::get('csrf_token')));
        if(!isset($_SESSION['jwt']))
        {
             $data=['user'=>'fe@fe.com','pass'=>'fe'];
            $apiAuth=new \Classes\RESTClient($data,_API_URL."login/");
            $_SESSION['jwt']=$apiAuth->post()->jwt;
            
        }
        $this->jwt=$_SESSION['jwt'];




       
    }
    public function index() {}
    protected function uploadFile($model,$studentID,$refModel="student")
    {
        $suffix=uniqid(rand());
        $uploaddir = realpath('views/mvp/uploads');
        $uploadfile = $uploaddir ."/thumbnail/". $suffix.".png";
        $uploadfile1 = $uploaddir ."/profile/". $suffix.".png";
        
        try {
            // Create a new SimpleImage object
            $image = new \claviska\SimpleImage();

            // Magic! ✨
            $image
              ->fromFile($_FILES['photo']['tmp_name'])                     // load image.jpg
              ->autoOrient()                              // adjust orientation based on exif data
              ->bestFit(270, 180)                          // resize to 320x200 pixels                                              // add a watermark image
              ->toFile($uploadfile ,'image/png')
              ->fromFile($_FILES['photo']['tmp_name'])                     // load image.jpg
              ->autoOrient() // adjust orientation based on exif data
              ->bestFit(367, 251)                          // resize to 320x200 pixels                                              // add a watermark image
              ->toFile($uploadfile1, 'image/png'); // convert to PNG and save a copy to new-image.png
            
            // output to the screen
            $refId=$refModel."_id";
            $image=new $model;
            $image->name=$suffix.".png";
            $image->is_profile=1;
            $image->$refId=$studentID;
            $image->save();
            return true;
            // And much more! 💪
          } catch(Exception $err) {
            // Handle errors
              return false;
            echo $err->getMessage();
          }
    }
}