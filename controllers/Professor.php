<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Controllers;

/**
 * Description of Professor
 *
 * @author Sonor Smart Force
 */
class Professor extends frontendController
{
    public function __construct() {
       $this->prof_instance=new \Modules\professors\Professors();
       parent::__construct();
       
    }
    public function index(){}
    public function registracijaProfesora()
    {
        
        $this->template->setData('contentHead','Registration - professors');
        $this->template->setData('days', \Classes\FormHelper::daysCombo());
        $this->template->setData('months', \Classes\FormHelper::monthsCombo());
        $this->template->setData('years', \Classes\FormHelper::yearsCombo());
        $this->template->render('registracijaProfesora');
    }
    public function listingProfesora()
    {
        $nizProfesoraSaSlikama=$this->prof_instance->listing();
        $this->template->setData('nizProfesoraSaSlikama',$nizProfesoraSaSlikama);
        //$this->template->setData('name',$this->url_friendly('Jonathan E. Adler'));
        $this->template->setData('contentHead','Listing - professors');
        $this->template->render('listingProfesora');
    }
    public function profesorInfo($name,$id)
    { 
        $prof=$this->prof_instance->info($id);
        $this->template->setData('prof',$prof);
        $this->template->setData('contentHead','Professor - info');
        $this->template->render('profesorInfo');
    }
    public function profesorProfile($name,$id)
    { 
        if($_SESSION['user']['user_type']!=2 || !isset($_SESSION['user']))
        {
            $this->redirect(_WEB_PATH);
            die;
        }
        $prof=$this->prof_instance->info($id);
        $this->template->setData('prof',$prof);
        $this->template->setData('contentHead','Professor - profile');
        $this->template->render('professorProfile');
    }
    public function registrujProfesora()
    {
        global $_val_added_rules_prof, $_val_rules_prof;
        
        $validator = new \Classes\Validator($_val_added_rules_prof);

        $data = array_merge($_POST, $_FILES);

        if (!$validator->isValid($data, $_val_rules_prof)) {
            $resp_data = \Classes\JsonHelper::setResponseData(1, '', $validator->getErrors());
            $this->response($resp_data);
            // dump($validator->getErrors());
        } else {
            $student = $this->prof_instance->register();
            $file = $this->uploadFile("\\Models\\Image", $student, "professor");
            if ($student && $file) {
                $resp_data = \Classes\JsonHelper::setResponseData(0, 'Your registration is successful, we sent you confirmation mail.');
                $this->response($resp_data);
            } else {
                $resp_data = \Classes\JsonHelper::setResponseData(1, '', array("reg" => array("Your registration was not successful.")));
                $this->response($resp_data);
            }
        }
    }
}
