<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Controllers;

/**
 * Description of Login
 *
 * @author Sonor Smart Force
 */
class Login extends frontendController
{
   public function __construct() {
       parent::__construct();
   }
   public function index()
   {
       
   }
   public function signInForma()
   {
       $this->template->setData('contentHead','Sign in');
       $this->template->render("signIn");
   }
   public function signIn()
   {
        global $_val_rules_log;
        
        $validator = new \Classes\Validator();
        
        $data = $_POST;
        
        if (!$validator->isValid($data, $_val_rules_log)) {
            $resp_data = \Classes\JsonHelper::setResponseData(1, '', $validator->getErrors());
            $this->response($resp_data);
        } else {
            
            if ($_POST['user'] == 1) {
                $model = "\\Models\\Student";
            } else {
                $model = "\\Models\\Professor";
            }
            
            $inst = new \Modules\login\SignIn();
            $loggedin = $inst->login($model);
            
            if ($loggedin) {
                //dump($_SESSION['user']);
               $name=$this->url_friendly($_SESSION['user']['first_name']);
               $surname=$this->url_friendly($_SESSION['user']['last_name']);
                if($_SESSION['user']['user_type']==2)
                {
                    $resp_data = \Classes\JsonHelper::setResponseData(0, _WEB_PATH."professor-profile/".$name."_".$surname."-".$_SESSION['user']['user_id']);
                     $this->response($resp_data);
                  //  $this->redirect(_WEB_PATH."professor-profile/".$name."_".$surname."-".$_SESSION['user']['user_id']);
                }
                else
                {
                    $resp_data = \Classes\JsonHelper::setResponseData(0, _WEB_PATH."student-profile/".$name."_".$surname."-".$_SESSION['user']['user_id']);
                     $this->response($resp_data);
                }
            } else {
                $resp_data = \Classes\JsonHelper::setResponseData(1, '', array("reg" => array("You entered wrong data!")));
                $this->response($resp_data);
            }
        }
    }
    public function logout()
    {
        $inst=new \Modules\login\SignIn();
        $inst->logout();
    }
}
