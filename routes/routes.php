<?php
/**
 * Define your routes
 */
$collector->get('/', function(){
   Loader::loadController('Home','index', array());
});
$collector->post('submit-reg', function(){
   Loader::loadController('Home','submitReg');
});
$collector->get('/test/{id}', function($id){
   Loader::loadController('Test','displayTest', array($id));
});
$collector->get('/saveTest/{id}', function($id){
   Loader::loadController('Test','saveTest', array($id));
});
$collector->post('results', function(){
   Loader::loadController('Test','results');
});
$collector->get('/calculating', function(){
   Loader::loadController('Test','calculating', array());
});
$collector->get('/result/{name}/{share}', function($name,$share){
   Loader::loadController('Test','result', array($name,$share));
});
$collector->get('/ukupno', function(){
   Loader::loadController('Test','ukupno', array());
});
$collector->get('/deleteParticipants', function(){
   Loader::loadController('Test','deleteParticipants', array());
});
$collector->post('/trenutno', function(){
   Loader::loadController('Test','trenutno', array());
});
///////////
$collector->get('/registration-students', function(){
   Loader::loadController('Student','registracijaStudenata', array());
});
$collector->get('/registration-professors', function(){
   Loader::loadController('Professor','registracijaProfesora', array());
});
$collector->get('/listing-professors', function(){
   Loader::loadController('Professor','listingProfesora', array());
});
$collector->get('/{name}-{id}', function($name,$id){
   Loader::loadController('Professor','profesorInfo', array($name,$id));
});
$collector->post('register-student', function(){
   Loader::loadController('Student','registrujStudenta');
});
$collector->post('register-professor', function(){
   Loader::loadController('Professor','registrujProfesora');
});
$collector->get('/confirm/{salt}/{model}', function($salt,$model){
   Loader::loadController('Home','confirmRegistration', array($salt,$model));
});
$collector->get('/lang/{lng}', function($lng){
   Loader::loadController('Home','setLanguage', array($lng));
});
$collector->get('signin', function(){
   Loader::loadController('Login','signInForma');
});
$collector->post('login', function(){
   Loader::loadController('Login','signIn');
});
$collector->get('/professor-profile/{name}-{id}', function($name,$id){
   Loader::loadController('Professor','profesorProfile', array($name,$id));
});
$collector->get('/student-profile/{name}-{id}', function($name,$id){
   Loader::loadController('Student','studentProfile', array($name,$id));
});
$collector->get('signout', function(){
   Loader::loadController('Login','logout');
});

