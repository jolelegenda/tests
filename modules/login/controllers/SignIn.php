<?php
namespace Modules\login;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of SignIn
 *
 * @author Sonor Smart Force
 */
class SignIn extends \Controllers\frontendController
{
    public function __construct() {
        
        $module_name=\Classes\ModuleHelper::getModuleName(dirname(__FILE__));
        parent::__construct($module_name);
        
    }
    public function index(){}
    public function login($model)
    {
        $user=$model::Email($_POST['email'])->get();
            if(count($user)>0 && $user[0]->status==1){
                
                if(\Triadev\PasswordHashing\PasswordHash::verify($_POST['password'], $user[0]->password))
                {                   
                   $_SESSION['user']['user_id'] = $user[0]->ID;
                   $_SESSION['user']['first_name'] = $user[0]->name;
                   $_SESSION['user']['last_name'] = $user[0]->surname;
                   $_SESSION['user']['email'] = $user[0]->email;
                   $_SESSION['user']['user_type'] = $_POST['user'];
                   return true;                   
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
    }
    public function logout()
    {
        if(isset($_SESSION['user'])){
            \Classes\Session::unsetSession("user");
        }
        $this->redirect(_WEB_PATH);
    }
}
