<?php
namespace Modules\students;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Students
 *
 * @author Sonor Smart Force
 */
class Students extends \Controllers\frontendController
{
    public function __construct() {
        
        $module_name=\Classes\ModuleHelper::getModuleName(dirname(__FILE__));
        parent::__construct($module_name);
        
    }
    public function index(){}
    public function register()
    {
        $_POST['months'] = (strlen($_POST['months']) == 1) ? "0" . $_POST['months'] : $_POST['months'];
        $_POST['days'] = (strlen($_POST['days']) == 1) ? "0" . $_POST['days'] : $_POST['days'];
        $_POST['dateofbirth'] = $_POST['years'] . "-" . $_POST['months'] . "-" . $_POST['days'] . " 00:00:00";
        $instance = new \Modules\register\Register();
        return $instance->register("\\Models\\Student", "Student");
    }
    public function info($id)
    {
        $stud=\Models\Student::find($id);
        return $stud;
    }
}
