<?php
$_SESSION['og_res']="Novice";
?>
<div class="container" id="calculate-box">  
        <div class="row">
            <div class="col-md-12 col-lg-10 col-lg-offset-1 text-center">

                <div class="result-box">

                  <img class="result-text1" src="<?=_WEB_PATH?>views/testsHome/images/result-score1.png" alt="Newbie">

                  <div class=" stars">
                    <div class="star"><img src="<?=_WEB_PATH?>views/testsHome/images/result-star.png"></div>
                  </div>

                  <img src="<?=_WEB_PATH?>views/testsHome/images/result-abc.png" style="margin: 40px 0px;">

                </div>


                <div class="result-text-div text-center">
                  <h3>Nice!</h3>
                  <p>You have just started learning English. Keep working, you are on a good path. Take this test again when you finish your course to check your progress. <br>If you are interested, you may take the Full test for only 6$, which is suitable for English learning beginners. </p>

                        <button class="btn btn-lg btn-primary btn-block btn-signin btn-def btn-result" data-toggle="modal" data-target="#myModal2" type="submit">GO TO FULL TEST</button>
                  <p>Or you can find your English teacher on</p>
                  <a class="btn btn-lg btn-primary btn-block btn-signin btn-def btn-result" style="background-color: #03a9f4;" href="<?=_WEB_PATH?>mvp">T-LIST PLATFORM</a>
  <div class="fb-share-button" data-href="<?=_WEB_PATH."result/Newbie/1"?>" data-layout="button" data-size="large" data-mobile-iframe="false"><a class="fb-xfbml-parse-ignore" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fdevelopers.facebook.com%2Fdocs%2Fplugins%2F&amp;src=sdkpreparse">Share</a></div>
                </div>

            
            </div>
        </div>
    </div>
<div id="myModal2" class="modal fade" role="dialog">
      <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Full English test</h4>
          </div>
          <div class="modal-body">
            <p>This is one of the three tests covering different levels of English language knowledge, each tailored for you and your English language skills.
                <br><br>
The test covers wide range of vocabulary and practical units/areas of grammar in use, giving you a thorough and complete evaluation of depths and width of your knowledge.
 <br><br>
Follow the instructions given in each exercise.</p>
          </div>
          <div class="modal-footer">
              <a href="<?=_WEB_PATH?>test/14"><button type="button" class="btn btn-default" >Start full English test</button></a>
          </div>
        </div>

      </div>
    </div>