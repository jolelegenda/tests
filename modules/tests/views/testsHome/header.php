<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Have fun and test your English knowledge at the same time">
    <meta name="author" content="">
    <meta name="robots" content="noindex, nofollow">
    <!--<link rel="icon" href="favicon.ico">-->
     <title>T-List - Test your English knowledge</title>


    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat:300,400,600,800">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Caveat:400,700">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    
    <link rel="stylesheet" href="<?=_WEB_PATH?>views/testsHome/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?=_WEB_PATH?>views/testsHome/style.css">
    <link rel="stylesheet" href="<?=_WEB_PATH?>views/testsHome/selectric.css">
    <link rel="stylesheet" href="<?=_WEB_PATH?>views/testsHome/test.css">
    <link rel="stylesheet" href="<?=_WEB_PATH?>views/testsHome/responsive.css">
     <link rel="stylesheet" href="<?=_WEB_PATH?>views/testsHome/custom.css">
     

<!--<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">-->
</head>

<body class="test <?=$body?>">
    <!-- Load Facebook SDK for JavaScript -->

 <div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.10&appId=1949722975350924';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
    <!-- Static navbar -->
    <nav class="navbar navbar-default navbar-static-top">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
                <a class="navbar-brand caveat" href="#"><img src="<?=_WEB_PATH?>views/testsHome/images/logo.png" alt="Holistic" class='logo'> <span>English test</span></a>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="#">HOME</a></li>
                    <li><a href="#about">TESTS</a></li>
                    <li><a href="#contact">FAQ</a></li>
                    <li><a href="#contact">TERMS</a></li>
                    <li><a class="lastA" href="#contact">CONTACT</a></li>
                </ul>
            </div>
            <!--/.nav-collapse -->
        </div>
    </nav>
    <!--/.navigation -->
