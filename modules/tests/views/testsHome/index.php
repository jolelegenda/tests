<?php
//var_dump($test);

$_SESSION['testname']=$test->testname;
//dump($_SESSION);
$eventListenrsString="";
?>
<style>
  .ui-selected
    {
         background: #03a9f4 url("<?=_WEB_PATH?>views/testsHome/images/check-wh.png") no-repeat scroll center right 10px !important;
        color: #fff;
    }
</style>

<div class="container" id="test-box">  
        <div class="row">
            <div class="col-md-12 col-lg-6 col-lg-offset-3">
            <form id="msform">
              <div id="progressbar">
               <table>
                <tbody>
                    <tr>
                        <?php
                        $i=0;
                         foreach($test->test as $t)
                         {
                          
                        if($i==0)
                        {
                            echo'<td class="active"></td>';
                        }
                        else
                        {
                            echo '<td></td>';
                        }
                    
                             
                             $i++;
                         }
                             ?>
                    </tr>
                </tbody>
                </table>
                <p class="text-left">Your progress</p>
              </div>
              <!-- fieldsets -->
<!--                     <fieldset>
                <h2 class="fs-title">Choose the word that fits the sentence</h2>
                <h3 class="fs-subtitle">Drag</h3>

                    <div class="row test6 testt">
                        <div class="col-xs-12">
                            <span>1. My brother’s daughter <input type="text" disabled class="form-control droppable"> my son</span>.
                        </div>
                    </div>

                    <div class="row test4 test4a testt">
                        <div class="col-xs-12">
                            <div class="draggable" style='z-index:1000'>and</div><div class="draggable" style='z-index:1000'>there</div><div class="draggable" style='z-index:1000'>from</div><div class="draggable" style='z-index:1000'>is</div>
                        </div>
                    </div>

                    <div class="row test6 testt">
                        <div class="col-xs-12">
                            <span>2. Land surrounded <input type="text" disabled class="form-control"> water from all sides.</span>
                        </div>
                    </div>

                    <div class="row test4 test4a testt">
                        <div class="col-xs-12">
                            <div>is</div><div>Carmen</div><div>there were</div><div>from</div>
                        </div>
                    </div>

                    <div class="row test6 testt">
                        <div class="col-xs-12">
                            <span>3. After sunny summer comes rainy </span><input type="text" disabled class="form-control">.
                        </div>
                    </div>

                    <div class="row test4 test4a testt">
                        <div class="col-xs-12">
                            <div>Carmen</div><div>is</div><div>from</div><div>is</div>
                        </div>
                    </div>

                <input type="button" name="next" class="next btn btn-lg btn-primary btn-block btn-signin" value="Next" />
             
              </fieldset>-->
              <?php
              $brojacZaPitanja=1;
                foreach($test->test as $t)
                {
                    
                    if($t->questionType=="connect")
                    {
              ?>
              <fieldset>
                <h2 class="fs-title"><?=$t->question?></h2>
                <h3 class="fs-subtitle"><?=$t->hint?></h3>
                 <div id="<?=$t->qid?>">
                <?php
                $rand=sizeof($t->answers)-1;
                $rand1=0;
                          foreach($t->answers as $a)
                          {
                              
                              $prop=$t->answers[$rand];
                              $prop1=$t->answers[$rand1];
                             // dump($t->additionalData->answerKey->$prop);
                        ?>
               
                    <div class="row test1 testt">
                       
                        <div class="col-xs-6 col-sm-4 col-sm-offset-2">
                            <img class="img-test1" src="<?=_WEB_PATH?>views/testsHome/images/test1/<?=$imgConnect[$t->answers[$rand]]?>" width="120" height="120" data-attr=<?=$t->additionalData->answerKey->$prop?> data-connectImg=<?=$t->additionalData->answerKey->$prop?>>
                        </div>
                        <div class="col-xs-6 col-sm-4">
                            <button type="button" class="dugme-test1" data-word=<?=$t->additionalData->answerKey->$prop1?> data-connectWrd=<?=$t->additionalData->answerKey->$prop1?>><?=$t->options->$prop1?></button>
                        </div>
                       
                    
                </div>
                  <?php
                  $rand--;
                  $rand1++;
                          }
                        ?>   
                     </div>
                <input type='hidden' id='pointsHid' value="<?=$t->points?>">
                <input type='hidden' id='trenutno<?=$t->qid?>' data-qtype="<?=$t->questionType?>" data-modul="<?=$t->modul?>" data-last="0" data-qid="<?=$t->qid?>" data-url='<?=_WEB_PATH."results"?>' data-url1='<?=_WEB_PATH."trenutno"?>'/>
                 <?php
                if($brojacZaPitanja>1)
                {
                ?>
                <input type="button" name="next" class="previous btn btn-lg btn-primary btn-block btn-signin" value="Back" /> 
                <?php
                }
                ?>
                <input type="button" name="next" class="next btn btn-lg btn-primary btn-block btn-signin" value="Next" data-qtype="<?=$t->questionType?>" data-modul="<?=$t->modul?>" data-last="<?=$t->final?>" data-qid="<?=$t->qid?>"/>
             
              </fieldset>
               <?php
               $_SESSION['correctAnswers'][$t->modul]=$t->correctAnswers;
               
                    }
                    if($t->questionType=="radioImage")
                    {
                    ?>
              <fieldset>
                <h2 class="fs-title"><?=$t->question?></h2>
                <h3 class="fs-subtitle"><?=$t->hint?></h3>
                <div id="<?=$t->qid?>">
                <?php
                
               foreach($t->answers as $a)
                          {
                   
               ?>
                    <div class="row test2 testt">
                        <div class="col-xs-6 col-sm-4">
                            <img src="<?=_WEB_PATH?>views/testsHome/images/test2/<?=$imgRadio[$a]?>" width="120" height="120">
                        </div>
                        <div class="col-xs-6 col-sm-8">
                             <?php
                                 foreach($t->options->$a as $o)
                                 {
                                           
                                ?>
                            <div class="radio radio-inline">
                               
                               
                                <input id="inlineRadio<?=$o?>" value="<?=$o?>" name="<?=$t->qid?>radio<?=$a?>" type="radio">
                                <label for="inlineRadio<?=$o?>"> <?=$o?> </label>
                               
                            </div>
                             <?php
                                     
                                 }
                                ?>
                        </div>
                    </div>
                <?php
                
                          }
                ?>
                </div>
                <input type='hidden' id='trenutno<?=$t->qid?>' data-qtype="<?=$t->questionType?>" data-modul="<?=$t->modul?>" data-last="0" data-qid="<?=$t->qid?>" data-url='<?=_WEB_PATH."results"?>' data-url1='<?=_WEB_PATH."trenutno"?>'/>
                <?php
                if($brojacZaPitanja>1)
                {
                ?>
                <input type="button" name="next" class="previous btn btn-lg btn-primary btn-block btn-signin" value="Back" /> 
                <?php
                }
                ?>
                <input type="button" name="next" class="next btn btn-lg btn-primary btn-block btn-signin" value="Next" data-qtype="<?=$t->questionType?>" data-modul="<?=$t->modul?>" data-param="2" data-last="<?=$t->final?>" data-qid="<?=$t->qid?>"/>
             
              </fieldset> 
              <?php
              $_SESSION['correctAnswers'][$t->modul]=$t->correctAnswers;
              $_SESSION['points'][$t->modul]=$t->points;
                    }
                     if($t->questionType=="dropdown")
                    {
                        // $i=0;
                    ?>
               <fieldset>
                <h2 class="fs-title"><?=$t->question?></h2>
                <h3 class="fs-subtitle"><?=$t->hint?></h3>
                <div id="<?=$t->qid?>">
                <?php
               //$i=0;
                 foreach($t->answers as $a)
                          {
                               $sentence="";
                                  // dump($t->options->$a);
                               
                                
                               
                                   if(isset($t->multi) && $t->multi==1)
                                   {
                                       
                                      
                                            
                                           $opcije=$t->options->$a;
                                           $forReplace=\Classes\TestHelper::forSentence($opcije);
                                            $sentence.=str_replace($forReplace[0], $forReplace[1], $t->sentences->$a);
                                          
                                     
                                   }
                                   else
                                   {
                                       $sentence.=str_replace("#replace#", \Classes\FormHelper::selectFromArray($t->options->$a), $t->sentences->$a);
                                   }
                                
                          
                 ?>
                 
                    <div class="row test3 testt text-left">
               
                        <div class="col-xs-12">
                   
                            <?php
                           echo $sentence;
                            ?>
                    
                 
                            </div>
                    </div>
                   
                 <?php
                            } 
                         
                 ?>
                </div>
                <input type='hidden' id='trenutno<?=$t->qid?>' data-qtype="<?=$t->questionType?>" data-modul="<?=$t->modul?>" data-last="0" data-qid="<?=$t->qid?>" data-url='<?=_WEB_PATH."results"?>' data-url1='<?=_WEB_PATH."trenutno"?>'/>
                 <?php
                if($brojacZaPitanja>1)
                {
                ?>
                <input type="button" name="next" class="previous btn btn-lg btn-primary btn-block btn-signin" value="Back" /> 
                <?php
                }
                ?>
                <input type="button" name="next" class="next btn btn-lg btn-primary btn-block btn-signin" value="Next" data-qtype="<?=$t->questionType?>" data-modul="<?=$t->modul?>" data-last="<?=$t->final?>" data-qid="<?=$t->qid?>"/>
             
              </fieldset>
              <?php
              $_SESSION['correctAnswers'][$t->modul]=$t->correctAnswers;
              $_SESSION['points'][$t->modul]=$t->points;
                    }
                    if($t->questionType=="rearrange")
                    {
                    ?>
              <fieldset>
                <h2 class="fs-title"><?=$t->question?></h2>
                <h3 class="fs-subtitle"><?=$t->hint?></h3>
                <div id="<?=$t->qid?>">
                 <?php
                  $i=1;
                 foreach($t->answers as $a)
                          {
                 ?>
                    <div class="row test4 testt">
                        
                        <div class="col-xs-12 sortable" id='sortable<?php echo $a?>'>
                            <div class='unsortable'><?=$i?>.</div>
                            <?php
                            foreach($t->options->$a as $o)
                                 {
                            ?>
                            <div><?=$o?></div>
                            <?php
                                 }
                            ?>
                        </div>
                    </div>
                  <?php
                    $i++;
                          }
                  ?>
                </div>
                <input type='hidden' id='trenutno<?=$t->qid?>' data-qtype="<?=$t->questionType?>" data-modul="<?=$t->modul?>" data-last="0" data-qid="<?=$t->qid?>" data-url='<?=_WEB_PATH."results"?>' data-url1='<?=_WEB_PATH."trenutno"?>'/>
                 <?php
                if($brojacZaPitanja>1)
                {
                ?>
                <input type="button" name="next" class="previous btn btn-lg btn-primary btn-block btn-signin" value="Back" /> 
                <?php
                }
                ?>
                <input type="button" name="next" class="next btn btn-lg btn-primary btn-block btn-signin" value="Next" data-qtype="<?=$t->questionType?>" data-modul="<?=$t->modul?>" data-last="<?=$t->final?>" data-qid="<?=$t->qid?>"/>
             
              </fieldset>
             
              
              <?php
              $_SESSION['correctAnswers'][$t->modul]=$t->correctAnswers;
              $_SESSION['points'][$t->modul]=$t->points;
                    }
                    if($t->questionType=="highlight")
                    {
                    
                    ?>
               <fieldset>
                <h2 class="fs-title"><?=$t->question?></h2>
                <h3 class="fs-subtitle"><?=$t->hint?></h3>
                <div id="<?=$t->qid?>">
               <?php
               $i=1;
                foreach($t->answers as $a)
                          {
                 
               ?>
                    <div class="row test5 testt">
                        <div class="col-xs-12 selectable" id='selectable q1'>
                            <div class='unselectable'><?=$i?>.</div>
                             <?php
                            foreach($t->options->$a as $o)
                                 {
                            ?>
                            <div><?php echo $o?></div>
                            
                              <?php
                                 }
                              ?>
                        </div>
                    </div>
                <?php
                $i++;
                          }
                ?>
                </div> 
                <input type='hidden' id='trenutno<?=$t->qid?>' data-qtype="<?=$t->questionType?>" data-modul="<?=$t->modul?>" data-last="0" data-qid="<?=$t->qid?>" data-url='<?=_WEB_PATH."results"?>' data-url1='<?=_WEB_PATH."trenutno"?>'/>
                 <?php
                if($brojacZaPitanja>1)
                {
                ?>
                <input type="button" name="next" class="previous btn btn-lg btn-primary btn-block btn-signin" value="Back" /> 
                <?php
                }
                ?>
                <input type="button" name="next" class="next btn btn-lg btn-primary btn-block btn-signin" value="Next" data-qtype="<?=$t->questionType?>" data-modul="<?=$t->modul?>" data-last="<?=$t->final?>" data-qid="<?=$t->qid?>"/>
             
              </fieldset>
              <?php
              $_SESSION['correctAnswers'][$t->modul]=$t->correctAnswers;
              $_SESSION['points'][$t->modul]=$t->points;
                    }
                    if($t->questionType=="radioSentence")
                    {
                    ?>
              <fieldset>
                 <h2 class="fs-title"><?=$t->question?></h2>
                <h3 class="fs-subtitle"><?=$t->hint?></h3>
                <div id="<?=$t->qid?>">
               <?php
                
               foreach($t->answers as $a)
                          {
                   $sentence=str_replace(array("#replace#","#replace1#"), array(\Classes\FormHelper::disabledField(),\Classes\FormHelper::disabledField()), $t->sentences->$a);
                   
               ?>
                    <div class="row test6 test2 testt">
                        <div class="col-xs-12">
                            <div style="margin-bottom:20px;">
                           <?=$sentence?>
                            </div>
                            <?php
                                 foreach($t->options->$a as $o)
                                 {
                                           
                                ?>
                            <div class="radio radio-inline">
                               
                               
                                <input id="inlineRadio<?=$o.$a?>" value="<?=$o?>" name="<?=$t->qid?>radio<?=$a?>" type="radio">
                                <label for="inlineRadio<?=$o.$a?>"> <?=$o?> </label>
                               
                            </div>
                             <?php
                                     
                                 }
                                ?>
                           
                        </div>
                    </div>
                <?php
                          }
                ?>
                </div>
                <input type='hidden' id='trenutno<?=$t->qid?>' data-qtype="radioImage" data-modul="<?=$t->modul?>" data-last="0" data-qid="<?=$t->qid?>" data-url='<?=_WEB_PATH."results"?>' data-url1='<?=_WEB_PATH."trenutno"?>'/>
                 <?php
                if($brojacZaPitanja>1)
                {
                ?>
                <input type="button" name="next" class="previous btn btn-lg btn-primary btn-block btn-signin" value="Back" /> 
                <?php
                }
                ?>
                <input type="button" name="next" class="next btn btn-lg btn-primary btn-block btn-signin" value="Next" data-qtype="radioImage" data-modul="<?=$t->modul?>" data-param="6" data-last="<?=$t->final?>" data-qid="<?=$t->qid?>"/>
             
              </fieldset> 
              <?php
              $_SESSION['correctAnswers'][$t->modul]=$t->correctAnswers;
              $_SESSION['points'][$t->modul]=$t->points;
                    }
                    if($t->questionType=="typein")
                    {
                        
                    ?>
              <fieldset>
                <h2 class="fs-title"><?=$t->question?></h2>
                <h3 class="fs-subtitle"><?=$t->hint?></h3>
               <?php
                
               foreach($t->answers as $a)
                          {
                   $sentence=str_replace("#replace#", \Classes\FormHelper::textField($t->qid,$t->correctAnswers->$a), $t->sentences->$a);
               ?>
                    <div class="row test6 testt test66">
                        <div class="col-xs-12">
                            <?=$sentence?>
                        </div>
                    </div>
                   <?php
                          }
                   ?>
                 <input type='hidden' id='trenutno<?=$t->qid?>' data-qtype="<?=$t->questionType?>" data-modul="<?=$t->modul?>" data-last="0" data-qid="<?=$t->qid?>" data-url='<?=_WEB_PATH."results"?>' data-url1='<?=_WEB_PATH."trenutno"?>'/>
                  <?php
                if($brojacZaPitanja>1)
                {
                ?>
                <input type="button" name="next" class="previous btn btn-lg btn-primary btn-block btn-signin" value="Back" /> 
                <?php
                }
                ?>
                <input type="button" name="next" class="next btn btn-lg btn-primary btn-block btn-signin" value="Next" data-qtype="<?=$t->questionType?>" data-modul="<?=$t->modul?>"  data-last="<?=$t->final?>" data-qid="<?=$t->qid?>"/>
             
              </fieldset>
              <?php
              $_SESSION['correctAnswers'][$t->modul]=$t->correctAnswers;
              $_SESSION['points'][$t->modul]=$t->points;
                    }
                    if($t->questionType=="dragdrop")
                    {
                    ?>
<fieldset>
                <h2 class="fs-title"><?=$t->question?></h2>
                <h3 class="fs-subtitle"><?=$t->hint?></h3>
                <div id="<?=$t->qid?>">
                <?php
                $i=0;
                $stringDrag="";
                $stringDrop="";
                
  foreach($t->answers as $a)
                          {
                   $sentence=str_replace("#replace#", \Classes\FormHelper::disabledField("droppable{$i}"), $t->sentences->$a);
               ?>
                <div id='pitanje<?=$i?>'>
                    <div class="row test6 testt">
                        <div class="col-xs-12">
                            <?php
                            echo $sentence;
                            ?>
                        </div>
                    </div>

                    <div class="row test4 test4a testt">
                        <div class="col-xs-12">
                            <?php
                            foreach($t->options->$a as $o)
                                 {
                             ?>
                            <div class="draggable<?=$i?>"   style='z-index:1000'><?=$o?></div>
                            <?php
                                 }
                                 ?>
                           
                        </div>
                    </div>
                </div>
                <?php
                $stringDrag.='$( ".draggable'.$i.'" ).draggable({ 
          revert: "invalid",
           containment: "#pitanje'.$i.'"
        });';
                
$stringDrop.='var i=[];$( ".droppable'.$i.'" ).droppable({
            drop: function( event, ui ) {
               if(typeof i['.$i.']==="undefined")
               {
               i['.$i.']=0;
               }
               i['.$i.']++;
               ui.draggable.addClass("draggedItem'.$i.'"+i['.$i.']);
               ui.draggable.addClass("draggedIndicate'.$i.'");
               $(".draggedItem'.$i.'"+i['.$i.']).css("z-index","0");
               var indicator=$(".draggedIndicate'.$i.'").length;
               //console.log(indicator);
               if(indicator=="2")
               {
               //console.log(indicator);
               i['.$i.']--;
               $(".draggedItem'.$i.'"+i['.$i.']).css("left","");
               $(".draggedItem'.$i.'"+i['.$i.']).css("top","");
               $(".draggedItem'.$i.'"+i['.$i.']).css("z-index","1000");
               $(".draggedItem'.$i.'"+i['.$i.']).removeClass("draggedIndicate'.$i.'");
              $(".draggedItem'.$i.'"+i['.$i.']).removeClass("draggedItem'.$i.'"+i['.$i.']);
              
              i['.$i.']++;
               }
              // ui.draggable.css("z-index","0");
               
                $(this).val(ui.draggable.text());
                var qid=$(this).parent().parent().parent().parent().attr("id");
                          var trenutnoData=$("#trenutno"+qid);
                           var function_name=trenutnoData.attr("data-qtype")+"_prepareResult";
            var answers;
            var qid=trenutnoData.attr("data-qid");
          /*  if($(this).attr("data-qtype")=="radioImage" || $(this).attr("data-qtype")=="radioSentence")
            {
                answers=radioImage_prepareResult($(this).attr("data-param"));
            }
            else
            {*/
                answers=window[function_name](qid);
          //  }
          //  console.log(answers);
         // var intervalAjax;
            var csrf_token=encodeURIComponent($("#csrf_token").val());
          // alert(csrf_token);
           var json_answers=JSON.stringify(answers);
           var data="csrf_token="+csrf_token+"&questionType="+trenutnoData.attr("data-qtype")+"&modul="+trenutnoData.attr("data-modul")+"&answers="+json_answers+"&last="+trenutnoData.attr("data-last"); 
           //var data="csrf_token="+csrf_token+"&questionType="+$(this).attr("data-qtype")+"&modul="+$(this).attr("data-modul")
            ajaxCall(data,trenutnoData.attr("data-url"),function(data){
                /*if(data!="")
                {*/
                    //window.location=data;
                    var csrf_token=encodeURIComponent($("#csrf_token").val());
                  ajaxCall("csrf_token="+csrf_token,trenutnoData.attr("data-url1"),function(data){
                     if(data!="")
                      {
                    //window.location=data;
                        // alert(data);
                          $("#congrats_span").html(data);
                         $("#medjustrana").addClass("active");
                         setTimeout(function(){ $("#medjustrana").removeClass("active"); }, 4000);
                     }
                     },"POST")
               // }
            })
                //ui.draggable.removeClass("draggable");
               // $(this).droppable("destroy");
            }
            });';

                $i++;
                          }
                ?>
                </div>
<input type='hidden' id='trenutno<?=$t->qid?>' data-qtype="<?=$t->questionType?>" data-modul="<?=$t->modul?>" data-last="0" data-qid="<?=$t->qid?>" data-url='<?=_WEB_PATH."results"?>' data-url1='<?=_WEB_PATH."trenutno"?>'/>
                              <?php
                if($brojacZaPitanja>1)
                {
                ?>
                <input type="button" name="next" class="previous btn btn-lg btn-primary btn-block btn-signin" value="Back" /> 
                <?php
                }
                ?>
                <input type="button" name="next" class="next btn btn-lg btn-primary btn-block btn-signin" value="Next" data-qtype="<?=$t->questionType?>" data-modul="<?=$t->modul?>"  data-last="<?=$t->final?>" data-qid="<?=$t->qid?>"/>
             
              </fieldset>
                    <?php
                     $_SESSION['correctAnswers'][$t->modul]=$t->correctAnswers;
              $_SESSION['points'][$t->modul]=$t->points;
                    }
                     if($t->questionType=="typein2")
                    {
                        
                    ?>
              <fieldset>
                <h2 class="fs-title"><?=$t->question?></h2>
                <h3 class="fs-subtitle"><?=$t->hint?></h3>
                <?php
                if(!is_null($t->answerHint))
                {
                ?>
                 <div class="row test4 test4a test4-premium testt">
                        <div class="col-xs-12">
                            <?php
                            foreach($t->answerHint as $ah)
                            {
                                echo "<div>{$ah}</div>";
                            }
                            ?>
                        </div>
                    </div>
               <?php
                }
                ?>
                <div class="row test6 test6-premium testt">
                    <div class="col-xs-12">
                <?php
               foreach($t->answers as $a)
                          {
                   $sentence=str_replace("#replace#", \Classes\FormHelper::textField($t->qid,$t->correctAnswers->$a), $t->sentences->$a);
               ?>
                
                        
                            <?=$sentence?>
                       
               
                   
                             
                   <?php
                
                          }
                   ?>
                         </div>
                     </div>
                 <input type='hidden' id='trenutno<?=$t->qid?>' data-qtype="typein" data-modul="<?=$t->modul?>" data-last="0" data-qid="<?=$t->qid?>" data-url='<?=_WEB_PATH."results"?>' data-url1='<?=_WEB_PATH."trenutno"?>'/>
                  <?php
                if($brojacZaPitanja>1)
                {
                ?>
                <input type="button" name="next" class="previous btn btn-lg btn-primary btn-block btn-signin" value="Back" /> 
                <?php
                }
                ?>
                <input type="button" name="next" class="next btn btn-lg btn-primary btn-block btn-signin" value="Next" data-qtype="typein" data-modul="<?=$t->modul?>"  data-last="<?=$t->final?>" data-qid="<?=$t->qid?>"/>
             
              </fieldset>
              <?php
              $_SESSION['correctAnswers'][$t->modul]=$t->correctAnswers;
              $_SESSION['points'][$t->modul]=$t->points;
                    }
                    ?>
                    <?php
                    if($t->questionType!="rearrange" && $t->questionType!="dragdrop" && $t->questionType!="highlight")
                    {
                        if($t->questionType=="radioSentence")
                        {
                            $t->questionType="radioImage";
                        }
                        if($t->questionType=="typein2")
                        {
                            $t->questionType="typein";
                        }
                        $eventListenrsString.="<script>document.addEventListener( 'DOMContentLoaded', function () {
                        {$t->questionType}_prepareClick('{$t->qid}');
}, false );</script>";
                    ?>
              
              <?php
                    }
                    $brojacZaPitanja++;
                }
                //var_dump($_SESSION);
              ?>
              <?=$csrf_field?>
            </form>
            </div>
        </div>
    </div>

<div id="medjustrana" class="text-center">
        <h3 class="caveat">Congratulations!</h3>
        <p>You’ve scored <span class="boldest colorYl" id="congrats_span"></span>  / <span class="boldest"> 18</span> points and <span class="boldest colorYl">unlocked</span> <span class="boldest colorBl">new level!</span></p>

    </div>
<?php
echo $eventListenrsString;
?>
