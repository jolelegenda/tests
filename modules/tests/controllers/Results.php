<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Modules\tests;

/**
 * Description of Results
 *
 * @author Sonor Smart Force
 */
class Results extends \Controllers\frontendController {

    public function __construct() {

        $module_name = \Classes\ModuleHelper::getModuleName(dirname(__FILE__));
        // echo dirname(__FILE__);
        parent::__construct($module_name);
    }

    public function CalculateResults() {
        $answers = json_decode($_POST['answers']);
        $resModul = 0;
        $ukupno = 0;
        $_SESSION['ukupnorez'] = 0;
        //echo $ukupno;
        if ($_POST['questionType'] == 'connect') {
            $_SESSION['result'][$_POST['modul']] = $answers->result;
        } else {
            foreach ($_SESSION['correctAnswers'][$_POST['modul']] as $k => $ca) {
                if (is_array($ca)) {
                    if (in_array($answers->$k, $ca)) {
                        $resModul += $_SESSION['points'][$_POST['modul']];
                    }
                } else {
                    if ($answers->$k == $ca) {
                        $resModul += $_SESSION['points'][$_POST['modul']];
                    }
                }
            }
            $_SESSION['result'][$_POST['modul']] = $resModul;
        }
        $_SESSION['odgovori']['answers'][]=$answers;
        foreach ($_SESSION['result'] as $k => $v) {
            $ukupno += $v;
        }
        if ($_POST['last'] == 1) {
            $_SESSION['ukupnorez'] = $ukupno;
             $_SESSION['odgovori']['result']=$ukupno;
             $_SESSION['odgovori']['test']=$_SESSION['testname'];
            unset($_SESSION['correctAnswers']);
            unset($_SESSION['points']);
            unset($_SESSION['result']);
            unset($_SESSION['trenutnorez']);
            if(isset($_SESSION['participant_id']))
            {
            $data=[
            'participant-id'=>(int)$_SESSION['participant_id'],
            'quizdata'=>json_encode($_SESSION['odgovori']),
            'test-id'=>$_SESSION['testname']
             ];
            $apiAuth=new \Classes\RESTClient($data,_API_URL."tests/",['X-JWT-Auth'=>$_SESSION['jwt']]);
           // dump($apiAuth->post()->data->id);
            $res=$apiAuth->post();
            /*dump($res);
            die;*/
            }
            unset($_SESSION['odgovori']);
            echo _WEB_PATH . "calculating";
        } else {

            $_SESSION['trenutnorez'] = $ukupno;

            echo "";
        }
    }

    public function Sum() {
        global $testids, $nextlevel;

        if ($_SESSION['testname'] == "Otherlevels") {
            if ($_SESSION['ukupnorez'] >= 0 && $_SESSION['ukupnorez'] <= 11) {
                echo _WEB_PATH . "result/Freshman/0";
            }
            if ($_SESSION['ukupnorez'] >= 12 && $_SESSION['ukupnorez'] <= 15) {
                echo _WEB_PATH . "result/Student/0";
            }
            if ($_SESSION['ukupnorez'] >= 16 && $_SESSION['ukupnorez'] <= 18) {
                echo _WEB_PATH . "result/Academic/0";
            }
        } else {
            $next = $testids[$nextlevel[$_SESSION['testname']]];
            if ($_SESSION['ukupnorez'] < 12) {
                echo _WEB_PATH . "result/{$_SESSION['testname']}/0";
            } else {
                echo _WEB_PATH . "test/{$next}";
            }
        }
    }

    public function RealTime() {
        if ($_SESSION['testname'] == "Otherlevels") {
            echo "";
        } else {
            if ($_SESSION['trenutnorez'] < 12) {
                // echo _WEB_PATH."result/{$_SESSION['testname']}";
                echo "";
            } else {
                if (\Classes\Cookie::get("congrats") == $_SESSION['testname']) {
                    echo "";
                } else {
                    echo $_SESSION['trenutnorez'];
                    \Classes\Cookie::set("congrats", $_SESSION['testname'], 30, "/");
                }
            }
        }
    }

}
