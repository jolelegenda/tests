<?php
namespace Modules\tests;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Tests
 *
 * @author Sonor Smart Force
 */
class Tests extends \Controllers\frontendController
{
    use \Classes\TestsTrait;
     public function __construct() {
      
        $module_name=\Classes\ModuleHelper::getModuleName(dirname(__FILE__));
       // echo dirname(__FILE__);
        parent::__construct($module_name);
      // echo $this->jwt;
      //  $this->testTemplate= new \Classes\TestTemplate();
    }
    
    public function result($name,$share)
    {

global $ogConfig;

        
        $name=strtolower($name);
        $this->template->setData("body","results {$name}");
        $this->template->setData("og_type",$ogConfig[$name]["type"]);
        $this->template->setData("og_url",$ogConfig[$name]["url"]);
        $this->template->setData("og_title",$ogConfig[$name]["title"]);
        $this->template->setData("og_description",$ogConfig[$name]["description"]);
        $this->template->setData("og_image",$ogConfig[$name]["image"]);
       // var_dump($share);
        if($share==1)
        {
            $this->template->module_name="";
            $this->template->setData('csrf_field', \Classes\FormHelper::csrfHiddenFiled(\Classes\Session::get('csrf_token')));
            $this->template->render("index");
        }
        else
        {
        $this->template->render($name);
        }
    }
    public function saveTest($id)
    {
//        $q1=new \Classes\QuestionData();
//        $q1->question="Match the shops to the things we usually buy in them:";
//        $q1->hint="Connect the right items, click and connect";
//        $q1->answers=['q1','q2','q3'];
//        $q1->options=['q1'=>'Dictionary','q2'=>'Newspapers','q3'=>'Medicine'];
//        $q1->additionalData=["images"=>['q1','q2','q3'],"answerKey"=>["q1"=>'44499dd7-8457-11e7-a60b-08f9c906bea8',"q2"=>'8ab6c399-8457-11e7-a60b-08f9c906bea8',"q3"=>'b3e2b501-8457-11e7-a60b-08f9c906bea8']];
//        $q1->questionType='connect';
//        $q1->modul='Module1a';
//        $q1->points=0.5;
//        $q1->final=0;
//        $q1->qid='newbie1';
//        $questionData[]=$q1;
//        
//        $q2=new \Classes\QuestionData();
//        $q2->question="Where can you see these notices?";
//        $q2->hint="Radio button, multiple choice";
//        $q2->answers=['q1','q2','q3'];
//        $q2->options=['q1'=>['In a library','At a restaurant','In a park'],'q2'=>['In a taxi','In a bank','In a hotel'],'q3'=>['At a swimming-pool','In a department store','In a theatre']];
//        $q2->additionalData=["images"=>['q1','q2','q3']];
//        $q2->questionType='radioImage';
//        $q2->correctAnswers=['q1'=>'At a restaurant','q2'=>'In a hotel','q3'=>'In a department store'];
//        $q2->modul='Module1b';
//        $q2->points=0.5;
//        $q2->final=0;
//        $q2->qid='newbie2';
//        $questionData[]=$q2;
//        //dd(json_encode($q1));
//     // var_dump(json_encode($q2));
//     // die;
//        ///dump($questionData);die;
//        $q3=new \Classes\QuestionData();
//        $q3->question="Choose the correct word.";
//        $q3->hint="Dropdown menu";
//        $q3->answers=['q1','q2','q3'];
//        $q3->sentences=['q1'=>'1. Carmen is from</span> #replace#.','q2'=>'2. There are many #replace# in the park.','q3'=>'3. Today is the #replace# of July.'];
//        $q3->options=['q1'=>['-- Choose --','Spain','Spanish'],'q2'=>['-- Choose --','children','childrens'],'q3'=>['-- Choose --','one','first']];
//       // $q3->additionalData=["images"=>['Restaurant','Hotel','Store']];
//        $q3->questionType='dropdown';
//        $q3->correctAnswers=['q1'=>'Spain','q2'=>'children','q3'=>'first'];
//        $q3->modul='Module2';
//        $q3->points=1;
//        $q3->final=0;
//        $q3->qid='newbie3';
//        $questionData[]=$q3;
////        
//        $q4=new \Classes\QuestionData();
//        $q4->question="Arrange and make sentences from these words.";
//        $q4->hint="Drag and rearrange";
//        $q4->answers=['q1','q2','q3'];
//        $q4->correctAnswers=['q1'=>'I am not from China .','q2'=>'Where does Jane’s sister live ?','q3'=>'What is Anna doing at the moment ?'];
//        $q4->options=['q1'=>['not','I','from','am','.','China'],'q2'=>['live','Where','?','Jane’s','sister','does'],'q3'=>['moment','is','?','Anna','at','the','What','doing']];
//       // $q3->additionalData=["images"=>['Restaurant','Hotel','Store']];
//        $q4->questionType='rearrange';
//        //$q4->correctAnswers=['Spain'=>'Spain','Children'=>'children','July'=>'first'];
//        $q4->modul='Module3';
//        $q4->points=1;
//        $q4->final=0;
//        $q4->qid='newbie4';
//        $questionData[]=$q4;
////        
//        $q5=new \Classes\QuestionData();
//        $q5->question="Click on the correct sentence:";
//        $q5->hint="Highlight";
//        $q5->answers=['q1','q2','q3'];
//        $q5->correctAnswers=['q1'=>'What are they doing tomorrow afternoon after school?','q2'=>'He doesn’t do his homework every day.','q3'=>'Has he got an aunt in Beijing?'];
//        $q5->options=['q1'=>['What are they doing tomorrow afternoon after school?','What is they doing tomorrow afternoon after school?'],'q2'=>['He don’t do his homework every day.','He doesn’t do his homework every day.'],'q3'=>['Does he has an aunt in Beijing?','Has he got an aunt in Beijing?']];
//       // $q3->additionalData=["images"=>['Restaurant','Hotel','Store']];
//        $q5->questionType='highlight';
//        //$q4->correctAnswers=['Spain'=>'Spain','Children'=>'children','July'=>'first'];
//        $q5->modul='Module4';
//        $q5->points=1;
//        $q5->final=0;
//        $q5->qid='newbie5';
//        $questionData[]=$q5;
//        
//        $q6=new \Classes\QuestionData();
//        $q6->question="Choose the word that fits the sentence.";
//        $q6->hint="Radio button, multiple choice";
//        $q6->answers=['q1','q2','q3','q4','q5','q6'];
//        $q6->sentences=['q1'=>'<span>1. I can see</span> #replace# <span>here.</span>','q2'=>'<span>2. It is a rabbit.</span> #replace# <span>hair is short.</span>','q3'=>'<span>3. How</span> #replace# <span>tea do you drink?</span>','q4'=>'<span>4. We’re students. That’s</span> #replace# <span>classroom.</span>','q5'=>'<span>5. There are</span> #replace# <span>apples on the table.</span>','q6'=>'<span>6. These are the</span> #replace# <span>books, they need them now.</span>'];
//        $q6->options=['q1'=>['him','me','his','he'],'q2'=>['It’s','Its','His','He’s'],'q3'=>['many','any','much','some'],'q4'=>['us','our','we','me'],'q5'=>['any','some','a','an'],'q6'=>['boys','boy’s','boys’','boys’s']];
//       // $q3->additionalData=["images"=>['Restaurant','Hotel','Store']];
//        $q6->questionType='radioSentence';
//        $q6->correctAnswers=['q1'=>'him','q2'=>'Its','q3'=>'much','q4'=>'our','q5'=>'some','q6'=>'boys’'];
//        $q6->modul='MixModule';
//        $q6->points=1;
//        $q6->final=1;
//        $q6->qid='newbie6';
//        $questionData[]=$q6;
//                $q1=new \Classes\QuestionData();
//        $q1->question="Type the missing word.";
//        $q1->hint="Type the answer";
//        $q1->answers=['q1','q2','q3'];
//        $q1->sentences=['q1'=>'<span>1. My brother’s daughter is my</span> #replace#.','q2'=>'<span>2. Land surrounded by water from all sides is an</span> #replace#.','q3'=>'<span>3. After sunny summer comes rainy</span> #replace#.'];
//       // $q1->options=['q1'=>['-- Choose --','Spain','Spanish'],'q2'=>['-- Choose --','children','childrens'],'q3'=>['-- Choose --','one','first']];
//       // $q3->additionalData=["images"=>['Restaurant','Hotel','Store']];
//        $q1->questionType='typein';
//        $q1->correctAnswers=['q1'=>'niece','q2'=>'island','q3'=>['autumn','fall']];
//        $q1->modul='Module1';
//        $q1->points=1;
//        $q1->final=0;
//        $q1->qid='novice1';
//        $questionData[]=$q1;
//                $q2=new \Classes\QuestionData();
//        $q2->question="Choose the correct option that fits the meaning.";
//        $q2->hint="Dropdown menu";
//        $q2->answers=['q1','q2','q3'];
//        $q2->sentences=['q1'=>'1. I don’t know</span> #replace#.','q2'=>'2. It’s #replace# I can do.','q3'=>'3. There’s a bus coming. #replace#!'];
//        $q2->options=['q1'=>['-- Choose --','nothing','anything','something'],'q2'=>['-- Choose --','less','the least','little'],'q3'=>['-- Choose --','Keep off','Look after','Look out']];
//       // $q3->additionalData=["images"=>['Restaurant','Hotel','Store']];
//        $q2->questionType='dropdown';
//        $q2->correctAnswers=['q1'=>'anything','q2'=>'the least','q3'=>'Look out'];
//        $q2->modul='Module2';
//        $q2->points=1;
//        $q2->final=0;
//        $q2->qid='novice2';
//        $questionData[]=$q2;
//                $q3=new \Classes\QuestionData();
//        $q3->question="One sentence is incorrect in each pair. Click on the correct sentence.";
//        $q3->hint="Highlight";
//        $q3->answers=['q1','q2','q3'];
//        $q3->correctAnswers=['q1'=>'What do you usually do at the weekend?','q2'=>'What were you doing at 8 o’clock last night?','q3'=>'Look at those clouds! It is going to rain tomorrow.'];
//        $q3->options=['q1'=>['What do you usually do at the weekend?','What are you usually doing at the weekend?'],'q2'=>['What did you do at 8 o’clock last night?','What were you doing at 8 o’clock last night?'],'q3'=>['Look at those clouds! It will rain tomorrow.','Look at those clouds! It is going to rain tomorrow.']];
//       // $q3->additionalData=["images"=>['Restaurant','Hotel','Store']];
//        $q3->questionType='highlight';
//        //$q4->correctAnswers=['Spain'=>'Spain','Children'=>'children','July'=>'first'];
//        $q3->modul='Module3';
//        $q3->points=1;
//        $q3->final=0;
//        $q3->qid='novice3';
//        $questionData[]=$q3;
//                $q4=new \Classes\QuestionData();
//        $q4->question="Choose the correct form of the modal verb.";
//        $q4->hint="Radio button, multiple choice";
//        $q4->answers=['q1','q2','q3'];
//        $q4->sentences=['q1'=>'<span>1. Simon</span> #replace# <span>come tomorrow. He has so much to do.</span>','q2'=>'<span>2. Unfortunately, I</span> #replace# <span>to help her with her task.</span>','q3'=>'<span>3. You</span> #replace# <span>do your homework first.</span>'];
//        $q4->options=['q1'=>['will must','will have to','will have','will must to'],'q2'=>['couldn’t','can’t','wasn’t able','weren’t able'],'q3'=>['should','should to','have','must to']];
//       // $q3->additionalData=["images"=>['Restaurant','Hotel','Store']];
//        $q4->questionType='radioSentence';
//        $q4->correctAnswers=['q1'=>'will have to','q2'=>'wasn’t able','q3'=>'should'];
//        $q4->modul='Module4';
//        $q4->points=1;
//        $q4->final=0;
//        $q4->qid='novice4';
//        $questionData[]=$q4;
//        $q5=new \Classes\QuestionData();
//        $q5->question="Choose the correct option";
//        $q5->hint="Dropdown menu";
//        $q5->answers=['q1','q2','q3','q4','q5','q6'];
//        $q5->sentences=['q1'=>'1. #replace# mother works in the hospital?','q2'=>'2. This pencil isn’t #replace#.'
//            ,'q3'=>'3. I really like the #replace#.','q4'=>'4. She has two pet #replace#.','q5'=>'5. #replace# brand new stereo system was very expensive.','q6'=>'6. How many people #replace# the party last night?'];
//        $q5->options=['q1'=>['-- Choose --','Whom','Whose','What'],'q2'=>['-- Choose --','your','your’s','yours'],'q3'=>['-- Choose --','roses’ colour','colour of the roses','rose colour'],'q4'=>['-- Choose --','mouses','mouse','mice'],'q5'=>['-- Choose --','Their','They’re','There'],'q6'=>['-- Choose --','did attend','did attended','attended']];
//       // $q3->additionalData=["images"=>['Restaurant','Hotel','Store']];
//        $q5->questionType='dropdown';
//        $q5->correctAnswers=['q1'=>'Whose','q2'=>'yours','q3'=>'colour of the roses','q4'=>'mice','q5'=>'Their','q6'=>'attended'];
//        $q5->modul='Module5';
//        $q5->points=1;
//        $q5->final=1;
//        $q5->qid='novice5';
//       $questionData[]=$q5;
//        $q1=new \Classes\QuestionData();
//        $q1->question="Choose the correct word/phrase to fit the meaning of the sentence:";
//        $q1->hint="Dropdown menu";
//        $q1->multi=1;
//        $q1->answers=['q1','q2'];
//        $q1->sentences=['q1'=>'Ariana Grande #replace0# Instagram over the weekend #replace1# her fans following the last European concert date of her &quot;Dangerous Woman&quot; '
//            . 'tour, #replace2# wrapped in Turin, Italy on June 17 th.<br><br>','q2'=>'"#replace0# the close of this European leg of my Dangerous Woman tour, I
//just wanted to thank you properly for the overwhelming love and
//support you&#39;ve #replace1# me, my crew, and each other during this #replace2# time," she wrote.'];
//        $q1->options=['q1'=>[['-- Choose --','took to','took on','took off'],['-- Choose --','thanks','thank','to thank'],['-- Choose --','who','which','what']],'q2'=>[['-- Choose --','In','Out','At'],['-- Choose --','shown','showed','showing'],['-- Choose --','challenged','challenging','challengeable']]];
//       // $q3->additionalData=["images"=>['Restaurant','Hotel','Store']];
//        $q1->questionType='dropdown';
//        $q1->correctAnswers=['q1'=>'took to','q2'=>'to thank','q3'=>'which','q4'=>'At','q5'=>'shown','q6'=>'challenging'];
//        $q1->modul='Module1';
//        $q1->points=1;
//        $q1->final=0;
//        $q1->qid='apprentice1';
//        $questionData[]=$q1;
//                        $q2=new \Classes\QuestionData();
//        $q2->question="Type and complete the indirect speech sentences.";
//        $q2->hint="Type the answer";
//        $q2->answers=['q1','q2','q3'];
//        $q2->sentences=['q1'=>'<span>1. “What’s the time?”<br>- He wanted to know</span> #replace#.','q2'=>'<span>2. “Do you know the girl standing there?”<br>- She asked me</span> #replace# <span>the girl standing there.</span>','q3'=>'<span>3. “I was cooking for hours.”<br>- Sarah complained that she</span> #replace# <span>for hours.</span>'];
//       // $q1->options=['q1'=>['-- Choose --','Spain','Spanish'],'q2'=>['-- Choose --','children','childrens'],'q3'=>['-- Choose --','one','first']];
//       // $q3->additionalData=["images"=>['Restaurant','Hotel','Store']];
//        $q2->questionType='typein';
//        $q2->correctAnswers=['q1'=>'what the time was','q2'=>['if I knew','whether I knew'],'q3'=>'had been cooking'];
//        $q2->modul='Module2';
//        $q2->points=1;
//        $q2->final=0;
//        $q2->qid='apprentice2';
//        $questionData[]=$q2;
//             $q3=new \Classes\QuestionData();
//        $q3->question="Drag the correct option into the gap to fit the meaning.";
//        $q3->hint="Drag and drop";
//        $q3->answers=['q1','q2','q3','q4','q5','q6'];
//        $q3->sentences=['q1'=>'<span>The fourth grade students John, Paul and Simon were taking a test about</span> #replace# <span>Asia on Monday morning.</span>','q2'=>'<span>Meanwhile, some children were playing football in the school yard. And then suddenly, they heard a
//strange noise. They looked up and saw pieces of glass under the window. It</span> #replace# <span>broken</span>','q3'=>'<span>Fortunately,'
//            . 'no one was hurt and, although alarmed, they all went back to their tests.<br><br>It seemed like minutes later when they heard their teacher saying: “You have five minutes left. Check your
//answers before you finish.”<br><br>After the class, the boys joined their friends, Sabrina and Lisa. “We were talking about</span> #replace# <span>a party on
//Friday.”</span>','q4'=>'<span>Lisa said. “Would you like to come?” “Sure, we would love to”, John answered. Paul added: “I</span> #replace# <span>better do the test well, or I’ll be grounded.”</span> ','q5'=>'<span>“We’ll all be grounded unless we</span> #replace# <span>it”, they
//laughed.</span> ','q6'=>'On Thursday, they got the results. #replace# <span>students passed the exam.</span>'];
//        $q3->options=['q1'=>['a','an','the','--'],'q2'=>['has','has been','was','was being'],'q3'=>['to have','to have had','having','having had'],'q4'=>['had','would','should','could'],'q5'=>['pass','passed','don`t pass','didn`t pass'],'q6'=>['None','All','Neither of','None of']];
//       // $q3->additionalData=["images"=>['Restaurant','Hotel','Store']];
//        $q3->questionType='dragdrop';
//        $q3->correctAnswers=['q1'=>'--','q2'=>'was','q3'=>'having','q4'=>'had','q5'=>'pass','q6'=>'All'];
//        $q3->modul='Module3';
//        $q3->points=1;
//        $q3->final=0;
//        $q3->qid='apprentice3';
//        $questionData[]=$q3;
//        $q4=new \Classes\QuestionData();
//        $q4->question="Choose the correct combination of tenses.";
//       $q4->hint="Radio button, multiple choice";
//        $q4->answers=['q1','q2','q3'];
//       $q4->sentences=['q1'=>'<span>1. I</span> #replace# <span>you know as soon as I</span> #replace1# <span>the news.</span>','q2'=>'<span>2. I</span> #replace# <span>up because I</span> #replace1# <span>to go abroad in July.</span>','q3'=>'<span>3. The convict</span> #replace# <span>by hopping over the wall of the garden where he</span> #replace1#<span>.</span>'];
//       $q4->options=['q1'=>['will let … shall hear','will let … have heard','would let … hear','would let … have heard'],'q2'=>['have saved … have wanted','have been saving … have wanted','have been saving … want','am saving … have wanted'],'q3'=>['has escaped … has been working','escaped … was working','escaped … had been working','has escaped … worked']];
//      // $q3->additionalData=["images"=>['Restaurant','Hotel','Store']];
//       $q4->questionType='radioSentence';
//        $q4->correctAnswers=['q1'=>'will let … have heard','q2'=>'have been saving … want','q3'=>'escaped … had been working'];
//        $q4->modul='Module4';
//        $q4->points=1;
//        $q4->final=1;
//        $q4->qid='apprentice4';
//        $questionData[]=$q4;
        
//                $q1=new \Classes\QuestionData();
//        $q1->question="Fill in the gaps with the correct form of the given words.  There are <em>three</em> extra words.";
//        $q1->hint="Type in";
//       // $q1->multi=1;
//        $q1->answers=['q1','q2','q3','q4','q5','q6'];
//        $q1->margin=['q1'=>'no','q2'=>'yes','q3'=>'yes','q4'=>'yes','q5'=>'no','q6'=>'yes'];
//        $q1->sentences=['q1'=>'<span>When the final Harry Potter book</span> #replace# back in 2007, Potterheads
//everywhere were pretty bummed because their favourite magical story was ending forever. But they quickly learned they had ',
//'q2'=>'nothing to worry about because J.K. Rowling was planning
//to keep the series alive through spinoffs like <em>Cursed Child</em> and <em>Fantastic Beasts</em> and #replace# tons of secrets about the wizarding world on Pottermore and Twitter.<br><br>In one of Rowling&#39;s latest posts on Pottermore, the author decided to delve deep into Harry&#39;s
//family tree.',
//            'q3'=>'<span>Apparently, the Potter line started with an eccentric wizard by the name of Linfred of
//Stinchcombe credited with</span> #replace# of Skele-gro, the very potion that fixed
//Harry&#39;s arm in <em>Chamber of Secrets</em>.<br><br>The most fascinating tidbit from the Potter history: the Harry Potter we know and love today
//wasn&#39;t the first Harry Potter. Harry&#39;s great-grandfather was named Henry Potter, but to his
//close friends, he was Harry.', 'q4'=>'<span>Harry&#39;s great-grandfather rocked the boat just as much as Harry did in his time. According to
//Rowling&#39;s history of the Potter clan, he caused a stir when he publicly condemned the then
//Minister for Magic, Archer Evermonde, for</span> #replace# the magical community
//from helping Muggles during the First World War.<br><br>','q5'=>'<span>His outspokenness on behalf of Muggles was pretty much why the Potters</span> #replace# from the Sacred Twenty-Eight — the twenty-eight British families that were still
//&quot;truly pure-blood&quot; by the 1930s. ','q6'=>'So defending the #replace# runs in Harry&#39;s blood!'];
//        $q1->answerHint=['establish','introduce','debar','conceal','defend','deport','release','banish','divulge'];
//     //   $q1->options=['q1'=>[['-- Choose --','Took to','Took on','Took off'],['-- Choose --','thanks','thank','to thank'],['-- Choose --','who','which','what']],'q2'=>[['-- Choose --','In','Out','At'],['-- Choose --','shown','showed','showing'],['-- Choose --','challenged','challenging','challengeable']]];
//       // $q3->additionalData=["images"=>['Restaurant','Hotel','Store']];
//        $q1->questionType='typein2';
//        $q1->correctAnswers=['q1'=>'was released','q2'=>'divulging','q3'=>'introduction','q4'=>'debarring','q5'=>'were banished','q6'=>['defenceless','defenseless']];
//        $q1->modul='Module1';
//        $q1->points=1;
//        $q1->final=0;
//        $q1->qid='otherlevels1';
//        $questionData[]=$q1;
//                $q2=new \Classes\QuestionData();
//        $q2->question="Choose the correct option to complete the sentences";
//        $q2->hint="Dropdown menu";
//        $q2->answers=['q1','q2','q3','q4','q5','q6'];
//        $q2->sentences=['q1'=>'1. Under no circumstances #replace# before Monday morning.','q2'=>'2. The pills are #replace# with any other medicine.',
//            'q3'=>'3. My washer broke down. I called a handyman and #replace#.','q4'=>'4. Had I been more ambitious, I #replace# a doctor now.',
//            'q5'=>'5. If only I #replace# him yesterday.','q6'=>'6. He #replace# the concert in the National theatre, but unfortunately the grand
//Concert Hall was pre-booked till late autumn.'];
//        $q2->options=['q1'=>['-- Choose --','this document shouldn’t be opened','shouldn’t this document be opened','this document should be opened','should this document be opened'],
//            'q2'=>['-- Choose --','not to take','to not be taken','not to be taken','to not take'],'q3'=>['-- Choose --','got it repaired','repaired it','got repaired it','had repaired it'],
//            'q4'=>['-- Choose --','could have been','can be','would have been','would be'],'q5'=>['-- Choose --','didn’t miss','hadn’t missed','wouldn’t miss','wasn’t missing'],
//            'q6'=>['-- Choose --','liked to have','liked to have had','would have liked','would like to have had']];
//       // $q3->additionalData=["images"=>['Restaurant','Hotel','Store']];
//        $q2->questionType='dropdown';
//        $q2->correctAnswers=['q1'=>'should this document be opened','q2'=>'not to be taken','q3'=>'got it repaired','q4'=>'would be','q5'=>'hadn’t missed','q6'=>'would like to have had'];
//        $q2->modul='Module2';
//        $q2->points=1;
//        $q2->final=0;
//        $q2->qid='otherlevels2';
//        $questionData[]=$q2;
//                        $q3=new \Classes\QuestionData();
//        $q3->question="Fill in the gaps with the correct form of the verbs given in the brackets";
//        $q3->hint="Type in";
//     //   $q3->multi=1;
//        $q3->answers=['q1','q2','q3','q4','q5','q6'];
//        $q3->margin=['q1'=>'no','q2'=>'no','q3'=>'no','q4'=>'no','q5'=>'no','q6'=>'yes'];
//        $q3->sentences=['q1'=>'<span>During the all-encompassing strategic meeting with the East Coast board
//members earlier today, James</span> #replace# (INFORM) by their','q2'=>'New York
//based company CEO that Mills &amp; Co. #replace# (DECIDE) to become the part
//of affiliation with a company in the UK. An ','q3'=>'expected outcome, since the two
//companies #replace# (NEGOTIATE) for the past month. Of','q4'=> ' course, this means that James #replace# (MUST) catch the','q5'=>' first morning flight back
//to New York. He #replace# (MEET) with his boss at this time tomorrow, but all of the details','q6'=>' of the merger #replace# (OUTLINE) by then.'];
//        $q3->answerHint=null;
//     //   $q1->options=['q1'=>[['-- Choose --','Took to','Took on','Took off'],['-- Choose --','thanks','thank','to thank'],['-- Choose --','who','which','what']],'q2'=>[['-- Choose --','In','Out','At'],['-- Choose --','shown','showed','showing'],['-- Choose --','challenged','challenging','challengeable']]];
//       // $q3->additionalData=["images"=>['Restaurant','Hotel','Store']];
//        $q3->questionType='typein2';
//        $q3->correctAnswers=['q1'=>'was informed','q2'=>'had decided','q3'=>'had been negotiating','q4'=>'will have to','q5'=>'will be meeting','q6'=>'will have been outlined'];
//        $q3->modul='Module3';
//        $q3->points=1;
//        $q3->final=1;
//        $q3->qid='otherlevels3';
//        $questionData[]=$q3;
//                $q1=new \Classes\QuestionData();
//        $q1->question="Choose the correct word that fits the meaning:";
//        $q1->hint="Dropdown menu";
//        $q1->multi=1;
//        $q1->answers=['q1'];
//        $q1->sentences=['q1'=>'Paul’s family lived by the sea. He didn’t have #replace0# friends and he was #replace1#  only child in the family. So he used to stay at home #replace2#. He was
//always very #replace3# to fill his time. He was often taking books from the #replace4# of the family library. Paul was familiar with many titles and it was not
//always #replace5# books that he read. #replace6# he went for a ride with his
//uncle in the park. But he #replace7# be back home before 8 o’clock.'];
//        $q1->options=['q1'=>[['-- Choose --','many','much','little'],['-- Choose --','--','a','an'],['-- Choose --','lot','a lot','a lot of'],['-- Choose --','creative','creating','creation'],['-- Choose --','shelf’s','shelve’s','shelves'],['-- Choose --','childrens','children’s','childrens’s'],['-- Choose --','Occasion','Occasional','Occasionally'],['-- Choose --','has to','had to','must']]];
//       // $q3->additionalData=["images"=>['Restaurant','Hotel','Store']];
//        $q1->questionType='dropdown';
//        $q1->correctAnswers=['q1'=>['Quantifiers'=>'many'],'q2'=>['Articles'=>'an'],'q3'=>['Quantifiers'=>'a lot'],'q4'=>['Vocabulary'=>'creative'],'q5'=>['Plural of nouns'=>'shelves'],'q6'=>['Genitive (Saxon)'=>'children’s'],'q7'=>['Vocabulary'=>'Occasionally'],'q8'=>['Modal verbs'=>'had to']];
//        $q1->modul='Module1';
//        $q1->points=1;
//        $q1->final=0;
//        $q1->qid='skolarci1';
//        $questionData[]=$q1;
//        $q2=new \Classes\QuestionData();
//        $q2->question="Fill in the gaps with the correct form of the words given in brackets:";
//        $q2->hint="Type in";
//       // $q1->multi=1;
//        $q2->answers=['q1','q2'];
//        $q2->sentences=['q1'=>'<span>This is a part of an interview with the</span> #replace# (SING) Alicia Keys, Manhattan native who has
//sold 28 million albums and singles worldwide and won, among other awards, nine Grammys.
//Here is what she says about books she would recommend for teen book #replace# (ADMIRE)<br>',
//            'q2'=>'<span>“Willa Cather is an incredible</span> #replace# (WRITE). Her #replace# (DESCRIBE) are unreal.
//Just the way she describes colours is #replace# (AMAZE). Her book My Antonia is #replace# (BEAUTY). I like Alice Walker, too. You can get wrapped up in her stories and feel
//like you are part of them. Who else do I like, there are so many! I’ll also go for a good Steven
//King book, #replace# (SPECIAL) on the beach. I like a #replace# (VARY) of genres.'];
//        $q2->answerHint=null;
//     //   $q1->options=['q1'=>[['-- Choose --','Took to','Took on','Took off'],['-- Choose --','thanks','thank','to thank'],['-- Choose --','who','which','what']],'q2'=>[['-- Choose --','In','Out','At'],['-- Choose --','shown','showed','showing'],['-- Choose --','challenged','challenging','challengeable']]];
//       // $q3->additionalData=["images"=>['Restaurant','Hotel','Store']];
//        $q2->questionType='typein2';
//        $q2->correctAnswers=['q1'=>['Vocabulary'=>'singer'],'q2'=>['Vocabulary'=>'admirers'],'q3'=>['Vocabulary'=>'writer'],'q4'=>['Vocabulary'=>'descriptions'],'q5'=>['Vocabulary'=>'amazing'],'q6'=>['Vocabulary'=>'beautiful'],'q7'=>['Vocabulary'=>'especially'],'q8'=>['Vocabulary'=>'variety']];
//        $q2->modul='Module2';
//        $q2->points=1;
//        $q2->final=0;
//        $q2->qid='skolarci2';
//        $questionData[]=$q2;
//        $q3=new \Classes\QuestionData();
//        $q3->question="Choose the correct word or phrase:";
//        $q3->hint="Radio button, multiple choice";
//        //$q3->multi=1;
//        $q3->answers=['q1','q2','q3','q4','q5','q6','q7','q8'];
//        $q3->sentences=['q1'=>'<span>1. Who</span> #replace# <span>Lara yesterday?</span>','q2'=>'<span>2. “Don’t be late, please.” They asked him</span> #replace# <span>late.</span>',
//            'q3'=>'<span>3. Riley</span> #replace# <span>at the football match now.</span>','q4'=>'<span>4. I really can’t go any</span> #replace# <span>.</span>',
//            'q5'=>'<span>5. This is</span> #replace# <span>doll. That one is</span>#replace#<span>.</span>','q6'=>'<span>6. I won’t</span> #replace# <span>to come to your party, I’m really sorry.</span>','q7'=>'<span>7.</span> #replace# <span>are new.</span>','q8'=>'<span>8. I didn’t meet</span> #replace# <span>who knows you well.</span>'];
//    //    $q3->options=['q1'=>[['-- Choose --','did visit','has visited','visited','should visit']],'q2'=>[['-- Choose --','to not be','not to be','that not be','that be not']],'q3'=>[['-- Choose --','couldn’t possibly be','mustn’t possibly be','shouldn’t possibly be','isn’t possibly']],'q4'=>[['-- Choose --','farer','farest','farther','farthest']],'q5'=>[['-- Choose --','our','ours'],['-- Choose --','her','hers']],'q6'=>[['-- Choose --','be able','have','can','need']],'q7'=>[['-- Choose --','This old table’s legs','The legs of this old table','This old table legs','The legs of this old table’s']],'q8'=>[['-- Choose --','nobody','no one','anyone','somebody']]];
//      $q3->options=['q1'=>['did visit','has visited','visited','should visit'],'q2'=>['to not be','not to be','that not be','that be not'],'q3'=>['couldn’t possibly be','mustn’t possibly be','shouldn’t possibly be','isn’t possibly'],'q4'=>['farer','farest','farther','farthest'],'q5'=>['our…her','ours…her','ours…hers','our…hers'],'q6'=>['be able','have','can','need'],'q7'=>['This old table’s legs','The legs of this old table','This old table legs','The legs of this old table’s'],'q8'=>['nobody','no one','anyone','somebody']];
//        // $q3->additionalData=["images"=>['Restaurant','Hotel','Store']];
//        $q3->questionType='radioSentence';
//        $q3->correctAnswers=['q1'=>['Subject and object questions'=>'visited'],'q2'=>['Reported speech'=>'not to be'],'q3'=>['Modal verbs'=>'couldn’t possibly be'],'q4'=>['Comparison of adjectives/adverbs'=>'farther'],'q5'=>['Possessive adjectives/pronouns'=>'our…hers'],'q6'=>['Modal verbs'=>'be able'],'q7'=>['Genitive (Norman)'=>'The legs of this old table'],'q8'=>['Indefinite pronouns'=>'anyone']];
//        $q3->modul='Module3';
//        $q3->points=1;
//        $q3->final=0;
//        $q3->qid='skolarci3';
//        $questionData[]=$q3;
//                $q4=new \Classes\QuestionData();
//        $q4->question="Complete the text with the correct form/tense of the verbs:";
//        $q4->hint="Type in";
//       // $q1->multi=1;
//        $q4->answers=['q1','q2'];
//        $q4->sentences=['q1'=>'<span>Dear Connie,<br><br>
//Thanks for your letter. I’m happy</span> #replace# (HEAR) that you #replace# (HAVE) a
//good time there, in Kent. Please make a lot of pictures, to show me.<br><br>I #replace# (COME/JUST) back from a wonderful trip in Mexico. My friend,
//Maria #replace# (INVITE) me to stay with her family in Quintana Roo. Do you remember
//her? She&#39;s the girl whose postcard I #replace# (GET) last summer while you #replace# (STAY) with us here in Scotland, too. They have a house on a hill just outside Cancun. There
//was a beautiful view of the town from my bedroom window. Maria&#39;s family were incredibly
//kind and hospitable. We ate out a lot, Mexican food is amazing! During the entire holiday,
//they #replace# (FIND/ALWAYS) new ideas where to take
//me so we #replace# (TAKE) many trips to see legendary Mayan temples and pyramids, like
//ancient ruins in Tulum. The tall cliffs and foamy Caribbean Sea waves were quite spectacular.<br><br>',
//            'q2'=>'<span>I have a lot of pictures, I</span> #replace# (SEND) you some when I develop them. Or you can
//see them when you finally #replace# (COME) here again, next summer. I have so many things
//planned for that holiday, I can’t wait! We #replace# (HAVE) so much fun, I
//am sure!<br><br>
//Love,<br>
//Beth.'];
//        $q4->answerHint=null;
//     //   $q1->options=['q1'=>[['-- Choose --','Took to','Took on','Took off'],['-- Choose --','thanks','thank','to thank'],['-- Choose --','who','which','what']],'q2'=>[['-- Choose --','In','Out','At'],['-- Choose --','shown','showed','showing'],['-- Choose --','challenged','challenging','challengeable']]];
//       // $q3->additionalData=["images"=>['Restaurant','Hotel','Store']];
//        $q4->questionType='typein2';
//        $q4->correctAnswers=['q1'=>['Tenses'=>'to hear'],'q2'=>['Tenses'=>['`re having','are having']],'q3'=>['Tenses'=>['`ve just come','have just come']],'q4'=>['Tenses'=>'invited'],'q5'=>['Tenses'=>'got'],'q6'=>['Tenses'=>'were staying'],'q7'=>['Tenses'=>['were always finding','would always find']],'q8'=>['Tenses'=>'took'],'q9'=>['Tenses'=>['`ll send','will send']],'q10'=>['Tenses'=>'come'],'q11'=>['Tenses'=>['`re going to have','are going to have']]];
//        $q4->modul='Module4';
//        $q4->points=1;
//        $q4->final=0;
//        $q4->qid='skolarci4';
//        $questionData[]=$q4;
//        $q5=new \Classes\QuestionData();
//        $q5->question="Choose a preposition from the box to fill the gaps. Not all prepositions will be used.";
//        $q5->hint="Drag and drop";
//        $q5->answers=['q1'];
//        $q5->sentences=['q1'=>'<span>Today was definitely not my lucky day! First, when I got</span> #replace# <span>the bus, I bumped my head.
//When I got</span> #replace# <span>work, my boss yelled at me. And</span> #replace# <span>a phone call, I spilled coffee on some
//important papers.</span> #replace#<span> that, I accidentally deleted some important files on my computer. My
//bad luck continued</span> #replace# <span>I went home. I hope tomorrow is a better day.</span>'];
//        $q5->options=['q1'=>['after','while','beyond','during','in','onto','to','until']];
//       // $q3->additionalData=["images"=>['Restaurant','Hotel','Store']];
//        $q5->questionType='dragdrop';
//        $q5->correctAnswers=['q1'=>['Prepositions'=>'onto'],'q2'=>['Prepositions'=>'to'],'q3'=>['Prepositions'=>'during'],'q4'=>['Prepositions'=>'after'],'q5'=>['Prepositions'=>'until']];
//        $q5->modul='Module5';
//        $q5->points=1;
//        $q5->final=0;
//        $q5->qid='skolarci5';
//        $questionData[]=$q5;
//        $q6=new \Classes\QuestionData();
//        $q6->question="READING COMPREHENSION TEST";
//        $q6->hint="Radio button, multiple choice";
//        //$q3->multi=1;
//     
//        $q6->story=true;
//        $q6->answers=['q1','q2','q3','q4','q5','q6'];
//        $q6->sentences=['q1'=>'<span>1. What time does Lana have breakfast?</span>','q2'=>'<span>2. How does she get to school?</span>',
//            'q3'=>'<span>3. Another way of saying ‘continue’ is:</span>','q4'=>'<span>4. What does she have for lunch?</span>',
//            'q5'=>'<span>5. How long is their lunch break?</span>','q6'=>'<span>6. Another way of saying ‘I arrive home’ is:</span>'];
//    //    $q3->options=['q1'=>[['-- Choose --','did visit','has visited','visited','should visit']],'q2'=>[['-- Choose --','to not be','not to be','that not be','that be not']],'q3'=>[['-- Choose --','couldn’t possibly be','mustn’t possibly be','shouldn’t possibly be','isn’t possibly']],'q4'=>[['-- Choose --','farer','farest','farther','farthest']],'q5'=>[['-- Choose --','our','ours'],['-- Choose --','her','hers']],'q6'=>[['-- Choose --','be able','have','can','need']],'q7'=>[['-- Choose --','This old table’s legs','The legs of this old table','This old table legs','The legs of this old table’s']],'q8'=>[['-- Choose --','nobody','no one','anyone','somebody']]];
//      $q6->options=['q1'=>['at a quarter to eight','in quarter past eight','in fifteen to eight','on eight fifteen'],'q2'=>['on bus','with a bus','with bus','by bus'],'q3'=>['go on','go past','go in','go you'],'q4'=>['scrambled eggs and pancakes','cereal, scrambled eggs, pancakes or sandwiches','fish and vegetables','vegetables and meatballs'],'q5'=>['half hour','an hour half','half past','half an hour'],'q6'=>['I go the home','I go to the home','I get home','I get to home']];
//        // $q3->additionalData=["images"=>['Restaurant','Hotel','Store']];
//        $q6->questionType='radioSentence';
//        $q6->correctAnswers=['q1'=>['Text understanding'=>'at a quarter to eight'],'q2'=>['Text understanding'=>'by bus'],'q3'=>['Text understanding'=>'go on'],'q4'=>['Text understanding'=>'fish and vegetables'],'q5'=>['Text understanding'=>'half an hour'],'q6'=>['Text understanding'=>'I get home']];
//        $q6->modul='Module6';
//        $q6->points=1;
//        $q6->final=0;
//        $q6->qid='skolarci6';
//        $questionData[]=$q6;
//                $q7=new \Classes\QuestionData();
//        $q7->question="Choose the correct word or phrase that fits the sentence best:";
//        $q7->hint="Radio button, multiple choice";
////        //$q3->multi=1;
//        $q7->answers=['q1','q2','q3','q4','q5','q6'];
//        $q7->sentences=['q1'=>'<span>1. Our teacher Mrs Brown is</span> #replace# <span>to her pupils.</span>','q2'=>'<span>2. You can come at 6.30. They will</span> #replace# <span>you.</span>',
//            'q3'=>'<span>3. It started raining</span> #replace#<span>.</span>','q4'=>'<span>4. The plane should</span> #replace# <span>at 8.00 p.m.</span>',
//            'q5'=>'<span>5. Those</span> #replace# <span>are going to check their</span>#replace#<span>.</span>','q6'=>'<span>6. A: How</span> #replace# <span>do you go swimming?</span><br><span>B: </span>#replace# <span>a week.</span>'];
//    //    $q3->options=['q1'=>[['-- Choose --','did visit','has visited','visited','should visit']],'q2'=>[['-- Choose --','to not be','not to be','that not be','that be not']],'q3'=>[['-- Choose --','couldn’t possibly be','mustn’t possibly be','shouldn’t possibly be','isn’t possibly']],'q4'=>[['-- Choose --','farer','farest','farther','farthest']],'q5'=>[['-- Choose --','our','ours'],['-- Choose --','her','hers']],'q6'=>[['-- Choose --','be able','have','can','need']],'q7'=>[['-- Choose --','This old table’s legs','The legs of this old table','This old table legs','The legs of this old table’s']],'q8'=>[['-- Choose --','nobody','no one','anyone','somebody']]];
//      $q7->options=['q1'=>['patient','horrible','silent','quiet'],'q2'=>['accept','except','expat','expect'],'q3'=>['an hour ago','last hour','ago one hour','a hour before'],'q4'=>['go off','go away','take off','take away'],'q5'=>['woman…tooths','women…tooths','womans…teeth','women…teeth'],'q6'=>['much / often','often / twice','many / so','often / five time']];
//        // $q3->additionalData=["images"=>['Restaurant','Hotel','Store']];
//        $q7->questionType='radioSentence';
//        $q7->correctAnswers=['q1'=>['Vocabulary'=>'horrible'],'q2'=>['Vocabulary'=>'expect'],'q3'=>['Vocabulary'=>'an hour ago'],'q4'=>['Vocabulary'=>'take off'],'q5'=>['Plural of nouns'=>'women…teeth'],'q6'=>['Vocabulary'=>'often / twice']];
//        $q7->modul='Module7';
//        $q7->points=1;
//        $q7->final=0;
//        $q7->qid='skolarci7';
//        $questionData[]=$q7;
//        
//        $q8=new \Classes\QuestionData();
//        $q8->question="Complete the second sentence so that it has a similar meaning to the first sentence, using
//the words given in brackets. Do not change the given words. Use between 3 and 7 words.";
//        $q8->hint="Type the answer";
//        $q8->story=true;
//        $q8->answers=['q1','q2','q3','q4','q5','q6','q7','q8'];
//        $q8->sentences=['q1'=>'<span>1. Hope found the History exam surprisingly easy. (MUCH) <br>The history exam</span> #replace# <span>than Hope expected.</span>','q2'=>'<span>2. It’s more than a week since she last saw her grandparents. (NOT)
//<br>She</span> #replace# <span>more than a week.</span>','q3'=>'<span>3. I lent you that magazine last month and now I’d like to have it back. (FROM)
//Can I have back that magazine</span> #replace# <span>last month?</span>',"q4"=>"<span>4. The last time I spoke to him was when he got married. (SINCE)
//<br>I</span> #replace# <span>he got married.</span>","q5"=>"<span>5. Does Angie’s sister go in for any sport? (WONDER) <br>I</span> #replace# <span>in for any sport.</span>","q6"=>"<span>6. Children, whose jacket is this? (WHO)
//<br>Children,</span> #replace# <span>to?</span>","q7"=>"<span>7. When was this cathedral built, Mr Miller? (OLD)
//<br>Mr Miller, do you know</span> #replace# <span>?</span>","q8"=>"<span>8. You can’t take my car! (BORROW)
//I won’t</span> #replace# <span>my car.</span>"];
//       // $q1->options=['q1'=>['-- Choose --','Spain','Spanish'],'q2'=>['-- Choose --','children','childrens'],'q3'=>['-- Choose --','one','first']];
//       // $q3->additionalData=["images"=>['Restaurant','Hotel','Store']];
//        $q8->questionType='typein';
//        $q8->correctAnswers=['q1'=>['Comparison of adjectives/adverbs'=>'was much easier'],'q2'=>['Tenses'=>['hasn`t seen her granparents for','has not seen her grandparents for']],'q3'=>['Relative clauses'=>['which you borrowed from me','that you borrowed from me']],'q4'=>['Tenses'=>['haven`t spoken to him since','have not spoken to him since']],'q5'=>['Reported speech'=>'wonder if Angie`s sister goes'],'q6'=>['Subject and object questions'=>'who does this jacket belong'],'q7'=>['Reported speech'=>'how old this cathedral is'],'q8'=>['Vocabulary'=>'let you borrow']];
//        $q8->modul='Module8';
//        $q8->points=1;
//        $q8->final=1;
//        $q8->qid='skolarci8';
//        $questionData[]=$q8;
//         $q1=new \Classes\QuestionData();
//        $q1->question="Match the beginnings and ends of the sentences:";
//        $q1->hint="Laura and Eddie’s wedding gone wrong";
//        $q1->answers=['q1','q2','q3','q4','q5','q6'];
//        $q1->options=['q1'=>'but the limo got stuck in a traffic jam.','q2'=>'but he got a strong migraine on the way to the church','q3'=>'but they had terrible hay fever.','q4'=>'but there was a thunderstorm.','q5'=>'but he decided to come.','q6'=>'but there was a strike at the airport.'];
//        $q1->additionalData=["images"=>['q1','q2','q3','q4','q5','q6'],"answerKey"=>["q1"=>'44499dd7-8457-11e7-a60b-08f9c906bea8',"q2"=>'8ab6c399-8457-11e7-a60b-08f9c906bea8',"q3"=>'b3e2b501-8457-11e7-a60b-08f9c906bea8',"q4"=>'b3e2b501-8457-11e7-a60b-08f9c906bea65',"q5"=>'b3e2b501-8457-11e7-a60b-08f9c906beas3',"q6"=>'b3e2b501-8457-11e7-a60b-08f9c906bea78']];
//        $q1->questionType='connect2';
//        $q1->modul='Module1';
//        $q1->points=0.5;
//        $q1->final=0;
//        $q1->qid='tinejdzeri1';
//        $questionData[]=$q1;
//                        $q2=new \Classes\QuestionData();
//        $q2->question="Choose the correct word or phrase:";
//        $q2->hint="Dropdown menu";
//        $q2->multi=1;
//        $q2->answers=['q1'];
//        $q2->sentences=['q1'=>'As technology continues to #replace0# our lives, the struggle to maintain privacy becomes ever
//harder. But while we may take special steps to update our privacy settings on Facebook or to
//limit what apps have our info, it #replace1#  that just #replace2#. your phone on you at all times has some
//creepy consequences.<br><br>Every place you&#39;ve ever visited #replace3# stored on your iPhone, #replace4# the address and the
//number of times you&#39;ve been to that location. Feeling a bit weird about that?<br><br>Well, the reason is a feature</span> #replace5# deep in your privacy settings called &#39;Frequent Locations,&#39;
//and #replace6# it&#39;s in no way new, it often goes unnoticed. For years, the system has been #replace7# the
//places you visit on a map and logging your arrival and #replace8# times from each location, so your
//iPhone can help #replace9# the Maps app and serve you best.'];
//        $q2->options=['q1'=>[['-- Choose --','take on','take in','take over','take up'],['-- Choose --','turns in','turns out','turns over','turns down'],['-- Choose --','having','had','having had','to have'],['-- Choose --','was','had been','is being','was being'],['-- Choose --','include','included','including','includes'],['-- Choose --','hides','hidden','hiding','hid'],['-- Choose --','while','when','which','what'],['-- Choose --','pinpoint','pinpointed','pinpointing','pinpointable'],['-- Choose --','depart','departing','department','departure'],['-- Choose --','improve','improved','improven','improvement']]];
//       // $q3->additionalData=["images"=>['Restaurant','Hotel','Store']];
//        $q2->questionType='dropdown';
//        $q2->correctAnswers=['q1'=>['Vocabulary'=>'take over'],'q2'=>['Vocabulary'=>'turns out'],'q3'=>['Gerund'=>'having'],'q4'=>['Passive voice'=>'is being'],'q5'=>['Participles'=>'including'],'q6'=>['Participles'=>'hidden'],'q7'=>['Conjunctions'=>'while'],'q8'=>['Concord of tenses'=>'pinpointing'],'q9'=>['Vocabulary'=>'departure'],'q10'=>['Bare infinitive'=>'improve']];
//        $q2->modul='Module2';
//        $q2->points=1;
//        $q2->final=0;
//        $q2->qid='tinejdzeri2';
//        $questionData[]=$q2;
//        
//        $q3=new \Classes\QuestionData();
//        $q3->question="Complete the text with the correct forms of the words given below:";
//        $q3->hint="Type in";
//       // $q1->multi=1;
//        $q3->answers=['q1'];
//        $q3->sentences=['q1'=>'<span>As you can imagine, my first day in my first full-time job ever was</span> #replace# less than #replace#. Despite all the #replace# mental work I had done, I arrived at the office so
//nervous I was almost #replace#. For the first few hours I remained totally at sea. I had half a
//mind to run out there and then. By lunchtime I was absolutely shaking with nerves, but I
//refused to throw in the towel. I had studied hard for this career; I just couldn&#39;t bring myself to
//give up quite yet, however #replace# my prospects of survival appeared. Three days later
//things were still at a low ebb but I refused point blank to admit defeat. It was clear my new
//colleagues were making #replace# for me and giving me the benefit of the doubt. They
//probably thought I was on my last legs anyway. The second week passed quite #replace# and believe it or not, at the end of it I was actually beginning to relax. Looking back after
//seventeen years, I&#39;m glad I stuck it out and didn&#39;t buckle under. I&#39;m pleased and proud to hold
//the position of Director General in this quite #replace# multinational company, currently
//the third biggest in the world.'];
//        $q3->answerHint=['prepare','allow','prosper','idyll','event','like','speak','some'];
//     //   $q1->options=['q1'=>[['-- Choose --','Took to','Took on','Took off'],['-- Choose --','thanks','thank','to thank'],['-- Choose --','who','which','what']],'q2'=>[['-- Choose --','In','Out','At'],['-- Choose --','shown','showed','showing'],['-- Choose --','challenged','challenging','challengeable']]];
//       // $q3->additionalData=["images"=>['Restaurant','Hotel','Store']];
//        $q3->questionType='typein2';
//        $q3->correctAnswers=['q1'=>['Vocabulary'=>'somewhat'],'q2'=>['Vocabulary'=>'idyllic'],'q3'=>['Vocabulary'=>'preparatory'],'q4'=>['Vocabulary'=>'speechless'],'q5'=>['Vocabulary'=>'unlikely'],'q6'=>['Vocabulary'=>'allowances'],'q7'=>['Vocabulary'=>'uneventfully'],'q8'=>['Vocabulary'=>'prosperous']];
//        $q3->modul='Module3';
//        $q3->points=1;
//        $q3->final=0;
//        $q3->qid='tinejdzeri3';
//        $questionData[]=$q3;
//                $q4=new \Classes\QuestionData();
//        $q4->question="Click on the correct option that fits the sentence best:";
//        $q4->hint="Radio button, multiple choice";
//        //$q3->multi=1;
//        $q4->answers=['q1','q2','q3','q4','q5','q6','q7'];
//        $q4->sentences=['q1'=>'<span>1.</span> #replace# <span>Smiths were </span>#replace#<span> huge stars.</span>','q2'=>'<span>2. What</span> #replace# <span>do this evening?<br>- Maybe we</span> #replace# <span>watch TV.</span>',
//            'q3'=>'<span>3. Unless you</span> #replace# <span>now, you’ll miss the next bus.</span>','q4'=>'<span>4. Joan is my first neighbour, she lives</span> #replace# <span>to me.</span>',
//            'q5'=>'<span>5. I think I’ve got</span> #replace# <span>money left in my wallet.</span>','q6'=>'<span>6. Will you phone me</span> #replace# <span>you get there?</span>','q7'=>'<span>7. The secretary showed me the cabinet</span> #replace# <span>important documents are filed.</span>'];
//    //    $q3->options=['q1'=>[['-- Choose --','did visit','has visited','visited','should visit']],'q2'=>[['-- Choose --','to not be','not to be','that not be','that be not']],'q3'=>[['-- Choose --','couldn’t possibly be','mustn’t possibly be','shouldn’t possibly be','isn’t possibly']],'q4'=>[['-- Choose --','farer','farest','farther','farthest']],'q5'=>[['-- Choose --','our','ours'],['-- Choose --','her','hers']],'q6'=>[['-- Choose --','be able','have','can','need']],'q7'=>[['-- Choose --','This old table’s legs','The legs of this old table','This old table legs','The legs of this old table’s']],'q8'=>[['-- Choose --','nobody','no one','anyone','somebody']]];
//      $q4->options=['q1'=>['the...the','the.../','/...the','/.../'],'q2'=>['will you...will','are you going to...are going to','will you...are going to','are you going to...will'],'q3'=>['leave','left','don’t leave','didn’t leave'],'q4'=>['near','nearer','the nearest','next'],'q5'=>['any','many','hardly any','hardly anything'],'q6'=>['by the time','by','while','as soon as'],'q7'=>['which','that','whose','where'],'q8'=>['nobody','no one','anyone','somebody']];
//        // $q3->additionalData=["images"=>['Restaurant','Hotel','Store']];
//        $q4->questionType='radioSentence';
//        $q4->correctAnswers=['q1'=>['Articles'=>'the.../'],'q2'=>['Expressing future'=>'are you going to...will'],'q3'=>['Conditional sentences'=>'leave'],'q4'=>['Comparison of adjectives/adverbs'=>'next'],'q5'=>['Quantifiers'=>'hardly any'],'q6'=>['Conjunctions'=>'as soon as'],'q7'=>['Relative pronouns'=>'where']];
//        $q4->modul='Module4';
//        $q4->points=1;
//        $q4->final=0;
//        $q4->qid='tinejdzeri4';
//        $questionData[]=$q4;
//                    $q5=new \Classes\QuestionData();
//        $q5->question="READING COMPREHENSION TEST";
//        $q5->hint="Radio button, multiple choice";
//        //$q3->multi=1;
//     
//        $q5->story=true;
//        $q5->answers=['q1','q2','q3','q4','q5','q6','q7','q8'];
//        $q5->sentences=['q1'=>'<span>1. We can identify a plant</span>','q2'=>'<span>2. The leaves</span>',
//            'q3'=>'<span>3. The leaves are green because of</span>','q4'=>'<span>4. It is in the flower</span>',
//            'q5'=>'<span>5. A flower attracts insects with</span>','q6'=>'<span>6. Bees and other insects</span>','q7'=>'<span>7. The seeds</span>','q8'=>'<span>8. The seed must</span>'];
//    //    $q3->options=['q1'=>[['-- Choose --','did visit','has visited','visited','should visit']],'q2'=>[['-- Choose --','to not be','not to be','that not be','that be not']],'q3'=>[['-- Choose --','couldn’t possibly be','mustn’t possibly be','shouldn’t possibly be','isn’t possibly']],'q4'=>[['-- Choose --','farer','farest','farther','farthest']],'q5'=>[['-- Choose --','our','ours'],['-- Choose --','her','hers']],'q6'=>[['-- Choose --','be able','have','can','need']],'q7'=>[['-- Choose --','This old table’s legs','The legs of this old table','This old table legs','The legs of this old table’s']],'q8'=>[['-- Choose --','nobody','no one','anyone','somebody']]];
//      $q5->options=['q1'=>['by its green colour','only by its leaves','by its leaves or flowers','by the shape of its flower'],'q2'=>['store food for the flowers','can make food for the plant','are always green','produce chlorophyll'],'q3'=>['the nourishment','the action of sunlight','air and moisture','a chemical substance in plants'],'q4'=>['that the excess moisture is stored','that the bees make their home','that the fruit and seed are produced','that the food is produced'],'q5'=>['its beautiful shape','its green colour','its pollen','its smell and colour'],'q6'=>['take the nectar in the hairs of their body','pick up the pollen in the hairs of their body','pick up the pollen from the pistil','take the seeds from a flower'],'q7'=>['are formed in the ovary','are formed in the pistil','are covered with a fine dust called pollen','are picked up by insects'],'q8'=>['be planted at night, in darkness','have moisture, warmth, air and darkness to grow','grow quickly to survive','grow in its hard shell']];
//        // $q3->additionalData=["images"=>['Restaurant','Hotel','Store']];
//        $q5->questionType='radioSentence';
//        $q5->correctAnswers=['q1'=>['Text understanding'=>'by its leaves or flowers'],'q2'=>['Text understanding'=>'can make food for the plant'],'q3'=>['Text understanding'=>'a chemical substance in plants'],'q4'=>['Text understanding'=>'that the fruit and seed are produced'],'q5'=>['Text understanding'=>'its smell and colour'],'q6'=>['Text understanding'=>'pick up the pollen in the hairs of their body'],'q7'=>['Text understanding'=>'are formed in the ovary'],'q8'=>['Text understanding'=>'have moisture, warmth, air and darkness to grow']];
//        $q5->modul='Module5';
//        $q5->points=1;
//        $q5->final=0;
//        $q5->qid='tinejdzeri5';
//        $questionData[]=$q5;
//                $q6=new \Classes\QuestionData();
//        $q6->question="Complete the text with the correct form/tense of the verbs:";
//        $q6->hint="Type in";
//       // $q1->multi=1;
//        $q6->answers=['q1','q2','q3','q4','q5','q6','q7','q8','q9','q10'];
//        $q6->sentences=['q1'=>'<span>Earthquakes are one of the most frightening and destructive happenings of nature that man</span> #replace# (EXPERIENCE). ','q2'=>'Over the course of time earthquakes #replace# (CAUSE) ','q3'=>'the death of many human beings, a lot of suffering and great damage to
//property, before there was any way possible to even measure their devastating power.<br><br>Since then, the study of earthquakes #replace# (GROW) ','q4'=>'greatly and steadily as
//scientists all over the world investigate what causes them. They hope their studies #replace# (IMPROVE) ','q5'=>'ways of predicting earthquakes and reduce their destructive effects.
//The scientific study of earthquakes is new. Until the 18 th century few factual descriptions of
//earthquakes #replace# (RECORD).','q6'=>' In general, people could not understand the cause of
//the phenomena, and saw them as the punishment from God. They were only certain that they #replace# (CAN) ','q7'=>'to avoid it when it inevitably came again.<br><br>
//    <span>In 1755 a serious earthquake</span> #replace# (OCCUR) ','q8'=>'near Lisbon, Portugal. Shocks from the
//quake #replace# (FEEL) ','q9'=>'in many parts of the world. After the quake, Portuguese priests were
//asked to observe the effects and to make written records. Since that time, detailed records #replace# (KEEP)','q10'=>' of almost every major earthquake.<br><br>Currently scientists #replace# (MAKE) studies to enable them to predict earthquakes.
//Although they have made considerable progress, the ability to predict the time, place and size of
//earthquakes is very limited.'];
//        $q6->answerHint=null;
//     //   $q1->options=['q1'=>[['-- Choose --','Took to','Took on','Took off'],['-- Choose --','thanks','thank','to thank'],['-- Choose --','who','which','what']],'q2'=>[['-- Choose --','In','Out','At'],['-- Choose --','shown','showed','showing'],['-- Choose --','challenged','challenging','challengeable']]];
//       // $q3->additionalData=["images"=>['Restaurant','Hotel','Store']];
//        $q6->questionType='typein2';
//        $q6->correctAnswers=['q1'=>['Concord of tenses'=>['can experience','has experienced']],'q2'=>['Concord of tenses'=>'had been causing'],'q3'=>['Concord of tenses'=>'has been growing'],'q4'=>['Concord of tenses,Expressing future'=>'will improve'],'q5'=>['Concord of tenses,Passive voice'=>'were recorded'],'q6'=>['Concord of tenses,Expressing future'=>['weren`t going to be able','were not going to be able']],'q7'=>['Concord of tenses'=>'occured'],'q8'=>['Concord of tenses,Passive voice'=>'were felt'],'q9'=>['Concord of tenses,Passive voice'=>'have been kept'],'q10'=>['Concord of tenses'=>'are making']];
//        $q6->modul='Module6';
//        $q6->points=1;
//        $q6->final=0;
//        $q6->qid='tinejdzeri6';
//        $questionData[]=$q6;
//        $q7=new \Classes\QuestionData();
//        $q7->question="Click on the correct word/phrase that fits the sentence best:";
//        $q7->hint="Radio button, multiple choice";
//        //$q3->multi=1;
//        $q7->answers=['q1','q2','q3','q4','q5','q6'];
//        $q7->sentences=['q1'=>'<span>1. Sarah and Peter moved to Paris but</span> #replace# <span>them speaks French.</span>','q2'=>'<span>2. This is</span> #replace# <span>adventure so far.</span>',
//            'q3'=>'<span>3.</span> #replace# <span>and</span> #replace# <span>are nothing alike.</span>','q4'=>'<span>4. Four</span> #replace# <span>work in this ambulance.</span>',
//            'q5'=>'<span>5. Cheating is not just</span> #replace# <span>but it also highly #replace#.</span>','q6'=>'<span>6. The effects of the medicine should</span> #replace# <span>in few hours.</span>'];
//    //    $q3->options=['q1'=>[['-- Choose --','did visit','has visited','visited','should visit']],'q2'=>[['-- Choose --','to not be','not to be','that not be','that be not']],'q3'=>[['-- Choose --','couldn’t possibly be','mustn’t possibly be','shouldn’t possibly be','isn’t possibly']],'q4'=>[['-- Choose --','farer','farest','farther','farthest']],'q5'=>[['-- Choose --','our','ours'],['-- Choose --','her','hers']],'q6'=>[['-- Choose --','be able','have','can','need']],'q7'=>[['-- Choose --','This old table’s legs','The legs of this old table','This old table legs','The legs of this old table’s']],'q8'=>[['-- Choose --','nobody','no one','anyone','somebody']]];
//      $q7->options=['q1'=>['neither','neither of','both','both of'],'q2'=>['more dangerous','such dangerous','so dangerous','the most dangerous'],'q3'=>['Oxen...deers','Oxen...deer','Ox...deers','Oxens...deers'],'q4'=>['woman doctors','women doctor','womans doctors','women doctors'],'q5'=>['unregular...unhonest','nonregular...inhonest','irregular...dishonest','inregular...nonhonest'],'q6'=>['turn off','put off','keep off','wear off']];
//        // $q3->additionalData=["images"=>['Restaurant','Hotel','Store']];
//        $q7->questionType='radioSentence';
//        $q7->correctAnswers=['q1'=>['Indefinite pronouns'=>'neither of'],'q2'=>['Comparison of adjectives/adverbs'=>'the most dangerous'],'q3'=>['Plural of nouns'=>'Oxen...deer'],'q4'=>['Plural of nouns'=>'women doctors'],'q5'=>['Vocabulary'=>'irregular...dishonest'],'q6'=>['Vocabulary'=>'wear off']];
//        $q7->modul='Module7';
//        $q7->points=1;
//        $q7->final=0;
//        $q7->qid='tinejdzeri7';
//        $questionData[]=$q7;
//            $q8=new \Classes\QuestionData();
//        $q8->question="Complete the second sentence so that it has a similar meaning to the first sentence, using
//the words given in brackets. Do not change the given words. Use between 3 and 7 words.";
//        $q8->hint="Type the answer";
//        $q8->story=true;
//        $q8->answers=['q1','q2','q3','q4','q5','q6','q7','q8'];
//        $q8->sentences=['q1'=>'<span>1. They don’t have a dog because they live in a small flat. (IF) <br>They would have</span> #replace# <span>in a small flat.</span>','q2'=>'<span>2. Although she was very beautiful, she didn’t win the contest. (GREAT)
//Despite</span> #replace# <span>, she didn’t win the contest.</span>','q3'=>'<span>3. I expect you are pleased to get home after such a long business trip. (MUST)
//<br>You</span> #replace# <span>get home after such a long business trip.</span>',"q4"=>"<span>4. Children should learn proper behaviour from both their parents and teachers. (BE)
//<br>Children should</span> #replace# <span>behaviour by both their parents and teachers.</span>","q5"=>"<span>5. “Dale must visit us”, Janet said. (ON) <br>Janet insisted</span> #replace#","q6"=>"<span>6. “Does John knock when he enters the manager’s office?” (WONDERED)
//<br>She</span> #replace# <span>the manager’s office.</span>","q7"=>"<span>7. It may make him unpopular, but Nicholas never abandons his principles. (STICKS)
//<br>However</span> #replace# <span>to his principles.</span>","q8"=>"<span>8. The President as well as the Congress don’t feel favourably of the new law. (ARE)
//</span> #replace# <span>in favour of the new law.</span>"];
//       // $q1->options=['q1'=>['-- Choose --','Spain','Spanish'],'q2'=>['-- Choose --','children','childrens'],'q3'=>['-- Choose --','one','first']];
//       // $q3->additionalData=["images"=>['Restaurant','Hotel','Store']];
//        $q8->questionType='typein';
//        $q8->correctAnswers=['q1'=>['Conditional sentences'=>['a dog if they did not live','a dog if they didn`t live']],'q2'=>['Vocabulary'=>'her great beauty'],'q3'=>['Passive voice'=>'must be pleased to'],'q4'=>['Passive voice'=>'be taught proper'],'q5'=>['Gerund,Reported speech'=>'on Dale visiting us'],'q6'=>['Reported speech'=>['wondered if John knocked when he entered','wondered whether John knocked when he entered']],'q7'=>['Vocabulary'=>'unpopular it makes him Nicholas always sticks'],'q8'=>['Conjunctions'=>'neither the president nor the congress are']];
//        $q8->modul='Module8';
//        $q8->points=1;
//        $q8->final=1;
//        $q8->qid='tinejdzeri8';
//        $questionData[]=$q8;
        
        
//         $q1=new \Classes\QuestionData();
//        $q1->question="Choose the correct word or phrase:";
//        $q1->hint="Dropdown menu";
//       // $q1->multi=1;
//        $q1->answers=['q1','q2','q3','q4','q5','q6','q7','q8','q9'];
//        $q1->sentences=['q1'=>'1. I’d visit Mark tomorrow providing he #replace# at home.','q2'=>'2. I #replace# the report by Friday at the latest.','q3'=>'3. He was cycling, and he stopped #replace#.',
//            'q4'=>'4. She seems #replace# exactly what was required of her.','q5'=>'5. Because English people #replace# on the left, it’s difficult for them to drive abroad.',
//            'q6'=>'6. I’m going to #replace# by my best friend.','q7'=>'7. I really object #replace# in public places.','q8'=>'8. I need to go out, I think #replace# sick.','q9'=>'9. #replace# to abandon our principles now, the society would suffer the consequences.'];
//        //$q1->options=['q1'=>[['-- Choose --','is','were','was','has been'],['-- Choose --','am sending','will send','will be sending','will have sent'],['-- Choose --','having','had','having had','to have'],['-- Choose --','was','had been','is being','was being'],['-- Choose --','include','included','including','includes'],['-- Choose --','hides','hidden','hiding','hid'],['-- Choose --','while','when','which','what'],['-- Choose --','pinpoint','pinpointed','pinpointing','pinpointable'],['-- Choose --','depart','departing','department','departure'],['-- Choose --','improve','improved','improven','improvement']]];
//         $q1->options=['q1'=>['-- Choose --','is','were','was','has been'],'q2'=>['-- Choose --','am sending','will send','will be sending','will have sent'],'q3'=>['-- Choose --','to have smoked','smoking','to smoke','having smoked']
//             ,'q4'=>['-- Choose --','to have not understood','to not understand','not to have understood','to don`t understand'],'q5'=>['-- Choose --','used to drive','get used to drive','got used to driving','are used to driving']
//             ,'q6'=>['-- Choose --','set up my Renren profle','have set up my Renren profile','have my Renren profile set up','have got my Renren profile set up'],'q7'=>['-- Choose --','to people smoke','to people smoking','people to smoke','people smoking']
//             ,'q8'=>['-- Choose --','will be','will have been','am to be','am going to be'],'q9'=>['-- Choose --','Were we','Should we','If we are','If we have']];
//       // $q3->additionalData=["images"=>['Restaurant','Hotel','Store']];
//        $q1->questionType='dropdown';
//        $q1->correctAnswers=['q1'=>['Conditional sentences'=>'were'],'q2'=>['Concord of tenses'=>'will have sent'],'q3'=>['Gerund vs infinitive'=>'to smoke'],'q4'=>['Perfect infinitive'=>'not to have understood'],'q5'=>['(be/get) used to'=>'are used to driving'],'q6'=>['Causative have/get'=>'have my Renren profile set up'],'q7'=>['Gerund vs infinitive'=>'to people smoking'],'q8'=>['Expressing future'=>'am going to be'],'q9'=>['Conditional sentences'=>'Were we']];
//        
//        $q1->modul='Module1';
//        $q1->points=1;
//        $q1->final=0;
//        $q1->qid='studenti1';
//        $questionData[]=$q1;
//                $q2=new \Classes\QuestionData();
//        $q2->question="Complete the text with the correct forms of the words given below:";
//        $q2->hint="Type in";
//       // $q1->multi=1;
//        $q2->answers=['q1','q2','q3','q4','q5','q6','q7','q8','q9','q10'];
//        $q2->sentences=['q1'=>'<span>The central idea of synthetic biology is that living cells can be programmed in the same way
//that computers can, in order to make them do things and produce compounds that their</span> #replace# ','q2'=>'counterparts do not. As with computers, though, scientists need a way to control
//their creations. To date, that has been done with chemical signals. In a paper published in
//Nature Chemical Biology, Christopher Voigt, a biologist at the Massachusetts Institute of
//Technology, describes an alternative. Instead of chemicals, he and his colleagues demonstrate
//how to control #replace# ','q3'=>'cells with coloured light.<br><br>Engineering cells to respond to light is not a new idea. The general approach is called
//optogenetics, and it has become a popular technique for controlling nerve cells in
//neuroscience. But Dr Voigt is not interested in nerve cells. In 2005 he altered four genes in a
//strain of Escherichia coli bacteria, which gave the #replace# ','q4'=>'the ability to respond to light
//and darkness by becoming lighter or darker in colour themselves. The state of the art has
//advanced since then. This time Dr Voigt wanted to see if he could give his bacterial
//charges—which normally live in the human gut, and therefore rarely see light of any
//sort—the ability to sense colour.<br><br>That meant tinkering with 18 different genes containing more than 46,000 base pairs of DNA .
//These changes persuaded the cells to build three different kinds of light sensors that are
//similar in structure to the photoreceptors found in plants and #replace#. ','q5'=>'These sensors allow
//their owners to track things like the time of day and the seasons, and to plan their
//reproduction #replace#. ','q6'=>'One sensor, for instance, harvested from a type of algae,
//switches on when struck by light with a wavelength of 535 nanometres (which humans would
//perceive as green) and then off again when exposed to light of a longer, redder wavelength.
//Other sensors responded to light at the blue and red ends of the visible spectrum.<br><br>Those photoreceptors, #replace# , ','q7'=>'control the expression of certain #replace# genes. ','q8'=>'For
//demonstration purposes, Dr Voigt&#39;s prototype E. coli have been designed to produce enzymes
//that cause the bacteria to become the same colour as the light being shone upon them. So, if a
//plate of the bacteria is struck by a beam of green light, the plate becomes green. If a plate has
//an image composed of red, green and blue projected upon it, the plate will reproduce the
//projection.<br><br>Such #replace# ','q9'=>'is the tip of the iceberg: the light sensors could be used to control genes that
//make all sorts of different chemicals. Scientists have toyed with the idea of using vats of
//genetically altered bacteria to produce things like artificial #replace# or drugs. ','q10'=>'But
//getting the chemistry right requires that the bugs execute several chemical reactions in
//exactly the right sequence. Doing that with chemicals is expensive, and fiddly, since the
//chemicals take time to circulate around the vat. Dr Voigt imagines instead vats fitted with
//coloured lights that wink on and off in sequence. It would look, he jokes, a bit like a bacterial
//disco. Such single-celled #replace# could yet be the future of chemical manufacturing.<br><br><strong> - from The Economist, Science and Technology Section</strong>'];
//        $q2->answerHint=['alga','percept','bacterium','refine','create','carouse','nature','revel','custom','bitter','sweet','art','habit','accord','turn'];
//     //   $q1->options=['q1'=>[['-- Choose --','Took to','Took on','Took off'],['-- Choose --','thanks','thank','to thank'],['-- Choose --','who','which','what']],'q2'=>[['-- Choose --','In','Out','At'],['-- Choose --','shown','showed','showing'],['-- Choose --','challenged','challenging','challengeable']]];
//       // $q3->additionalData=["images"=>['Restaurant','Hotel','Store']];
//        $q2->questionType='typein2';
//        $q2->correctAnswers=['q1'=>['Vocabulary'=>'natural'],'q2'=>['Vocabulary'=>['customised','customized']],'q3'=>['Vocabulary'=>'creatures'],'q4'=>['Vocabulary'=>'algae'],'q5'=>['Vocabulary'=>'accordingly'],'q6'=>['Vocabulary'=>'in turn'],'q7'=>['Vocabulary'=>'bacterial'],'q8'=>['Vocabulary'=>'artistry'],'q9'=>['Vocabulary'=>'sweeteners'],'q10'=>['Vocabulary'=>'revellers']];
//        $q2->modul='Module2';
//        $q2->points=1;
//        $q2->final=0;
//        $q2->qid='studenti2';
//        $questionData[]=$q2;
//
//                $q3=new \Classes\QuestionData();
//        $q3->question="Choose the correct word or phrase:";
//        $q3->hint="Radio button, multiple choice";
//        //$q3->multi=1;
//        $q3->answers=['q1','q2','q3','q4','q5','q6','q7','q8','q9'];
//        $q3->sentences=['q1'=>'<span>1. His role as professor Snape made the</span> #replace# <span>Alan Rickman famous worldwide.</span>','q2'=>'<span>2. We all wish you luck as you</span> #replace# <span>on a new career.</span>',
//            'q3'=>'<span>3. They’re frequent</span> #replace# <span>in most of </span>#replace#','q4'=>'<span>4. IIt’s totally</span> #replace# <span>. The scale must be </span>#replace#',
//            'q5'=>'<span>5. I</span> #replace# <span>Jim last month and we haven’t spoken ever since.</span>','q6'=>'<span>6. I highly doubt he bought that phone. He must have</span> #replace# <span>it.</span>','q7'=>'<span>7. There are still a number of apparently</span> #replace# <span>differences between the two sides in the dispute.</span>','q8'=>'<span>8. The issue of European Unity remains a bone of</span> #replace# <span>among many political parties.</span>','q9'=>'<span>9. The two countries have been</span> #replace# <span>for months over the issue of trade.</span>'];
//    //    $q3->options=['q1'=>[['-- Choose --','did visit','has visited','visited','should visit']],'q2'=>[['-- Choose --','to not be','not to be','that not be','that be not']],'q3'=>[['-- Choose --','couldn’t possibly be','mustn’t possibly be','shouldn’t possibly be','isn’t possibly']],'q4'=>[['-- Choose --','farer','farest','farther','farthest']],'q5'=>[['-- Choose --','our','ours'],['-- Choose --','her','hers']],'q6'=>[['-- Choose --','be able','have','can','need']],'q7'=>[['-- Choose --','This old table’s legs','The legs of this old table','This old table legs','The legs of this old table’s']],'q8'=>[['-- Choose --','nobody','no one','anyone','somebody']]];
//      $q3->options=['q1'=>['passed away','perished','depated','late'],'q2'=>['move','alight','embark','start'],'q3'=>['phenomenons...oases','phenomena...oases','phenomenons...oasis','phenomenon...oasis'],'q4'=>['unlogical...disaccurate','nonlogical...unaccurate','illogical...inaccurate','dislogical...misaccurate'],'q5'=>['fell off with','fell out with','fell back on','fell behind with'],'q6'=>['headed out with','taken off with','stepped off with','made off with'],'q7'=>['irreconcilable','unopposable','incopatible','wide'],'q8'=>['discussion','contention','controversy','division'],'q9'=>['at loggerheads','on tenterhooks','face to face','eye to eye']];
//        // $q3->additionalData=["images"=>['Restaurant','Hotel','Store']];
//        $q3->questionType='radioSentence';
//        $q3->correctAnswers=['q1'=>['Vocabulary'=>'late'],'q2'=>['Vocabulary'=>'embark'],'q3'=>['Plural of nouns'=>'phenomena...oases'],'q4'=>['Vocabulary'=>'illogical...inaccurate'],'q5'=>['Vocabulary'=>'fell out with'],'q6'=>['Vocabulary'=>'made off with'],'q7'=>['Vocabulary'=>'irreconcilable'],'q8'=>['Vocabulary'=>'contention'],'q9'=>['Vocabulary'=>'at loggerheads']];
//        $q3->modul='Module3';
//        $q3->points=1;
//        $q3->final=0;
//        $q3->qid='studenti3';
//        $questionData[]=$q3;
//                $q4=new \Classes\QuestionData();
//        $q4->question="Complete the text with the correct form/tense of the verbs:";
//        $q4->hint="Type in";
//       // $q1->multi=1;
//        $q4->answers=['q1','q2','q3','q4','q5','q6','q7','q8','q9','q10','q11','q12','q13','q14','q15','q16'];
//        $q4->sentences=['q1'=>'<span>A quarter of young children around the world unremittingly</span> #replace# (GET)
//','q2'=>'enough nutrients to grow properly, and 300 die of malnutrition every hour, according to a new
//report that #replace# (LAY) ','q3'=>'bare the effects of the global food crisis.<br>
//    <span>By 2013 there were 170 million children aged under five whose development</span> #replace# (STUNT) ','q4'=>'by malnutrition because of lack of food for them and their breastfeeding
//mothers, and the situation #replace# (GET/STEADILY) ','q5'=>'significantly worse even
//before this research by the charity Save the Children. In Pakistan, Bangladesh, India, Peru and
//Nigeria – countries which are the home of half of the world&#39;s children suffering from hindered
//development – recent rises in global food prices #replace# (FORCE/CONSISTENTLY) ','q6'=>'the parents of malnourished children to cut back on
//food and pull children out of school to work. According to the report, A Life Free from Hunger:
//Tackling Child Malnutrition, a third of parents surveyed said their children unfortunately
//developed a habit and #replace# (COMPLAIN/OFTEN) ','q7'=>'they do not have enough
//to eat. One in six parents can never afford to buy meat, milk or vegetables. It suggests that six
//out of 10 children in Afghanistan are not getting enough nutrients to avoid stunted growth.<br>
//           &quot;If no concerted action #replace# (TAKE),&quot; ','q8'=>'warns Justin Forsyth, the charity&#39;s chief executive,
//&quot;half a billion children will be physically and mentally stunted over the next 15 years&quot;. Over the
//past five years the price of food #replace# (SOAR) ','q9'=>'across the globe, due to extreme
//weather conditions, #replace# (DIVERT) ','q10'=>'farmland to grow biofuels, speculative trading of
//food commodities and the global financial crisis. The poor, who spend the bulk of their income
//on food, are hit hardest. One in four parents in the countries surveyed has been forced to cut
//back on food for their families. One in six #replace# (HAVE) ','q11'=>'children skip school to help their
//parents at work. Save the Children described malnutrition as a silent killer because
//unfortunately more often than not it #replace# (RECORD) ','q12'=>'as a cause of
//death on birth certificates, leading to a lack of action across the developing world.<br>
//            Most malnourished children, around 85 per cent, do not die but #replace# (DIMINISH), ','q13'=>'physically and mentally. But with early intervention, the lifelong physical and
//mental stunting from hunger can be eased, enabling individuals to reach their potential. In
//northern Afghanistan that year for instance, Mohammed Jan was only half the weight he #replace# (BE) ','q14'=>'at seven months because his mother was so poor that
//she did not have enough food to produce breast milk. It was evidenced by Save the Children that
//he #replace# (SLIP) ','q15'=>'into death but he #replace# (SPOT) ','q16'=>'by a voluntary
//community health worker and sent to Khulm District Hospital near Mazar-e- Sharif.<br>
//            The majority of children #replace# (EXPERIENCE) malnutrition in countries are not as
//lucky, according to the report.'
//            ];
//        $q4->answerHint=null;
//     //   $q1->options=['q1'=>[['-- Choose --','Took to','Took on','Took off'],['-- Choose --','thanks','thank','to thank'],['-- Choose --','who','which','what']],'q2'=>[['-- Choose --','In','Out','At'],['-- Choose --','shown','showed','showing'],['-- Choose --','challenged','challenging','challengeable']]];
//       // $q3->additionalData=["images"=>['Restaurant','Hotel','Store']];
//        $q4->questionType='typein2';
//        $q4->correctAnswers=['q1'=>['Concord of tenses'=>['are not getting','aren`t getting']],'q2'=>['Concord of tenses'=>'lays'],'q3'=>['Concord of tenses,Passive voice'=>'had been stunted'],'q4'=>['Concord of tenses'=>'was steadily getting'],'q5'=>['Concord of tenses'=>['are consistently forcing','have consistently been forcing']],'q6'=>['Concord of tenses,Reported speech'=>'would often complain'],'q7'=>['Concord of tenses,Passive voice'=>'is taken']
//            ,'q8'=>['Concord of tenses'=>'has been soaring'],'q9'=>['Concord of tenses,Participles'=>'diverting'],'q10'=>['Concord of tenses'=>'have had'],'q11'=>['Concord of tenses,Passive voice'=>['is not being recorded','isn`t being recorded']],'q12'=>['Concord of tenses,Passive voice'=>'are diminished']
//            ,'q13'=>['Concord of tenses,Modal verbs'=>['was supposed to be','should have been']],'q14'=>['Concord of tenses'=>'had been slipping'],'q15'=>['Concord of tenses,Passive voice'=>'had been spotted'],'q16'=>['Concord of tenses,Participles'=>'experiencing']];
//        $q4->modul='Module4';
//        $q4->points=1;
//        $q4->final=0;
//        $q4->qid='stduenti4';
//        $questionData[]=$q4;
//                            $q5=new \Classes\QuestionData();
//        $q5->question="READING COMPREHENSION TEST";
//        $q5->hint="Radio button, multiple choice";
//        //$q3->multi=1;
//     
//        $q5->story=true;
//        $q5->answers=['q1','q2','q3','q4','q5','q6','q7','q8'];
//        $q5->sentences=['q1'=>'<span>1. What is the best title for this article?</span>','q2'=>'<span>2. According to the article, what will be
//gained through the process of trial and
//error?</span>',
//            'q3'=>'<span>3. Which of the following adjectives best
//describes the Virtual Reality Cinema
//goers?</span>','q4'=>'<span>4. What is the meaning of the expression “to
//come into vogue” (paragraph 5)?</span>',
//            'q5'=>'<span>5. What is the author&#39;s attitude towards VR
//cinema era?</span>','q6'=>'<span>6. Which of the following statements is true?</span>','q7'=>'<span>7. What does the phrase “self-sustaining”
//(paragraph 2) mean?</span>','q8'=>'<span>8. What does the word “dabbled” (paragraph
//4) refer to?</span>'];
//    //    $q3->options=['q1'=>[['-- Choose --','did visit','has visited','visited','should visit']],'q2'=>[['-- Choose --','to not be','not to be','that not be','that be not']],'q3'=>[['-- Choose --','couldn’t possibly be','mustn’t possibly be','shouldn’t possibly be','isn’t possibly']],'q4'=>[['-- Choose --','farer','farest','farther','farthest']],'q5'=>[['-- Choose --','our','ours'],['-- Choose --','her','hers']],'q6'=>[['-- Choose --','be able','have','can','need']],'q7'=>[['-- Choose --','This old table’s legs','The legs of this old table','This old table legs','The legs of this old table’s']],'q8'=>[['-- Choose --','nobody','no one','anyone','somebody']]];
//      $q5->options=['q1'=>['Bedtime story','VR is the new black','VR hype cycle recycled','Fandom overtaking VR industry'],'q2'=>['A new grammar book being written for VR','The audience participating and changing a story','Every cinema having the necesary gadgets to experience VR','Separating brains frm thinking in the film way'],'q3'=>['excluded','reclusive','secluded','incognizant'],'q4'=>['to be discovered','to come into use','to start a craze','to be abandoned'],'q5'=>['doubtful','critical','praising','enthusiastic'],'q6'=>['Spectatcles, melodrama and jump-cuts were extremely popular unitl the 1950s','Finding an audience and the content is a
//challenge for the Virtual Reality Cinema.','People who don`t own the necessary gadgets
//can experience VR only in Amsterdam.','Big Chinese gaming and messaging giants
//have commissioned VR movies.'],'q7'=>['self-sufficient','self-supporting','self-reliant','self-contained'],'q8'=>['early film-makers were engulfed by spectacle','early film-makers immersed themselves in
//spectacle','early film-makers tinkered with spectacle','early film-makers fiddled with spectacle']];
//        // $q3->additionalData=["images"=>['Restaurant','Hotel','Store']];
//        $q5->questionType='radioSentence';
//        $q5->correctAnswers=['q1'=>['Text understanding'=>'VR is the new black'],'q2'=>['Text understanding'=>'A new grammar book being written for VR'],'q3'=>['Text understanding'=>'incognizant'],'q4'=>['Text understanding'=>'to start a craze'],'q5'=>['Text understanding'=>'enthusiastic'],'q6'=>['Text understanding'=>'People who don`t own the necessary gadgets
//can experience VR only in Amsterdam.'],'q7'=>['Text understanding'=>'self-supporting'],'q8'=>['Text understanding'=>'early film-makers tinkered with spectacle']];
//        $q5->modul='Module5';
//        $q5->points=1;
//        $q5->final=0;
//        $q5->qid='stduenti5';
//        $questionData[]=$q5;
//                    $q6=new \Classes\QuestionData();
//        $q6->question="Complete the second sentence so that it has a similar meaning to the first sentence, using
//the words given in brackets. Do not change the given words. Use between 3 and 7 words.";
//        $q6->hint="Type the answer";
//       // $q8->story=true;
//        $q6->answers=['q1','q2','q3','q4','q5','q6','q7','q8'];
//        $q6->sentences=['q1'=>'<span>1. If you wish to book our services, please contact our customer desk. (SHOULD)<br></span> #replace# <span>, please contact our customer desk.</span>','q2'=>'<span>2. It appears that the harvest workers thought that they were underpaid. (CLAIM)
//<br>The harvest workers</span> #replace# <span>underpaid.</span>','q3'=>'<span>3. People think that the singer has disappeared. (THOUGHT) <br>The singer</span> #replace#.',"q4"=>"<span>4. “The truth didn&#39;t become known before 1815.” (UNTIL)<br>
//He said that not</span> #replace# known.","q5"=>"<span>5. You really should stop apologizing for their mistakes. (HIGH TIME)<br>
//It’s</span> #replace# <span>their mistakes.</span>","q6"=>"<span>6. Ann got the flu. (CAME)
//<br>Anna</span> #replace#","q7"=>"<span>7. Please don&#39;t invite him to the party.   (RATHER)<br>
//I&#39;d rather</span> #replace# <span>to the party.</span>","q8"=>"<span>8. I called all my friends after I had been told the news. (HEARD)
//</span> #replace# <span>my friends.</span>"];
//       // $q1->options=['q1'=>['-- Choose --','Spain','Spanish'],'q2'=>['-- Choose --','children','childrens'],'q3'=>['-- Choose --','one','first']];
//       // $q3->additionalData=["images"=>['Restaurant','Hotel','Store']];
//        $q6->questionType='typein';
//        $q6->correctAnswers=['q1'=>['Conditional sentences'=>'should you wish to book our services'],'q2'=>['Perfect infinitive'=>'claim to have been'],'q3'=>['Passive voice,Perfect infinitive'=>'is thought to have disappeared'],'q4'=>['Reported speech,Inversion'=>'until 1815 had the truth become'],'q5'=>['Subjunctive mood'=>'high time you stopped apologizing for'],'q6'=>['Vocabulary'=>'came down with the flu'],'q7'=>['Subjunctive mood'=>['you did not invite him','you didn`t invite him']],'q8'=>['Participles'=>'having heard the news I called all']];
//        $q6->modul='Module6';
//        $q6->points=1;
//        $q6->final=1;
//        $q6->qid='studenti6';
//        $questionData[]=$q6;
//        $q8=new \Classes\QuestionData();
//        $q8->question="Complete the second sentence so that it has a similar meaning to the first sentence, using
//the words given in brackets. Do not change the given words. Use between 3 and 7 words.";
//        $q8->hint="Type the answer";
//        $q8->story=true;
//        $q8->answers=['q1','q2','q3','q4','q5','q6','q7','q8'];
//        $q8->sentences=['q1'=>'<span>1. Hope found the History exam surprisingly easy. (MUCH) The history exam</span> #replace# <span>than Hope expected.</span>','q2'=>'<span>2. It’s more than a week since she last saw her grandparents. (NOT)
//She</span> #replace# <span>more than a week.</span>','q3'=>'<span>3. I lent you that magazine last month and now I’d like to have it back. (FROM)
//Can I have back that magazine</span> #replace# <span>last month?</span>',"q4"=>"<span>4. The last time I spoke to him was when he got married. (SINCE)
//I</span> #replace# <span>he got married.</span>","q5"=>"<span>5. Does Angie’s sister go in for any sport? (WONDER) I</span> #replace# <span>in for any sport.</span>","q6"=>"<span>6. Children, whose jacket is this? (WHO)
//Children,</span> #replace# <span>to?</span>","q7"=>"<span>7. When was this cathedral built, Mr Miller? (OLD)
//Mr Miller, do you know</span> #replace# <span>?</span>","q8"=>"<span>8. You can’t take my car! (BORROW)
//I won’t</span> #replace# <span>my car.</span>"];
//       // $q1->options=['q1'=>['-- Choose --','Spain','Spanish'],'q2'=>['-- Choose --','children','childrens'],'q3'=>['-- Choose --','one','first']];
//       // $q3->additionalData=["images"=>['Restaurant','Hotel','Store']];
//        $q8->questionType='typein';
//        $q8->correctAnswers=['q1'=>['Comparison of adjectives/adverbs'=>'was much easier'],'q2'=>['Tenses'=>['hasn`t seen her granparents for','has not seen her granparents for']],'q3'=>['Relative clauses'=>['which you borrowed from me','that you borrowed from me']],'q4'=>['Tenses'=>['haven`t spoken to him since','have not spoken to him since']],'q5'=>['Reported speech'=>'wonder if Angie`s sister goes'],'q6'=>['Subject and object questions'=>'who does this jacket belong'],'q7'=>['Reported speech'=>'how old this cathedral is'],'q8'=>['Vocabulary'=>'let you borrow']];
//        $q8->modul='Module8';
//        $q8->points=1;
//        $q8->final=1;
//        $q8->qid='skolarci8';
 //       $questionData[]=$q8;
//                $q1=new \Classes\QuestionData();
//        $q1->question="";
//        $q1->hint="Radio button, multiple choice";
//        //$q3->multi=1;
//     
//        //$q1->story=true;
//        $q1->answers=['q1'];
//        $q1->sentences=['q1'=>'<span>1. Pažljivo posmatraj sliku i reci mi šta vidiš.</span><br><br>'];
//        $q1->image='1.png';
//    //    $q3->options=['q1'=>[['-- Choose --','did visit','has visited','visited','should visit']],'q2'=>[['-- Choose --','to not be','not to be','that not be','that be not']],'q3'=>[['-- Choose --','couldn’t possibly be','mustn’t possibly be','shouldn’t possibly be','isn’t possibly']],'q4'=>[['-- Choose --','farer','farest','farther','farthest']],'q5'=>[['-- Choose --','our','ours'],['-- Choose --','her','hers']],'q6'=>[['-- Choose --','be able','have','can','need']],'q7'=>[['-- Choose --','This old table’s legs','The legs of this old table','This old table legs','The legs of this old table’s']],'q8'=>[['-- Choose --','nobody','no one','anyone','somebody']]];
//      $q1->options=['q1'=>['vidim belu pozadinu','vidim crnu kružnu liniju – krug','vidim krug u kvadratu','ne vidim ništa interesantno']];
//        // $q3->additionalData=["images"=>['Restaurant','Hotel','Store']];
//        $q1->questionType='radioSentence';
//       // $q1->correctAnswers=['q1'=>'vidim belu pozadinu','q2'=>'vidim crnu kružnu liniju – krug','q3'=>'vidim krug u kvadratu','q4'=>'ne vidim ništa interesantno'];
//        $q1->modul='Module1';
//        $q1->points=["vidim belu pozadinu"=>1,"vidim crnu kružnu liniju – krug"=>2,"vidim krug u kvadratu"=>3,"ne vidim ništa interesantno"=>4];
//        $q1->final=0;
//        $q1->qid='psiho1';
//        $questionData[]=$q1;
//                        $q2=new \Classes\QuestionData();
//        $q2->question="";
//        $q2->hint="Radio button, multiple choice";
//        //$q3->multi=1;
//     
//        //$q1->story=true;
//        $q2->answers=['q1'];
//        $q2->sentences=['q1'=>'<span>2. Pred tobom su vrata. Posmatraj ih par sekundi, a zatim zamisli šta bi se nalazilo iza njih, kada
//bi se ona otvorila.</span><br><br>'];
//        $q2->image='2.png';
//    //    $q3->options=['q1'=>[['-- Choose --','did visit','has visited','visited','should visit']],'q2'=>[['-- Choose --','to not be','not to be','that not be','that be not']],'q3'=>[['-- Choose --','couldn’t possibly be','mustn’t possibly be','shouldn’t possibly be','isn’t possibly']],'q4'=>[['-- Choose --','farer','farest','farther','farthest']],'q5'=>[['-- Choose --','our','ours'],['-- Choose --','her','hers']],'q6'=>[['-- Choose --','be able','have','can','need']],'q7'=>[['-- Choose --','This old table’s legs','The legs of this old table','This old table legs','The legs of this old table’s']],'q8'=>[['-- Choose --','nobody','no one','anyone','somebody']]];
//      $q2->options=['q1'=>['brigu','spokoj','ne izaziva ništa','smeh']];
//        // $q3->additionalData=["images"=>['Restaurant','Hotel','Store']];
//        $q2->questionType='radioSentence';
//      //  $q1->correctAnswers=['q1'=>'vidim belu pozadinu','q2'=>'vidim crnu kružnu liniju – krug','q3'=>'vidim krug u kvadratu','q4'=>'ne vidim ništa interesantno'];
//        $q2->modul='Module2';
//        $q2->points=["brigu"=>2,"spokoj"=>3,"ne izaziva ništa"=>4,"smeh"=>1];
//        $q2->final=0;
//        $q2->qid='psiho2';
//      $questionData[]=$q2;
//                           $q3=new \Classes\QuestionData();
//        $q3->question="";
//        $q3->hint="Radio button, multiple choice";
//        //$q3->multi=1;
//     
//        //$q1->story=true;
//        $q3->answers=['q1'];
//        $q3->sentences=['q1'=>'<span>3. Ako sijalica u stanu pregori, ja :</span><br><br>'];
//        //$q3->image='2.png';
//    //    $q3->options=['q1'=>[['-- Choose --','did visit','has visited','visited','should visit']],'q2'=>[['-- Choose --','to not be','not to be','that not be','that be not']],'q3'=>[['-- Choose --','couldn’t possibly be','mustn’t possibly be','shouldn’t possibly be','isn’t possibly']],'q4'=>[['-- Choose --','farer','farest','farther','farthest']],'q5'=>[['-- Choose --','our','ours'],['-- Choose --','her','hers']],'q6'=>[['-- Choose --','be able','have','can','need']],'q7'=>[['-- Choose --','This old table’s legs','The legs of this old table','This old table legs','The legs of this old table’s']],'q8'=>[['-- Choose --','nobody','no one','anyone','somebody']]];
//      $q3->options=['q1'=>['nikada nisam menjao/la sijalicu','zamolim nekoga da je zameni','čekam da mi neko kaže da zamenim','odmah primetim i zamenim']];
//        // $q3->additionalData=["images"=>['Restaurant','Hotel','Store']];
//        $q3->questionType='radioSentence';
//      //  $q1->correctAnswers=['q1'=>'vidim belu pozadinu','q2'=>'vidim crnu kružnu liniju – krug','q3'=>'vidim krug u kvadratu','q4'=>'ne vidim ništa interesantno'];
//        $q3->modul='Module3';
//        $q3->points=["nikada nisam menjao/la sijalicu"=>4,"zamolim nekoga da je zameni"=>1,"čekam da mi neko kaže da zamenim"=>2,"odmah primetim i zamenim"=>3];
//        $q3->final=0;
//        $q3->qid='psiho3';
//      $questionData[]=$q3;
//         $q4=new \Classes\QuestionData();
//        $q4->question="4. Pred tobom je plodno drvo sa jabukama. Izaberi jedan plod (jabuku) za sebe.";
//        $q4->hint="Click and choose";
//        //$q3->multi=1;
//     
//        //$q1->story=true;
//        $q4->answers=['q1'];
//        $q4->sentences=['q1'=>'<span>4. Pred tobom je plodno drvo sa jabukama. Izaberi jedan plod (jabuku) za sebe.</span><br><br>'];
//        //$q3->image='2.png';
//    //    $q3->options=['q1'=>[['-- Choose --','did visit','has visited','visited','should visit']],'q2'=>[['-- Choose --','to not be','not to be','that not be','that be not']],'q3'=>[['-- Choose --','couldn’t possibly be','mustn’t possibly be','shouldn’t possibly be','isn’t possibly']],'q4'=>[['-- Choose --','farer','farest','farther','farthest']],'q5'=>[['-- Choose --','our','ours'],['-- Choose --','her','hers']],'q6'=>[['-- Choose --','be able','have','can','need']],'q7'=>[['-- Choose --','This old table’s legs','The legs of this old table','This old table legs','The legs of this old table’s']],'q8'=>[['-- Choose --','nobody','no one','anyone','somebody']]];
//      $q4->options=['q1'=>['1','2','3','4','5','6','7','8','9']];
//        // $q3->additionalData=["images"=>['Restaurant','Hotel','Store']];
//        $q4->questionType='clickArea';
//      //  $q1->correctAnswers=['q1'=>'vidim belu pozadinu','q2'=>'vidim crnu kružnu liniju – krug','q3'=>'vidim krug u kvadratu','q4'=>'ne vidim ništa interesantno'];
//        $q4->modul='Module4';
//        $q4->points=["1"=>2,"2"=>2,"3"=>1,"4"=>4,"5"=>4,"6"=>1,"7"=>3,"8"=>3,"9"=>3];
//        $q4->final=0;
//        $q4->qid='psiho4';
//      $questionData[]=$q4;
//      $q5=new \Classes\QuestionData();
//        $q5->question="";
//        $q5->hint="Radio button, multiple choice";
//        //$q3->multi=1;
//     
//        //$q1->story=true;
//        $q5->answers=['q1'];
//        $q5->sentences=['q1'=>'<span>5. Šta vas tokom noći čini budnim:</span><br><br>'];
//        //$q3->image='2.png';
//    //    $q3->options=['q1'=>[['-- Choose --','did visit','has visited','visited','should visit']],'q2'=>[['-- Choose --','to not be','not to be','that not be','that be not']],'q3'=>[['-- Choose --','couldn’t possibly be','mustn’t possibly be','shouldn’t possibly be','isn’t possibly']],'q4'=>[['-- Choose --','farer','farest','farther','farthest']],'q5'=>[['-- Choose --','our','ours'],['-- Choose --','her','hers']],'q6'=>[['-- Choose --','be able','have','can','need']],'q7'=>[['-- Choose --','This old table’s legs','The legs of this old table','This old table legs','The legs of this old table’s']],'q8'=>[['-- Choose --','nobody','no one','anyone','somebody']]];
//      $q5->options=['q1'=>['misao da niste najbolji u svemu','osećaj da niste bili „dovoljno dobri“ u komunikaciji sa drugima','spisak aktivnosti za dan koji dolazi','činjenica da je dan bio pun vaših pogrešaka']];
//        // $q3->additionalData=["images"=>['Restaurant','Hotel','Store']];
//        $q5->questionType='radioSentence';
//      //  $q1->correctAnswers=['q1'=>'vidim belu pozadinu','q2'=>'vidim crnu kružnu liniju – krug','q3'=>'vidim krug u kvadratu','q4'=>'ne vidim ništa interesantno'];
//        $q5->modul='Module5';
//        $q5->points=["misao da niste najbolji u svemu"=>4,"osećaj da niste bili „dovoljno dobri“ u komunikaciji sa drugima"=>1,"spisak aktivnosti za dan koji dolazi"=>3,"činjenica da je dan bio pun vaših pogrešaka"=>2];
//        $q5->final=0;
//        $q5->qid='psiho5';
//      $questionData[]=$q5;
//      $q6=new \Classes\QuestionData();
//        $q6->question="";
//        $q6->hint="Radio button, multiple choice";
//        //$q3->multi=1;
//     
//        //$q1->story=true;
//        $q6->answers=['q1'];
//        $q6->sentences=['q1'=>'<span>6. Znam tačan broj dečaka i devojčica u odeljenju:</span><br><br>'];
//        //$q3->image='2.png';
//    //    $q3->options=['q1'=>[['-- Choose --','did visit','has visited','visited','should visit']],'q2'=>[['-- Choose --','to not be','not to be','that not be','that be not']],'q3'=>[['-- Choose --','couldn’t possibly be','mustn’t possibly be','shouldn’t possibly be','isn’t possibly']],'q4'=>[['-- Choose --','farer','farest','farther','farthest']],'q5'=>[['-- Choose --','our','ours'],['-- Choose --','her','hers']],'q6'=>[['-- Choose --','be able','have','can','need']],'q7'=>[['-- Choose --','This old table’s legs','The legs of this old table','This old table legs','The legs of this old table’s']],'q8'=>[['-- Choose --','nobody','no one','anyone','somebody']]];
//      $q6->options=['q1'=>['DA','NE']];
//        // $q3->additionalData=["images"=>['Restaurant','Hotel','Store']];
//        $q6->questionType='radioSentence';
//      //  $q1->correctAnswers=['q1'=>'vidim belu pozadinu','q2'=>'vidim crnu kružnu liniju – krug','q3'=>'vidim krug u kvadratu','q4'=>'ne vidim ništa interesantno'];
//        $q6->modul='Module6';
//        $q6->points=["DA"=>3,"NE"=>2];
//        $q6->final=0;
//        $q6->qid='psiho6';
//      $questionData[]=$q6;
//                        $q7=new \Classes\QuestionData();
//        $q7->question="";
//        $q7->hint="Radio button, multiple choice";
//        //$q3->multi=1;
//     
//        //$q1->story=true;
//        $q7->answers=['q1'];
//        $q7->sentences=['q1'=>'<span>7. Na fotografiji je mnogo ljudi. Koliko je tvojih prijatelja među njima?</span><br><br>'];
//        $q7->image='7.png';
//    //    $q3->options=['q1'=>[['-- Choose --','did visit','has visited','visited','should visit']],'q2'=>[['-- Choose --','to not be','not to be','that not be','that be not']],'q3'=>[['-- Choose --','couldn’t possibly be','mustn’t possibly be','shouldn’t possibly be','isn’t possibly']],'q4'=>[['-- Choose --','farer','farest','farther','farthest']],'q5'=>[['-- Choose --','our','ours'],['-- Choose --','her','hers']],'q6'=>[['-- Choose --','be able','have','can','need']],'q7'=>[['-- Choose --','This old table’s legs','The legs of this old table','This old table legs','The legs of this old table’s']],'q8'=>[['-- Choose --','nobody','no one','anyone','somebody']]];
//      $q7->options=['q1'=>['vidim svoje prijatelje','moji prijatelji nisu u ovoj grupi ljudi','moji prijatelji imaju lica, a ovi ljudi nemaju']];
//        // $q3->additionalData=["images"=>['Restaurant','Hotel','Store']];
//        $q7->questionType='radioSentence';
//      //  $q1->correctAnswers=['q1'=>'vidim belu pozadinu','q2'=>'vidim crnu kružnu liniju – krug','q3'=>'vidim krug u kvadratu','q4'=>'ne vidim ništa interesantno'];
//        $q7->modul='Module7';
//        $q7->points=["vidim svoje prijatelje"=>3,"moji prijatelji nisu u ovoj grupi ljudi"=>4,"moji prijatelji imaju lica, a ovi ljudi nemaju"=>1];
//        $q7->final=0;
//        $q7->qid='psiho7';
//      $questionData[]=$q7;
//      
//                              $q8=new \Classes\QuestionData();
//        $q8->question="";
//        $q8->hint="Radio button, multiple choice";
//        //$q3->multi=1;
//     
//        //$q1->story=true;
//        $q8->answers=['q1'];
//        $q8->sentences=['q1'=>'<span>8. Posmatraj fotografiju par sekundi. Koji deo fotografije ostaje u fokusu tvog posmatranja i
//kada prestaneš sa gledanjem:</span><br><br>'];
//        $q8->image='8.png';
//    //    $q3->options=['q1'=>[['-- Choose --','did visit','has visited','visited','should visit']],'q2'=>[['-- Choose --','to not be','not to be','that not be','that be not']],'q3'=>[['-- Choose --','couldn’t possibly be','mustn’t possibly be','shouldn’t possibly be','isn’t possibly']],'q4'=>[['-- Choose --','farer','farest','farther','farthest']],'q5'=>[['-- Choose --','our','ours'],['-- Choose --','her','hers']],'q6'=>[['-- Choose --','be able','have','can','need']],'q7'=>[['-- Choose --','This old table’s legs','The legs of this old table','This old table legs','The legs of this old table’s']],'q8'=>[['-- Choose --','nobody','no one','anyone','somebody']]];
//      $q8->options=['q1'=>['Sunce','čovek','reka','nijedan, već asocijacija koju mi je fotografija nametnula']];
//        // $q3->additionalData=["images"=>['Restaurant','Hotel','Store']];
//        $q8->questionType='radioSentence';
//      //  $q1->correctAnswers=['q1'=>'vidim belu pozadinu','q2'=>'vidim crnu kružnu liniju – krug','q3'=>'vidim krug u kvadratu','q4'=>'ne vidim ništa interesantno'];
//        $q8->modul='Module8';
//        $q8->points=["Sunce"=>3,"čovek"=>4,"reka"=>2,"nijedan, već asocijacija koju mi je fotografija nametnula"=>1];
//        $q8->final=0;
//        $q8->qid='psiho8';
//      $questionData[]=$q8;
//         $q9=new \Classes\QuestionData();
//        $q9->question="";
//        $q9->hint="Radio button, multiple choice";
//        //$q3->multi=1;
//     
//        //$q1->story=true;
//        $q9->answers=['q1'];
//        $q9->sentences=['q1'=>'<span>9. Pred tobom su dva puta. Prijatelji ti govore da kreneš levim, roditelji desnim. Ti odlučuješ:</span><br><br>'];
//        $q9->image='9.png';
//    //    $q3->options=['q1'=>[['-- Choose --','did visit','has visited','visited','should visit']],'q2'=>[['-- Choose --','to not be','not to be','that not be','that be not']],'q3'=>[['-- Choose --','couldn’t possibly be','mustn’t possibly be','shouldn’t possibly be','isn’t possibly']],'q4'=>[['-- Choose --','farer','farest','farther','farthest']],'q5'=>[['-- Choose --','our','ours'],['-- Choose --','her','hers']],'q6'=>[['-- Choose --','be able','have','can','need']],'q7'=>[['-- Choose --','This old table’s legs','The legs of this old table','This old table legs','The legs of this old table’s']],'q8'=>[['-- Choose --','nobody','no one','anyone','somebody']]];
//      $q9->options=['q1'=>['da zastaneš i još malo razmisliš, a zatim da napraviš izbor bez uticaja roditelja i prijatelja','da poslušaš prijatelje','da poslušaš roditelje','odustao si od puta, jer ne želiš da povrediš ni roditelje, ni prijatelje']];
//        // $q3->additionalData=["images"=>['Restaurant','Hotel','Store']];
//        $q9->questionType='radioSentence';
//      //  $q1->correctAnswers=['q1'=>'vidim belu pozadinu','q2'=>'vidim crnu kružnu liniju – krug','q3'=>'vidim krug u kvadratu','q4'=>'ne vidim ništa interesantno'];
//        $q9->modul='Module9';
//        $q9->points=["da zastaneš i još malo razmisliš, a zatim da napraviš izbor bez uticaja roditelja i prijatelja"=>3,"da poslušaš prijatelje"=>1,"da poslušaš roditelje"=>4,"odustao si od puta, jer ne želiš da povrediš ni roditelje, ni prijatelje"=>2];
//        $q9->final=0;
//        $q9->qid='psiho9';
//      $questionData[]=$q9;
//      $q10=new \Classes\QuestionData();
//        $q10->question="";
//        $q10->hint="Radio button, multiple choice";
//        //$q3->multi=1;
//     
//        //$q1->story=true;
//        $q10->answers=['q1'];
//        $q10->sentences=['q1'=>'<span>10. Na slici koju gledaš numerisani su facijalni izrazi emocija. Pokušaj da se prisetiš prethodnih
//mesec dana tvoga života, a zatim izaberi broj pored lica koje najutentičnije opisuje to sećanje.</span><br><br>'];
//        $q10->image='10.png';
//    //    $q3->options=['q1'=>[['-- Choose --','did visit','has visited','visited','should visit']],'q2'=>[['-- Choose --','to not be','not to be','that not be','that be not']],'q3'=>[['-- Choose --','couldn’t possibly be','mustn’t possibly be','shouldn’t possibly be','isn’t possibly']],'q4'=>[['-- Choose --','farer','farest','farther','farthest']],'q5'=>[['-- Choose --','our','ours'],['-- Choose --','her','hers']],'q6'=>[['-- Choose --','be able','have','can','need']],'q7'=>[['-- Choose --','This old table’s legs','The legs of this old table','This old table legs','The legs of this old table’s']],'q8'=>[['-- Choose --','nobody','no one','anyone','somebody']]];
//      $q10->options=['q1'=>['1','2','3','4','5','6']];
//        // $q3->additionalData=["images"=>['Restaurant','Hotel','Store']];
//        $q10->questionType='radioSentence';
//      //  $q1->correctAnswers=['q1'=>'vidim belu pozadinu','q2'=>'vidim crnu kružnu liniju – krug','q3'=>'vidim krug u kvadratu','q4'=>'ne vidim ništa interesantno'];
//        $q10->modul='Module10';
//        $q10->points=["1"=>2,"2"=>1,"3"=>3,"4"=>4,"5"=>4,"6"=>2];
//        $q10->final=0;
//        $q10->qid='psiho10';
//      $questionData[]=$q10;
//       $q11=new \Classes\QuestionData();
//        $q11->question="";
//        $q11->hint="Radio button, multiple choice";
//        //$q3->multi=1;
//     
//        //$q1->story=true;
//        $q11->answers=['q1'];
//        $q11->sentences=['q1'=>'<span>11. Osećate se prijatno kada:</span><br><br>'];
//        //$q3->image='2.png';
//    //    $q3->options=['q1'=>[['-- Choose --','did visit','has visited','visited','should visit']],'q2'=>[['-- Choose --','to not be','not to be','that not be','that be not']],'q3'=>[['-- Choose --','couldn’t possibly be','mustn’t possibly be','shouldn’t possibly be','isn’t possibly']],'q4'=>[['-- Choose --','farer','farest','farther','farthest']],'q5'=>[['-- Choose --','our','ours'],['-- Choose --','her','hers']],'q6'=>[['-- Choose --','be able','have','can','need']],'q7'=>[['-- Choose --','This old table’s legs','The legs of this old table','This old table legs','The legs of this old table’s']],'q8'=>[['-- Choose --','nobody','no one','anyone','somebody']]];
//      $q11->options=['q1'=>['znate šta drugi očekuju od vas','ste u društvu prijatelja','komunicirate sa drugima','vas niko ne gleda']];
//        // $q3->additionalData=["images"=>['Restaurant','Hotel','Store']];
//        $q11->questionType='radioSentence';
//      //  $q1->correctAnswers=['q1'=>'vidim belu pozadinu','q2'=>'vidim crnu kružnu liniju – krug','q3'=>'vidim krug u kvadratu','q4'=>'ne vidim ništa interesantno'];
//        $q11->modul='Module11';
//        $q11->points=["znate šta drugi očekuju od vas"=>4,"ste u društvu prijatelja"=>1,"komunicirate sa drugima"=>3,"vas niko ne gleda"=>2];
//        $q11->final=0;
//        $q11->qid='psiho11';
//      $questionData[]=$q11;
//      
//       $q12=new \Classes\QuestionData();
//        $q12->question="";
//        $q12->hint="Radio button, multiple choice";
//        //$q3->multi=1;
//     
//        //$q1->story=true;
//        $q12->answers=['q1'];
//        $q12->sentences=['q1'=>'<span>12. Do kraja ovog testa sam stigla/o:</span><br><br>'];
//        //$q3->image='2.png';
//    //    $q3->options=['q1'=>[['-- Choose --','did visit','has visited','visited','should visit']],'q2'=>[['-- Choose --','to not be','not to be','that not be','that be not']],'q3'=>[['-- Choose --','couldn’t possibly be','mustn’t possibly be','shouldn’t possibly be','isn’t possibly']],'q4'=>[['-- Choose --','farer','farest','farther','farthest']],'q5'=>[['-- Choose --','our','ours'],['-- Choose --','her','hers']],'q6'=>[['-- Choose --','be able','have','can','need']],'q7'=>[['-- Choose --','This old table’s legs','The legs of this old table','This old table legs','The legs of this old table’s']],'q8'=>[['-- Choose --','nobody','no one','anyone','somebody']]];
//      $q12->options=['q1'=>['svojom voljom','zahvaljujući tvorcu testa']];
//        // $q3->additionalData=["images"=>['Restaurant','Hotel','Store']];
//        $q12->questionType='radioSentence';
//      //  $q1->correctAnswers=['q1'=>'vidim belu pozadinu','q2'=>'vidim crnu kružnu liniju – krug','q3'=>'vidim krug u kvadratu','q4'=>'ne vidim ništa interesantno'];
//        $q12->modul='Module12';
//        $q12->points=["svojom voljom"=>3,"zahvaljujući tvorcu testa"=>2];
//        $q12->final=1;
//        $q12->qid='psiho12';
//      $questionData[]=$q12;
//      
//                $q1=new \Classes\QuestionData();
//        $q1->question="";
//        $q1->hint="Radio button, multiple choice";
//        //$q3->multi=1;
//     
//        //$q1->story=true;
//        $q1->answers=['q1'];
//        $q1->sentences=['q1'=>'<span>1. Look the picture carefully and tell me what you see.</span><br><br>'];
//        $q1->image='1.png';
//    //    $q3->options=['q1'=>[['-- Choose --','did visit','has visited','visited','should visit']],'q2'=>[['-- Choose --','to not be','not to be','that not be','that be not']],'q3'=>[['-- Choose --','couldn’t possibly be','mustn’t possibly be','shouldn’t possibly be','isn’t possibly']],'q4'=>[['-- Choose --','farer','farest','farther','farthest']],'q5'=>[['-- Choose --','our','ours'],['-- Choose --','her','hers']],'q6'=>[['-- Choose --','be able','have','can','need']],'q7'=>[['-- Choose --','This old table’s legs','The legs of this old table','This old table legs','The legs of this old table’s']],'q8'=>[['-- Choose --','nobody','no one','anyone','somebody']]];
//      $q1->options=['q1'=>['I see white background','I see a black circular line - circle','I see a circle in the square','I do not see anything interesting']];
//        // $q3->additionalData=["images"=>['Restaurant','Hotel','Store']];
//        $q1->questionType='radioSentence';
//       // $q1->correctAnswers=['q1'=>'vidim belu pozadinu','q2'=>'vidim crnu kružnu liniju – krug','q3'=>'vidim krug u kvadratu','q4'=>'ne vidim ništa interesantno'];
//        $q1->modul='Module1';
//        $q1->points=["I see white background"=>1,"I see a black circular line - circle"=>2,"I see a circle in the square"=>3,"I do not see anything interesting"=>4];
//        $q1->final=0;
//        $q1->qid='psiho1';
//        $questionData[]=$q1;
//                        $q2=new \Classes\QuestionData();
//        $q2->question="";
//        $q2->hint="Radio button, multiple choice";
//        //$q3->multi=1;
//     
//        //$q1->story=true;
//        $q2->answers=['q1'];
//        $q2->sentences=['q1'=>'<span>2. There is a door is before you. Look at it for a few seconds, and then imagine what would be behind, if it opened.</span><br><br>'];
//        $q2->image='2.png';
//    //    $q3->options=['q1'=>[['-- Choose --','did visit','has visited','visited','should visit']],'q2'=>[['-- Choose --','to not be','not to be','that not be','that be not']],'q3'=>[['-- Choose --','couldn’t possibly be','mustn’t possibly be','shouldn’t possibly be','isn’t possibly']],'q4'=>[['-- Choose --','farer','farest','farther','farthest']],'q5'=>[['-- Choose --','our','ours'],['-- Choose --','her','hers']],'q6'=>[['-- Choose --','be able','have','can','need']],'q7'=>[['-- Choose --','This old table’s legs','The legs of this old table','This old table legs','The legs of this old table’s']],'q8'=>[['-- Choose --','nobody','no one','anyone','somebody']]];
//      $q2->options=['q1'=>['worry ','serenity','it does not cause anything','laughter']];
//        // $q3->additionalData=["images"=>['Restaurant','Hotel','Store']];
//        $q2->questionType='radioSentence';
//      //  $q1->correctAnswers=['q1'=>'vidim belu pozadinu','q2'=>'vidim crnu kružnu liniju – krug','q3'=>'vidim krug u kvadratu','q4'=>'ne vidim ništa interesantno'];
//        $q2->modul='Module2';
//        $q2->points=["worry"=>2,"serenity"=>3,"it does not cause anything"=>4,"laughter"=>1];
//        $q2->final=0;
//        $q2->qid='psiho2';
//      $questionData[]=$q2;
//                           $q3=new \Classes\QuestionData();
//        $q3->question="";
//        $q3->hint="Radio button, multiple choice";
//        //$q3->multi=1;
//     
//        //$q1->story=true;
//        $q3->answers=['q1'];
//        $q3->sentences=['q1'=>'<span>3. If the lightbulb in the apartment burns out, I:</span><br><br>'];
//        //$q3->image='2.png';
//    //    $q3->options=['q1'=>[['-- Choose --','did visit','has visited','visited','should visit']],'q2'=>[['-- Choose --','to not be','not to be','that not be','that be not']],'q3'=>[['-- Choose --','couldn’t possibly be','mustn’t possibly be','shouldn’t possibly be','isn’t possibly']],'q4'=>[['-- Choose --','farer','farest','farther','farthest']],'q5'=>[['-- Choose --','our','ours'],['-- Choose --','her','hers']],'q6'=>[['-- Choose --','be able','have','can','need']],'q7'=>[['-- Choose --','This old table’s legs','The legs of this old table','This old table legs','The legs of this old table’s']],'q8'=>[['-- Choose --','nobody','no one','anyone','somebody']]];
//      $q3->options=['q1'=>['I have never changed a light bulb','I ask someone to replace it','I wait for someone to tell me to replace it','I immediately notice and replace it']];
//        // $q3->additionalData=["images"=>['Restaurant','Hotel','Store']];
//        $q3->questionType='radioSentence';
//      //  $q1->correctAnswers=['q1'=>'vidim belu pozadinu','q2'=>'vidim crnu kružnu liniju – krug','q3'=>'vidim krug u kvadratu','q4'=>'ne vidim ništa interesantno'];
//        $q3->modul='Module3';
//        $q3->points=["I have never changed a light bulb"=>4,"I ask someone to replace it"=>1,"I wait for someone to tell me to replace it"=>2,"I immediately notice and replace it"=>3];
//        $q3->final=0;
//        $q3->qid='psiho3';
//      $questionData[]=$q3;
//         $q4=new \Classes\QuestionData();
//        $q4->question="4. There is a fruitful apple tree before you. Choose one fruit (an apple) for yourself.";
//        $q4->hint="Click and choose";
//        //$q3->multi=1;
//     
//        //$q1->story=true;
//        $q4->answers=['q1'];
//        $q4->sentences=['q1'=>'<span>4. There is a fruitful apple tree before you. Choose one fruit (an apple) for yourself.</span><br><br>'];
//        //$q3->image='2.png';
//    //    $q3->options=['q1'=>[['-- Choose --','did visit','has visited','visited','should visit']],'q2'=>[['-- Choose --','to not be','not to be','that not be','that be not']],'q3'=>[['-- Choose --','couldn’t possibly be','mustn’t possibly be','shouldn’t possibly be','isn’t possibly']],'q4'=>[['-- Choose --','farer','farest','farther','farthest']],'q5'=>[['-- Choose --','our','ours'],['-- Choose --','her','hers']],'q6'=>[['-- Choose --','be able','have','can','need']],'q7'=>[['-- Choose --','This old table’s legs','The legs of this old table','This old table legs','The legs of this old table’s']],'q8'=>[['-- Choose --','nobody','no one','anyone','somebody']]];
//      $q4->options=['q1'=>['1','2','3','4','5','6','7','8','9']];
//        // $q3->additionalData=["images"=>['Restaurant','Hotel','Store']];
//        $q4->questionType='clickArea';
//      //  $q1->correctAnswers=['q1'=>'vidim belu pozadinu','q2'=>'vidim crnu kružnu liniju – krug','q3'=>'vidim krug u kvadratu','q4'=>'ne vidim ništa interesantno'];
//        $q4->modul='Module4';
//        $q4->points=["1"=>2,"2"=>2,"3"=>1,"4"=>4,"5"=>4,"6"=>1,"7"=>3,"8"=>3,"9"=>3];
//        $q4->final=0;
//        $q4->qid='psiho4';
//      $questionData[]=$q4;
//      $q5=new \Classes\QuestionData();
//        $q5->question="";
//        $q5->hint="Radio button, multiple choice";
//        //$q3->multi=1;
//     
//        //$q1->story=true;
//        $q5->answers=['q1'];
//        $q5->sentences=['q1'=>'<span>5. What keeps you awake at night:</span><br><br>'];
//        //$q3->image='2.png';
//    //    $q3->options=['q1'=>[['-- Choose --','did visit','has visited','visited','should visit']],'q2'=>[['-- Choose --','to not be','not to be','that not be','that be not']],'q3'=>[['-- Choose --','couldn’t possibly be','mustn’t possibly be','shouldn’t possibly be','isn’t possibly']],'q4'=>[['-- Choose --','farer','farest','farther','farthest']],'q5'=>[['-- Choose --','our','ours'],['-- Choose --','her','hers']],'q6'=>[['-- Choose --','be able','have','can','need']],'q7'=>[['-- Choose --','This old table’s legs','The legs of this old table','This old table legs','The legs of this old table’s']],'q8'=>[['-- Choose --','nobody','no one','anyone','somebody']]];
//      $q5->options=['q1'=>['the idea that you are not the best at everything ','feeling that you were not "good enough" in communication with others','the list of activities for the upcoming day','the fact that the day was riddled with your mistakes']];
//        // $q3->additionalData=["images"=>['Restaurant','Hotel','Store']];
//        $q5->questionType='radioSentence';
//      //  $q1->correctAnswers=['q1'=>'vidim belu pozadinu','q2'=>'vidim crnu kružnu liniju – krug','q3'=>'vidim krug u kvadratu','q4'=>'ne vidim ništa interesantno'];
//        $q5->modul='Module5';
//        $q5->points=["the idea that you are not the best at everything"=>4,'feeling that you were not "good enough" in communication with others'=>1,"the list of activities for the upcoming day"=>3,"the fact that the day was riddled with your mistakes"=>2];
//        $q5->final=0;
//        $q5->qid='psiho5';
//      $questionData[]=$q5;
//      $q6=new \Classes\QuestionData();
//        $q6->question="";
//        $q6->hint="Radio button, multiple choice";
//        //$q3->multi=1;
//     
//        //$q1->story=true;
//        $q6->answers=['q1'];
//        $q6->sentences=['q1'=>'<span>6. I know the exact number of boys and girls in the class:</span><br><br>'];
//        //$q3->image='2.png';
//    //    $q3->options=['q1'=>[['-- Choose --','did visit','has visited','visited','should visit']],'q2'=>[['-- Choose --','to not be','not to be','that not be','that be not']],'q3'=>[['-- Choose --','couldn’t possibly be','mustn’t possibly be','shouldn’t possibly be','isn’t possibly']],'q4'=>[['-- Choose --','farer','farest','farther','farthest']],'q5'=>[['-- Choose --','our','ours'],['-- Choose --','her','hers']],'q6'=>[['-- Choose --','be able','have','can','need']],'q7'=>[['-- Choose --','This old table’s legs','The legs of this old table','This old table legs','The legs of this old table’s']],'q8'=>[['-- Choose --','nobody','no one','anyone','somebody']]];
//      $q6->options=['q1'=>['YES','NO']];
//        // $q3->additionalData=["images"=>['Restaurant','Hotel','Store']];
//        $q6->questionType='radioSentence';
//      //  $q1->correctAnswers=['q1'=>'vidim belu pozadinu','q2'=>'vidim crnu kružnu liniju – krug','q3'=>'vidim krug u kvadratu','q4'=>'ne vidim ništa interesantno'];
//        $q6->modul='Module6';
//        $q6->points=["YES"=>3,"NO"=>2];
//        $q6->final=0;
//        $q6->qid='psiho6';
//      $questionData[]=$q6;
//                        $q7=new \Classes\QuestionData();
//        $q7->question="";
//        $q7->hint="Radio button, multiple choice";
//        //$q3->multi=1;
//     
//        //$q1->story=true;
//        $q7->answers=['q1'];
//        $q7->sentences=['q1'=>'<span>7. There are many people on the photo. How many of your friends are there among them?</span><br><br>'];
//        $q7->image='7.png';
//    //    $q3->options=['q1'=>[['-- Choose --','did visit','has visited','visited','should visit']],'q2'=>[['-- Choose --','to not be','not to be','that not be','that be not']],'q3'=>[['-- Choose --','couldn’t possibly be','mustn’t possibly be','shouldn’t possibly be','isn’t possibly']],'q4'=>[['-- Choose --','farer','farest','farther','farthest']],'q5'=>[['-- Choose --','our','ours'],['-- Choose --','her','hers']],'q6'=>[['-- Choose --','be able','have','can','need']],'q7'=>[['-- Choose --','This old table’s legs','The legs of this old table','This old table legs','The legs of this old table’s']],'q8'=>[['-- Choose --','nobody','no one','anyone','somebody']]];
//      $q7->options=['q1'=>['I see my friends','my friends are not in this group of people','my friends have faces, and these people do not']];
//        // $q3->additionalData=["images"=>['Restaurant','Hotel','Store']];
//        $q7->questionType='radioSentence';
//      //  $q1->correctAnswers=['q1'=>'vidim belu pozadinu','q2'=>'vidim crnu kružnu liniju – krug','q3'=>'vidim krug u kvadratu','q4'=>'ne vidim ništa interesantno'];
//        $q7->modul='Module7';
//        $q7->points=["I see my friends"=>3,"my friends are not in this group of people"=>4,"my friends have faces, and these people do not"=>1];
//        $q7->final=0;
//        $q7->qid='psiho7';
//      $questionData[]=$q7;
//      
//                              $q8=new \Classes\QuestionData();
//        $q8->question="";
//        $q8->hint="Radio button, multiple choice";
//        //$q3->multi=1;
//     
//        //$q1->story=true;
//        $q8->answers=['q1'];
//        $q8->sentences=['q1'=>'<span>8. Watch the photograph for a few seconds. Which part of the photo remains in the focus of your observation even after you have stopped looking:</span><br><br>'];
//        $q8->image='8.png';
//    //    $q3->options=['q1'=>[['-- Choose --','did visit','has visited','visited','should visit']],'q2'=>[['-- Choose --','to not be','not to be','that not be','that be not']],'q3'=>[['-- Choose --','couldn’t possibly be','mustn’t possibly be','shouldn’t possibly be','isn’t possibly']],'q4'=>[['-- Choose --','farer','farest','farther','farthest']],'q5'=>[['-- Choose --','our','ours'],['-- Choose --','her','hers']],'q6'=>[['-- Choose --','be able','have','can','need']],'q7'=>[['-- Choose --','This old table’s legs','The legs of this old table','This old table legs','The legs of this old table’s']],'q8'=>[['-- Choose --','nobody','no one','anyone','somebody']]];
//      $q8->options=['q1'=>['the sun','the man','the river','none but the association imposed on me by the photo']];
//        // $q3->additionalData=["images"=>['Restaurant','Hotel','Store']];
//        $q8->questionType='radioSentence';
//      //  $q1->correctAnswers=['q1'=>'vidim belu pozadinu','q2'=>'vidim crnu kružnu liniju – krug','q3'=>'vidim krug u kvadratu','q4'=>'ne vidim ništa interesantno'];
//        $q8->modul='Module8';
//        $q8->points=["the sun"=>3,"the man"=>4,"the river"=>2,"none but the association imposed on me by the photo"=>1];
//        $q8->final=0;
//        $q8->qid='psiho8';
//      $questionData[]=$q8;
//         $q9=new \Classes\QuestionData();
//        $q9->question="";
//        $q9->hint="Radio button, multiple choice";
//        //$q3->multi=1;
//     
//        //$q1->story=true;
//        $q9->answers=['q1'];
//        $q9->sentences=['q1'=>'<span>9. There are two paths ahead of you. Your friends tell you to take the one on the left, your parents - the one on the right. You decide:</span><br><br>'];
//        $q9->image='9.png';
//    //    $q3->options=['q1'=>[['-- Choose --','did visit','has visited','visited','should visit']],'q2'=>[['-- Choose --','to not be','not to be','that not be','that be not']],'q3'=>[['-- Choose --','couldn’t possibly be','mustn’t possibly be','shouldn’t possibly be','isn’t possibly']],'q4'=>[['-- Choose --','farer','farest','farther','farthest']],'q5'=>[['-- Choose --','our','ours'],['-- Choose --','her','hers']],'q6'=>[['-- Choose --','be able','have','can','need']],'q7'=>[['-- Choose --','This old table’s legs','The legs of this old table','This old table legs','The legs of this old table’s']],'q8'=>[['-- Choose --','nobody','no one','anyone','somebody']]];
//      $q9->options=['q1'=>['to stop and think a bit a little bit longer, and then make the choice without the influence of either your parents or friends','to listen to your friends','to listen to your parents','you give up on taking any of them, because you do not want to hurt either your parents or your friends']];
//        // $q3->additionalData=["images"=>['Restaurant','Hotel','Store']];
//        $q9->questionType='radioSentence';
//      //  $q1->correctAnswers=['q1'=>'vidim belu pozadinu','q2'=>'vidim crnu kružnu liniju – krug','q3'=>'vidim krug u kvadratu','q4'=>'ne vidim ništa interesantno'];
//        $q9->modul='Module9';
//        $q9->points=["to stop and think a bit a little bit longer, and then make the choice without the influence of either your parents or friends"=>3,"to listen to your friends"=>1,"to listen to your parents"=>4,"you give up on taking any of them, because you do not want to hurt either your parents or your friends"=>2];
//        $q9->final=0;
//        $q9->qid='psiho9';
//      $questionData[]=$q9;
//      $q10=new \Classes\QuestionData();
//        $q10->question="";
//        $q10->hint="Radio button, multiple choice";
//        //$q3->multi=1;
//     
//        //$q1->story=true;
//        $q10->answers=['q1'];
//        $q10->sentences=['q1'=>'<span>10. In the image you’re looking at, the facial expressions of different emotions are marked with numbers. Try to recall the previous month of your life, and then pick the number next to the face that most authentically describes that memory.</span><br><br>'];
//        $q10->image='10.png';
//    //    $q3->options=['q1'=>[['-- Choose --','did visit','has visited','visited','should visit']],'q2'=>[['-- Choose --','to not be','not to be','that not be','that be not']],'q3'=>[['-- Choose --','couldn’t possibly be','mustn’t possibly be','shouldn’t possibly be','isn’t possibly']],'q4'=>[['-- Choose --','farer','farest','farther','farthest']],'q5'=>[['-- Choose --','our','ours'],['-- Choose --','her','hers']],'q6'=>[['-- Choose --','be able','have','can','need']],'q7'=>[['-- Choose --','This old table’s legs','The legs of this old table','This old table legs','The legs of this old table’s']],'q8'=>[['-- Choose --','nobody','no one','anyone','somebody']]];
//      $q10->options=['q1'=>['1','2','3','4','5','6']];
//        // $q3->additionalData=["images"=>['Restaurant','Hotel','Store']];
//        $q10->questionType='radioSentence';
//      //  $q1->correctAnswers=['q1'=>'vidim belu pozadinu','q2'=>'vidim crnu kružnu liniju – krug','q3'=>'vidim krug u kvadratu','q4'=>'ne vidim ništa interesantno'];
//        $q10->modul='Module10';
//        $q10->points=["1"=>2,"2"=>1,"3"=>3,"4"=>4,"5"=>4,"6"=>2];
//        $q10->final=0;
//        $q10->qid='psiho10';
//      $questionData[]=$q10;
//       $q11=new \Classes\QuestionData();
//        $q11->question="";
//        $q11->hint="Radio button, multiple choice";
//        //$q3->multi=1;
//     
//        //$q1->story=true;
//        $q11->answers=['q1'];
//        $q11->sentences=['q1'=>'<span>11. You feel pleasant when:</span><br><br>'];
//        //$q3->image='2.png';
//    //    $q3->options=['q1'=>[['-- Choose --','did visit','has visited','visited','should visit']],'q2'=>[['-- Choose --','to not be','not to be','that not be','that be not']],'q3'=>[['-- Choose --','couldn’t possibly be','mustn’t possibly be','shouldn’t possibly be','isn’t possibly']],'q4'=>[['-- Choose --','farer','farest','farther','farthest']],'q5'=>[['-- Choose --','our','ours'],['-- Choose --','her','hers']],'q6'=>[['-- Choose --','be able','have','can','need']],'q7'=>[['-- Choose --','This old table’s legs','The legs of this old table','This old table legs','The legs of this old table’s']],'q8'=>[['-- Choose --','nobody','no one','anyone','somebody']]];
//      $q11->options=['q1'=>['you know what others expect from you','you are in the company of your friends','communicate with others','nobody is watching you']];
//        // $q3->additionalData=["images"=>['Restaurant','Hotel','Store']];
//        $q11->questionType='radioSentence';
//      //  $q1->correctAnswers=['q1'=>'vidim belu pozadinu','q2'=>'vidim crnu kružnu liniju – krug','q3'=>'vidim krug u kvadratu','q4'=>'ne vidim ništa interesantno'];
//        $q11->modul='Module11';
//        $q11->points=["you know what others expect from you"=>4,"you are in the company of your friends"=>1,"communicate with others"=>3,"nobody is watching you"=>2];
//        $q11->final=0;
//        $q11->qid='psiho11';
//      $questionData[]=$q11;
//      
//       $q12=new \Classes\QuestionData();
//        $q12->question="";
//        $q12->hint="Radio button, multiple choice";
//        //$q3->multi=1;
//     
//        //$q1->story=true;
//        $q12->answers=['q1'];
//        $q12->sentences=['q1'=>'<span>12. I came to the end of this test:</span><br><br>'];
//        //$q3->image='2.png';
//    //    $q3->options=['q1'=>[['-- Choose --','did visit','has visited','visited','should visit']],'q2'=>[['-- Choose --','to not be','not to be','that not be','that be not']],'q3'=>[['-- Choose --','couldn’t possibly be','mustn’t possibly be','shouldn’t possibly be','isn’t possibly']],'q4'=>[['-- Choose --','farer','farest','farther','farthest']],'q5'=>[['-- Choose --','our','ours'],['-- Choose --','her','hers']],'q6'=>[['-- Choose --','be able','have','can','need']],'q7'=>[['-- Choose --','This old table’s legs','The legs of this old table','This old table legs','The legs of this old table’s']],'q8'=>[['-- Choose --','nobody','no one','anyone','somebody']]];
//      $q12->options=['q1'=>['on my own','thanks to the creator of the test']];
//        // $q3->additionalData=["images"=>['Restaurant','Hotel','Store']];
//        $q12->questionType='radioSentence';
//      //  $q1->correctAnswers=['q1'=>'vidim belu pozadinu','q2'=>'vidim crnu kružnu liniju – krug','q3'=>'vidim krug u kvadratu','q4'=>'ne vidim ništa interesantno'];
//        $q12->modul='Module12';
//        $q12->points=["on my own"=>3,"thanks to the creator of the test"=>2];
//        $q12->final=1;
//        $q12->qid='psiho12';
//      $questionData[]=$q12;
//      
//    $q1=new \Classes\QuestionData();
//        $q1->question="";
//        $q1->hint="Radio button, multiple choice";
//        //$q3->multi=1;
//     
//        //$q1->story=true;
//        $q1->answers=['q1'];
//        $q1->sentences=['q1'=>'<span>1. Uživam u organizovanju nekog posla</span><br><br>'];
//        //$q1->image='1.png';
//    //    $q3->options=['q1'=>[['-- Choose --','did visit','has visited','visited','should visit']],'q2'=>[['-- Choose --','to not be','not to be','that not be','that be not']],'q3'=>[['-- Choose --','couldn’t possibly be','mustn’t possibly be','shouldn’t possibly be','isn’t possibly']],'q4'=>[['-- Choose --','farer','farest','farther','farthest']],'q5'=>[['-- Choose --','our','ours'],['-- Choose --','her','hers']],'q6'=>[['-- Choose --','be able','have','can','need']],'q7'=>[['-- Choose --','This old table’s legs','The legs of this old table','This old table legs','The legs of this old table’s']],'q8'=>[['-- Choose --','nobody','no one','anyone','somebody']]];
//      $q1->options=['q1'=>['Uopšte se ne slažem','Uglavnom se ne slažem','Uglavnom se slažem','U potpunosti se slažem']];
//        // $q3->additionalData=["images"=>['Restaurant','Hotel','Store']];
//        $q1->questionType='radioSentence';
//       // $q1->correctAnswers=['q1'=>'vidim belu pozadinu','q2'=>'vidim crnu kružnu liniju – krug','q3'=>'vidim krug u kvadratu','q4'=>'ne vidim ništa interesantno'];
//        $q1->modul='Module1';
//        $q1->points=["Uopšte se ne slažem"=>1,"Uglavnom se ne slažem"=>2,"Uglavnom se slažem"=>3,"U potpunosti se slažem"=>4];
//        $q1->final=0;
//        $q1->qid='psiho21';
//        $questionData[]=$q1;
//        
//        $q2=new \Classes\QuestionData();
//        $q2->question="";
//        $q2->hint="Radio button, multiple choice";
//        //$q3->multi=1;
//     
//        //$q1->story=true;
//        $q2->answers=['q1'];
//        $q2->sentences=['q1'=>'<span>2. Obraćam pažnju na detalje u poslu I posvećujem im se koliko god da je potrebno</span><br><br>'];
//        //$q1->image='1.png';
//    //    $q3->options=['q1'=>[['-- Choose --','did visit','has visited','visited','should visit']],'q2'=>[['-- Choose --','to not be','not to be','that not be','that be not']],'q3'=>[['-- Choose --','couldn’t possibly be','mustn’t possibly be','shouldn’t possibly be','isn’t possibly']],'q4'=>[['-- Choose --','farer','farest','farther','farthest']],'q5'=>[['-- Choose --','our','ours'],['-- Choose --','her','hers']],'q6'=>[['-- Choose --','be able','have','can','need']],'q7'=>[['-- Choose --','This old table’s legs','The legs of this old table','This old table legs','The legs of this old table’s']],'q8'=>[['-- Choose --','nobody','no one','anyone','somebody']]];
//      $q2->options=['q1'=>['Uopšte se ne slažem','Uglavnom se ne slažem','Uglavnom se slažem','U potpunosti se slažem']];
//        // $q3->additionalData=["images"=>['Restaurant','Hotel','Store']];
//        $q2->questionType='radioSentence';
//       // $q1->correctAnswers=['q1'=>'vidim belu pozadinu','q2'=>'vidim crnu kružnu liniju – krug','q3'=>'vidim krug u kvadratu','q4'=>'ne vidim ništa interesantno'];
//        $q2->modul='Module2';
//        $q2->points=["Uopšte se ne slažem"=>1,"Uglavnom se ne slažem"=>2,"Uglavnom se slažem"=>3,"U potpunosti se slažem"=>4];
//        $q2->final=0;
//        $q2->qid='psiho22';
//        $questionData[]=$q2;
//        
//        $q3=new \Classes\QuestionData();
//        $q3->question="";
//        $q3->hint="Radio button, multiple choice";
//        //$q3->multi=1;
//     
//        //$q1->story=true;
//        $q3->answers=['q1'];
//        $q3->sentences=['q1'=>'<span>3. Uvek me ponese mogućnost novog projekta</span><br><br>'];
//        //$q1->image='1.png';
//    //    $q3->options=['q1'=>[['-- Choose --','did visit','has visited','visited','should visit']],'q2'=>[['-- Choose --','to not be','not to be','that not be','that be not']],'q3'=>[['-- Choose --','couldn’t possibly be','mustn’t possibly be','shouldn’t possibly be','isn’t possibly']],'q4'=>[['-- Choose --','farer','farest','farther','farthest']],'q5'=>[['-- Choose --','our','ours'],['-- Choose --','her','hers']],'q6'=>[['-- Choose --','be able','have','can','need']],'q7'=>[['-- Choose --','This old table’s legs','The legs of this old table','This old table legs','The legs of this old table’s']],'q8'=>[['-- Choose --','nobody','no one','anyone','somebody']]];
//      $q3->options=['q1'=>['Uopšte se ne slažem','Uglavnom se ne slažem','Uglavnom se slažem','U potpunosti se slažem']];
//        // $q3->additionalData=["images"=>['Restaurant','Hotel','Store']];
//        $q3->questionType='radioSentence';
//       // $q1->correctAnswers=['q1'=>'vidim belu pozadinu','q2'=>'vidim crnu kružnu liniju – krug','q3'=>'vidim krug u kvadratu','q4'=>'ne vidim ništa interesantno'];
//        $q3->modul='Module3';
//        $q3->points=["Uopšte se ne slažem"=>1,"Uglavnom se ne slažem"=>2,"Uglavnom se slažem"=>3,"U potpunosti se slažem"=>4];
//        $q3->final=0;
//        $q3->qid='psiho23';
//        $questionData[]=$q3;
//        
//                $q4=new \Classes\QuestionData();
//        $q4->question="";
//        $q4->hint="Radio button, multiple choice";
//        //$q3->multi=1;
//     
//        //$q1->story=true;
//        $q4->answers=['q1'];
//        $q4->sentences=['q1'=>'<span>4. Važno mi je da su mi u poslu definisana pravila koja ću ja slediti</span><br><br>'];
//        //$q1->image='1.png';
//    //    $q3->options=['q1'=>[['-- Choose --','did visit','has visited','visited','should visit']],'q2'=>[['-- Choose --','to not be','not to be','that not be','that be not']],'q3'=>[['-- Choose --','couldn’t possibly be','mustn’t possibly be','shouldn’t possibly be','isn’t possibly']],'q4'=>[['-- Choose --','farer','farest','farther','farthest']],'q5'=>[['-- Choose --','our','ours'],['-- Choose --','her','hers']],'q6'=>[['-- Choose --','be able','have','can','need']],'q7'=>[['-- Choose --','This old table’s legs','The legs of this old table','This old table legs','The legs of this old table’s']],'q8'=>[['-- Choose --','nobody','no one','anyone','somebody']]];
//      $q4->options=['q1'=>['Uopšte se ne slažem','Uglavnom se ne slažem','Uglavnom se slažem','U potpunosti se slažem']];
//        // $q3->additionalData=["images"=>['Restaurant','Hotel','Store']];
//        $q4->questionType='radioSentence';
//       // $q1->correctAnswers=['q1'=>'vidim belu pozadinu','q2'=>'vidim crnu kružnu liniju – krug','q3'=>'vidim krug u kvadratu','q4'=>'ne vidim ništa interesantno'];
//        $q4->modul='Module4';
//        $q4->points=["Uopšte se ne slažem"=>1,"Uglavnom se ne slažem"=>2,"Uglavnom se slažem"=>3,"U potpunosti se slažem"=>4];
//        $q4->final=0;
//        $q4->qid='psiho24';
//        $questionData[]=$q4;
//        
//                      $q5=new \Classes\QuestionData();
//        $q5->question="";
//        $q5->hint="Radio button, multiple choice";
//        //$q3->multi=1;
//     
//        //$q1->story=true;
//        $q5->answers=['q1'];
//        $q5->sentences=['q1'=>'<span>5. Preuzimam inicijativu da temu sastanka vratim na početak ako primetime da gubimo vreme</span><br><br>'];
//        //$q1->image='1.png';
//    //    $q3->options=['q1'=>[['-- Choose --','did visit','has visited','visited','should visit']],'q2'=>[['-- Choose --','to not be','not to be','that not be','that be not']],'q3'=>[['-- Choose --','couldn’t possibly be','mustn’t possibly be','shouldn’t possibly be','isn’t possibly']],'q4'=>[['-- Choose --','farer','farest','farther','farthest']],'q5'=>[['-- Choose --','our','ours'],['-- Choose --','her','hers']],'q6'=>[['-- Choose --','be able','have','can','need']],'q7'=>[['-- Choose --','This old table’s legs','The legs of this old table','This old table legs','The legs of this old table’s']],'q8'=>[['-- Choose --','nobody','no one','anyone','somebody']]];
//      $q5->options=['q1'=>['Uopšte se ne slažem','Uglavnom se ne slažem','Uglavnom se slažem','U potpunosti se slažem']];
//        // $q3->additionalData=["images"=>['Restaurant','Hotel','Store']];
//        $q5->questionType='radioSentence';
//       // $q1->correctAnswers=['q1'=>'vidim belu pozadinu','q2'=>'vidim crnu kružnu liniju – krug','q3'=>'vidim krug u kvadratu','q4'=>'ne vidim ništa interesantno'];
//        $q5->modul='Module5';
//        $q5->points=["Uopšte se ne slažem"=>1,"Uglavnom se ne slažem"=>2,"Uglavnom se slažem"=>3,"U potpunosti se slažem"=>4];
//        $q5->final=0;
//        $q5->qid='psiho25';
//        $questionData[]=$q5;
//        
//                              $q6=new \Classes\QuestionData();
//        $q6->question="";
//        $q6->hint="Radio button, multiple choice";
//        //$q3->multi=1;
//     
//        //$q1->story=true;
//        $q6->answers=['q1'];
//        $q6->sentences=['q1'=>'<span>6. Svačiju ideju sam spreman da podržim ako dovodi do rešavanja problema</span><br><br>'];
//        //$q1->image='1.png';
//    //    $q3->options=['q1'=>[['-- Choose --','did visit','has visited','visited','should visit']],'q2'=>[['-- Choose --','to not be','not to be','that not be','that be not']],'q3'=>[['-- Choose --','couldn’t possibly be','mustn’t possibly be','shouldn’t possibly be','isn’t possibly']],'q4'=>[['-- Choose --','farer','farest','farther','farthest']],'q5'=>[['-- Choose --','our','ours'],['-- Choose --','her','hers']],'q6'=>[['-- Choose --','be able','have','can','need']],'q7'=>[['-- Choose --','This old table’s legs','The legs of this old table','This old table legs','The legs of this old table’s']],'q8'=>[['-- Choose --','nobody','no one','anyone','somebody']]];
//      $q6->options=['q1'=>['Uopšte se ne slažem','Uglavnom se ne slažem','Uglavnom se slažem','U potpunosti se slažem']];
//        // $q3->additionalData=["images"=>['Restaurant','Hotel','Store']];
//        $q6->questionType='radioSentence';
//       // $q1->correctAnswers=['q1'=>'vidim belu pozadinu','q2'=>'vidim crnu kružnu liniju – krug','q3'=>'vidim krug u kvadratu','q4'=>'ne vidim ništa interesantno'];
//        $q6->modul='Module6';
//        $q6->points=["Uopšte se ne slažem"=>1,"Uglavnom se ne slažem"=>2,"Uglavnom se slažem"=>3,"U potpunosti se slažem"=>4];
//        $q6->final=0;
//        $q6->qid='psiho26';
//        $questionData[]=$q6;
//        
//                               $q7=new \Classes\QuestionData();
//        $q7->question="";
//        $q7->hint="Radio button, multiple choice";
//        //$q3->multi=1;
//     
//        //$q1->story=true;
//        $q7->answers=['q1'];
//        $q7->sentences=['q1'=>'<span>7. Gotovo da ne postoji osoba sa kojom ja ne mogu da sarađujem</span><br><br>'];
//        $q7->image='2/07.jpg';
//    //    $q3->options=['q1'=>[['-- Choose --','did visit','has visited','visited','should visit']],'q2'=>[['-- Choose --','to not be','not to be','that not be','that be not']],'q3'=>[['-- Choose --','couldn’t possibly be','mustn’t possibly be','shouldn’t possibly be','isn’t possibly']],'q4'=>[['-- Choose --','farer','farest','farther','farthest']],'q5'=>[['-- Choose --','our','ours'],['-- Choose --','her','hers']],'q6'=>[['-- Choose --','be able','have','can','need']],'q7'=>[['-- Choose --','This old table’s legs','The legs of this old table','This old table legs','The legs of this old table’s']],'q8'=>[['-- Choose --','nobody','no one','anyone','somebody']]];
//      $q7->options=['q1'=>['Uopšte se ne slažem','Uglavnom se ne slažem','Uglavnom se slažem','U potpunosti se slažem']];
//        // $q3->additionalData=["images"=>['Restaurant','Hotel','Store']];
//        $q7->questionType='radioSentence';
//       // $q1->correctAnswers=['q1'=>'vidim belu pozadinu','q2'=>'vidim crnu kružnu liniju – krug','q3'=>'vidim krug u kvadratu','q4'=>'ne vidim ništa interesantno'];
//        $q7->modul='Module7';
//        $q7->points=["Uopšte se ne slažem"=>1,"Uglavnom se ne slažem"=>2,"Uglavnom se slažem"=>3,"U potpunosti se slažem"=>4];
//        $q7->final=0;
//        $q7->qid='psiho27';
//        $questionData[]=$q7;
//        
//                                       $q8=new \Classes\QuestionData();
//        $q8->question="";
//        $q8->hint="Radio button, multiple choice";
//        //$q3->multi=1;
//     
//        //$q1->story=true;
//        $q8->answers=['q1'];
//        $q8->sentences=['q1'=>'<span>8. Volim da radim sam</span><br><br>'];
//      //  $q8->image='2/7.jpg';
//    //    $q3->options=['q1'=>[['-- Choose --','did visit','has visited','visited','should visit']],'q2'=>[['-- Choose --','to not be','not to be','that not be','that be not']],'q3'=>[['-- Choose --','couldn’t possibly be','mustn’t possibly be','shouldn’t possibly be','isn’t possibly']],'q4'=>[['-- Choose --','farer','farest','farther','farthest']],'q5'=>[['-- Choose --','our','ours'],['-- Choose --','her','hers']],'q6'=>[['-- Choose --','be able','have','can','need']],'q7'=>[['-- Choose --','This old table’s legs','The legs of this old table','This old table legs','The legs of this old table’s']],'q8'=>[['-- Choose --','nobody','no one','anyone','somebody']]];
//      $q8->options=['q1'=>['Uopšte se ne slažem','Uglavnom se ne slažem','Uglavnom se slažem','U potpunosti se slažem']];
//        // $q3->additionalData=["images"=>['Restaurant','Hotel','Store']];
//        $q8->questionType='radioSentence';
//       // $q1->correctAnswers=['q1'=>'vidim belu pozadinu','q2'=>'vidim crnu kružnu liniju – krug','q3'=>'vidim krug u kvadratu','q4'=>'ne vidim ništa interesantno'];
//        $q8->modul='Module8';
//        $q8->points=["Uopšte se ne slažem"=>1,"Uglavnom se ne slažem"=>2,"Uglavnom se slažem"=>3,"U potpunosti se slažem"=>4];
//        $q8->final=0;
//        $q8->qid='psiho28';
//        $questionData[]=$q8;
//        
//                                               $q9=new \Classes\QuestionData();
//        $q9->question="";
//        $q9->hint="Radio button, multiple choice";
//        //$q3->multi=1;
//     
//        //$q1->story=true;
//        $q9->answers=['q1'];
//        $q9->sentences=['q1'=>'<span>9. Uživam u mentorisanju I prenošenju znanja drugima</span><br><br>'];
//      //  $q8->image='2/7.jpg';
//    //    $q3->options=['q1'=>[['-- Choose --','did visit','has visited','visited','should visit']],'q2'=>[['-- Choose --','to not be','not to be','that not be','that be not']],'q3'=>[['-- Choose --','couldn’t possibly be','mustn’t possibly be','shouldn’t possibly be','isn’t possibly']],'q4'=>[['-- Choose --','farer','farest','farther','farthest']],'q5'=>[['-- Choose --','our','ours'],['-- Choose --','her','hers']],'q6'=>[['-- Choose --','be able','have','can','need']],'q7'=>[['-- Choose --','This old table’s legs','The legs of this old table','This old table legs','The legs of this old table’s']],'q8'=>[['-- Choose --','nobody','no one','anyone','somebody']]];
//      $q9->options=['q1'=>['Uopšte se ne slažem','Uglavnom se ne slažem','Uglavnom se slažem','U potpunosti se slažem']];
//        // $q3->additionalData=["images"=>['Restaurant','Hotel','Store']];
//        $q9->questionType='radioSentence';
//       // $q1->correctAnswers=['q1'=>'vidim belu pozadinu','q2'=>'vidim crnu kružnu liniju – krug','q3'=>'vidim krug u kvadratu','q4'=>'ne vidim ništa interesantno'];
//        $q9->modul='Module9';
//        $q9->points=["Uopšte se ne slažem"=>1,"Uglavnom se ne slažem"=>2,"Uglavnom se slažem"=>3,"U potpunosti se slažem"=>4];
//        $q9->final=0;
//        $q9->qid='psiho29';
//        $questionData[]=$q9;
//        
//                                                   $q10=new \Classes\QuestionData();
//        $q10->question="";
//        $q10->hint="Radio button, multiple choice";
//        //$q3->multi=1;
//     
//        //$q1->story=true;
//        $q10->answers=['q1'];
//        $q10->sentences=['q1'=>'<span>10. Važno mi je da imam uticaj na odluke</span><br><br>'];
//      //  $q8->image='2/7.jpg';
//    //    $q3->options=['q1'=>[['-- Choose --','did visit','has visited','visited','should visit']],'q2'=>[['-- Choose --','to not be','not to be','that not be','that be not']],'q3'=>[['-- Choose --','couldn’t possibly be','mustn’t possibly be','shouldn’t possibly be','isn’t possibly']],'q4'=>[['-- Choose --','farer','farest','farther','farthest']],'q5'=>[['-- Choose --','our','ours'],['-- Choose --','her','hers']],'q6'=>[['-- Choose --','be able','have','can','need']],'q7'=>[['-- Choose --','This old table’s legs','The legs of this old table','This old table legs','The legs of this old table’s']],'q8'=>[['-- Choose --','nobody','no one','anyone','somebody']]];
//      $q10->options=['q1'=>['Uopšte se ne slažem','Uglavnom se ne slažem','Uglavnom se slažem','U potpunosti se slažem']];
//        // $q3->additionalData=["images"=>['Restaurant','Hotel','Store']];
//        $q10->questionType='radioSentence';
//       // $q1->correctAnswers=['q1'=>'vidim belu pozadinu','q2'=>'vidim crnu kružnu liniju – krug','q3'=>'vidim krug u kvadratu','q4'=>'ne vidim ništa interesantno'];
//        $q10->modul='Module10';
//        $q10->points=["Uopšte se ne slažem"=>1,"Uglavnom se ne slažem"=>2,"Uglavnom se slažem"=>3,"U potpunosti se slažem"=>4];
//        $q10->final=0;
//        $q10->qid='psiho210';
//        $questionData[]=$q10;
//        
//                                                           $q11=new \Classes\QuestionData();
//        $q11->question="";
//        $q11->hint="Radio button, multiple choice";
//        //$q3->multi=1;
//     
//        //$q1->story=true;
//        $q11->answers=['q1'];
//        $q11->sentences=['q1'=>'<span>11. Važno mi je da pomognem kolegama kada imaju problem</span><br><br>'];
//      //  $q8->image='2/7.jpg';
//    //    $q3->options=['q1'=>[['-- Choose --','did visit','has visited','visited','should visit']],'q2'=>[['-- Choose --','to not be','not to be','that not be','that be not']],'q3'=>[['-- Choose --','couldn’t possibly be','mustn’t possibly be','shouldn’t possibly be','isn’t possibly']],'q4'=>[['-- Choose --','farer','farest','farther','farthest']],'q5'=>[['-- Choose --','our','ours'],['-- Choose --','her','hers']],'q6'=>[['-- Choose --','be able','have','can','need']],'q7'=>[['-- Choose --','This old table’s legs','The legs of this old table','This old table legs','The legs of this old table’s']],'q8'=>[['-- Choose --','nobody','no one','anyone','somebody']]];
//      $q11->options=['q1'=>['Uopšte se ne slažem','Uglavnom se ne slažem','Uglavnom se slažem','U potpunosti se slažem']];
//        // $q3->additionalData=["images"=>['Restaurant','Hotel','Store']];
//        $q11->questionType='radioSentence';
//       // $q1->correctAnswers=['q1'=>'vidim belu pozadinu','q2'=>'vidim crnu kružnu liniju – krug','q3'=>'vidim krug u kvadratu','q4'=>'ne vidim ništa interesantno'];
//        $q11->modul='Module11';
//        $q11->points=["Uopšte se ne slažem"=>1,"Uglavnom se ne slažem"=>2,"Uglavnom se slažem"=>3,"U potpunosti se slažem"=>4];
//        $q11->final=0;
//        $q11->qid='psiho211';
//        $questionData[]=$q11;
//        
//                                                          $q12=new \Classes\QuestionData();
//        $q12->question="";
//        $q12->hint="Radio button, multiple choice";
//        //$q3->multi=1;
//     
//        //$q1->story=true;
//        $q12->answers=['q1'];
//        $q12->sentences=['q1'=>'<span>12. Važna mi je praktičnost mnogo više od novih ideja</span><br><br>'];
//        $q12->image='2/12-b.jpg';
//    //    $q3->options=['q1'=>[['-- Choose --','did visit','has visited','visited','should visit']],'q2'=>[['-- Choose --','to not be','not to be','that not be','that be not']],'q3'=>[['-- Choose --','couldn’t possibly be','mustn’t possibly be','shouldn’t possibly be','isn’t possibly']],'q4'=>[['-- Choose --','farer','farest','farther','farthest']],'q5'=>[['-- Choose --','our','ours'],['-- Choose --','her','hers']],'q6'=>[['-- Choose --','be able','have','can','need']],'q7'=>[['-- Choose --','This old table’s legs','The legs of this old table','This old table legs','The legs of this old table’s']],'q8'=>[['-- Choose --','nobody','no one','anyone','somebody']]];
//      $q12->options=['q1'=>['Uopšte se ne slažem','Uglavnom se ne slažem','Uglavnom se slažem','U potpunosti se slažem']];
//        // $q3->additionalData=["images"=>['Restaurant','Hotel','Store']];
//        $q12->questionType='radioSentence';
//       // $q1->correctAnswers=['q1'=>'vidim belu pozadinu','q2'=>'vidim crnu kružnu liniju – krug','q3'=>'vidim krug u kvadratu','q4'=>'ne vidim ništa interesantno'];
//        $q12->modul='Module12';
//        $q12->points=["Uopšte se ne slažem"=>1,"Uglavnom se ne slažem"=>2,"Uglavnom se slažem"=>3,"U potpunosti se slažem"=>4];
//        $q12->final=0;
//        $q12->qid='psiho212';
//        $questionData[]=$q12;
//        
//                                                                  $q13=new \Classes\QuestionData();
//        $q13->question="";
//        $q13->hint="Radio button, multiple choice";
//        //$q3->multi=1;
//     
//        //$q1->story=true;
//        $q13->answers=['q1'];
//        $q13->sentences=['q1'=>'<span>13. Volim da održavam red</span><br><br>'];
//       // $q13->image='2/12-b.jpg';
//    //    $q3->options=['q1'=>[['-- Choose --','did visit','has visited','visited','should visit']],'q2'=>[['-- Choose --','to not be','not to be','that not be','that be not']],'q3'=>[['-- Choose --','couldn’t possibly be','mustn’t possibly be','shouldn’t possibly be','isn’t possibly']],'q4'=>[['-- Choose --','farer','farest','farther','farthest']],'q5'=>[['-- Choose --','our','ours'],['-- Choose --','her','hers']],'q6'=>[['-- Choose --','be able','have','can','need']],'q7'=>[['-- Choose --','This old table’s legs','The legs of this old table','This old table legs','The legs of this old table’s']],'q8'=>[['-- Choose --','nobody','no one','anyone','somebody']]];
//      $q13->options=['q1'=>['Uopšte se ne slažem','Uglavnom se ne slažem','Uglavnom se slažem','U potpunosti se slažem']];
//        // $q3->additionalData=["images"=>['Restaurant','Hotel','Store']];
//        $q13->questionType='radioSentence';
//       // $q1->correctAnswers=['q1'=>'vidim belu pozadinu','q2'=>'vidim crnu kružnu liniju – krug','q3'=>'vidim krug u kvadratu','q4'=>'ne vidim ništa interesantno'];
//        $q13->modul='Module13';
//        $q13->points=["Uopšte se ne slažem"=>1,"Uglavnom se ne slažem"=>2,"Uglavnom se slažem"=>3,"U potpunosti se slažem"=>4];
//        $q13->final=0;
//        $q13->qid='psiho213';
//        $questionData[]=$q13;
//        
//        $q14=new \Classes\QuestionData();
//        $q14->question="";
//        $q14->hint="Radio button, multiple choice";
//        //$q3->multi=1;
//     
//        //$q1->story=true;
//        $q14->answers=['q1'];
//        $q14->sentences=['q1'=>'<span>14. Pratim područja gde se mogu pojaviti problemi</span><br><br>'];
//       // $q13->image='2/12-b.jpg';
//    //    $q3->options=['q1'=>[['-- Choose --','did visit','has visited','visited','should visit']],'q2'=>[['-- Choose --','to not be','not to be','that not be','that be not']],'q3'=>[['-- Choose --','couldn’t possibly be','mustn’t possibly be','shouldn’t possibly be','isn’t possibly']],'q4'=>[['-- Choose --','farer','farest','farther','farthest']],'q5'=>[['-- Choose --','our','ours'],['-- Choose --','her','hers']],'q6'=>[['-- Choose --','be able','have','can','need']],'q7'=>[['-- Choose --','This old table’s legs','The legs of this old table','This old table legs','The legs of this old table’s']],'q8'=>[['-- Choose --','nobody','no one','anyone','somebody']]];
//      $q14->options=['q1'=>['Uopšte se ne slažem','Uglavnom se ne slažem','Uglavnom se slažem','U potpunosti se slažem']];
//        // $q3->additionalData=["images"=>['Restaurant','Hotel','Store']];
//        $q14->questionType='radioSentence';
//       // $q1->correctAnswers=['q1'=>'vidim belu pozadinu','q2'=>'vidim crnu kružnu liniju – krug','q3'=>'vidim krug u kvadratu','q4'=>'ne vidim ništa interesantno'];
//        $q14->modul='Module14';
//        $q14->points=["Uopšte se ne slažem"=>1,"Uglavnom se ne slažem"=>2,"Uglavnom se slažem"=>3,"U potpunosti se slažem"=>4];
//        $q14->final=0;
//        $q14->qid='psiho214';
//        $questionData[]=$q14;
//        
//           $q15=new \Classes\QuestionData();
//        $q15->question="";
//        $q15->hint="Radio button, multiple choice";
//        //$q3->multi=1;
//     
//        //$q1->story=true;
//        $q15->answers=['q1'];
//        $q15->sentences=['q1'=>'<span>15. Uvek ostavim neko vreme da pregledam šta sam radio pre nego kažem da sam posao završio</span><br><br>'];
//       // $q13->image='2/12-b.jpg';
//    //    $q3->options=['q1'=>[['-- Choose --','did visit','has visited','visited','should visit']],'q2'=>[['-- Choose --','to not be','not to be','that not be','that be not']],'q3'=>[['-- Choose --','couldn’t possibly be','mustn’t possibly be','shouldn’t possibly be','isn’t possibly']],'q4'=>[['-- Choose --','farer','farest','farther','farthest']],'q5'=>[['-- Choose --','our','ours'],['-- Choose --','her','hers']],'q6'=>[['-- Choose --','be able','have','can','need']],'q7'=>[['-- Choose --','This old table’s legs','The legs of this old table','This old table legs','The legs of this old table’s']],'q8'=>[['-- Choose --','nobody','no one','anyone','somebody']]];
//      $q15->options=['q1'=>['Uopšte se ne slažem','Uglavnom se ne slažem','Uglavnom se slažem','U potpunosti se slažem']];
//        // $q3->additionalData=["images"=>['Restaurant','Hotel','Store']];
//        $q15->questionType='radioSentence';
//       // $q1->correctAnswers=['q1'=>'vidim belu pozadinu','q2'=>'vidim crnu kružnu liniju – krug','q3'=>'vidim krug u kvadratu','q4'=>'ne vidim ništa interesantno'];
//        $q15->modul='Module15';
//        $q15->points=["Uopšte se ne slažem"=>1,"Uglavnom se ne slažem"=>2,"Uglavnom se slažem"=>3,"U potpunosti se slažem"=>4];
//        $q15->final=0;
//        $q15->qid='psiho215';
//        $questionData[]=$q15;
//        
//            $q16=new \Classes\QuestionData();
//        $q16->question="";
//        $q16->hint="Radio button, multiple choice";
//        //$q3->multi=1;
//     
//        //$q1->story=true;
//        $q16->answers=['q1'];
//        $q16->sentences=['q1'=>'<span>16. Često imam originalne ideje</span><br><br>'];
//       // $q13->image='2/12-b.jpg';
//    //    $q3->options=['q1'=>[['-- Choose --','did visit','has visited','visited','should visit']],'q2'=>[['-- Choose --','to not be','not to be','that not be','that be not']],'q3'=>[['-- Choose --','couldn’t possibly be','mustn’t possibly be','shouldn’t possibly be','isn’t possibly']],'q4'=>[['-- Choose --','farer','farest','farther','farthest']],'q5'=>[['-- Choose --','our','ours'],['-- Choose --','her','hers']],'q6'=>[['-- Choose --','be able','have','can','need']],'q7'=>[['-- Choose --','This old table’s legs','The legs of this old table','This old table legs','The legs of this old table’s']],'q8'=>[['-- Choose --','nobody','no one','anyone','somebody']]];
//      $q16->options=['q1'=>['Uopšte se ne slažem','Uglavnom se ne slažem','Uglavnom se slažem','U potpunosti se slažem']];
//        // $q3->additionalData=["images"=>['Restaurant','Hotel','Store']];
//        $q16->questionType='radioSentence';
//       // $q1->correctAnswers=['q1'=>'vidim belu pozadinu','q2'=>'vidim crnu kružnu liniju – krug','q3'=>'vidim krug u kvadratu','q4'=>'ne vidim ništa interesantno'];
//        $q16->modul='Module16';
//        $q16->points=["Uopšte se ne slažem"=>1,"Uglavnom se ne slažem"=>2,"Uglavnom se slažem"=>3,"U potpunosti se slažem"=>4];
//        $q16->final=0;
//        $q16->qid='psiho216';
//        $questionData[]=$q16;
//        
//             $q17=new \Classes\QuestionData();
//        $q17->question="";
//        $q17->hint="Radio button, multiple choice";
//        //$q3->multi=1;
//     
//        //$q1->story=true;
//        $q17->answers=['q1'];
//        $q17->sentences=['q1'=>'<span>17. Lako preuzimam odgovornost u poslu kada je to potrebno</span><br><br>'];
//       // $q13->image='2/12-b.jpg';
//    //    $q3->options=['q1'=>[['-- Choose --','did visit','has visited','visited','should visit']],'q2'=>[['-- Choose --','to not be','not to be','that not be','that be not']],'q3'=>[['-- Choose --','couldn’t possibly be','mustn’t possibly be','shouldn’t possibly be','isn’t possibly']],'q4'=>[['-- Choose --','farer','farest','farther','farthest']],'q5'=>[['-- Choose --','our','ours'],['-- Choose --','her','hers']],'q6'=>[['-- Choose --','be able','have','can','need']],'q7'=>[['-- Choose --','This old table’s legs','The legs of this old table','This old table legs','The legs of this old table’s']],'q8'=>[['-- Choose --','nobody','no one','anyone','somebody']]];
//      $q17->options=['q1'=>['Uopšte se ne slažem','Uglavnom se ne slažem','Uglavnom se slažem','U potpunosti se slažem']];
//        // $q3->additionalData=["images"=>['Restaurant','Hotel','Store']];
//        $q17->questionType='radioSentence';
//       // $q1->correctAnswers=['q1'=>'vidim belu pozadinu','q2'=>'vidim crnu kružnu liniju – krug','q3'=>'vidim krug u kvadratu','q4'=>'ne vidim ništa interesantno'];
//        $q17->modul='Module17';
//        $q17->points=["Uopšte se ne slažem"=>1,"Uglavnom se ne slažem"=>2,"Uglavnom se slažem"=>3,"U potpunosti se slažem"=>4];
//        $q17->final=0;
//        $q17->qid='psiho217';
//        $questionData[]=$q17;
//        
//                    $q18=new \Classes\QuestionData();
//        $q18->question="";
//        $q18->hint="Radio button, multiple choice";
//        //$q3->multi=1;
//     
//        //$q1->story=true;
//        $q18->answers=['q1'];
//        $q18->sentences=['q1'=>'<span>18. Smatram da je sve već “izmišljeno” samo treba primeniti</span><br><br>'];
//       // $q13->image='2/12-b.jpg';
//    //    $q3->options=['q1'=>[['-- Choose --','did visit','has visited','visited','should visit']],'q2'=>[['-- Choose --','to not be','not to be','that not be','that be not']],'q3'=>[['-- Choose --','couldn’t possibly be','mustn’t possibly be','shouldn’t possibly be','isn’t possibly']],'q4'=>[['-- Choose --','farer','farest','farther','farthest']],'q5'=>[['-- Choose --','our','ours'],['-- Choose --','her','hers']],'q6'=>[['-- Choose --','be able','have','can','need']],'q7'=>[['-- Choose --','This old table’s legs','The legs of this old table','This old table legs','The legs of this old table’s']],'q8'=>[['-- Choose --','nobody','no one','anyone','somebody']]];
//      $q18->options=['q1'=>['Uopšte se ne slažem','Uglavnom se ne slažem','Uglavnom se slažem','U potpunosti se slažem']];
//        // $q3->additionalData=["images"=>['Restaurant','Hotel','Store']];
//        $q18->questionType='radioSentence';
//       // $q1->correctAnswers=['q1'=>'vidim belu pozadinu','q2'=>'vidim crnu kružnu liniju – krug','q3'=>'vidim krug u kvadratu','q4'=>'ne vidim ništa interesantno'];
//        $q18->modul='Module18';
//        $q18->points=["Uopšte se ne slažem"=>1,"Uglavnom se ne slažem"=>2,"Uglavnom se slažem"=>3,"U potpunosti se slažem"=>4];
//        $q18->final=0;
//        $q18->qid='psiho218';
//        $questionData[]=$q18;
//        
//                          $q19=new \Classes\QuestionData();
//        $q19->question="";
//        $q19->hint="Radio button, multiple choice";
//        //$q3->multi=1;
//     
//        //$q1->story=true;
//        $q19->answers=['q1'];
//        $q19->sentences=['q1'=>'<span>19. Važno mi je da u poslu uvek bude i zabave. Koliko vam je vazna ovakva atmosfera na poslu?</span><br><br>'];
//       $q19->image='2/19.jpg';
//    //    $q3->options=['q1'=>[['-- Choose --','did visit','has visited','visited','should visit']],'q2'=>[['-- Choose --','to not be','not to be','that not be','that be not']],'q3'=>[['-- Choose --','couldn’t possibly be','mustn’t possibly be','shouldn’t possibly be','isn’t possibly']],'q4'=>[['-- Choose --','farer','farest','farther','farthest']],'q5'=>[['-- Choose --','our','ours'],['-- Choose --','her','hers']],'q6'=>[['-- Choose --','be able','have','can','need']],'q7'=>[['-- Choose --','This old table’s legs','The legs of this old table','This old table legs','The legs of this old table’s']],'q8'=>[['-- Choose --','nobody','no one','anyone','somebody']]];
//      $q19->options=['q1'=>['Uopšte se ne slažem','Uglavnom se ne slažem','Uglavnom se slažem','U potpunosti se slažem']];
//        // $q3->additionalData=["images"=>['Restaurant','Hotel','Store']];
//        $q19->questionType='radioSentence';
//       // $q1->correctAnswers=['q1'=>'vidim belu pozadinu','q2'=>'vidim crnu kružnu liniju – krug','q3'=>'vidim krug u kvadratu','q4'=>'ne vidim ništa interesantno'];
//        $q19->modul='Module19';
//        $q19->points=["Uopšte se ne slažem"=>1,"Uglavnom se ne slažem"=>2,"Uglavnom se slažem"=>3,"U potpunosti se slažem"=>4];
//        $q19->final=0;
//        $q19->qid='psiho219';
//        $questionData[]=$q19;
//        
//                                  $q20=new \Classes\QuestionData();
//        $q20->question="";
//        $q20->hint="Radio button, multiple choice";
//        //$q3->multi=1;
//     
//        //$q1->story=true;
//        $q20->answers=['q1'];
//        $q20->sentences=['q1'=>'<span>20. Uočavam propuste u poslu I koje drugi ne vide</span><br><br>'];
//       // $q13->image='2/12-b.jpg';
//    //    $q3->options=['q1'=>[['-- Choose --','did visit','has visited','visited','should visit']],'q2'=>[['-- Choose --','to not be','not to be','that not be','that be not']],'q3'=>[['-- Choose --','couldn’t possibly be','mustn’t possibly be','shouldn’t possibly be','isn’t possibly']],'q4'=>[['-- Choose --','farer','farest','farther','farthest']],'q5'=>[['-- Choose --','our','ours'],['-- Choose --','her','hers']],'q6'=>[['-- Choose --','be able','have','can','need']],'q7'=>[['-- Choose --','This old table’s legs','The legs of this old table','This old table legs','The legs of this old table’s']],'q8'=>[['-- Choose --','nobody','no one','anyone','somebody']]];
//      $q20->options=['q1'=>['Uopšte se ne slažem','Uglavnom se ne slažem','Uglavnom se slažem','U potpunosti se slažem']];
//        // $q3->additionalData=["images"=>['Restaurant','Hotel','Store']];
//        $q20->questionType='radioSentence';
//       // $q1->correctAnswers=['q1'=>'vidim belu pozadinu','q2'=>'vidim crnu kružnu liniju – krug','q3'=>'vidim krug u kvadratu','q4'=>'ne vidim ništa interesantno'];
//        $q20->modul='Module20';
//        $q20->points=["Uopšte se ne slažem"=>1,"Uglavnom se ne slažem"=>2,"Uglavnom se slažem"=>3,"U potpunosti se slažem"=>4];
//        $q20->final=0;
//        $q20->qid='psiho220';
//        $questionData[]=$q20;
//        
//                                    $q21=new \Classes\QuestionData();
//        $q21->question="";
//        $q21->hint="Radio button, multiple choice";
//        //$q3->multi=1;
//     
//        //$q1->story=true;
//        $q21->answers=['q1'];
//        $q21->sentences=['q1'=>'<span>21. Dobro sam organizovan</span><br><br>'];
//       // $q13->image='2/12-b.jpg';
//    //    $q3->options=['q1'=>[['-- Choose --','did visit','has visited','visited','should visit']],'q2'=>[['-- Choose --','to not be','not to be','that not be','that be not']],'q3'=>[['-- Choose --','couldn’t possibly be','mustn’t possibly be','shouldn’t possibly be','isn’t possibly']],'q4'=>[['-- Choose --','farer','farest','farther','farthest']],'q5'=>[['-- Choose --','our','ours'],['-- Choose --','her','hers']],'q6'=>[['-- Choose --','be able','have','can','need']],'q7'=>[['-- Choose --','This old table’s legs','The legs of this old table','This old table legs','The legs of this old table’s']],'q8'=>[['-- Choose --','nobody','no one','anyone','somebody']]];
//      $q21->options=['q1'=>['Uopšte se ne slažem','Uglavnom se ne slažem','Uglavnom se slažem','U potpunosti se slažem']];
//        // $q3->additionalData=["images"=>['Restaurant','Hotel','Store']];
//        $q21->questionType='radioSentence';
//       // $q1->correctAnswers=['q1'=>'vidim belu pozadinu','q2'=>'vidim crnu kružnu liniju – krug','q3'=>'vidim krug u kvadratu','q4'=>'ne vidim ništa interesantno'];
//        $q21->modul='Module21';
//        $q21->points=["Uopšte se ne slažem"=>1,"Uglavnom se ne slažem"=>2,"Uglavnom se slažem"=>3,"U potpunosti se slažem"=>4];
//        $q21->final=0;
//        $q21->qid='psiho221';
//        $questionData[]=$q21;
//        
//                                      $q22=new \Classes\QuestionData();
//        $q22->question="";
//        $q22->hint="Radio button, multiple choice";
//        //$q3->multi=1;
//     
//        //$q1->story=true;
//        $q22->answers=['q1'];
//        $q22->sentences=['q1'=>'<span>22. Spreman sam da prihvatim posao o kome ne znam dovoljno</span><br><br>'];
//       // $q13->image='2/12-b.jpg';
//    //    $q3->options=['q1'=>[['-- Choose --','did visit','has visited','visited','should visit']],'q2'=>[['-- Choose --','to not be','not to be','that not be','that be not']],'q3'=>[['-- Choose --','couldn’t possibly be','mustn’t possibly be','shouldn’t possibly be','isn’t possibly']],'q4'=>[['-- Choose --','farer','farest','farther','farthest']],'q5'=>[['-- Choose --','our','ours'],['-- Choose --','her','hers']],'q6'=>[['-- Choose --','be able','have','can','need']],'q7'=>[['-- Choose --','This old table’s legs','The legs of this old table','This old table legs','The legs of this old table’s']],'q8'=>[['-- Choose --','nobody','no one','anyone','somebody']]];
//      $q22->options=['q1'=>['Uopšte se ne slažem','Uglavnom se ne slažem','Uglavnom se slažem','U potpunosti se slažem']];
//        // $q3->additionalData=["images"=>['Restaurant','Hotel','Store']];
//        $q22->questionType='radioSentence';
//       // $q1->correctAnswers=['q1'=>'vidim belu pozadinu','q2'=>'vidim crnu kružnu liniju – krug','q3'=>'vidim krug u kvadratu','q4'=>'ne vidim ništa interesantno'];
//        $q22->modul='Module22';
//        $q22->points=["Uopšte se ne slažem"=>1,"Uglavnom se ne slažem"=>2,"Uglavnom se slažem"=>3,"U potpunosti se slažem"=>4];
//        $q22->final=0;
//        $q22->qid='psiho222';
//        $questionData[]=$q22;
//        
//                                           $q23=new \Classes\QuestionData();
//        $q23->question="";
//        $q23->hint="Radio button, multiple choice";
//        //$q3->multi=1;
//     
//        //$q1->story=true;
//        $q23->answers=['q1'];
//        $q23->sentences=['q1'=>'<span>23. Često sam kritičan prema sebi I drugima u poslu</span><br><br>'];
//        $q23->image='2/23.jpg';
//    //    $q3->options=['q1'=>[['-- Choose --','did visit','has visited','visited','should visit']],'q2'=>[['-- Choose --','to not be','not to be','that not be','that be not']],'q3'=>[['-- Choose --','couldn’t possibly be','mustn’t possibly be','shouldn’t possibly be','isn’t possibly']],'q4'=>[['-- Choose --','farer','farest','farther','farthest']],'q5'=>[['-- Choose --','our','ours'],['-- Choose --','her','hers']],'q6'=>[['-- Choose --','be able','have','can','need']],'q7'=>[['-- Choose --','This old table’s legs','The legs of this old table','This old table legs','The legs of this old table’s']],'q8'=>[['-- Choose --','nobody','no one','anyone','somebody']]];
//      $q23->options=['q1'=>['Uopšte se ne slažem','Uglavnom se ne slažem','Uglavnom se slažem','U potpunosti se slažem']];
//        // $q3->additionalData=["images"=>['Restaurant','Hotel','Store']];
//        $q23->questionType='radioSentence';
//       // $q1->correctAnswers=['q1'=>'vidim belu pozadinu','q2'=>'vidim crnu kružnu liniju – krug','q3'=>'vidim krug u kvadratu','q4'=>'ne vidim ništa interesantno'];
//        $q23->modul='Module23';
//        $q23->points=["Uopšte se ne slažem"=>1,"Uglavnom se ne slažem"=>2,"Uglavnom se slažem"=>3,"U potpunosti se slažem"=>4];
//        $q23->final=0;
//        $q23->qid='psiho223';
//        $questionData[]=$q23;
//        
//         $q24=new \Classes\QuestionData();
//        $q24->question="";
//        $q24->hint="Radio button, multiple choice";
//        //$q3->multi=1;
//     
//        //$q1->story=true;
//        $q24->answers=['q1'];
//        $q24->sentences=['q1'=>'<span>24. Nije mi vazno koliko ce neki posao da traje, samo da se dobro uradi</span><br><br>'];
//        //$q24->image='2/23.jpg';
//    //    $q3->options=['q1'=>[['-- Choose --','did visit','has visited','visited','should visit']],'q2'=>[['-- Choose --','to not be','not to be','that not be','that be not']],'q3'=>[['-- Choose --','couldn’t possibly be','mustn’t possibly be','shouldn’t possibly be','isn’t possibly']],'q4'=>[['-- Choose --','farer','farest','farther','farthest']],'q5'=>[['-- Choose --','our','ours'],['-- Choose --','her','hers']],'q6'=>[['-- Choose --','be able','have','can','need']],'q7'=>[['-- Choose --','This old table’s legs','The legs of this old table','This old table legs','The legs of this old table’s']],'q8'=>[['-- Choose --','nobody','no one','anyone','somebody']]];
//      $q24->options=['q1'=>['Uopšte se ne slažem','Uglavnom se ne slažem','Uglavnom se slažem','U potpunosti se slažem']];
//        // $q3->additionalData=["images"=>['Restaurant','Hotel','Store']];
//        $q24->questionType='radioSentence';
//       // $q1->correctAnswers=['q1'=>'vidim belu pozadinu','q2'=>'vidim crnu kružnu liniju – krug','q3'=>'vidim krug u kvadratu','q4'=>'ne vidim ništa interesantno'];
//        $q24->modul='Module24';
//        $q24->points=["Uopšte se ne slažem"=>1,"Uglavnom se ne slažem"=>2,"Uglavnom se slažem"=>3,"U potpunosti se slažem"=>4];
//        $q24->final=0;
//        $q24->qid='psiho224';
//        $questionData[]=$q24;
//        
//            $q25=new \Classes\QuestionData();
//        $q25->question="";
//        $q25->hint="Radio button, multiple choice";
//        //$q3->multi=1;
//     
//        //$q1->story=true;
//        $q25->answers=['q1'];
//        $q25->sentences=['q1'=>'<span>25. Važno mi je da kroz posao upoznajem nove ljude</span><br><br>'];
//        //$q24->image='2/23.jpg';
//    //    $q3->options=['q1'=>[['-- Choose --','did visit','has visited','visited','should visit']],'q2'=>[['-- Choose --','to not be','not to be','that not be','that be not']],'q3'=>[['-- Choose --','couldn’t possibly be','mustn’t possibly be','shouldn’t possibly be','isn’t possibly']],'q4'=>[['-- Choose --','farer','farest','farther','farthest']],'q5'=>[['-- Choose --','our','ours'],['-- Choose --','her','hers']],'q6'=>[['-- Choose --','be able','have','can','need']],'q7'=>[['-- Choose --','This old table’s legs','The legs of this old table','This old table legs','The legs of this old table’s']],'q8'=>[['-- Choose --','nobody','no one','anyone','somebody']]];
//      $q25->options=['q1'=>['Uopšte se ne slažem','Uglavnom se ne slažem','Uglavnom se slažem','U potpunosti se slažem']];
//        // $q3->additionalData=["images"=>['Restaurant','Hotel','Store']];
//        $q25->questionType='radioSentence';
//       // $q1->correctAnswers=['q1'=>'vidim belu pozadinu','q2'=>'vidim crnu kružnu liniju – krug','q3'=>'vidim krug u kvadratu','q4'=>'ne vidim ništa interesantno'];
//        $q25->modul='Module25';
//        $q25->points=["Uopšte se ne slažem"=>1,"Uglavnom se ne slažem"=>2,"Uglavnom se slažem"=>3,"U potpunosti se slažem"=>4];
//        $q25->final=0;
//        $q25->qid='psiho225';
//        $questionData[]=$q25;
//        
//                $q26=new \Classes\QuestionData();
//        $q26->question="";
//        $q26->hint="Radio button, multiple choice";
//        //$q3->multi=1;
//     
//        //$q1->story=true;
//        $q26->answers=['q1'];
//        $q26->sentences=['q1'=>'<span>26. Dobro funkcionišem pod pritiskom</span><br><br>'];
//        //$q24->image='2/23.jpg';
//    //    $q3->options=['q1'=>[['-- Choose --','did visit','has visited','visited','should visit']],'q2'=>[['-- Choose --','to not be','not to be','that not be','that be not']],'q3'=>[['-- Choose --','couldn’t possibly be','mustn’t possibly be','shouldn’t possibly be','isn’t possibly']],'q4'=>[['-- Choose --','farer','farest','farther','farthest']],'q5'=>[['-- Choose --','our','ours'],['-- Choose --','her','hers']],'q6'=>[['-- Choose --','be able','have','can','need']],'q7'=>[['-- Choose --','This old table’s legs','The legs of this old table','This old table legs','The legs of this old table’s']],'q8'=>[['-- Choose --','nobody','no one','anyone','somebody']]];
//      $q26->options=['q1'=>['Uopšte se ne slažem','Uglavnom se ne slažem','Uglavnom se slažem','U potpunosti se slažem']];
//        // $q3->additionalData=["images"=>['Restaurant','Hotel','Store']];
//        $q26->questionType='radioSentence';
//       // $q1->correctAnswers=['q1'=>'vidim belu pozadinu','q2'=>'vidim crnu kružnu liniju – krug','q3'=>'vidim krug u kvadratu','q4'=>'ne vidim ništa interesantno'];
//        $q26->modul='Module26';
//        $q26->points=["Uopšte se ne slažem"=>1,"Uglavnom se ne slažem"=>2,"Uglavnom se slažem"=>3,"U potpunosti se slažem"=>4];
//        $q26->final=0;
//        $q26->qid='psiho226';
//        $questionData[]=$q26;
//        
//                   $q27=new \Classes\QuestionData();
//        $q27->question="";
//        $q27->hint="Radio button, multiple choice";
//        //$q3->multi=1;
//     
//        //$q1->story=true;
//        $q27->answers=['q1'];
//        $q27->sentences=['q1'=>'<span>27. Više volim rad sa papirima I za mašinama nego rad sa ljudima</span><br><br>'];
//        //$q24->image='2/23.jpg';
//    //    $q3->options=['q1'=>[['-- Choose --','did visit','has visited','visited','should visit']],'q2'=>[['-- Choose --','to not be','not to be','that not be','that be not']],'q3'=>[['-- Choose --','couldn’t possibly be','mustn’t possibly be','shouldn’t possibly be','isn’t possibly']],'q4'=>[['-- Choose --','farer','farest','farther','farthest']],'q5'=>[['-- Choose --','our','ours'],['-- Choose --','her','hers']],'q6'=>[['-- Choose --','be able','have','can','need']],'q7'=>[['-- Choose --','This old table’s legs','The legs of this old table','This old table legs','The legs of this old table’s']],'q8'=>[['-- Choose --','nobody','no one','anyone','somebody']]];
//      $q27->options=['q1'=>['Uopšte se ne slažem','Uglavnom se ne slažem','Uglavnom se slažem','U potpunosti se slažem']];
//        // $q3->additionalData=["images"=>['Restaurant','Hotel','Store']];
//        $q27->questionType='radioSentence';
//       // $q1->correctAnswers=['q1'=>'vidim belu pozadinu','q2'=>'vidim crnu kružnu liniju – krug','q3'=>'vidim krug u kvadratu','q4'=>'ne vidim ništa interesantno'];
//        $q27->modul='Module27';
//        $q27->points=["Uopšte se ne slažem"=>1,"Uglavnom se ne slažem"=>2,"Uglavnom se slažem"=>3,"U potpunosti se slažem"=>4];
//        $q27->final=0;
//        $q27->qid='psiho227';
//        $questionData[]=$q27;
//        
//                           $q28=new \Classes\QuestionData();
//        $q28->question="";
//        $q28->hint="Radio button, multiple choice";
//        //$q3->multi=1;
//     
//        //$q1->story=true;
//        $q28->answers=['q1'];
//        $q28->sentences=['q1'=>'<span>28. Nemam problem da se suprotstavim celom timu ako je to u funkciji posla</span><br><br>'];
//        //$q24->image='2/23.jpg';
//    //    $q3->options=['q1'=>[['-- Choose --','did visit','has visited','visited','should visit']],'q2'=>[['-- Choose --','to not be','not to be','that not be','that be not']],'q3'=>[['-- Choose --','couldn’t possibly be','mustn’t possibly be','shouldn’t possibly be','isn’t possibly']],'q4'=>[['-- Choose --','farer','farest','farther','farthest']],'q5'=>[['-- Choose --','our','ours'],['-- Choose --','her','hers']],'q6'=>[['-- Choose --','be able','have','can','need']],'q7'=>[['-- Choose --','This old table’s legs','The legs of this old table','This old table legs','The legs of this old table’s']],'q8'=>[['-- Choose --','nobody','no one','anyone','somebody']]];
//      $q28->options=['q1'=>['Uopšte se ne slažem','Uglavnom se ne slažem','Uglavnom se slažem','U potpunosti se slažem']];
//        // $q3->additionalData=["images"=>['Restaurant','Hotel','Store']];
//        $q28->questionType='radioSentence';
//       // $q1->correctAnswers=['q1'=>'vidim belu pozadinu','q2'=>'vidim crnu kružnu liniju – krug','q3'=>'vidim krug u kvadratu','q4'=>'ne vidim ništa interesantno'];
//        $q28->modul='Module28';
//        $q28->points=["Uopšte se ne slažem"=>1,"Uglavnom se ne slažem"=>2,"Uglavnom se slažem"=>3,"U potpunosti se slažem"=>4];
//        $q28->final=0;
//        $q28->qid='psiho228';
//        $questionData[]=$q28;
//        
//                             $q29=new \Classes\QuestionData();
//        $q29->question="";
//        $q29->hint="Radio button, multiple choice";
//        //$q3->multi=1;
//     
//        //$q1->story=true;
//        $q29->answers=['q1'];
//        $q29->sentences=['q1'=>'<span>29. Teško mi je da radim kada cilj nije jasno definisan</span><br><br>'];
//        //$q24->image='2/23.jpg';
//    //    $q3->options=['q1'=>[['-- Choose --','did visit','has visited','visited','should visit']],'q2'=>[['-- Choose --','to not be','not to be','that not be','that be not']],'q3'=>[['-- Choose --','couldn’t possibly be','mustn’t possibly be','shouldn’t possibly be','isn’t possibly']],'q4'=>[['-- Choose --','farer','farest','farther','farthest']],'q5'=>[['-- Choose --','our','ours'],['-- Choose --','her','hers']],'q6'=>[['-- Choose --','be able','have','can','need']],'q7'=>[['-- Choose --','This old table’s legs','The legs of this old table','This old table legs','The legs of this old table’s']],'q8'=>[['-- Choose --','nobody','no one','anyone','somebody']]];
//      $q29->options=['q1'=>['Uopšte se ne slažem','Uglavnom se ne slažem','Uglavnom se slažem','U potpunosti se slažem']];
//        // $q3->additionalData=["images"=>['Restaurant','Hotel','Store']];
//        $q29->questionType='radioSentence';
//       // $q1->correctAnswers=['q1'=>'vidim belu pozadinu','q2'=>'vidim crnu kružnu liniju – krug','q3'=>'vidim krug u kvadratu','q4'=>'ne vidim ništa interesantno'];
//        $q29->modul='Module29';
//        $q29->points=["Uopšte se ne slažem"=>1,"Uglavnom se ne slažem"=>2,"Uglavnom se slažem"=>3,"U potpunosti se slažem"=>4];
//        $q29->final=0;
//        $q29->qid='psiho229';
//        $questionData[]=$q29;
//        
//         $q30=new \Classes\QuestionData();
//        $q30->question="";
//        $q30->hint="Radio button, multiple choice";
//        //$q3->multi=1;
//     
//        //$q1->story=true;
//        $q30->answers=['q1'];
//        $q30->sentences=['q1'=>'<span>30. Na sastancima uvek imam nešto da kažem</span><br><br>'];
//        //$q24->image='2/23.jpg';
//    //    $q3->options=['q1'=>[['-- Choose --','did visit','has visited','visited','should visit']],'q2'=>[['-- Choose --','to not be','not to be','that not be','that be not']],'q3'=>[['-- Choose --','couldn’t possibly be','mustn’t possibly be','shouldn’t possibly be','isn’t possibly']],'q4'=>[['-- Choose --','farer','farest','farther','farthest']],'q5'=>[['-- Choose --','our','ours'],['-- Choose --','her','hers']],'q6'=>[['-- Choose --','be able','have','can','need']],'q7'=>[['-- Choose --','This old table’s legs','The legs of this old table','This old table legs','The legs of this old table’s']],'q8'=>[['-- Choose --','nobody','no one','anyone','somebody']]];
//      $q30->options=['q1'=>['Uopšte se ne slažem','Uglavnom se ne slažem','Uglavnom se slažem','U potpunosti se slažem']];
//        // $q3->additionalData=["images"=>['Restaurant','Hotel','Store']];
//        $q30->questionType='radioSentence';
//       // $q1->correctAnswers=['q1'=>'vidim belu pozadinu','q2'=>'vidim crnu kružnu liniju – krug','q3'=>'vidim krug u kvadratu','q4'=>'ne vidim ništa interesantno'];
//        $q30->modul='Module30';
//        $q30->points=["Uopšte se ne slažem"=>1,"Uglavnom se ne slažem"=>2,"Uglavnom se slažem"=>3,"U potpunosti se slažem"=>4];
//        $q30->final=0;
//        $q30->qid='psiho230';
//        $questionData[]=$q30;
//        
//          $q31=new \Classes\QuestionData();
//        $q31->question="";
//        $q31->hint="Radio button, multiple choice";
//        //$q3->multi=1;
//     
//        //$q1->story=true;
//        $q31->answers=['q1'];
//        $q31->sentences=['q1'=>'<span>31. Često posećujem stručne skupove na kojima nešto mogu da naučim</span><br><br>'];
//        $q31->image='2/31.jpg';
//    //    $q3->options=['q1'=>[['-- Choose --','did visit','has visited','visited','should visit']],'q2'=>[['-- Choose --','to not be','not to be','that not be','that be not']],'q3'=>[['-- Choose --','couldn’t possibly be','mustn’t possibly be','shouldn’t possibly be','isn’t possibly']],'q4'=>[['-- Choose --','farer','farest','farther','farthest']],'q5'=>[['-- Choose --','our','ours'],['-- Choose --','her','hers']],'q6'=>[['-- Choose --','be able','have','can','need']],'q7'=>[['-- Choose --','This old table’s legs','The legs of this old table','This old table legs','The legs of this old table’s']],'q8'=>[['-- Choose --','nobody','no one','anyone','somebody']]];
//      $q31->options=['q1'=>['Uopšte se ne slažem','Uglavnom se ne slažem','Uglavnom se slažem','U potpunosti se slažem']];
//        // $q3->additionalData=["images"=>['Restaurant','Hotel','Store']];
//        $q31->questionType='radioSentence';
//       // $q1->correctAnswers=['q1'=>'vidim belu pozadinu','q2'=>'vidim crnu kružnu liniju – krug','q3'=>'vidim krug u kvadratu','q4'=>'ne vidim ništa interesantno'];
//        $q31->modul='Module31';
//        $q31->points=["Uopšte se ne slažem"=>1,"Uglavnom se ne slažem"=>2,"Uglavnom se slažem"=>3,"U potpunosti se slažem"=>4];
//        $q31->final=0;
//        $q31->qid='psiho231';
//        $questionData[]=$q31;
//        
//         $q32=new \Classes\QuestionData();
//        $q32->question="";
//        $q32->hint="Radio button, multiple choice";
//        //$q3->multi=1;
//     
//        //$q1->story=true;
//        $q32->answers=['q1'];
//        $q32->sentences=['q1'=>'<span>32. Ne ustručavam se da isprobavam nove načine rada</span><br><br>'];
//       // $q31->image='2/31.jpg';
//    //    $q3->options=['q1'=>[['-- Choose --','did visit','has visited','visited','should visit']],'q2'=>[['-- Choose --','to not be','not to be','that not be','that be not']],'q3'=>[['-- Choose --','couldn’t possibly be','mustn’t possibly be','shouldn’t possibly be','isn’t possibly']],'q4'=>[['-- Choose --','farer','farest','farther','farthest']],'q5'=>[['-- Choose --','our','ours'],['-- Choose --','her','hers']],'q6'=>[['-- Choose --','be able','have','can','need']],'q7'=>[['-- Choose --','This old table’s legs','The legs of this old table','This old table legs','The legs of this old table’s']],'q8'=>[['-- Choose --','nobody','no one','anyone','somebody']]];
//      $q32->options=['q1'=>['Uopšte se ne slažem','Uglavnom se ne slažem','Uglavnom se slažem','U potpunosti se slažem']];
//        // $q3->additionalData=["images"=>['Restaurant','Hotel','Store']];
//        $q32->questionType='radioSentence';
//       // $q1->correctAnswers=['q1'=>'vidim belu pozadinu','q2'=>'vidim crnu kružnu liniju – krug','q3'=>'vidim krug u kvadratu','q4'=>'ne vidim ništa interesantno'];
//        $q32->modul='Module32';
//        $q32->points=["Uopšte se ne slažem"=>1,"Uglavnom se ne slažem"=>2,"Uglavnom se slažem"=>3,"U potpunosti se slažem"=>4];
//        $q32->final=0;
//        $q32->qid='psiho232';
//        $questionData[]=$q32;
//        
//            $q33=new \Classes\QuestionData();
//        $q33->question="";
//        $q33->hint="Radio button, multiple choice";
//        //$q3->multi=1;
//     
//        //$q1->story=true;
//        $q33->answers=['q1'];
//        $q33->sentences=['q1'=>'<span>33. Gotovo uvek doprinesem postizanju dogovora</span><br><br>'];
//       // $q31->image='2/31.jpg';
//    //    $q3->options=['q1'=>[['-- Choose --','did visit','has visited','visited','should visit']],'q2'=>[['-- Choose --','to not be','not to be','that not be','that be not']],'q3'=>[['-- Choose --','couldn’t possibly be','mustn’t possibly be','shouldn’t possibly be','isn’t possibly']],'q4'=>[['-- Choose --','farer','farest','farther','farthest']],'q5'=>[['-- Choose --','our','ours'],['-- Choose --','her','hers']],'q6'=>[['-- Choose --','be able','have','can','need']],'q7'=>[['-- Choose --','This old table’s legs','The legs of this old table','This old table legs','The legs of this old table’s']],'q8'=>[['-- Choose --','nobody','no one','anyone','somebody']]];
//      $q33->options=['q1'=>['Uopšte se ne slažem','Uglavnom se ne slažem','Uglavnom se slažem','U potpunosti se slažem']];
//        // $q3->additionalData=["images"=>['Restaurant','Hotel','Store']];
//        $q33->questionType='radioSentence';
//       // $q1->correctAnswers=['q1'=>'vidim belu pozadinu','q2'=>'vidim crnu kružnu liniju – krug','q3'=>'vidim krug u kvadratu','q4'=>'ne vidim ništa interesantno'];
//        $q33->modul='Module33';
//        $q33->points=["Uopšte se ne slažem"=>1,"Uglavnom se ne slažem"=>2,"Uglavnom se slažem"=>3,"U potpunosti se slažem"=>4];
//        $q33->final=0;
//        $q33->qid='psiho233';
//        $questionData[]=$q33;
//        
//                 $q34=new \Classes\QuestionData();
//        $q34->question="";
//        $q34->hint="Radio button, multiple choice";
//        //$q3->multi=1;
//     
//        //$q1->story=true;
//        $q34->answers=['q1'];
//        $q34->sentences=['q1'=>'<span>34. Podržavam kolege u njihovim inicijativama I često im se priključujem</span><br><br>'];
//       // $q31->image='2/31.jpg';
//    //    $q3->options=['q1'=>[['-- Choose --','did visit','has visited','visited','should visit']],'q2'=>[['-- Choose --','to not be','not to be','that not be','that be not']],'q3'=>[['-- Choose --','couldn’t possibly be','mustn’t possibly be','shouldn’t possibly be','isn’t possibly']],'q4'=>[['-- Choose --','farer','farest','farther','farthest']],'q5'=>[['-- Choose --','our','ours'],['-- Choose --','her','hers']],'q6'=>[['-- Choose --','be able','have','can','need']],'q7'=>[['-- Choose --','This old table’s legs','The legs of this old table','This old table legs','The legs of this old table’s']],'q8'=>[['-- Choose --','nobody','no one','anyone','somebody']]];
//      $q34->options=['q1'=>['Uopšte se ne slažem','Uglavnom se ne slažem','Uglavnom se slažem','U potpunosti se slažem']];
//        // $q3->additionalData=["images"=>['Restaurant','Hotel','Store']];
//        $q34->questionType='radioSentence';
//       // $q1->correctAnswers=['q1'=>'vidim belu pozadinu','q2'=>'vidim crnu kružnu liniju – krug','q3'=>'vidim krug u kvadratu','q4'=>'ne vidim ništa interesantno'];
//        $q34->modul='Module34';
//        $q34->points=["Uopšte se ne slažem"=>1,"Uglavnom se ne slažem"=>2,"Uglavnom se slažem"=>3,"U potpunosti se slažem"=>4];
//        $q34->final=0;
//        $q34->qid='psiho234';
//        $questionData[]=$q34;
//        
//                  $q35=new \Classes\QuestionData();
//        $q35->question="";
//        $q35->hint="Radio button, multiple choice";
//        //$q3->multi=1;
//     
//        //$q1->story=true;
//        $q35->answers=['q1'];
//        $q35->sentences=['q1'=>'<span>35. Volim da predstavljam svoj tim</span><br><br>'];
//       // $q31->image='2/31.jpg';
//    //    $q3->options=['q1'=>[['-- Choose --','did visit','has visited','visited','should visit']],'q2'=>[['-- Choose --','to not be','not to be','that not be','that be not']],'q3'=>[['-- Choose --','couldn’t possibly be','mustn’t possibly be','shouldn’t possibly be','isn’t possibly']],'q4'=>[['-- Choose --','farer','farest','farther','farthest']],'q5'=>[['-- Choose --','our','ours'],['-- Choose --','her','hers']],'q6'=>[['-- Choose --','be able','have','can','need']],'q7'=>[['-- Choose --','This old table’s legs','The legs of this old table','This old table legs','The legs of this old table’s']],'q8'=>[['-- Choose --','nobody','no one','anyone','somebody']]];
//      $q35->options=['q1'=>['Uopšte se ne slažem','Uglavnom se ne slažem','Uglavnom se slažem','U potpunosti se slažem']];
//        // $q3->additionalData=["images"=>['Restaurant','Hotel','Store']];
//        $q35->questionType='radioSentence';
//       // $q1->correctAnswers=['q1'=>'vidim belu pozadinu','q2'=>'vidim crnu kružnu liniju – krug','q3'=>'vidim krug u kvadratu','q4'=>'ne vidim ništa interesantno'];
//        $q35->modul='Module35';
//        $q35->points=["Uopšte se ne slažem"=>1,"Uglavnom se ne slažem"=>2,"Uglavnom se slažem"=>3,"U potpunosti se slažem"=>4];
//        $q35->final=0;
//        $q35->qid='psiho235';
//        $questionData[]=$q35;
//        
//                     $q36=new \Classes\QuestionData();
//        $q36->question="";
//        $q36->hint="Radio button, multiple choice";
//        //$q3->multi=1;
//     
//        //$q1->story=true;
//        $q36->answers=['q1'];
//        $q36->sentences=['q1'=>'<span>36. Posao zaahteva visok stepen koncentracije sa moje strane kako bi greske bile minimalne</span><br><br>'];
//       // $q31->image='2/31.jpg';
//    //    $q3->options=['q1'=>[['-- Choose --','did visit','has visited','visited','should visit']],'q2'=>[['-- Choose --','to not be','not to be','that not be','that be not']],'q3'=>[['-- Choose --','couldn’t possibly be','mustn’t possibly be','shouldn’t possibly be','isn’t possibly']],'q4'=>[['-- Choose --','farer','farest','farther','farthest']],'q5'=>[['-- Choose --','our','ours'],['-- Choose --','her','hers']],'q6'=>[['-- Choose --','be able','have','can','need']],'q7'=>[['-- Choose --','This old table’s legs','The legs of this old table','This old table legs','The legs of this old table’s']],'q8'=>[['-- Choose --','nobody','no one','anyone','somebody']]];
//      $q36->options=['q1'=>['Uopšte se ne slažem','Uglavnom se ne slažem','Uglavnom se slažem','U potpunosti se slažem']];
//        // $q3->additionalData=["images"=>['Restaurant','Hotel','Store']];
//        $q36->questionType='radioSentence';
//       // $q1->correctAnswers=['q1'=>'vidim belu pozadinu','q2'=>'vidim crnu kružnu liniju – krug','q3'=>'vidim krug u kvadratu','q4'=>'ne vidim ništa interesantno'];
//        $q36->modul='Module36';
//        $q36->points=["Uopšte se ne slažem"=>1,"Uglavnom se ne slažem"=>2,"Uglavnom se slažem"=>3,"U potpunosti se slažem"=>4];
//        $q36->final=0;
//        $q36->qid='psiho236';
//        $questionData[]=$q36;
//        
//                          $q37=new \Classes\QuestionData();
//        $q37->question="";
//        $q37->hint="Radio button, multiple choice";
//        //$q3->multi=1;
//     
//        //$q1->story=true;
//        $q37->answers=['q1'];
//        $q37->sentences=['q1'=>'<span>37. Uživam kad sam obuzet poslom</span><br><br>'];
//        $q37->image='2/37.jpg';
//    //    $q3->options=['q1'=>[['-- Choose --','did visit','has visited','visited','should visit']],'q2'=>[['-- Choose --','to not be','not to be','that not be','that be not']],'q3'=>[['-- Choose --','couldn’t possibly be','mustn’t possibly be','shouldn’t possibly be','isn’t possibly']],'q4'=>[['-- Choose --','farer','farest','farther','farthest']],'q5'=>[['-- Choose --','our','ours'],['-- Choose --','her','hers']],'q6'=>[['-- Choose --','be able','have','can','need']],'q7'=>[['-- Choose --','This old table’s legs','The legs of this old table','This old table legs','The legs of this old table’s']],'q8'=>[['-- Choose --','nobody','no one','anyone','somebody']]];
//      $q37->options=['q1'=>['Uopšte se ne slažem','Uglavnom se ne slažem','Uglavnom se slažem','U potpunosti se slažem']];
//        // $q3->additionalData=["images"=>['Restaurant','Hotel','Store']];
//        $q37->questionType='radioSentence';
//       // $q1->correctAnswers=['q1'=>'vidim belu pozadinu','q2'=>'vidim crnu kružnu liniju – krug','q3'=>'vidim krug u kvadratu','q4'=>'ne vidim ništa interesantno'];
//        $q37->modul='Module37';
//        $q37->points=["Uopšte se ne slažem"=>1,"Uglavnom se ne slažem"=>2,"Uglavnom se slažem"=>3,"U potpunosti se slažem"=>4];
//        $q37->final=0;
//        $q37->qid='psiho237';
//        $questionData[]=$q37;
//        
//                                $q38=new \Classes\QuestionData();
//        $q38->question="";
//        $q38->hint="Radio button, multiple choice";
//        //$q3->multi=1;
//     
//        //$q1->story=true;
//        $q38->answers=['q1'];
//        $q38->sentences=['q1'=>'<span>38. Stalo mi je da sve bude perfektno bez obzira koliko traje</span><br><br>'];
//       // $q31->image='2/31.jpg';
//    //    $q3->options=['q1'=>[['-- Choose --','did visit','has visited','visited','should visit']],'q2'=>[['-- Choose --','to not be','not to be','that not be','that be not']],'q3'=>[['-- Choose --','couldn’t possibly be','mustn’t possibly be','shouldn’t possibly be','isn’t possibly']],'q4'=>[['-- Choose --','farer','farest','farther','farthest']],'q5'=>[['-- Choose --','our','ours'],['-- Choose --','her','hers']],'q6'=>[['-- Choose --','be able','have','can','need']],'q7'=>[['-- Choose --','This old table’s legs','The legs of this old table','This old table legs','The legs of this old table’s']],'q8'=>[['-- Choose --','nobody','no one','anyone','somebody']]];
//      $q38->options=['q1'=>['Uopšte se ne slažem','Uglavnom se ne slažem','Uglavnom se slažem','U potpunosti se slažem']];
//        // $q3->additionalData=["images"=>['Restaurant','Hotel','Store']];
//        $q38->questionType='radioSentence';
//       // $q1->correctAnswers=['q1'=>'vidim belu pozadinu','q2'=>'vidim crnu kružnu liniju – krug','q3'=>'vidim krug u kvadratu','q4'=>'ne vidim ništa interesantno'];
//        $q38->modul='Module38';
//        $q38->points=["Uopšte se ne slažem"=>1,"Uglavnom se ne slažem"=>2,"Uglavnom se slažem"=>3,"U potpunosti se slažem"=>4];
//        $q38->final=0;
//        $q38->qid='psiho238';
//        $questionData[]=$q38;
//        
//                                   $q39=new \Classes\QuestionData();
//        $q39->question="";
//        $q39->hint="Radio button, multiple choice";
//        //$q3->multi=1;
//     
//        //$q1->story=true;
//        $q39->answers=['q1'];
//        $q39->sentences=['q1'=>'<span>39. Nemam problem da drugima ukazem na greske</span><br><br>'];
//       // $q31->image='2/31.jpg';
//    //    $q3->options=['q1'=>[['-- Choose --','did visit','has visited','visited','should visit']],'q2'=>[['-- Choose --','to not be','not to be','that not be','that be not']],'q3'=>[['-- Choose --','couldn’t possibly be','mustn’t possibly be','shouldn’t possibly be','isn’t possibly']],'q4'=>[['-- Choose --','farer','farest','farther','farthest']],'q5'=>[['-- Choose --','our','ours'],['-- Choose --','her','hers']],'q6'=>[['-- Choose --','be able','have','can','need']],'q7'=>[['-- Choose --','This old table’s legs','The legs of this old table','This old table legs','The legs of this old table’s']],'q8'=>[['-- Choose --','nobody','no one','anyone','somebody']]];
//      $q39->options=['q1'=>['Uopšte se ne slažem','Uglavnom se ne slažem','Uglavnom se slažem','U potpunosti se slažem']];
//        // $q3->additionalData=["images"=>['Restaurant','Hotel','Store']];
//        $q39->questionType='radioSentence';
//       // $q1->correctAnswers=['q1'=>'vidim belu pozadinu','q2'=>'vidim crnu kružnu liniju – krug','q3'=>'vidim krug u kvadratu','q4'=>'ne vidim ništa interesantno'];
//        $q39->modul='Module39';
//        $q39->points=["Uopšte se ne slažem"=>1,"Uglavnom se ne slažem"=>2,"Uglavnom se slažem"=>3,"U potpunosti se slažem"=>4];
//        $q39->final=0;
//        $q39->qid='psiho239';
//        $questionData[]=$q39;
//        
//                                        $q40=new \Classes\QuestionData();
//        $q40->question="";
//        $q40->hint="Radio button, multiple choice";
//        //$q3->multi=1;
//     
//        //$q1->story=true;
//        $q40->answers=['q1'];
//        $q40->sentences=['q1'=>'<span>40. Kada naiđu problem važnije mi je da se posao uradi dobro od održavanja međuljudskih odnosa</span><br><br>'];
//       // $q31->image='2/31.jpg';
//    //    $q3->options=['q1'=>[['-- Choose --','did visit','has visited','visited','should visit']],'q2'=>[['-- Choose --','to not be','not to be','that not be','that be not']],'q3'=>[['-- Choose --','couldn’t possibly be','mustn’t possibly be','shouldn’t possibly be','isn’t possibly']],'q4'=>[['-- Choose --','farer','farest','farther','farthest']],'q5'=>[['-- Choose --','our','ours'],['-- Choose --','her','hers']],'q6'=>[['-- Choose --','be able','have','can','need']],'q7'=>[['-- Choose --','This old table’s legs','The legs of this old table','This old table legs','The legs of this old table’s']],'q8'=>[['-- Choose --','nobody','no one','anyone','somebody']]];
//      $q40->options=['q1'=>['Uopšte se ne slažem','Uglavnom se ne slažem','Uglavnom se slažem','U potpunosti se slažem']];
//        // $q3->additionalData=["images"=>['Restaurant','Hotel','Store']];
//        $q40->questionType='radioSentence';
//       // $q1->correctAnswers=['q1'=>'vidim belu pozadinu','q2'=>'vidim crnu kružnu liniju – krug','q3'=>'vidim krug u kvadratu','q4'=>'ne vidim ništa interesantno'];
//        $q40->modul='Module40';
//        $q40->points=["Uopšte se ne slažem"=>1,"Uglavnom se ne slažem"=>2,"Uglavnom se slažem"=>3,"U potpunosti se slažem"=>4];
//        $q40->final=1;
//        $q40->qid='psiho240';
//        $questionData[]=$q40;
        
        ///
        ///
            $q1=new \Classes\QuestionData();
        $q1->question="";
        $q1->hint="Radio button, multiple choice";
        //$q3->multi=1;
     
        //$q1->story=true;
        $q1->answers=['q1'];
        $q1->sentences=['q1'=>'<span>1. I enjoy organizing a job or a task.</span><br><br>'];
        //$q1->image='1.png';
    //    $q3->options=['q1'=>[['-- Choose --','did visit','has visited','visited','should visit']],'q2'=>[['-- Choose --','to not be','not to be','that not be','that be not']],'q3'=>[['-- Choose --','couldn’t possibly be','mustn’t possibly be','shouldn’t possibly be','isn’t possibly']],'q4'=>[['-- Choose --','farer','farest','farther','farthest']],'q5'=>[['-- Choose --','our','ours'],['-- Choose --','her','hers']],'q6'=>[['-- Choose --','be able','have','can','need']],'q7'=>[['-- Choose --','This old table’s legs','The legs of this old table','This old table legs','The legs of this old table’s']],'q8'=>[['-- Choose --','nobody','no one','anyone','somebody']]];
      $q1->options=['q1'=>['I completely disagree','I mostly disagree','I mostly agree','I completely agree']];
        // $q3->additionalData=["images"=>['Restaurant','Hotel','Store']];
        $q1->questionType='radioSentence';
       // $q1->correctAnswers=['q1'=>'vidim belu pozadinu','q2'=>'vidim crnu kružnu liniju – krug','q3'=>'vidim krug u kvadratu','q4'=>'ne vidim ništa interesantno'];
        $q1->modul='Module1';
        $q1->points=["I completely disagree"=>1,"I mostly disagree"=>2,"I mostly agree"=>3,"I completely agree"=>4];
        $q1->final=0;
        $q1->qid='psiho21';
        $questionData[]=$q1;
        
        $q2=new \Classes\QuestionData();
        $q2->question="";
        $q2->hint="Radio button, multiple choice";
        //$q3->multi=1;
     
        //$q1->story=true;
        $q2->answers=['q1'];
        $q2->sentences=['q1'=>'<span>2. I pay attention to details while doing the task and I invest myself as much as necessary</span><br><br>'];
        //$q1->image='1.png';
    //    $q3->options=['q1'=>[['-- Choose --','did visit','has visited','visited','should visit']],'q2'=>[['-- Choose --','to not be','not to be','that not be','that be not']],'q3'=>[['-- Choose --','couldn’t possibly be','mustn’t possibly be','shouldn’t possibly be','isn’t possibly']],'q4'=>[['-- Choose --','farer','farest','farther','farthest']],'q5'=>[['-- Choose --','our','ours'],['-- Choose --','her','hers']],'q6'=>[['-- Choose --','be able','have','can','need']],'q7'=>[['-- Choose --','This old table’s legs','The legs of this old table','This old table legs','The legs of this old table’s']],'q8'=>[['-- Choose --','nobody','no one','anyone','somebody']]];
      $q2->options=['q1'=>['I completely disagree','I mostly disagree','I mostly agree','I completely agree']];
        // $q3->additionalData=["images"=>['Restaurant','Hotel','Store']];
        $q2->questionType='radioSentence';
       // $q1->correctAnswers=['q1'=>'vidim belu pozadinu','q2'=>'vidim crnu kružnu liniju – krug','q3'=>'vidim krug u kvadratu','q4'=>'ne vidim ništa interesantno'];
        $q2->modul='Module2';
        $q2->points=["I completely disagree"=>1,"I mostly disagree"=>2,"I mostly agree"=>3,"I completely agree"=>4];
        $q2->final=0;
        $q2->qid='psiho22';
        $questionData[]=$q2;
        
        $q3=new \Classes\QuestionData();
        $q3->question="";
        $q3->hint="Radio button, multiple choice";
        //$q3->multi=1;
     
        //$q1->story=true;
        $q3->answers=['q1'];
        $q3->sentences=['q1'=>'<span>3. I am always taken by a prospect of a new project</span><br><br>'];
        //$q1->image='1.png';
    //    $q3->options=['q1'=>[['-- Choose --','did visit','has visited','visited','should visit']],'q2'=>[['-- Choose --','to not be','not to be','that not be','that be not']],'q3'=>[['-- Choose --','couldn’t possibly be','mustn’t possibly be','shouldn’t possibly be','isn’t possibly']],'q4'=>[['-- Choose --','farer','farest','farther','farthest']],'q5'=>[['-- Choose --','our','ours'],['-- Choose --','her','hers']],'q6'=>[['-- Choose --','be able','have','can','need']],'q7'=>[['-- Choose --','This old table’s legs','The legs of this old table','This old table legs','The legs of this old table’s']],'q8'=>[['-- Choose --','nobody','no one','anyone','somebody']]];
      $q3->options=['q1'=>['I completely disagree','I mostly disagree','I mostly agree','I completely agree']];
        // $q3->additionalData=["images"=>['Restaurant','Hotel','Store']];
        $q3->questionType='radioSentence';
       // $q1->correctAnswers=['q1'=>'vidim belu pozadinu','q2'=>'vidim crnu kružnu liniju – krug','q3'=>'vidim krug u kvadratu','q4'=>'ne vidim ništa interesantno'];
        $q3->modul='Module3';
        $q3->points=["I completely disagree"=>1,"I mostly disagree"=>2,"I mostly agree"=>3,"I completely agree"=>4];
        $q3->final=0;
        $q3->qid='psiho23';
        $questionData[]=$q3;
        
                $q4=new \Classes\QuestionData();
        $q4->question="";
        $q4->hint="Radio button, multiple choice";
        //$q3->multi=1;
     
        //$q1->story=true;
        $q4->answers=['q1'];
        $q4->sentences=['q1'=>'<span>4. It is important for me to have clearly defined rules at work, that I shall follow </span><br><br>'];
        //$q1->image='1.png';
    //    $q3->options=['q1'=>[['-- Choose --','did visit','has visited','visited','should visit']],'q2'=>[['-- Choose --','to not be','not to be','that not be','that be not']],'q3'=>[['-- Choose --','couldn’t possibly be','mustn’t possibly be','shouldn’t possibly be','isn’t possibly']],'q4'=>[['-- Choose --','farer','farest','farther','farthest']],'q5'=>[['-- Choose --','our','ours'],['-- Choose --','her','hers']],'q6'=>[['-- Choose --','be able','have','can','need']],'q7'=>[['-- Choose --','This old table’s legs','The legs of this old table','This old table legs','The legs of this old table’s']],'q8'=>[['-- Choose --','nobody','no one','anyone','somebody']]];
      $q4->options=['q1'=>['I completely disagree','I mostly disagree','I mostly agree','I completely agree']];
        // $q3->additionalData=["images"=>['Restaurant','Hotel','Store']];
        $q4->questionType='radioSentence';
       // $q1->correctAnswers=['q1'=>'vidim belu pozadinu','q2'=>'vidim crnu kružnu liniju – krug','q3'=>'vidim krug u kvadratu','q4'=>'ne vidim ništa interesantno'];
        $q4->modul='Module4';
        $q4->points=["I completely disagree"=>1,"I mostly disagree"=>2,"I mostly agree"=>3,"I completely agree"=>4];
        $q4->final=0;
        $q4->qid='psiho24';
        $questionData[]=$q4;
        
                      $q5=new \Classes\QuestionData();
        $q5->question="";
        $q5->hint="Radio button, multiple choice";
        //$q3->multi=1;
     
        //$q1->story=true;
        $q5->answers=['q1'];
        $q5->sentences=['q1'=>'<span>5. I take the initiative to go back to the initial topic of the meeting if I notice that time is being wasted</span><br><br>'];
        //$q1->image='1.png';
    //    $q3->options=['q1'=>[['-- Choose --','did visit','has visited','visited','should visit']],'q2'=>[['-- Choose --','to not be','not to be','that not be','that be not']],'q3'=>[['-- Choose --','couldn’t possibly be','mustn’t possibly be','shouldn’t possibly be','isn’t possibly']],'q4'=>[['-- Choose --','farer','farest','farther','farthest']],'q5'=>[['-- Choose --','our','ours'],['-- Choose --','her','hers']],'q6'=>[['-- Choose --','be able','have','can','need']],'q7'=>[['-- Choose --','This old table’s legs','The legs of this old table','This old table legs','The legs of this old table’s']],'q8'=>[['-- Choose --','nobody','no one','anyone','somebody']]];
      $q5->options=['q1'=>['I completely disagree','I mostly disagree','I mostly agree','I completely agree']];
        // $q3->additionalData=["images"=>['Restaurant','Hotel','Store']];
        $q5->questionType='radioSentence';
       // $q1->correctAnswers=['q1'=>'vidim belu pozadinu','q2'=>'vidim crnu kružnu liniju – krug','q3'=>'vidim krug u kvadratu','q4'=>'ne vidim ništa interesantno'];
        $q5->modul='Module5';
        $q5->points=["I completely disagree"=>1,"I mostly disagree"=>2,"I mostly agree"=>3,"I completely agree"=>4];
        $q5->final=0;
        $q5->qid='psiho25';
        $questionData[]=$q5;
        
                              $q6=new \Classes\QuestionData();
        $q6->question="";
        $q6->hint="Radio button, multiple choice";
        //$q3->multi=1;
     
        //$q1->story=true;
        $q6->answers=['q1'];
        $q6->sentences=['q1'=>'<span>6. I am ready to support anyone`s idea if it leads to solving of the problem</span><br><br>'];
        //$q1->image='1.png';
    //    $q3->options=['q1'=>[['-- Choose --','did visit','has visited','visited','should visit']],'q2'=>[['-- Choose --','to not be','not to be','that not be','that be not']],'q3'=>[['-- Choose --','couldn’t possibly be','mustn’t possibly be','shouldn’t possibly be','isn’t possibly']],'q4'=>[['-- Choose --','farer','farest','farther','farthest']],'q5'=>[['-- Choose --','our','ours'],['-- Choose --','her','hers']],'q6'=>[['-- Choose --','be able','have','can','need']],'q7'=>[['-- Choose --','This old table’s legs','The legs of this old table','This old table legs','The legs of this old table’s']],'q8'=>[['-- Choose --','nobody','no one','anyone','somebody']]];
      $q6->options=['q1'=>['I completely disagree','I mostly disagree','I mostly agree','I completely agree']];
        // $q3->additionalData=["images"=>['Restaurant','Hotel','Store']];
        $q6->questionType='radioSentence';
       // $q1->correctAnswers=['q1'=>'vidim belu pozadinu','q2'=>'vidim crnu kružnu liniju – krug','q3'=>'vidim krug u kvadratu','q4'=>'ne vidim ništa interesantno'];
        $q6->modul='Module6';
        $q6->points=["I completely disagree"=>1,"I mostly disagree"=>2,"I mostly agree"=>3,"I completely agree"=>4];
        $q6->final=0;
        $q6->qid='psiho26';
        $questionData[]=$q6;
        
                               $q7=new \Classes\QuestionData();
        $q7->question="";
        $q7->hint="Radio button, multiple choice";
        //$q3->multi=1;
     
        //$q1->story=true;
        $q7->answers=['q1'];
        $q7->sentences=['q1'=>'<span>7. There isn’t a single person that I cannot cooperate with</span><br><br>'];
        $q7->image='2/07.jpg';
    //    $q3->options=['q1'=>[['-- Choose --','did visit','has visited','visited','should visit']],'q2'=>[['-- Choose --','to not be','not to be','that not be','that be not']],'q3'=>[['-- Choose --','couldn’t possibly be','mustn’t possibly be','shouldn’t possibly be','isn’t possibly']],'q4'=>[['-- Choose --','farer','farest','farther','farthest']],'q5'=>[['-- Choose --','our','ours'],['-- Choose --','her','hers']],'q6'=>[['-- Choose --','be able','have','can','need']],'q7'=>[['-- Choose --','This old table’s legs','The legs of this old table','This old table legs','The legs of this old table’s']],'q8'=>[['-- Choose --','nobody','no one','anyone','somebody']]];
      $q7->options=['q1'=>['I completely disagree','I mostly disagree','I mostly agree','I completely agree']];
        // $q3->additionalData=["images"=>['Restaurant','Hotel','Store']];
        $q7->questionType='radioSentence';
       // $q1->correctAnswers=['q1'=>'vidim belu pozadinu','q2'=>'vidim crnu kružnu liniju – krug','q3'=>'vidim krug u kvadratu','q4'=>'ne vidim ništa interesantno'];
        $q7->modul='Module7';
        $q7->points=["I completely disagree"=>1,"I mostly disagree"=>2,"I mostly agree"=>3,"I completely agree"=>4];
        $q7->final=0;
        $q7->qid='psiho27';
        $questionData[]=$q7;
        
                                       $q8=new \Classes\QuestionData();
        $q8->question="";
        $q8->hint="Radio button, multiple choice";
        //$q3->multi=1;
     
        //$q1->story=true;
        $q8->answers=['q1'];
        $q8->sentences=['q1'=>'<span>8. I like to work on my own</span><br><br>'];
      //  $q8->image='2/7.jpg';
    //    $q3->options=['q1'=>[['-- Choose --','did visit','has visited','visited','should visit']],'q2'=>[['-- Choose --','to not be','not to be','that not be','that be not']],'q3'=>[['-- Choose --','couldn’t possibly be','mustn’t possibly be','shouldn’t possibly be','isn’t possibly']],'q4'=>[['-- Choose --','farer','farest','farther','farthest']],'q5'=>[['-- Choose --','our','ours'],['-- Choose --','her','hers']],'q6'=>[['-- Choose --','be able','have','can','need']],'q7'=>[['-- Choose --','This old table’s legs','The legs of this old table','This old table legs','The legs of this old table’s']],'q8'=>[['-- Choose --','nobody','no one','anyone','somebody']]];
      $q8->options=['q1'=>['I completely disagree','I mostly disagree','I mostly agree','I completely agree']];
        // $q3->additionalData=["images"=>['Restaurant','Hotel','Store']];
        $q8->questionType='radioSentence';
       // $q1->correctAnswers=['q1'=>'vidim belu pozadinu','q2'=>'vidim crnu kružnu liniju – krug','q3'=>'vidim krug u kvadratu','q4'=>'ne vidim ništa interesantno'];
        $q8->modul='Module8';
        $q8->points=["I completely disagree"=>1,"I mostly disagree"=>2,"I mostly agree"=>3,"I completely agree"=>4];
        $q8->final=0;
        $q8->qid='psiho28';
        $questionData[]=$q8;
        
                                               $q9=new \Classes\QuestionData();
        $q9->question="";
        $q9->hint="Radio button, multiple choice";
        //$q3->multi=1;
     
        //$q1->story=true;
        $q9->answers=['q1'];
        $q9->sentences=['q1'=>'<span>9. I enjoy mentoring and transferring knowledge to others</span><br><br>'];
      //  $q8->image='2/7.jpg';
    //    $q3->options=['q1'=>[['-- Choose --','did visit','has visited','visited','should visit']],'q2'=>[['-- Choose --','to not be','not to be','that not be','that be not']],'q3'=>[['-- Choose --','couldn’t possibly be','mustn’t possibly be','shouldn’t possibly be','isn’t possibly']],'q4'=>[['-- Choose --','farer','farest','farther','farthest']],'q5'=>[['-- Choose --','our','ours'],['-- Choose --','her','hers']],'q6'=>[['-- Choose --','be able','have','can','need']],'q7'=>[['-- Choose --','This old table’s legs','The legs of this old table','This old table legs','The legs of this old table’s']],'q8'=>[['-- Choose --','nobody','no one','anyone','somebody']]];
      $q9->options=['q1'=>['I completely disagree','I mostly disagree','I mostly agree','I completely agree']];
        // $q3->additionalData=["images"=>['Restaurant','Hotel','Store']];
        $q9->questionType='radioSentence';
       // $q1->correctAnswers=['q1'=>'vidim belu pozadinu','q2'=>'vidim crnu kružnu liniju – krug','q3'=>'vidim krug u kvadratu','q4'=>'ne vidim ništa interesantno'];
        $q9->modul='Module9';
        $q9->points=["I completely disagree"=>1,"I mostly disagree"=>2,"I mostly agree"=>3,"I completely agree"=>4];
        $q9->final=0;
        $q9->qid='psiho29';
        $questionData[]=$q9;
        
                                                   $q10=new \Classes\QuestionData();
        $q10->question="";
        $q10->hint="Radio button, multiple choice";
        //$q3->multi=1;
     
        //$q1->story=true;
        $q10->answers=['q1'];
        $q10->sentences=['q1'=>'<span>10. It is important to me that I have influence over decisions</span><br><br>'];
      //  $q8->image='2/7.jpg';
    //    $q3->options=['q1'=>[['-- Choose --','did visit','has visited','visited','should visit']],'q2'=>[['-- Choose --','to not be','not to be','that not be','that be not']],'q3'=>[['-- Choose --','couldn’t possibly be','mustn’t possibly be','shouldn’t possibly be','isn’t possibly']],'q4'=>[['-- Choose --','farer','farest','farther','farthest']],'q5'=>[['-- Choose --','our','ours'],['-- Choose --','her','hers']],'q6'=>[['-- Choose --','be able','have','can','need']],'q7'=>[['-- Choose --','This old table’s legs','The legs of this old table','This old table legs','The legs of this old table’s']],'q8'=>[['-- Choose --','nobody','no one','anyone','somebody']]];
      $q10->options=['q1'=>['I completely disagree','I mostly disagree','I mostly agree','I completely agree']];
        // $q3->additionalData=["images"=>['Restaurant','Hotel','Store']];
        $q10->questionType='radioSentence';
       // $q1->correctAnswers=['q1'=>'vidim belu pozadinu','q2'=>'vidim crnu kružnu liniju – krug','q3'=>'vidim krug u kvadratu','q4'=>'ne vidim ništa interesantno'];
        $q10->modul='Module10';
        $q10->points=["I completely disagree"=>1,"I mostly disagree"=>2,"I mostly agree"=>3,"I completely agree"=>4];
        $q10->final=0;
        $q10->qid='psiho210';
        $questionData[]=$q10;
        
                                                           $q11=new \Classes\QuestionData();
        $q11->question="";
        $q11->hint="Radio button, multiple choice";
        //$q3->multi=1;
     
        //$q1->story=true;
        $q11->answers=['q1'];
        $q11->sentences=['q1'=>'<span>11. It`s important for me to help my colleagues when they have a problem</span><br><br>'];
      //  $q8->image='2/7.jpg';
    //    $q3->options=['q1'=>[['-- Choose --','did visit','has visited','visited','should visit']],'q2'=>[['-- Choose --','to not be','not to be','that not be','that be not']],'q3'=>[['-- Choose --','couldn’t possibly be','mustn’t possibly be','shouldn’t possibly be','isn’t possibly']],'q4'=>[['-- Choose --','farer','farest','farther','farthest']],'q5'=>[['-- Choose --','our','ours'],['-- Choose --','her','hers']],'q6'=>[['-- Choose --','be able','have','can','need']],'q7'=>[['-- Choose --','This old table’s legs','The legs of this old table','This old table legs','The legs of this old table’s']],'q8'=>[['-- Choose --','nobody','no one','anyone','somebody']]];
      $q11->options=['q1'=>['I completely disagree','I mostly disagree','I mostly agree','I completely agree']];
        // $q3->additionalData=["images"=>['Restaurant','Hotel','Store']];
        $q11->questionType='radioSentence';
       // $q1->correctAnswers=['q1'=>'vidim belu pozadinu','q2'=>'vidim crnu kružnu liniju – krug','q3'=>'vidim krug u kvadratu','q4'=>'ne vidim ništa interesantno'];
        $q11->modul='Module11';
        $q11->points=["I completely disagree"=>1,"I mostly disagree"=>2,"I mostly agree"=>3,"I completely agree"=>4];
        $q11->final=0;
        $q11->qid='psiho211';
        $questionData[]=$q11;
        
                                                          $q12=new \Classes\QuestionData();
        $q12->question="";
        $q12->hint="Radio button, multiple choice";
        //$q3->multi=1;
     
        //$q1->story=true;
        $q12->answers=['q1'];
        $q12->sentences=['q1'=>'<span>12. Being practical is much more important than the new ideas</span><br><br>'];
        $q12->image='2/12-b.jpg';
    //    $q3->options=['q1'=>[['-- Choose --','did visit','has visited','visited','should visit']],'q2'=>[['-- Choose --','to not be','not to be','that not be','that be not']],'q3'=>[['-- Choose --','couldn’t possibly be','mustn’t possibly be','shouldn’t possibly be','isn’t possibly']],'q4'=>[['-- Choose --','farer','farest','farther','farthest']],'q5'=>[['-- Choose --','our','ours'],['-- Choose --','her','hers']],'q6'=>[['-- Choose --','be able','have','can','need']],'q7'=>[['-- Choose --','This old table’s legs','The legs of this old table','This old table legs','The legs of this old table’s']],'q8'=>[['-- Choose --','nobody','no one','anyone','somebody']]];
      $q12->options=['q1'=>['I completely disagree','I mostly disagree','I mostly agree','I completely agree']];
        // $q3->additionalData=["images"=>['Restaurant','Hotel','Store']];
        $q12->questionType='radioSentence';
       // $q1->correctAnswers=['q1'=>'vidim belu pozadinu','q2'=>'vidim crnu kružnu liniju – krug','q3'=>'vidim krug u kvadratu','q4'=>'ne vidim ništa interesantno'];
        $q12->modul='Module12';
        $q12->points=["I completely disagree"=>1,"I mostly disagree"=>2,"I mostly agree"=>3,"I completely agree"=>4];
        $q12->final=0;
        $q12->qid='psiho212';
        $questionData[]=$q12;
        
                                                                  $q13=new \Classes\QuestionData();
        $q13->question="";
        $q13->hint="Radio button, multiple choice";
        //$q3->multi=1;
     
        //$q1->story=true;
        $q13->answers=['q1'];
        $q13->sentences=['q1'=>'<span>13. I like to keep, and stick to the order</span><br><br>'];
       // $q13->image='2/12-b.jpg';
    //    $q3->options=['q1'=>[['-- Choose --','did visit','has visited','visited','should visit']],'q2'=>[['-- Choose --','to not be','not to be','that not be','that be not']],'q3'=>[['-- Choose --','couldn’t possibly be','mustn’t possibly be','shouldn’t possibly be','isn’t possibly']],'q4'=>[['-- Choose --','farer','farest','farther','farthest']],'q5'=>[['-- Choose --','our','ours'],['-- Choose --','her','hers']],'q6'=>[['-- Choose --','be able','have','can','need']],'q7'=>[['-- Choose --','This old table’s legs','The legs of this old table','This old table legs','The legs of this old table’s']],'q8'=>[['-- Choose --','nobody','no one','anyone','somebody']]];
      $q13->options=['q1'=>['I completely disagree','I mostly disagree','I mostly agree','I completely agree']];
        // $q3->additionalData=["images"=>['Restaurant','Hotel','Store']];
        $q13->questionType='radioSentence';
       // $q1->correctAnswers=['q1'=>'vidim belu pozadinu','q2'=>'vidim crnu kružnu liniju – krug','q3'=>'vidim krug u kvadratu','q4'=>'ne vidim ništa interesantno'];
        $q13->modul='Module13';
        $q13->points=["I completely disagree"=>1,"I mostly disagree"=>2,"I mostly agree"=>3,"I completely agree"=>4];
        $q13->final=0;
        $q13->qid='psiho213';
        $questionData[]=$q13;
        
        $q14=new \Classes\QuestionData();
        $q14->question="";
        $q14->hint="Radio button, multiple choice";
        //$q3->multi=1;
     
        //$q1->story=true;
        $q14->answers=['q1'];
        $q14->sentences=['q1'=>'<span>14. I monitor areas where problems may possibly arise</span><br><br>'];
       // $q13->image='2/12-b.jpg';
    //    $q3->options=['q1'=>[['-- Choose --','did visit','has visited','visited','should visit']],'q2'=>[['-- Choose --','to not be','not to be','that not be','that be not']],'q3'=>[['-- Choose --','couldn’t possibly be','mustn’t possibly be','shouldn’t possibly be','isn’t possibly']],'q4'=>[['-- Choose --','farer','farest','farther','farthest']],'q5'=>[['-- Choose --','our','ours'],['-- Choose --','her','hers']],'q6'=>[['-- Choose --','be able','have','can','need']],'q7'=>[['-- Choose --','This old table’s legs','The legs of this old table','This old table legs','The legs of this old table’s']],'q8'=>[['-- Choose --','nobody','no one','anyone','somebody']]];
      $q14->options=['q1'=>['I completely disagree','I mostly disagree','I mostly agree','I completely agree']];
        // $q3->additionalData=["images"=>['Restaurant','Hotel','Store']];
        $q14->questionType='radioSentence';
       // $q1->correctAnswers=['q1'=>'vidim belu pozadinu','q2'=>'vidim crnu kružnu liniju – krug','q3'=>'vidim krug u kvadratu','q4'=>'ne vidim ništa interesantno'];
        $q14->modul='Module14';
        $q14->points=["I completely disagree"=>1,"I mostly disagree"=>2,"I mostly agree"=>3,"I completely agree"=>4];
        $q14->final=0;
        $q14->qid='psiho214';
        $questionData[]=$q14;
        
           $q15=new \Classes\QuestionData();
        $q15->question="";
        $q15->hint="Radio button, multiple choice";
        //$q3->multi=1;
     
        //$q1->story=true;
        $q15->answers=['q1'];
        $q15->sentences=['q1'=>'<span>15. I always leave some time to review what I have done before I say that I finished the job</span><br><br>'];
       // $q13->image='2/12-b.jpg';
    //    $q3->options=['q1'=>[['-- Choose --','did visit','has visited','visited','should visit']],'q2'=>[['-- Choose --','to not be','not to be','that not be','that be not']],'q3'=>[['-- Choose --','couldn’t possibly be','mustn’t possibly be','shouldn’t possibly be','isn’t possibly']],'q4'=>[['-- Choose --','farer','farest','farther','farthest']],'q5'=>[['-- Choose --','our','ours'],['-- Choose --','her','hers']],'q6'=>[['-- Choose --','be able','have','can','need']],'q7'=>[['-- Choose --','This old table’s legs','The legs of this old table','This old table legs','The legs of this old table’s']],'q8'=>[['-- Choose --','nobody','no one','anyone','somebody']]];
      $q15->options=['q1'=>['I completely disagree','I mostly disagree','I mostly agree','I completely agree']];
        // $q3->additionalData=["images"=>['Restaurant','Hotel','Store']];
        $q15->questionType='radioSentence';
       // $q1->correctAnswers=['q1'=>'vidim belu pozadinu','q2'=>'vidim crnu kružnu liniju – krug','q3'=>'vidim krug u kvadratu','q4'=>'ne vidim ništa interesantno'];
        $q15->modul='Module15';
        $q15->points=["I completely disagree"=>1,"I mostly disagree"=>2,"I mostly agree"=>3,"I completely agree"=>4];
        $q15->final=0;
        $q15->qid='psiho215';
        $questionData[]=$q15;
        
            $q16=new \Classes\QuestionData();
        $q16->question="";
        $q16->hint="Radio button, multiple choice";
        //$q3->multi=1;
     
        //$q1->story=true;
        $q16->answers=['q1'];
        $q16->sentences=['q1'=>'<span>16. I often have original ideas</span><br><br>'];
       // $q13->image='2/12-b.jpg';
    //    $q3->options=['q1'=>[['-- Choose --','did visit','has visited','visited','should visit']],'q2'=>[['-- Choose --','to not be','not to be','that not be','that be not']],'q3'=>[['-- Choose --','couldn’t possibly be','mustn’t possibly be','shouldn’t possibly be','isn’t possibly']],'q4'=>[['-- Choose --','farer','farest','farther','farthest']],'q5'=>[['-- Choose --','our','ours'],['-- Choose --','her','hers']],'q6'=>[['-- Choose --','be able','have','can','need']],'q7'=>[['-- Choose --','This old table’s legs','The legs of this old table','This old table legs','The legs of this old table’s']],'q8'=>[['-- Choose --','nobody','no one','anyone','somebody']]];
      $q16->options=['q1'=>['I completely disagree','I mostly disagree','I mostly agree','I completely agree']];
        // $q3->additionalData=["images"=>['Restaurant','Hotel','Store']];
        $q16->questionType='radioSentence';
       // $q1->correctAnswers=['q1'=>'vidim belu pozadinu','q2'=>'vidim crnu kružnu liniju – krug','q3'=>'vidim krug u kvadratu','q4'=>'ne vidim ništa interesantno'];
        $q16->modul='Module16';
        $q16->points=["I completely disagree"=>1,"I mostly disagree"=>2,"I mostly agree"=>3,"I completely agree"=>4];
        $q16->final=0;
        $q16->qid='psiho216';
        $questionData[]=$q16;
        
             $q17=new \Classes\QuestionData();
        $q17->question="";
        $q17->hint="Radio button, multiple choice";
        //$q3->multi=1;
     
        //$q1->story=true;
        $q17->answers=['q1'];
        $q17->sentences=['q1'=>'<span>17. Whenever it is needed, I easily take over responsibility at work</span><br><br>'];
       // $q13->image='2/12-b.jpg';
    //    $q3->options=['q1'=>[['-- Choose --','did visit','has visited','visited','should visit']],'q2'=>[['-- Choose --','to not be','not to be','that not be','that be not']],'q3'=>[['-- Choose --','couldn’t possibly be','mustn’t possibly be','shouldn’t possibly be','isn’t possibly']],'q4'=>[['-- Choose --','farer','farest','farther','farthest']],'q5'=>[['-- Choose --','our','ours'],['-- Choose --','her','hers']],'q6'=>[['-- Choose --','be able','have','can','need']],'q7'=>[['-- Choose --','This old table’s legs','The legs of this old table','This old table legs','The legs of this old table’s']],'q8'=>[['-- Choose --','nobody','no one','anyone','somebody']]];
      $q17->options=['q1'=>['I completely disagree','I mostly disagree','I mostly agree','I completely agree']];
        // $q3->additionalData=["images"=>['Restaurant','Hotel','Store']];
        $q17->questionType='radioSentence';
       // $q1->correctAnswers=['q1'=>'vidim belu pozadinu','q2'=>'vidim crnu kružnu liniju – krug','q3'=>'vidim krug u kvadratu','q4'=>'ne vidim ništa interesantno'];
        $q17->modul='Module17';
        $q17->points=["I completely disagree"=>1,"I mostly disagree"=>2,"I mostly agree"=>3,"I completely agree"=>4];
        $q17->final=0;
        $q17->qid='psiho217';
        $questionData[]=$q17;
        
                    $q18=new \Classes\QuestionData();
        $q18->question="";
        $q18->hint="Radio button, multiple choice";
        //$q3->multi=1;
     
        //$q1->story=true;
        $q18->answers=['q1'];
        $q18->sentences=['q1'=>'<span>18. I believe that everything has already been "invented", it only needs to be applied</span><br><br>'];
       // $q13->image='2/12-b.jpg';
    //    $q3->options=['q1'=>[['-- Choose --','did visit','has visited','visited','should visit']],'q2'=>[['-- Choose --','to not be','not to be','that not be','that be not']],'q3'=>[['-- Choose --','couldn’t possibly be','mustn’t possibly be','shouldn’t possibly be','isn’t possibly']],'q4'=>[['-- Choose --','farer','farest','farther','farthest']],'q5'=>[['-- Choose --','our','ours'],['-- Choose --','her','hers']],'q6'=>[['-- Choose --','be able','have','can','need']],'q7'=>[['-- Choose --','This old table’s legs','The legs of this old table','This old table legs','The legs of this old table’s']],'q8'=>[['-- Choose --','nobody','no one','anyone','somebody']]];
      $q18->options=['q1'=>['I completely disagree','I mostly disagree','I mostly agree','I completely agree']];
        // $q3->additionalData=["images"=>['Restaurant','Hotel','Store']];
        $q18->questionType='radioSentence';
       // $q1->correctAnswers=['q1'=>'vidim belu pozadinu','q2'=>'vidim crnu kružnu liniju – krug','q3'=>'vidim krug u kvadratu','q4'=>'ne vidim ništa interesantno'];
        $q18->modul='Module18';
        $q18->points=["I completely disagree"=>1,"I mostly disagree"=>2,"I mostly agree"=>3,"I completely agree"=>4];
        $q18->final=0;
        $q18->qid='psiho218';
        $questionData[]=$q18;
        
                          $q19=new \Classes\QuestionData();
        $q19->question="";
        $q19->hint="Radio button, multiple choice";
        //$q3->multi=1;
     
        //$q1->story=true;
        $q19->answers=['q1'];
        $q19->sentences=['q1'=>'<span>19. It`s important for me to always have fun at work, too. How important is that kind work atmosphere to you?</span><br><br>'];
       $q19->image='2/19.jpg';
    //    $q3->options=['q1'=>[['-- Choose --','did visit','has visited','visited','should visit']],'q2'=>[['-- Choose --','to not be','not to be','that not be','that be not']],'q3'=>[['-- Choose --','couldn’t possibly be','mustn’t possibly be','shouldn’t possibly be','isn’t possibly']],'q4'=>[['-- Choose --','farer','farest','farther','farthest']],'q5'=>[['-- Choose --','our','ours'],['-- Choose --','her','hers']],'q6'=>[['-- Choose --','be able','have','can','need']],'q7'=>[['-- Choose --','This old table’s legs','The legs of this old table','This old table legs','The legs of this old table’s']],'q8'=>[['-- Choose --','nobody','no one','anyone','somebody']]];
      $q19->options=['q1'=>['I completely disagree','I mostly disagree','I mostly agree','I completely agree']];
        // $q3->additionalData=["images"=>['Restaurant','Hotel','Store']];
        $q19->questionType='radioSentence';
       // $q1->correctAnswers=['q1'=>'vidim belu pozadinu','q2'=>'vidim crnu kružnu liniju – krug','q3'=>'vidim krug u kvadratu','q4'=>'ne vidim ništa interesantno'];
        $q19->modul='Module19';
        $q19->points=["I completely disagree"=>1,"I mostly disagree"=>2,"I mostly agree"=>3,"I completely agree"=>4];
        $q19->final=0;
        $q19->qid='psiho219';
        $questionData[]=$q19;
        
                                  $q20=new \Classes\QuestionData();
        $q20->question="";
        $q20->hint="Radio button, multiple choice";
        //$q3->multi=1;
     
        //$q1->story=true;
        $q20->answers=['q1'];
        $q20->sentences=['q1'=>'<span>20. I notice omissions at work, even those that others do not see</span><br><br>'];
       // $q13->image='2/12-b.jpg';
    //    $q3->options=['q1'=>[['-- Choose --','did visit','has visited','visited','should visit']],'q2'=>[['-- Choose --','to not be','not to be','that not be','that be not']],'q3'=>[['-- Choose --','couldn’t possibly be','mustn’t possibly be','shouldn’t possibly be','isn’t possibly']],'q4'=>[['-- Choose --','farer','farest','farther','farthest']],'q5'=>[['-- Choose --','our','ours'],['-- Choose --','her','hers']],'q6'=>[['-- Choose --','be able','have','can','need']],'q7'=>[['-- Choose --','This old table’s legs','The legs of this old table','This old table legs','The legs of this old table’s']],'q8'=>[['-- Choose --','nobody','no one','anyone','somebody']]];
      $q20->options=['q1'=>['I completely disagree','I mostly disagree','I mostly agree','I completely agree']];
        // $q3->additionalData=["images"=>['Restaurant','Hotel','Store']];
        $q20->questionType='radioSentence';
       // $q1->correctAnswers=['q1'=>'vidim belu pozadinu','q2'=>'vidim crnu kružnu liniju – krug','q3'=>'vidim krug u kvadratu','q4'=>'ne vidim ništa interesantno'];
        $q20->modul='Module20';
        $q20->points=["I completely disagree"=>1,"I mostly disagree"=>2,"I mostly agree"=>3,"I completely agree"=>4];
        $q20->final=0;
        $q20->qid='psiho220';
        $questionData[]=$q20;
        
                                    $q21=new \Classes\QuestionData();
        $q21->question="";
        $q21->hint="Radio button, multiple choice";
        //$q3->multi=1;
     
        //$q1->story=true;
        $q21->answers=['q1'];
        $q21->sentences=['q1'=>'<span>21. I am very well organized</span><br><br>'];
       // $q13->image='2/12-b.jpg';
    //    $q3->options=['q1'=>[['-- Choose --','did visit','has visited','visited','should visit']],'q2'=>[['-- Choose --','to not be','not to be','that not be','that be not']],'q3'=>[['-- Choose --','couldn’t possibly be','mustn’t possibly be','shouldn’t possibly be','isn’t possibly']],'q4'=>[['-- Choose --','farer','farest','farther','farthest']],'q5'=>[['-- Choose --','our','ours'],['-- Choose --','her','hers']],'q6'=>[['-- Choose --','be able','have','can','need']],'q7'=>[['-- Choose --','This old table’s legs','The legs of this old table','This old table legs','The legs of this old table’s']],'q8'=>[['-- Choose --','nobody','no one','anyone','somebody']]];
      $q21->options=['q1'=>['I completely disagree','I mostly disagree','I mostly agree','I completely agree']];
        // $q3->additionalData=["images"=>['Restaurant','Hotel','Store']];
        $q21->questionType='radioSentence';
       // $q1->correctAnswers=['q1'=>'vidim belu pozadinu','q2'=>'vidim crnu kružnu liniju – krug','q3'=>'vidim krug u kvadratu','q4'=>'ne vidim ništa interesantno'];
        $q21->modul='Module21';
        $q21->points=["I completely disagree"=>1,"I mostly disagree"=>2,"I mostly agree"=>3,"I completely agree"=>4];
        $q21->final=0;
        $q21->qid='psiho221';
        $questionData[]=$q21;
        
                                      $q22=new \Classes\QuestionData();
        $q22->question="";
        $q22->hint="Radio button, multiple choice";
        //$q3->multi=1;
     
        //$q1->story=true;
        $q22->answers=['q1'];
        $q22->sentences=['q1'=>'<span>22. I am ready to accept a job or task that I do not know enough about</span><br><br>'];
       // $q13->image='2/12-b.jpg';
    //    $q3->options=['q1'=>[['-- Choose --','did visit','has visited','visited','should visit']],'q2'=>[['-- Choose --','to not be','not to be','that not be','that be not']],'q3'=>[['-- Choose --','couldn’t possibly be','mustn’t possibly be','shouldn’t possibly be','isn’t possibly']],'q4'=>[['-- Choose --','farer','farest','farther','farthest']],'q5'=>[['-- Choose --','our','ours'],['-- Choose --','her','hers']],'q6'=>[['-- Choose --','be able','have','can','need']],'q7'=>[['-- Choose --','This old table’s legs','The legs of this old table','This old table legs','The legs of this old table’s']],'q8'=>[['-- Choose --','nobody','no one','anyone','somebody']]];
      $q22->options=['q1'=>['I completely disagree','I mostly disagree','I mostly agree','I completely agree']];
        // $q3->additionalData=["images"=>['Restaurant','Hotel','Store']];
        $q22->questionType='radioSentence';
       // $q1->correctAnswers=['q1'=>'vidim belu pozadinu','q2'=>'vidim crnu kružnu liniju – krug','q3'=>'vidim krug u kvadratu','q4'=>'ne vidim ništa interesantno'];
        $q22->modul='Module22';
        $q22->points=["I completely disagree"=>1,"I mostly disagree"=>2,"I mostly agree"=>3,"I completely agree"=>4];
        $q22->final=0;
        $q22->qid='psiho222';
        $questionData[]=$q22;
        
                                           $q23=new \Classes\QuestionData();
        $q23->question="";
        $q23->hint="Radio button, multiple choice";
        //$q3->multi=1;
     
        //$q1->story=true;
        $q23->answers=['q1'];
        $q23->sentences=['q1'=>'<span>23. I am often critical of myself as well as others at work</span><br><br>'];
        $q23->image='2/23.jpg';
    //    $q3->options=['q1'=>[['-- Choose --','did visit','has visited','visited','should visit']],'q2'=>[['-- Choose --','to not be','not to be','that not be','that be not']],'q3'=>[['-- Choose --','couldn’t possibly be','mustn’t possibly be','shouldn’t possibly be','isn’t possibly']],'q4'=>[['-- Choose --','farer','farest','farther','farthest']],'q5'=>[['-- Choose --','our','ours'],['-- Choose --','her','hers']],'q6'=>[['-- Choose --','be able','have','can','need']],'q7'=>[['-- Choose --','This old table’s legs','The legs of this old table','This old table legs','The legs of this old table’s']],'q8'=>[['-- Choose --','nobody','no one','anyone','somebody']]];
      $q23->options=['q1'=>['I completely disagree','I mostly disagree','I mostly agree','I completely agree']];
        // $q3->additionalData=["images"=>['Restaurant','Hotel','Store']];
        $q23->questionType='radioSentence';
       // $q1->correctAnswers=['q1'=>'vidim belu pozadinu','q2'=>'vidim crnu kružnu liniju – krug','q3'=>'vidim krug u kvadratu','q4'=>'ne vidim ništa interesantno'];
        $q23->modul='Module23';
        $q23->points=["I completely disagree"=>1,"I mostly disagree"=>2,"I mostly agree"=>3,"I completely agree"=>4];
        $q23->final=0;
        $q23->qid='psiho223';
        $questionData[]=$q23;
        
         $q24=new \Classes\QuestionData();
        $q24->question="";
        $q24->hint="Radio button, multiple choice";
        //$q3->multi=1;
     
        //$q1->story=true;
        $q24->answers=['q1'];
        $q24->sentences=['q1'=>'<span>24. It does not matter to me how long some job is going to last, as long as it is done well</span><br><br>'];
        //$q24->image='2/23.jpg';
    //    $q3->options=['q1'=>[['-- Choose --','did visit','has visited','visited','should visit']],'q2'=>[['-- Choose --','to not be','not to be','that not be','that be not']],'q3'=>[['-- Choose --','couldn’t possibly be','mustn’t possibly be','shouldn’t possibly be','isn’t possibly']],'q4'=>[['-- Choose --','farer','farest','farther','farthest']],'q5'=>[['-- Choose --','our','ours'],['-- Choose --','her','hers']],'q6'=>[['-- Choose --','be able','have','can','need']],'q7'=>[['-- Choose --','This old table’s legs','The legs of this old table','This old table legs','The legs of this old table’s']],'q8'=>[['-- Choose --','nobody','no one','anyone','somebody']]];
      $q24->options=['q1'=>['I completely disagree','I mostly disagree','I mostly agree','I completely agree']];
        // $q3->additionalData=["images"=>['Restaurant','Hotel','Store']];
        $q24->questionType='radioSentence';
       // $q1->correctAnswers=['q1'=>'vidim belu pozadinu','q2'=>'vidim crnu kružnu liniju – krug','q3'=>'vidim krug u kvadratu','q4'=>'ne vidim ništa interesantno'];
        $q24->modul='Module24';
        $q24->points=["I completely disagree"=>1,"I mostly disagree"=>2,"I mostly agree"=>3,"I completely agree"=>4];
        $q24->final=0;
        $q24->qid='psiho224';
        $questionData[]=$q24;
        
            $q25=new \Classes\QuestionData();
        $q25->question="";
        $q25->hint="Radio button, multiple choice";
        //$q3->multi=1;
     
        //$q1->story=true;
        $q25->answers=['q1'];
        $q25->sentences=['q1'=>'<span>25.  It`s important for me to meet new people through my work</span><br><br>'];
        //$q24->image='2/23.jpg';
    //    $q3->options=['q1'=>[['-- Choose --','did visit','has visited','visited','should visit']],'q2'=>[['-- Choose --','to not be','not to be','that not be','that be not']],'q3'=>[['-- Choose --','couldn’t possibly be','mustn’t possibly be','shouldn’t possibly be','isn’t possibly']],'q4'=>[['-- Choose --','farer','farest','farther','farthest']],'q5'=>[['-- Choose --','our','ours'],['-- Choose --','her','hers']],'q6'=>[['-- Choose --','be able','have','can','need']],'q7'=>[['-- Choose --','This old table’s legs','The legs of this old table','This old table legs','The legs of this old table’s']],'q8'=>[['-- Choose --','nobody','no one','anyone','somebody']]];
      $q25->options=['q1'=>['I completely disagree','I mostly disagree','I mostly agree','I completely agree']];
        // $q3->additionalData=["images"=>['Restaurant','Hotel','Store']];
        $q25->questionType='radioSentence';
       // $q1->correctAnswers=['q1'=>'vidim belu pozadinu','q2'=>'vidim crnu kružnu liniju – krug','q3'=>'vidim krug u kvadratu','q4'=>'ne vidim ništa interesantno'];
        $q25->modul='Module25';
        $q25->points=["I completely disagree"=>1,"I mostly disagree"=>2,"I mostly agree"=>3,"I completely agree"=>4];
        $q25->final=0;
        $q25->qid='psiho225';
        $questionData[]=$q25;
        
                $q26=new \Classes\QuestionData();
        $q26->question="";
        $q26->hint="Radio button, multiple choice";
        //$q3->multi=1;
     
        //$q1->story=true;
        $q26->answers=['q1'];
        $q26->sentences=['q1'=>'<span>26. I function well under pressure</span><br><br>'];
        //$q24->image='2/23.jpg';
    //    $q3->options=['q1'=>[['-- Choose --','did visit','has visited','visited','should visit']],'q2'=>[['-- Choose --','to not be','not to be','that not be','that be not']],'q3'=>[['-- Choose --','couldn’t possibly be','mustn’t possibly be','shouldn’t possibly be','isn’t possibly']],'q4'=>[['-- Choose --','farer','farest','farther','farthest']],'q5'=>[['-- Choose --','our','ours'],['-- Choose --','her','hers']],'q6'=>[['-- Choose --','be able','have','can','need']],'q7'=>[['-- Choose --','This old table’s legs','The legs of this old table','This old table legs','The legs of this old table’s']],'q8'=>[['-- Choose --','nobody','no one','anyone','somebody']]];
      $q26->options=['q1'=>['I completely disagree','I mostly disagree','I mostly agree','I completely agree']];
        // $q3->additionalData=["images"=>['Restaurant','Hotel','Store']];
        $q26->questionType='radioSentence';
       // $q1->correctAnswers=['q1'=>'vidim belu pozadinu','q2'=>'vidim crnu kružnu liniju – krug','q3'=>'vidim krug u kvadratu','q4'=>'ne vidim ništa interesantno'];
        $q26->modul='Module26';
        $q26->points=["I completely disagree"=>1,"I mostly disagree"=>2,"I mostly agree"=>3,"I completely agree"=>4];
        $q26->final=0;
        $q26->qid='psiho226';
        $questionData[]=$q26;
        
                   $q27=new \Classes\QuestionData();
        $q27->question="";
        $q27->hint="Radio button, multiple choice";
        //$q3->multi=1;
     
        //$q1->story=true;
        $q27->answers=['q1'];
        $q27->sentences=['q1'=>'<span>27. I prefer working with paperwork and technology rather than with people</span><br><br>'];
        //$q24->image='2/23.jpg';
    //    $q3->options=['q1'=>[['-- Choose --','did visit','has visited','visited','should visit']],'q2'=>[['-- Choose --','to not be','not to be','that not be','that be not']],'q3'=>[['-- Choose --','couldn’t possibly be','mustn’t possibly be','shouldn’t possibly be','isn’t possibly']],'q4'=>[['-- Choose --','farer','farest','farther','farthest']],'q5'=>[['-- Choose --','our','ours'],['-- Choose --','her','hers']],'q6'=>[['-- Choose --','be able','have','can','need']],'q7'=>[['-- Choose --','This old table’s legs','The legs of this old table','This old table legs','The legs of this old table’s']],'q8'=>[['-- Choose --','nobody','no one','anyone','somebody']]];
      $q27->options=['q1'=>['I completely disagree','I mostly disagree','I mostly agree','I completely agree']];
        // $q3->additionalData=["images"=>['Restaurant','Hotel','Store']];
        $q27->questionType='radioSentence';
       // $q1->correctAnswers=['q1'=>'vidim belu pozadinu','q2'=>'vidim crnu kružnu liniju – krug','q3'=>'vidim krug u kvadratu','q4'=>'ne vidim ništa interesantno'];
        $q27->modul='Module27';
        $q27->points=["I completely disagree"=>1,"I mostly disagree"=>2,"I mostly agree"=>3,"I completely agree"=>4];
        $q27->final=0;
        $q27->qid='psiho227';
        $questionData[]=$q27;
        
                           $q28=new \Classes\QuestionData();
        $q28->question="";
        $q28->hint="Radio button, multiple choice";
        //$q3->multi=1;
     
        //$q1->story=true;
        $q28->answers=['q1'];
        $q28->sentences=['q1'=>'<span>28. I have no problem to oppose the entire team if it is in the best interest of the job itself</span><br><br>'];
        //$q24->image='2/23.jpg';
    //    $q3->options=['q1'=>[['-- Choose --','did visit','has visited','visited','should visit']],'q2'=>[['-- Choose --','to not be','not to be','that not be','that be not']],'q3'=>[['-- Choose --','couldn’t possibly be','mustn’t possibly be','shouldn’t possibly be','isn’t possibly']],'q4'=>[['-- Choose --','farer','farest','farther','farthest']],'q5'=>[['-- Choose --','our','ours'],['-- Choose --','her','hers']],'q6'=>[['-- Choose --','be able','have','can','need']],'q7'=>[['-- Choose --','This old table’s legs','The legs of this old table','This old table legs','The legs of this old table’s']],'q8'=>[['-- Choose --','nobody','no one','anyone','somebody']]];
      $q28->options=['q1'=>['I completely disagree','I mostly disagree','I mostly agree','I completely agree']];
        // $q3->additionalData=["images"=>['Restaurant','Hotel','Store']];
        $q28->questionType='radioSentence';
       // $q1->correctAnswers=['q1'=>'vidim belu pozadinu','q2'=>'vidim crnu kružnu liniju – krug','q3'=>'vidim krug u kvadratu','q4'=>'ne vidim ništa interesantno'];
        $q28->modul='Module28';
        $q28->points=["I completely disagree"=>1,"I mostly disagree"=>2,"I mostly agree"=>3,"I completely agree"=>4];
        $q28->final=0;
        $q28->qid='psiho228';
        $questionData[]=$q28;
        
                             $q29=new \Classes\QuestionData();
        $q29->question="";
        $q29->hint="Radio button, multiple choice";
        //$q3->multi=1;
     
        //$q1->story=true;
        $q29->answers=['q1'];
        $q29->sentences=['q1'=>'<span>29. It`s hard for me to work when the goal is not clearly defined</span><br><br>'];
        //$q24->image='2/23.jpg';
    //    $q3->options=['q1'=>[['-- Choose --','did visit','has visited','visited','should visit']],'q2'=>[['-- Choose --','to not be','not to be','that not be','that be not']],'q3'=>[['-- Choose --','couldn’t possibly be','mustn’t possibly be','shouldn’t possibly be','isn’t possibly']],'q4'=>[['-- Choose --','farer','farest','farther','farthest']],'q5'=>[['-- Choose --','our','ours'],['-- Choose --','her','hers']],'q6'=>[['-- Choose --','be able','have','can','need']],'q7'=>[['-- Choose --','This old table’s legs','The legs of this old table','This old table legs','The legs of this old table’s']],'q8'=>[['-- Choose --','nobody','no one','anyone','somebody']]];
      $q29->options=['q1'=>['I completely disagree','I mostly disagree','I mostly agree','I completely agree']];
        // $q3->additionalData=["images"=>['Restaurant','Hotel','Store']];
        $q29->questionType='radioSentence';
       // $q1->correctAnswers=['q1'=>'vidim belu pozadinu','q2'=>'vidim crnu kružnu liniju – krug','q3'=>'vidim krug u kvadratu','q4'=>'ne vidim ništa interesantno'];
        $q29->modul='Module29';
        $q29->points=["I completely disagree"=>1,"I mostly disagree"=>2,"I mostly agree"=>3,"I completely agree"=>4];
        $q29->final=0;
        $q29->qid='psiho229';
        $questionData[]=$q29;
        
         $q30=new \Classes\QuestionData();
        $q30->question="";
        $q30->hint="Radio button, multiple choice";
        //$q3->multi=1;
     
        //$q1->story=true;
        $q30->answers=['q1'];
        $q30->sentences=['q1'=>'<span>30. At meetings, I always have something to say.</span><br><br>'];
        //$q24->image='2/23.jpg';
    //    $q3->options=['q1'=>[['-- Choose --','did visit','has visited','visited','should visit']],'q2'=>[['-- Choose --','to not be','not to be','that not be','that be not']],'q3'=>[['-- Choose --','couldn’t possibly be','mustn’t possibly be','shouldn’t possibly be','isn’t possibly']],'q4'=>[['-- Choose --','farer','farest','farther','farthest']],'q5'=>[['-- Choose --','our','ours'],['-- Choose --','her','hers']],'q6'=>[['-- Choose --','be able','have','can','need']],'q7'=>[['-- Choose --','This old table’s legs','The legs of this old table','This old table legs','The legs of this old table’s']],'q8'=>[['-- Choose --','nobody','no one','anyone','somebody']]];
      $q30->options=['q1'=>['I completely disagree','I mostly disagree','I mostly agree','I completely agree']];
        // $q3->additionalData=["images"=>['Restaurant','Hotel','Store']];
        $q30->questionType='radioSentence';
       // $q1->correctAnswers=['q1'=>'vidim belu pozadinu','q2'=>'vidim crnu kružnu liniju – krug','q3'=>'vidim krug u kvadratu','q4'=>'ne vidim ništa interesantno'];
        $q30->modul='Module30';
        $q30->points=["I completely disagree"=>1,"I mostly disagree"=>2,"I mostly agree"=>3,"I completely agree"=>4];
        $q30->final=0;
        $q30->qid='psiho230';
        $questionData[]=$q30;
        
          $q31=new \Classes\QuestionData();
        $q31->question="";
        $q31->hint="Radio button, multiple choice";
        //$q3->multi=1;
     
        //$q1->story=true;
        $q31->answers=['q1'];
        $q31->sentences=['q1'=>'<span>31. I often visit professional meetings where I can learn something</span><br><br>'];
        $q31->image='2/31.jpg';
    //    $q3->options=['q1'=>[['-- Choose --','did visit','has visited','visited','should visit']],'q2'=>[['-- Choose --','to not be','not to be','that not be','that be not']],'q3'=>[['-- Choose --','couldn’t possibly be','mustn’t possibly be','shouldn’t possibly be','isn’t possibly']],'q4'=>[['-- Choose --','farer','farest','farther','farthest']],'q5'=>[['-- Choose --','our','ours'],['-- Choose --','her','hers']],'q6'=>[['-- Choose --','be able','have','can','need']],'q7'=>[['-- Choose --','This old table’s legs','The legs of this old table','This old table legs','The legs of this old table’s']],'q8'=>[['-- Choose --','nobody','no one','anyone','somebody']]];
      $q31->options=['q1'=>['I completely disagree','I mostly disagree','I mostly agree','I completely agree']];
        // $q3->additionalData=["images"=>['Restaurant','Hotel','Store']];
        $q31->questionType='radioSentence';
       // $q1->correctAnswers=['q1'=>'vidim belu pozadinu','q2'=>'vidim crnu kružnu liniju – krug','q3'=>'vidim krug u kvadratu','q4'=>'ne vidim ništa interesantno'];
        $q31->modul='Module31';
        $q31->points=["I completely disagree"=>1,"I mostly disagree"=>2,"I mostly agree"=>3,"I completely agree"=>4];
        $q31->final=0;
        $q31->qid='psiho231';
        $questionData[]=$q31;
        
         $q32=new \Classes\QuestionData();
        $q32->question="";
        $q32->hint="Radio button, multiple choice";
        //$q3->multi=1;
     
        //$q1->story=true;
        $q32->answers=['q1'];
        $q32->sentences=['q1'=>'<span>32. I do not hesitate to try new ways of working</span><br><br>'];
       // $q31->image='2/31.jpg';
    //    $q3->options=['q1'=>[['-- Choose --','did visit','has visited','visited','should visit']],'q2'=>[['-- Choose --','to not be','not to be','that not be','that be not']],'q3'=>[['-- Choose --','couldn’t possibly be','mustn’t possibly be','shouldn’t possibly be','isn’t possibly']],'q4'=>[['-- Choose --','farer','farest','farther','farthest']],'q5'=>[['-- Choose --','our','ours'],['-- Choose --','her','hers']],'q6'=>[['-- Choose --','be able','have','can','need']],'q7'=>[['-- Choose --','This old table’s legs','The legs of this old table','This old table legs','The legs of this old table’s']],'q8'=>[['-- Choose --','nobody','no one','anyone','somebody']]];
      $q32->options=['q1'=>['I completely disagree','I mostly disagree','I mostly agree','I completely agree']];
        // $q3->additionalData=["images"=>['Restaurant','Hotel','Store']];
        $q32->questionType='radioSentence';
       // $q1->correctAnswers=['q1'=>'vidim belu pozadinu','q2'=>'vidim crnu kružnu liniju – krug','q3'=>'vidim krug u kvadratu','q4'=>'ne vidim ništa interesantno'];
        $q32->modul='Module32';
        $q32->points=["I completely disagree"=>1,"I mostly disagree"=>2,"I mostly agree"=>3,"I completely agree"=>4];
        $q32->final=0;
        $q32->qid='psiho232';
        $questionData[]=$q32;
        
            $q33=new \Classes\QuestionData();
        $q33->question="";
        $q33->hint="Radio button, multiple choice";
        //$q3->multi=1;
     
        //$q1->story=true;
        $q33->answers=['q1'];
        $q33->sentences=['q1'=>'<span>33. I almost always contribute if the agreement is being reached</span><br><br>'];
       // $q31->image='2/31.jpg';
    //    $q3->options=['q1'=>[['-- Choose --','did visit','has visited','visited','should visit']],'q2'=>[['-- Choose --','to not be','not to be','that not be','that be not']],'q3'=>[['-- Choose --','couldn’t possibly be','mustn’t possibly be','shouldn’t possibly be','isn’t possibly']],'q4'=>[['-- Choose --','farer','farest','farther','farthest']],'q5'=>[['-- Choose --','our','ours'],['-- Choose --','her','hers']],'q6'=>[['-- Choose --','be able','have','can','need']],'q7'=>[['-- Choose --','This old table’s legs','The legs of this old table','This old table legs','The legs of this old table’s']],'q8'=>[['-- Choose --','nobody','no one','anyone','somebody']]];
      $q33->options=['q1'=>['I completely disagree','I mostly disagree','I mostly agree','I completely agree']];
        // $q3->additionalData=["images"=>['Restaurant','Hotel','Store']];
        $q33->questionType='radioSentence';
       // $q1->correctAnswers=['q1'=>'vidim belu pozadinu','q2'=>'vidim crnu kružnu liniju – krug','q3'=>'vidim krug u kvadratu','q4'=>'ne vidim ništa interesantno'];
        $q33->modul='Module33';
        $q33->points=["I completely disagree"=>1,"I mostly disagree"=>2,"I mostly agree"=>3,"I completely agree"=>4];
        $q33->final=0;
        $q33->qid='psiho233';
        $questionData[]=$q33;
        
                 $q34=new \Classes\QuestionData();
        $q34->question="";
        $q34->hint="Radio button, multiple choice";
        //$q3->multi=1;
     
        //$q1->story=true;
        $q34->answers=['q1'];
        $q34->sentences=['q1'=>'<span>34. I support colleagues in their initiatives and I also join them often</span><br><br>'];
       // $q31->image='2/31.jpg';
    //    $q3->options=['q1'=>[['-- Choose --','did visit','has visited','visited','should visit']],'q2'=>[['-- Choose --','to not be','not to be','that not be','that be not']],'q3'=>[['-- Choose --','couldn’t possibly be','mustn’t possibly be','shouldn’t possibly be','isn’t possibly']],'q4'=>[['-- Choose --','farer','farest','farther','farthest']],'q5'=>[['-- Choose --','our','ours'],['-- Choose --','her','hers']],'q6'=>[['-- Choose --','be able','have','can','need']],'q7'=>[['-- Choose --','This old table’s legs','The legs of this old table','This old table legs','The legs of this old table’s']],'q8'=>[['-- Choose --','nobody','no one','anyone','somebody']]];
      $q34->options=['q1'=>['I completely disagree','I mostly disagree','I mostly agree','I completely agree']];
        // $q3->additionalData=["images"=>['Restaurant','Hotel','Store']];
        $q34->questionType='radioSentence';
       // $q1->correctAnswers=['q1'=>'vidim belu pozadinu','q2'=>'vidim crnu kružnu liniju – krug','q3'=>'vidim krug u kvadratu','q4'=>'ne vidim ništa interesantno'];
        $q34->modul='Module34';
        $q34->points=["I completely disagree"=>1,"I mostly disagree"=>2,"I mostly agree"=>3,"I completely agree"=>4];
        $q34->final=0;
        $q34->qid='psiho234';
        $questionData[]=$q34;
        
                  $q35=new \Classes\QuestionData();
        $q35->question="";
        $q35->hint="Radio button, multiple choice";
        //$q3->multi=1;
     
        //$q1->story=true;
        $q35->answers=['q1'];
        $q35->sentences=['q1'=>'<span>35. I love representing my team</span><br><br>'];
       // $q31->image='2/31.jpg';
    //    $q3->options=['q1'=>[['-- Choose --','did visit','has visited','visited','should visit']],'q2'=>[['-- Choose --','to not be','not to be','that not be','that be not']],'q3'=>[['-- Choose --','couldn’t possibly be','mustn’t possibly be','shouldn’t possibly be','isn’t possibly']],'q4'=>[['-- Choose --','farer','farest','farther','farthest']],'q5'=>[['-- Choose --','our','ours'],['-- Choose --','her','hers']],'q6'=>[['-- Choose --','be able','have','can','need']],'q7'=>[['-- Choose --','This old table’s legs','The legs of this old table','This old table legs','The legs of this old table’s']],'q8'=>[['-- Choose --','nobody','no one','anyone','somebody']]];
      $q35->options=['q1'=>['I completely disagree','I mostly disagree','I mostly agree','I completely agree']];
        // $q3->additionalData=["images"=>['Restaurant','Hotel','Store']];
        $q35->questionType='radioSentence';
       // $q1->correctAnswers=['q1'=>'vidim belu pozadinu','q2'=>'vidim crnu kružnu liniju – krug','q3'=>'vidim krug u kvadratu','q4'=>'ne vidim ništa interesantno'];
        $q35->modul='Module35';
        $q35->points=["I completely disagree"=>1,"I mostly disagree"=>2,"I mostly agree"=>3,"I completely agree"=>4];
        $q35->final=0;
        $q35->qid='psiho235';
        $questionData[]=$q35;
        
                     $q36=new \Classes\QuestionData();
        $q36->question="";
        $q36->hint="Radio button, multiple choice";
        //$q3->multi=1;
     
        //$q1->story=true;
        $q36->answers=['q1'];
        $q36->sentences=['q1'=>'<span>36. Work requires a high level of concentration on my part in order for mistakes to be reduced to minimum</span><br><br>'];
       // $q31->image='2/31.jpg';
    //    $q3->options=['q1'=>[['-- Choose --','did visit','has visited','visited','should visit']],'q2'=>[['-- Choose --','to not be','not to be','that not be','that be not']],'q3'=>[['-- Choose --','couldn’t possibly be','mustn’t possibly be','shouldn’t possibly be','isn’t possibly']],'q4'=>[['-- Choose --','farer','farest','farther','farthest']],'q5'=>[['-- Choose --','our','ours'],['-- Choose --','her','hers']],'q6'=>[['-- Choose --','be able','have','can','need']],'q7'=>[['-- Choose --','This old table’s legs','The legs of this old table','This old table legs','The legs of this old table’s']],'q8'=>[['-- Choose --','nobody','no one','anyone','somebody']]];
      $q36->options=['q1'=>['I completely disagree','I mostly disagree','I mostly agree','I completely agree']];
        // $q3->additionalData=["images"=>['Restaurant','Hotel','Store']];
        $q36->questionType='radioSentence';
       // $q1->correctAnswers=['q1'=>'vidim belu pozadinu','q2'=>'vidim crnu kružnu liniju – krug','q3'=>'vidim krug u kvadratu','q4'=>'ne vidim ništa interesantno'];
        $q36->modul='Module36';
        $q36->points=["I completely disagree"=>1,"I mostly disagree"=>2,"I mostly agree"=>3,"I completely agree"=>4];
        $q36->final=0;
        $q36->qid='psiho236';
        $questionData[]=$q36;
        
                          $q37=new \Classes\QuestionData();
        $q37->question="";
        $q37->hint="Radio button, multiple choice";
        //$q3->multi=1;
     
        //$q1->story=true;
        $q37->answers=['q1'];
        $q37->sentences=['q1'=>'<span>37. I enjoy when I`m preoccupied with work</span><br><br>'];
        $q37->image='2/37.jpg';
    //    $q3->options=['q1'=>[['-- Choose --','did visit','has visited','visited','should visit']],'q2'=>[['-- Choose --','to not be','not to be','that not be','that be not']],'q3'=>[['-- Choose --','couldn’t possibly be','mustn’t possibly be','shouldn’t possibly be','isn’t possibly']],'q4'=>[['-- Choose --','farer','farest','farther','farthest']],'q5'=>[['-- Choose --','our','ours'],['-- Choose --','her','hers']],'q6'=>[['-- Choose --','be able','have','can','need']],'q7'=>[['-- Choose --','This old table’s legs','The legs of this old table','This old table legs','The legs of this old table’s']],'q8'=>[['-- Choose --','nobody','no one','anyone','somebody']]];
      $q37->options=['q1'=>['I completely disagree','I mostly disagree','I mostly agree','I completely agree']];
        // $q3->additionalData=["images"=>['Restaurant','Hotel','Store']];
        $q37->questionType='radioSentence';
       // $q1->correctAnswers=['q1'=>'vidim belu pozadinu','q2'=>'vidim crnu kružnu liniju – krug','q3'=>'vidim krug u kvadratu','q4'=>'ne vidim ništa interesantno'];
        $q37->modul='Module37';
        $q37->points=["I completely disagree"=>1,"I mostly disagree"=>2,"I mostly agree"=>3,"I completely agree"=>4];
        $q37->final=0;
        $q37->qid='psiho237';
        $questionData[]=$q37;
        
                                $q38=new \Classes\QuestionData();
        $q38->question="";
        $q38->hint="Radio button, multiple choice";
        //$q3->multi=1;
     
        //$q1->story=true;
        $q38->answers=['q1'];
        $q38->sentences=['q1'=>'<span>38. It matters to me that that everything works out perfectly no matter how long it takes</span><br><br>'];
       // $q31->image='2/31.jpg';
    //    $q3->options=['q1'=>[['-- Choose --','did visit','has visited','visited','should visit']],'q2'=>[['-- Choose --','to not be','not to be','that not be','that be not']],'q3'=>[['-- Choose --','couldn’t possibly be','mustn’t possibly be','shouldn’t possibly be','isn’t possibly']],'q4'=>[['-- Choose --','farer','farest','farther','farthest']],'q5'=>[['-- Choose --','our','ours'],['-- Choose --','her','hers']],'q6'=>[['-- Choose --','be able','have','can','need']],'q7'=>[['-- Choose --','This old table’s legs','The legs of this old table','This old table legs','The legs of this old table’s']],'q8'=>[['-- Choose --','nobody','no one','anyone','somebody']]];
      $q38->options=['q1'=>['I completely disagree','I mostly disagree','I mostly agree','I completely agree']];
        // $q3->additionalData=["images"=>['Restaurant','Hotel','Store']];
        $q38->questionType='radioSentence';
       // $q1->correctAnswers=['q1'=>'vidim belu pozadinu','q2'=>'vidim crnu kružnu liniju – krug','q3'=>'vidim krug u kvadratu','q4'=>'ne vidim ništa interesantno'];
        $q38->modul='Module38';
        $q38->points=["I completely disagree"=>1,"I mostly disagree"=>2,"I mostly agree"=>3,"I completely agree"=>4];
        $q38->final=0;
        $q38->qid='psiho238';
        $questionData[]=$q38;
        
                                   $q39=new \Classes\QuestionData();
        $q39->question="";
        $q39->hint="Radio button, multiple choice";
        //$q3->multi=1;
     
        //$q1->story=true;
        $q39->answers=['q1'];
        $q39->sentences=['q1'=>'<span>39. I have no problem pointing out mistakes and errors to others</span><br><br>'];
       // $q31->image='2/31.jpg';
    //    $q3->options=['q1'=>[['-- Choose --','did visit','has visited','visited','should visit']],'q2'=>[['-- Choose --','to not be','not to be','that not be','that be not']],'q3'=>[['-- Choose --','couldn’t possibly be','mustn’t possibly be','shouldn’t possibly be','isn’t possibly']],'q4'=>[['-- Choose --','farer','farest','farther','farthest']],'q5'=>[['-- Choose --','our','ours'],['-- Choose --','her','hers']],'q6'=>[['-- Choose --','be able','have','can','need']],'q7'=>[['-- Choose --','This old table’s legs','The legs of this old table','This old table legs','The legs of this old table’s']],'q8'=>[['-- Choose --','nobody','no one','anyone','somebody']]];
      $q39->options=['q1'=>['I completely disagree','I mostly disagree','I mostly agree','I completely agree']];
        // $q3->additionalData=["images"=>['Restaurant','Hotel','Store']];
        $q39->questionType='radioSentence';
       // $q1->correctAnswers=['q1'=>'vidim belu pozadinu','q2'=>'vidim crnu kružnu liniju – krug','q3'=>'vidim krug u kvadratu','q4'=>'ne vidim ništa interesantno'];
        $q39->modul='Module39';
        $q39->points=["I completely disagree"=>1,"I mostly disagree"=>2,"I mostly agree"=>3,"I completely agree"=>4];
        $q39->final=0;
        $q39->qid='psiho239';
        $questionData[]=$q39;
        
                                        $q40=new \Classes\QuestionData();
        $q40->question="";
        $q40->hint="Radio button, multiple choice";
        //$q3->multi=1;
     
        //$q1->story=true;
        $q40->answers=['q1'];
        $q40->sentences=['q1'=>'<span>40. When the problem arises, it is more important for me to do the work well than to maintain interpersonal relationships</span><br><br>'];
       // $q31->image='2/31.jpg';
    //    $q3->options=['q1'=>[['-- Choose --','did visit','has visited','visited','should visit']],'q2'=>[['-- Choose --','to not be','not to be','that not be','that be not']],'q3'=>[['-- Choose --','couldn’t possibly be','mustn’t possibly be','shouldn’t possibly be','isn’t possibly']],'q4'=>[['-- Choose --','farer','farest','farther','farthest']],'q5'=>[['-- Choose --','our','ours'],['-- Choose --','her','hers']],'q6'=>[['-- Choose --','be able','have','can','need']],'q7'=>[['-- Choose --','This old table’s legs','The legs of this old table','This old table legs','The legs of this old table’s']],'q8'=>[['-- Choose --','nobody','no one','anyone','somebody']]];
      $q40->options=['q1'=>['I completely disagree','I mostly disagree','I mostly agree','I completely agree']];
        // $q3->additionalData=["images"=>['Restaurant','Hotel','Store']];
        $q40->questionType='radioSentence';
       // $q1->correctAnswers=['q1'=>'vidim belu pozadinu','q2'=>'vidim crnu kružnu liniju – krug','q3'=>'vidim krug u kvadratu','q4'=>'ne vidim ništa interesantno'];
        $q40->modul='Module40';
        $q40->points=["I completely disagree"=>1,"I mostly disagree"=>2,"I mostly agree"=>3,"I completely agree"=>4];
        $q40->final=1;
        $q40->qid='psiho240';
        $questionData[]=$q40;
        ///
        
        $test=new \Classes\TestTemplate($questionData,'Psiho2Eng');
        $tdata=json_encode($test);
      /* var_dump($test);
        die;*/
        if($id==0)
        {
        $data=[
            'quizdata'=>$tdata,
            'test-id'=>'Psiho2Eng'
             ];
            $apiAuth=new \Classes\RESTClient($data,_API_URL."/testdescriptors/",['X-JWT-Auth'=>$_SESSION['jwt']]);
           // dump($apiAuth->post()->data->id);
            $res=$apiAuth->post();
            if($res->status=='Success')
            {
              echo "in";
            }
        }
       else 
       {
           $data=[
            'quizdata'=>$tdata
             ];
            $apiAuth=new \Classes\RESTClient($data,_API_URL."/testdescriptors/update/{$id}",['X-JWT-Auth'=>$_SESSION['jwt']]);
           // dump($apiAuth->post()->data->id);
            $res=$apiAuth->put();
            if($res->status=='Success')
            {
              echo "in upd";
            }
       }
        
     
    }
  
    public function calculating()
    {
         $this->template->setData("body",'');
        $this->template->render('calc');
    }
}
