<div class="container" id="calculate-box">  
        <div class="row">
            <div class="col-md-12 col-lg-10 col-lg-offset-1 text-center">

                <div class="result-box">

                  <img class="result-text1" src="<?=_WEB_PATH?>views/testsHome/images/result-score3.png" alt="Apprentice">

                  <div class=" stars">
                    <div class="star"><img src="<?=_WEB_PATH?>views/testsHome/images/result-star.png"></div>
                    <div class="star"><img src="<?=_WEB_PATH?>views/testsHome/images/result-star.png"></div>
                    <div class="star"><img src="<?=_WEB_PATH?>views/testsHome/images/result-star.png"></div>
                  </div>

                  <img src="<?=_WEB_PATH?>views/testsHome/images/result-abc3.png" style="margin: 40px 0px;">

                </div>


                <div class="result-text-div text-center">
                  <h3>Very good!</h3>
                  <p>You are currently at the apprentice level but you can do it even better. If you want to find out which areas you should work on to improve your English, you can take: <a href="#">full test - tinejdžeri</a>. </p>

                  <button class="btn btn-lg btn-primary btn-block btn-signin btn-def" type="submit">GO TO TEST</button>
                  <p>If you want to get detailed evaluation of your current English language knowledge, you can also take: <a href="#">full test - školarci</a>. </p>
                </div>

            
            </div>
        </div>
    </div>

