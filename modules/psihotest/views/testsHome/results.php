<?php
//var_dump($_SESSION['ukupnorez']);
//
//dump($interpretations);
$keyTst="";
if($_SESSION['ukupnorez']=="1")
{
    if($idTest==17)
    $keyTst="Otkačeni moler";
    if($idTest==24)
       $keyTst="Wacky wall-painter"; 
    $imgRes="moler.png";
}
if($_SESSION['ukupnorez']=="2")
{
    if($idTest==17)
    $keyTst="Tihi vetar";
    
    if($idTest==24)
       $keyTst="Silent wind";
    
    $imgRes="vetar.png";
}
if($_SESSION['ukupnorez']=="3")
{
    if($idTest==17)
     $keyTst="Nedodirljivi dirigent";
    if($idTest==24)
        $keyTst="Untouchable conductor";
   
    $imgRes="dirigent.png";
}
if($_SESSION['ukupnorez']=="4")
{
    if($idTest==17)
     $keyTst="Ulični borac";
    if($idTest==24)
        $keyTst="Street fighter";
    
    $imgRes="fighter.png";
}
//dump($keyTst);
//dump($additional);
//dump($finalText);
//echo "res";
//die;
?>
<div class="container" id="calculate-box">  
        <div class="row">
            <div class="col-md-12 col-lg-10 col-lg-offset-1 text-center">

                <div class="result-box">

                  <h1>CONGRATULATIONS</h1>
                  <p>on a successfully finished test.<br>
                  <strong>Here are your results:</strong></p>

                  <img class="result-text1" src="<?=_WEB_PATH?>views/testsHome/images/psiho/<?=$imgRes?>" style="display: block; margin: 30px auto !important;">

                </div>


                <div class="result-text-div text-left">
                    <h2 class="bold"><?=$keyTst?></h2>
                    <?php
                     
                                            echo "<p>".$interpretations[$keyTst]."</p><br><br>";
                        
                    ?>
<!--                  <p>Your English vocabulary is quite limited for this level and you need to work more on enriching and expanding it. It is essential to have a wide vocabulary in order to be able to communicate in a particular language. You are recommended to read more English texts, have oral practice (speaking as well as having dialogues) and do vocabulary exercises (translating, writing essays and word building exercises) covering various fields of topics (general, science, environment, culture, technology, engineering, business etc.), learn phrasal verbs, as well as various idiomatic phrases that allow one to get into the spirit of the language, to accomplish this goal.<br><br>
                  You need to work more on use and concord of tenses. It’s not only important what you say, but also how you say it. Correct use of tenses will help you be understood properly. Also, mastering tenses is necessary in order to use more complex grammatical constructions like reported speech, conditional sentences and subjunctive mood.
                  You are very good at understanding contexts of sentences and recognizing subtle differences in meaning when the context is changed. You’re on the right track toward highest levels.</p>-->

<!--                  <h3>You do have basic knowledge about the passive voice, but you haven’t mastered its forming and use yet. Pay attention to its use after modal verbs, in infinitive and progressive constructions, and especially to its forming in concord of tenses.</h3>-->

<!--                  <div class="row premium-kolone3">
                      <div class="col-md-4">
                        <p>You are familiar with the perfect infinitive, but you need to learn more about its forming and use, especially in negative sentences and with passive voice.</p>
                      </div>
                      <div class="col-md-4">
                        <p>You have mastered all three types of participles as well as different contexts in which you should use them. Well done!</p>
                      </div>
                      <div class="col-md-4">
                        <p>You have mastered the types and use of conditional sentences. You are ready to tackle the most complex grammatical and syntactic structures.</p>
                      </div>
                  </div>-->

                  

  <!-- <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal2" id="psiho-trigger" style='display:block'>Free English test</button>-->
<button class="btn btn-lg btn-primary btn-block btn-signin btn-def" style="background-color: #03a9f4;width:310px" data-toggle="modal" data-target="#myModal2" type="submit">Free English test</button>
                </div>

            
            </div>
        </div>
    </div>
 
      <div id="myModal2" class="modal fade" role="dialog">
      <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Free English test</h4>
          </div>
          <div class="modal-body">
            <p>This test consists of four different parts, which fit different levels of difficulty.
By solving sufficient amount of tasks correctly, you can unlock the next, higher level.
After you have finished the test, you will get the result telling you at which level your
English language skills currently are.
  <br><br>
Follow the instructions given in each exercise.</p>
          </div>
          <div class="modal-footer">
              <a href="<?=_WEB_PATH?>test/4"><button type="button" class="btn btn-default" >Start free English test</button></a>
          </div>
        </div>

      </div>
    </div>