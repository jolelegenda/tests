<div class="container" id="calculate-box">  
        <div class="row">
            <div class="col-md-12 col-lg-10 col-lg-offset-1 text-center">

                <div class="result-box">

                  <img class="result-text1" src="<?=_WEB_PATH?>views/testsHome/images/result-score4.png" alt="Freshman">

                  <div class=" stars">
                    <div class="star"><img src="<?=_WEB_PATH?>views/testsHome/images/result-star.png"></div>
                    <div class="star"><img src="<?=_WEB_PATH?>views/testsHome/images/result-star.png"></div>
                    <div class="star"><img src="<?=_WEB_PATH?>views/testsHome/images/result-star.png"></div>
                    <div class="star"><img src="<?=_WEB_PATH?>views/testsHome/images/result-star.png"></div>
                  </div>

                  <img src="<?=_WEB_PATH?>views/testsHome/images/result-abc4.png" style="margin: 40px 0px;">

                </div>


                <div class="result-text-div text-center">
                  <h3>Great job!</h3>
                  <p>Your English is at freshman level and very close to the next one. To find out what it takes to reach the student level, you can take: <a href="#">full test - studenti</a>. </p>

                  <button class="btn btn-lg btn-primary btn-block btn-signin btn-def" type="submit">GO TO TEST</button>
                  <p>If you want to get detailed evaluation of your current English language knowledge, you can also take: <a href="#">full test - tinejdžeri</a>. </p>
                </div>

            
            </div>
        </div>
    </div>
