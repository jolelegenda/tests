<div class="container" id="calculate-box">  
        <div class="row">
            <div class="col-md-12 col-lg-10 col-lg-offset-1 text-center">

                <div class="result-box">

                  <img class="result-text1" src="<?=_WEB_PATH?>views/testsHome/images/result-score6.png" alt="Academic">

                  <div class=" stars">
                    <div class="star"><img src="<?=_WEB_PATH?>views/testsHome/images/result-star.png"></div>
                    <div class="star"><img src="<?=_WEB_PATH?>views/testsHome/images/result-star.png"></div>
                    <div class="star"><img src="<?=_WEB_PATH?>views/testsHome/images/result-star.png"></div>
                    <div class="star"><img src="<?=_WEB_PATH?>views/testsHome/images/result-star.png"></div>
                    <div class="star"><img src="<?=_WEB_PATH?>views/testsHome/images/result-star.png"></div>
                    <div class="star"><img src="<?=_WEB_PATH?>views/testsHome/images/result-star.png"></div>
                  </div>

                  <img src="<?=_WEB_PATH?>views/testsHome/images/result-abc6.png" style="margin: 40px 0px;">

                </div>


                <div class="result-text-div text-center">
                  <h3>Congratulations!</h3>
                  <p>You have mastered your English to academic level. Only few have reached this level so far. If you are interested in detailed evaluation of your English language knowledge, you can take: <a href="#">full test - studenti</a>. </p>

                  <button class="btn btn-lg btn-primary btn-block btn-signin btn-def" type="submit">GO TO TEST</button>
                </div>

            
            </div>
        </div>
    </div>
