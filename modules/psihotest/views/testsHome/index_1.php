<?php
var_dump($test->test[0]->answers);
?>
<div class="container" id="test-box">  
        <div class="row">
            <div class="col-md-12 col-lg-6 col-lg-offset-3">
            <form id="msform">
              <div id="progressbar">
               <table>
                <tbody>
                    <tr>
                     <td class="active"></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                </tbody>
                </table>
                <p class="text-left">Your progress</p>
              </div>
              <!-- fieldsets -->
              <fieldset>
                <h2 class="fs-title">Match the shops to the things we usually buy in them</h2>
                <h3 class="fs-subtitle">Connect the right items, drag and connect</h3>
               
                    <div class="row test1 testt">
                        <div class="col-xs-6 col-sm-4 col-sm-offset-2">
                            <img class="img-test1" src="<?=_WEB_PATH?>views/testsHome/images/test1/1.png" width="120" height="120" data-attr='book'>
                        </div>
                        <div class="col-xs-6 col-sm-4">
                            <button type="button" class="dugme-test1" data-word='book'>Dictionary</button>
                        </div>
                    </div>
                    <div class="row test1 testt">
                        <div class="col-xs-6 col-sm-4 col-sm-offset-2">
                            <img class="img-test1" src="<?=_WEB_PATH?>views/testsHome/images/test1/2.png" width="120" height="120" data-attr='kiosk'>
                        </div>
                        <div class="col-xs-6 col-sm-4">
                            <button type="button" class="dugme-test1" data-word='kiosk'>Newspaper</button>
                        </div>
                    </div>
                    <div class="row test1 testt">
                        <div class="col-xs-6 col-sm-4 col-sm-offset-2">
                            <img class="img-test1" src="<?=_WEB_PATH?>views/testsHome/images/test1/3.png" width="120" height="120" data-attr='apoteka'>
                        </div>
                        <div class="col-xs-6 col-sm-4">
                            <button type="button" class="dugme-test1" data-word='apoteka'>Medicine</button>
                        </div>
                    </div>                                      
                <input type="button" name="next" class="next btn btn-lg btn-primary btn-block btn-signin" value="Next" />
             
              </fieldset>
              <fieldset>
                <h2 class="fs-title">Where can you see these notices?</h2>
                <h3 class="fs-subtitle">Radio button, multiple choice</h3>
               
                    <div class="row test2 testt">
                        <div class="col-xs-6 col-sm-4">
                            <img src="<?=_WEB_PATH?>views/testsHome/images/test1/1.png" width="120" height="120">
                        </div>
                        <div class="col-xs-6 col-sm-8">
                            <div class="radio radio-inline">
                                <input id="inlineRadio1" value="option1" name="radioInline" checked="" type="radio">
                                <label for="inlineRadio1"> In a library </label>
                            </div>
                            <div class="radio radio-inline">
                                <input id="inlineRadio2" value="option2" name="radioInline" checked="" type="radio">
                                <label for="inlineRadio2"> At a restaurant </label>
                            </div>
                            <div class="radio radio-inline">
                                <input id="inlineRadio3" value="option3" name="radioInline" checked="" type="radio">
                                <label for="inlineRadio3"> In a park </label>
                            </div>
                        </div>
                    </div>
                    <div class="row test2 testt">
                        <div class="col-xs-6 col-sm-4">
                            <img src="<?=_WEB_PATH?>views/testsHome/images/test1/2.png" width="120" height="120">
                        </div>
                        <div class="col-xs-6 col-sm-8">
                            <div class="radio radio-inline">
                                <input id="inlineRadio4" value="option4" name="radioInline1" checked="" type="radio">
                                <label for="inlineRadio4"> In a library </label>
                            </div>
                            <div class="radio radio-inline">
                                <input id="inlineRadio5" value="option5" name="radioInline1" checked="" type="radio">
                                <label for="inlineRadio5"> At a restaurant </label>
                            </div>
                            <div class="radio radio-inline">
                                <input id="inlineRadio6" value="option6" name="radioInline1" checked="" type="radio">
                                <label for="inlineRadio6"> In a park </label>
                            </div>
                        </div>
                    </div>
                    <div class="row test2 testt">
                        <div class="col-xs-6 col-sm-4" >
                            <img src="<?=_WEB_PATH?>views/testsHome/images/test1/3.png" width="120" height="120">
                        </div>
                        <div class="col-xs-6 col-sm-8">
                            <div class="radio radio-inline">
                                <input id="inlineRadio7" value="option7" name="radioInline" checked="" type="radio">
                                <label for="inlineRadio7"> In a library </label>
                            </div>
                            <div class="radio radio-inline">
                                <input id="inlineRadio8" value="option8" name="radioInline" checked="" type="radio">
                                <label for="inlineRadio8"> At a restaurant </label>
                            </div>
                            <div class="radio radio-inline">
                                <input id="inlineRadio9" value="option9" name="radioInline" checked="" type="radio">
                                <label for="inlineRadio9"> In a park </label>
                            </div>
                        </div>
                    </div>                                      
                <input type="button" name="next" class="next btn btn-lg btn-primary btn-block btn-signin" value="Next" />
             
              </fieldset> 
              <fieldset>
                <h2 class="fs-title">Choose the correct word</h2>
                <h3 class="fs-subtitle">Dropdown menu</h3>
               
                    <div class="row test3 testt">
                        <div class="col-xs-12">
                            <span>1. Carmen is from</span><select class="form-control"><option>1</option><option>2</option><option>3</option></select>
                        </div>
                    </div>
                    <div class="row test3 testt">
                        <div class="col-xs-12">
                            <span>2. There are many </span><select class="form-control"><option>There</option><option>many</option><option>There</option><option>are</option></select><span>in the park.</span>
                        </div>
                    </div>
                    <div class="row test3 testt">
                        <div class="col-xs-12">
                            <span>3. Today is the </span><select class="form-control"><option>1</option><option>2</option><option>3</option></select><span>of July.</span>
                        </div>
                    </div>                                     
                <input type="button" name="next" class="next btn btn-lg btn-primary btn-block btn-signin" value="Next" />
             
              </fieldset>
              <fieldset>
                <h2 class="fs-title">Arrange and make sentences from these words</h2>
                <h3 class="fs-subtitle">Drag and rearrange</h3>
               
                    <div class="row test4 testt">
                        
                        <div class="col-xs-12" id='sortable'>
                            <div>1.</div><div>Carmen</div><div>is</div><div>.</div><div>from</div><div>Carmen</div><div>is</div><div>.</div><div>from</div><div>Carmen</div><div>is</div><div>.</div><div>from</div><div>Carmen</div>
                        </div>
                    </div>
                    <div class="row test4 testt">
                        <div class="col-xs-12">
                            <div>2.</div><div>Carmen</div><div>is</div><div>.</div><div>from</div><div>Carmen</div>
                        </div>
                    </div>
                    <div class="row test4 testt">
                        <div class="col-xs-12">
                            <div>3.</div><div>is</div><div>.</div><div>from</div><div>Carmen</div>
                        </div>
                    </div>                                   
                <input type="button" name="next" class="next btn btn-lg btn-primary btn-block btn-signin" value="Next" />
             
              </fieldset>
              <fieldset>
                <h2 class="fs-title">Click on the correct sentence:</h2>
                <h3 class="fs-subtitle">Highlight</h3>
               
                    <div class="row test5 testt">
                        <div class="col-xs-12">
                            <div>1.</div>
                            <div>What are they doing tomorrow afternoon after school?</div>
                            <div>What is they doing tomorrow afternoon after school?</div>
                        </div>
                    </div>
                    <div class="row test5 testt">
                        <div class="col-xs-12">
                            <div>2.</div>
                            <div>He dont does his homework every day.?</div>
                            <div>He dont do his homework every day.?</div>
                        </div>
                    </div>
                    <div class="row test5 testt">
                        <div class="col-xs-12">
                            <div>3.</div>
                            <div>Does he have an aunt in Beijing??</div>
                            <div>Does he has an aunt in Beijing??</div>
                        </div>
                    </div>                                   
                <input type="button" name="next" class="next btn btn-lg btn-primary btn-block btn-signin" value="Next" />
             
              </fieldset>
              <fieldset>
                <h2 class="fs-title">Type the missing word</h2>
                <h3 class="fs-subtitle">Type the answer</h3>
               
                    <div class="row test6 testt">
                        <div class="col-xs-12">
                            <span>1. My brother’s daughter is my </span><input type="text" class="form-control">.
                        </div>
                    </div>
                    <div class="row test6 testt">
                        <div class="col-xs-12">
                            <span>2. Land surrounded by water from all sides is an </span><input type="text" class="form-control">.
                        </div>
                    </div>
                    <div class="row test6 testt">
                        <div class="col-xs-12">
                            <span>3. After sunny summer comes rainy </span><input type="text" class="form-control">.
                        </div>
                    </div>                                     
                <input type="button" name="next" class="next btn btn-lg btn-primary btn-block btn-signin" value="Next" />
             
             
                <!--<input type="submit" name="submit" class="submit action-button" value="Submit" />-->
              </fieldset>
            </form>
            </div>
        </div>
    </div>
