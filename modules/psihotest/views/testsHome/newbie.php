<div class="container" id="calculate-box">  
        <div class="row">
            <div class="col-md-12 col-lg-10 col-lg-offset-1 text-center">

                <div class="result-box">

                  <img class="result-text1" src="<?=_WEB_PATH?>views/testsHome/images/result-score1.png" alt="Newbie">

                  <div class=" stars">
                    <div class="star"><img src="<?=_WEB_PATH?>views/testsHome/images/result-star.png"></div>
                  </div>

                  <img src="<?=_WEB_PATH?>views/testsHome/images/result-abc.png" style="margin: 40px 0px;">

                </div>


                <div class="result-text-div text-center">
                  <h3>Nice!</h3>
                  <p>You have just started learning English. Keep working, you are on a good path. Take this test again when you finish your course to check your progress. If you are interested, you may take the Junior test: <a href="#">full test - Juniors</a>, which is suitable for English learning beginners. </p>

                  <button class="btn btn-lg btn-primary btn-block btn-signin btn-def" type="submit">GO TO TEST</button>
                </div>

            
            </div>
        </div>
    </div>