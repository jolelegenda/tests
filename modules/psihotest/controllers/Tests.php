<?php
namespace Modules\psihotest;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Tests
 *
 * @author Sonor Smart Force
 */
class Tests extends \Controllers\frontendController
{
    use \Classes\TestsTrait;
     public function __construct() {
        
        $module_name=\Classes\ModuleHelper::getModuleName(dirname(__FILE__));
       // echo dirname(__FILE__);
        parent::__construct($module_name);
       
      //  $this->testTemplate= new \Classes\TestTemplate();
    }
      public function result($name)
    {
        global $_premiumResults,$_additionalUnits,$_finalText,$_psihoResults,$_psiho2Results,$_psihoResultsEng,$_psiho2ResultsEng;
        //$name=strtolower($name);
        if($_SESSION['idTesta']==17)
        $this->template->setData("interpretations",$_psihoResults);
        if($_SESSION['idTesta']==21)
        $this->template->setData("interpretations",$_psiho2Results);
         if($_SESSION['idTesta']==25)
        $this->template->setData("interpretations",$_psiho2ResultsEng);
        if($_SESSION['idTesta']==24)
        $this->template->setData("interpretations",$_psihoResultsEng);
       /* $this->template->setData("additional",$_additionalUnits);
        $this->template->setData("finalText",$_finalText);*/
        $this->template->setData("idTest",$_SESSION['idTesta']);
        if($_SESSION['idTesta']==17 || $_SESSION['idTesta']==24)
        $this->template->render('results');
        if($_SESSION['idTesta']==21 || $_SESSION['idTesta']==25)
        $this->template->render('results2');
    }
     public function calculating()
    {
         $this->template->setData("body",'');
        $this->template->render('calc');
    }
}
