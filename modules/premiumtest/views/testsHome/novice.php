<div class="container" id="calculate-box">  
        <div class="row">
            <div class="col-md-12 col-lg-10 col-lg-offset-1 text-center">

                <div class="result-box">

                  <img class="result-text1" src="<?=_WEB_PATH?>views/testsHome/images/result-score2.png" alt="Novice">

                  <div class=" stars">
                    <div class="star"><img src="<?=_WEB_PATH?>views/testsHome/images/result-star.png"></div>
                    <div class="star"><img src="<?=_WEB_PATH?>views/testsHome/images/result-star.png"></div>
                  </div>

                  <img src="<?=_WEB_PATH?>views/testsHome/images/result-abc2.png" style="margin: 40px 0px;">

                </div>


                <div class="result-text-div text-center">
                  <h3>Good job!</h3>
                  <p>You are familiar with basic grammar structures and vocabulary. If you want to find out what it takes to improve your English, you can take: <a href="#">Full Test - školarci</a>. </p>

                  <button class="btn btn-lg btn-primary btn-block btn-signin btn-def" type="submit">GO TO TEST</button>
                </div>

            
            </div>
        </div>
    </div>
