<?php
//var_dump($_SESSION['ukupnorez']);
//
//dump($interpretations);
//dump($additional);
//dump($finalText);
?>
<div class="container" id="calculate-box">  
        <div class="row">
            <div class="col-md-12 col-lg-10 col-lg-offset-1 text-center">

                <div class="result-box">

                  <h1>CONGRATULATIONS</h1>
                  <p>on a successfully finished test.<br>
                  <strong>Here are your results:</strong></p>

                  <img class="result-text1" src="<?=_WEB_PATH?>views/testsHome/images/result-premium.png" style="display: block; margin: 30px auto !important;">

                </div>


                <div class="result-text-div text-left">
                    <?php
                        foreach($_SESSION['ukupnorez'] as $key=>$value)
                        {
                            foreach($interpretations[$_SESSION['testname']] as $t)
                            {
                                foreach($t as $k=>$v)
                               {
                                if($key==$k)
                                {
                                   // foreach($v as )
                                  //  dump($v);
                                    foreach($v as $vv)
                                    {
                                        //dump($vv);
                                        if($value>=$vv['points'][0] && $value<=$vv['points'][1])
                                        {
                                            echo "<p>".$vv['interpretation']."</p>";
                                        }
                                    }
                                }
                             }
                            }
                        }
                    ?>
<!--                  <p>Your English vocabulary is quite limited for this level and you need to work more on enriching and expanding it. It is essential to have a wide vocabulary in order to be able to communicate in a particular language. You are recommended to read more English texts, have oral practice (speaking as well as having dialogues) and do vocabulary exercises (translating, writing essays and word building exercises) covering various fields of topics (general, science, environment, culture, technology, engineering, business etc.), learn phrasal verbs, as well as various idiomatic phrases that allow one to get into the spirit of the language, to accomplish this goal.<br><br>
                  You need to work more on use and concord of tenses. It’s not only important what you say, but also how you say it. Correct use of tenses will help you be understood properly. Also, mastering tenses is necessary in order to use more complex grammatical constructions like reported speech, conditional sentences and subjunctive mood.
                  You are very good at understanding contexts of sentences and recognizing subtle differences in meaning when the context is changed. You’re on the right track toward highest levels.</p>-->

<!--                  <h3>You do have basic knowledge about the passive voice, but you haven’t mastered its forming and use yet. Pay attention to its use after modal verbs, in infinitive and progressive constructions, and especially to its forming in concord of tenses.</h3>-->

<!--                  <div class="row premium-kolone3">
                      <div class="col-md-4">
                        <p>You are familiar with the perfect infinitive, but you need to learn more about its forming and use, especially in negative sentences and with passive voice.</p>
                      </div>
                      <div class="col-md-4">
                        <p>You have mastered all three types of participles as well as different contexts in which you should use them. Well done!</p>
                      </div>
                      <div class="col-md-4">
                        <p>You have mastered the types and use of conditional sentences. You are ready to tackle the most complex grammatical and syntactic structures.</p>
                      </div>
                  </div>-->

                  <h2 class="bold">The grammar units you (also) need to work on:</h2>

                  <div class="row premium-kolone3b text-center">
                      <?php
                      $i=1;
                      foreach($_SESSION['ukupnorez'] as $key=>$value)
                      {
                          foreach($additional[$_SESSION['testname']] as $add)
                          {
                              foreach($add as $ka=>$va)
                              {
                                  if($ka=="1")
                                  {
                                    foreach($va as $vva)
                                    {
                                      //echo $vva;
                                      if($key==$vva && $value<1)
                                      {
                                         // echo $vva;
                                          ?>
                                           <div class="col-sm-4">
                                            <span><?=$i?></span>
                                         <p class="bold"><?=$vva?></p>
                                        </div>
                                          <?php
                                          $i++;
                                      }
                                    }
                                  }
                                   if($ka=="2")
                                  {
                                    foreach($va as $vva)
                                    {
                                      //echo $vva;
                                      if($key==$vva && $value<2)
                                      {
                                          ?>
                                           <div class="col-sm-4">
                                            <span><?=$i?></span>
                                         <p class="bold"><?=$vva?></p>
                                        </div>
                                          <?php
                                          $i++;
                                      }
                                    }
                                  }
                              }
                          }
                      }
                      ?>
<!--                      <div class="col-sm-4">
                        <span>1</span>
                        <p class="bold">Gerund vs infinitive</p>
                      </div>
                      <div class="col-sm-4">
                        <span>2</span>
                        <p class="bold">Subjunctive mood</p>
                      </div>
                      <div class="col-sm-4">
                        <span>3</span>
                        <p class="bold">Reported speechs</p>
                      </div>-->
                  </div>
                  <h2 class="bold">The grammar units you did well:</h2>

                  <div class="row premium-kolone3b text-center">
                      <?php
                      $i=1;
                      foreach($_SESSION['ukupnorez'] as $key=>$value)
                      {
                          foreach($additional[$_SESSION['testname']] as $add)
                          {
                              foreach($add as $ka=>$va)
                              {
                                  if($ka=="1")
                                  {
                                    foreach($va as $vva)
                                    {
                                      //echo $vva;
                                      if($key==$vva && $value==1)
                                      {
                                         // echo $vva;
                                          ?>
                                           <div class="col-sm-4">
                                            <span><?=$i?></span>
                                         <p class="bold"><?=$vva?></p>
                                        </div>
                                          <?php
                                          $i++;
                                      }
                                    }
                                  }
                                   if($ka=="2")
                                  {
                                    foreach($va as $vva)
                                    {
                                      //echo $vva;
                                      if($key==$vva && $value==2)
                                      {
                                          ?>
                                           <div class="col-sm-4">
                                            <span><?=$i?></span>
                                         <p class="bold"><?=$vva?></p>
                                        </div>
                                          <?php
                                          $i++;
                                      }
                                    }
                                  }
                              }
                          }
                      }
                      ?>
<!--                      <div class="col-sm-4">
                        <span>1</span>
                        <p class="bold">Gerund vs infinitive</p>
                      </div>
                      <div class="col-sm-4">
                        <span>2</span>
                        <p class="bold">Subjunctive mood</p>
                      </div>
                      <div class="col-sm-4">
                        <span>3</span>
                        <p class="bold">Reported speechs</p>
                      </div>-->
                  </div>
                  <div class="row">
                    <div class="col-md-6">
                      <p><?=$finalText['Skolarci'][0]['levo']?></p>
                    </div>
                    <div class="col-md-6">
                      <p><?=$finalText['Skolarci'][1]['desno']?></p>
                    </div>


                  </div>
                  <br><br>
                  <a href="<?=_WEB_PATH?>mvp"><button class="btn btn-lg btn-primary btn-block btn-signin btn-def" style="background-color: #03a9f4;width:310px" type="submit">T-LIST PLATFORM</button></a>

                </div>

            
            </div>
        </div>
    </div>
