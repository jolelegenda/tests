<?php
//var_dump($test->test[0]->answers);
?>
<div class="container" id="test-box">  
        <div class="row">
            <div class="col-md-12 col-lg-6 col-lg-offset-3">
            <form id="msform">
              <div id="progressbar">
               <table>
                <tbody>
                    <tr>
                     <td class="active"></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                </tbody>
                </table>
                <p class="text-left">Your progress</p>
              </div>
              <!-- fieldsets -->
              <fieldset>
                <h2 class="fs-title">Match the shops to the things we usually buy in them</h2>
                <h3 class="fs-subtitle">Connect the right items, drag and connect</h3>
               
                    <div class="row test1 testt">
                        <div class="col-xs-6 col-sm-4 col-sm-offset-2">
                            <img class="img-test1" src="<?=_WEB_PATH?>views/testsHome/images/test1/1.png" width="120" height="120" data-attr='book'>
                        </div>
                        <div class="col-xs-6 col-sm-4">
                            <button type="button" class="dugme-test1" data-word='book'>Dictionary</button>
                        </div>
                    </div>
                    <div class="row test1 testt">
                        <div class="col-xs-6 col-sm-4 col-sm-offset-2">
                            <img class="img-test1" src="<?=_WEB_PATH?>views/testsHome/images/test1/2.png" width="120" height="120" data-attr='kiosk'>
                        </div>
                        <div class="col-xs-6 col-sm-4">
                            <button type="button" class="dugme-test1" data-word='kiosk'>Newspaper</button>
                        </div>
                    </div>
                    <div class="row test1 testt">
                        <div class="col-xs-6 col-sm-4 col-sm-offset-2">
                            <img class="img-test1" src="<?=_WEB_PATH?>views/testsHome/images/test1/3.png" width="120" height="120" data-attr='apoteka'>
                        </div>
                        <div class="col-xs-6 col-sm-4">
                            <button type="button" class="dugme-test1" data-word='apoteka'>Medicine</button>
                        </div>
                    </div>                                      
                <input type="button" name="next" class="next btn btn-lg btn-primary btn-block btn-signin" value="Next" />
             
              </fieldset>
              
            </form>
            </div>
        </div>
    </div>
