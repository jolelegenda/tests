<footer class="footer">
      <div class="container">
        <p class="text-center colorWh">Copyright &copy; 2017 T-List. All rights reserved.</p>
      </div>
    </footer>





   <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
    <script src="<?=_WEB_PATH?>views/testsHome/js/bootstrap.min.js"></script>
    <script src="<?=_WEB_PATH?>views/testsHome/js/jquery.selectric.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="<?=_WEB_PATH?>views/testsHome/js/touchpunch.js"></script>
     <script src="<?=_WEB_PATH?>views/testsHome/js/custom.js"></script>
    <script>
        $(function() {
          // This will select everything with the class smoothScroll
          // This should prevent problems with carousel, scrollspy, etc...
          $('.smoothScroll').click(function() {
            if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
              var target = $(this.hash);
              target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
              if (target.length) {
                $('html,body').animate({
                  scrollTop: target.offset().top
                }, 1000); // The number here represents the speed of the scroll in milliseconds
                return false;
              }
            }
          });
        });



    </script>
    <script type="text/javascript">
        
        //jQuery time
        var current_fs, next_fs, previous_fs; //fieldsets
        var left, opacity, scale; //fieldset properties which we will animate
        var animating; //flag to prevent quick multi-click glitches

        $(".next").click(function(){
            if(animating) return false;
            animating = true;
            
            current_fs = $(this).parent();
            next_fs = $(this).parent().next();
            
            var function_name=$(this).attr("data-qtype")+"_prepareResult";
            var answers;
            var qid=$(this).attr("data-qid");
          /*  if($(this).attr("data-qtype")=="radioImage" || $(this).attr("data-qtype")=="radioSentence")
            {
                answers=radioImage_prepareResult($(this).attr("data-param"));
            }
            else
            {*/
                answers=window[function_name](qid);
          //  }
          //  console.log(answers);
         // var intervalAjax;
            var csrf_token=encodeURIComponent($("#csrf_token").val());
          // alert(csrf_token);
           var json_answers=JSON.stringify(answers);
           var data="csrf_token="+csrf_token+"&questionType="+$(this).attr("data-qtype")+"&modul="+$(this).attr("data-modul")+"&answers="+json_answers+"&last="+$(this).attr("data-last"); 
           //var data="csrf_token="+csrf_token+"&questionType="+$(this).attr("data-qtype")+"&modul="+$(this).attr("data-modul")
            ajaxCall(data,'<?=_WEB_PATH."results"?>',function(data){
                if(data!="")
                {
                  window.location=data;
                  //alert(data);
                }
            })
            // var data="csrf_token="+csrf_token+"&questionType="+$(this).attr("data-qtype")+"&modul="+$(this).attr("data-modul")+"&answers="+json_answers+"&last=0"; 
              //console.log(intervalAjax);
           /* if(typeof intervalAjax!=="undefined")
            {
                clearInterval(intervalAjax);
            }*/
   // window.clearInterval();
         //  var intervalAjax=setInterval(function(){ console.log(qid); }, 1000);
            
           
            //activate next step on progressbar using the index of next_fs
            $("#progressbar table tbody tr td").eq($("fieldset").index(next_fs)).addClass("active");
            
            //show the next fieldset
            next_fs.show(); 
            next_fs.css("position","static");
            //hide the current fieldset with style
            current_fs.animate({opacity: 0}, {
                step: function(now, mx) {
                    //as the opacity of current_fs reduces to 0 - stored in "now"
                    //1. scale current_fs down to 80%
                    scale = 1 - (1 - now) * 0.2;
                    //2. bring next_fs from the right(50%)
                    left = (now * 50)+"%";
                    //3. increase opacity of next_fs to 1 as it moves in
                    opacity = 1 - now;
                    current_fs.css({
                'transform': 'scale('+scale+')',
                'position': 'absolute'
              });
                    next_fs.css({'left': left, 'opacity': opacity});
                }, 
                duration: 800, 
                complete: function(){
                    current_fs.hide();
                    animating = false;
                }, 
                //this comes from the custom easing plugin
                easing: 'easeInOutBack'
            });
            window.scrollTo(0, 0);
        });

        $(".previous").click(function(){
            if(animating) return false;
            animating = true;
            
            current_fs = $(this).parent();
            previous_fs = $(this).parent().prev();
            
            //de-activate current step on progressbar
            $("#progressbar table tbody tr td").eq($("fieldset").index(current_fs)).removeClass("active");
            
            //show the previous fieldset
            previous_fs.css("position","static");
            current_fs.css("position","absolute");
            previous_fs.show(); 
          
            //hide the current fieldset with style
            current_fs.animate({opacity: 0}, {
                step: function(now, mx) {
                    //as the opacity of current_fs reduces to 0 - stored in "now"
                    //1. scale previous_fs from 80% to 100%
                 //   scale = 0.8 + (1 - now) * 0.2;
                    scale = 0.8 + (1 - now) * 0.2;
                    //2. take current_fs to the right(50%) - from 0%
                    left = ((1-now) * 50)+"%";
                    //3. increase opacity of previous_fs to 1 as it moves in
                    opacity = 1 - now;
                    current_fs.css({'left': left});
                    previous_fs.css({'transform': 'scale('+scale+')', 'opacity': opacity});
                    
                }, 
                duration: 800, 
                complete: function(){
                    current_fs.hide();
                    animating = false;
                }, 
                //this comes from the custom easing plugin
                easing: 'easeInOutBack'
            });
            window.scrollTo(0, 0);
        });

        $('select').selectric();

        $(function() { 
             $( ".sortable" ).sortable(
              {
      items: "div:not(.unsortable)",
      stop: function(event, ui) {
          var qid=ui.item.parent().parent().parent().attr("id");
          //console.log(qid);
          var trenutnoData=$("#trenutno"+qid);
                           var function_name=trenutnoData.attr("data-qtype")+"_prepareResult";
            var answers;
            var qid=trenutnoData.attr("data-qid");
          /*  if($(this).attr("data-qtype")=="radioImage" || $(this).attr("data-qtype")=="radioSentence")
            {
                answers=radioImage_prepareResult($(this).attr("data-param"));
            }
            else
            {*/
                answers=window[function_name](qid);
          //  }
          //  console.log(answers);
         // var intervalAjax;
            var csrf_token=encodeURIComponent($("#csrf_token").val());
          // alert(csrf_token);
           var json_answers=JSON.stringify(answers);
           var data="csrf_token="+csrf_token+"&questionType="+trenutnoData.attr("data-qtype")+"&modul="+trenutnoData.attr("data-modul")+"&answers="+json_answers+"&last="+trenutnoData.attr("data-last"); 
           //var data="csrf_token="+csrf_token+"&questionType="+$(this).attr("data-qtype")+"&modul="+$(this).attr("data-modul")
            ajaxCall(data,trenutnoData.attr("data-url"),function(data){
                /*if(data!="")
                {*/
                    //window.location=data;
                    var csrf_token=encodeURIComponent($("#csrf_token").val());
                  ajaxCall("csrf_token="+csrf_token,trenutnoData.attr("data-url1"),function(data){
                     if(data!="")
                      {
                    //window.location=data;
                        // alert(data);
                          $("#congrats_span").html(data);
                         $("#medjustrana").addClass("active");
                         setTimeout(function(){ $("#medjustrana").removeClass("active"); }, 4000);
                     }
                     },'POST')
               // }
            })
      }
              }
                );
        
    $( ".sortable" ).disableSelection();
   $( ".selectable" ).selectable({
        selected : function(){
            $('.unselectable').removeClass('ui-selected');
            var qid=$(this).parent().parent().attr("id");
                     var trenutnoData=$("#trenutno"+qid);
                           var function_name=trenutnoData.attr("data-qtype")+"_prepareResult";
            var answers;
            var qid=trenutnoData.attr("data-qid");
          /*  if($(this).attr("data-qtype")=="radioImage" || $(this).attr("data-qtype")=="radioSentence")
            {
                answers=radioImage_prepareResult($(this).attr("data-param"));
            }
            else
            {*/
                answers=window[function_name](qid);
          //  }
          //  console.log(answers);
         // var intervalAjax;
            var csrf_token=encodeURIComponent($("#csrf_token").val());
          // alert(csrf_token);
           var json_answers=JSON.stringify(answers);
           var data="csrf_token="+csrf_token+"&questionType="+trenutnoData.attr("data-qtype")+"&modul="+trenutnoData.attr("data-modul")+"&answers="+json_answers+"&last="+trenutnoData.attr("data-last"); 
           //var data="csrf_token="+csrf_token+"&questionType="+$(this).attr("data-qtype")+"&modul="+$(this).attr("data-modul")
            ajaxCall(data,trenutnoData.attr("data-url"),function(data){
                /*if(data!="")
                {*/
                    //window.location=data;
                    var csrf_token=encodeURIComponent($("#csrf_token").val());
                  ajaxCall("csrf_token="+csrf_token,trenutnoData.attr("data-url1"),function(data){
                     // alert(data);
                     if(data!="")
                      {
                    //window.location=data;
                        // alert(data);
                          $("#congrats_span").html(data);
                         $("#medjustrana").addClass("active");
                         setTimeout(function(){ $("#medjustrana").removeClass("active"); }, 4000);
                     }
                     },'POST')
               // }
            })
        }
    });
//      $( ".draggable" ).draggable({ 
//          revert: "invalid",
//          containment:"parent"
//        });
//        var i=0;
//    $( ".droppable" ).droppable({
//            drop: function( event, ui ) {
//                
//               i++;
//               ui.draggable.addClass("draggedItem"+i);
//               ui.draggable.addClass("draggedIndicate");
//               $(".draggedItem"+i).css("z-index","0");
//               var indicator=$('.draggedIndicate').length;
//               //console.log(indicator);
//               if(indicator=='2')
//               {
//               i--;
//               $(".draggedItem"+i).css("left","");
//               $(".draggedItem"+i).css("top","");
//               $(".draggedItem"+i).css("z-index","1000");
//               $(".draggedItem"+i).removeClass("draggedIndicate");
//              $(".draggedItem"+i).removeClass("draggedItem"+i);
//              
//              i++;
//               }
//              // ui.draggable.css("z-index","0");
//               
//                $(this).val(ui.draggable.text());
//                //ui.draggable.removeClass("draggable");
//            }
//            });
           
           $('body').on("click", '.dugme-test1', function(){
              var vrednost="";
              $(".img-test1").each(function(){
                  if($(this).attr('class')=="img-test1 img-test1-boja")
                  {
                      vrednost= $(this).attr("data-attr");
                           }
                       })


                       if (vrednost !== "")
                       {
                           $(".dugme-test1").removeClass("dugme-test1-boja");
                           $(this).toggleClass("dugme-test1-boja");
                           $(this).attr("data-attr", vrednost);
                           $(this).removeClass("dugme-test1 dugme-test1-boja").addClass("dugme1-test1 dugme-test1-boja oznaceno");
                           $(".img-test1").each(function () {
                               if ($(this).attr('class') == "img-test1 img-test1-boja")
                               {
                                   // $(this).removeClass("img-test1 img-test1-boja").addClass("img1-test1 img-test1-boja oznaceno");
                                   $(this).removeClass("img-test1 img-test1-boja").addClass("img1-test1 img-test1-boja oznaceno");
                               }

                           })
                       } else
                       {
                           $(".dugme-test1").removeClass("dugme-test1-boja");
                           $(this).toggleClass("dugme-test1-boja");
                       }
                   });
                   $('body').on("click", '.img-test1', function () {
                       var vrednost = "";
                       $(".dugme-test1").each(function () {
                           if ($(this).attr('class') == "dugme-test1 dugme-test1-boja")
                           {
                               vrednost = $(this).attr("data-word");
                           }
                       })
                       if (vrednost !== "")
                       {
                           $(".img-test1").removeClass("img-test1-boja");
                           $(this).toggleClass("img-test1-boja");
                           $(this).attr("data-word", vrednost);
                           // $(this).removeClass("dugme-test1 dugme-test1-boja").addClass("dugme1-test1 dugme-test1-boja oznaceno");
                           $(this).removeClass("img-test1 img-test1-boja").addClass("img1-test1 img-test1-boja oznaceno");
                           $(".dugme-test1").each(function () {
                               if ($(this).attr('class') == "dugme-test1 dugme-test1-boja")
                               {
                                   // $(this).removeClass("img-test1 img-test1-boja").addClass("img1-test1 img-test1-boja oznaceno");
                                   //$(this).removeClass("img-test1 img-test1-boja").addClass("img1-test1 img-test1-boja oznaceno");
                                   $(this).removeClass("dugme-test1 dugme-test1-boja").addClass("dugme1-test1 dugme-test1-boja oznaceno");
                               }

                           })
                       } else
                       {
                           $(".img-test1").removeClass("img-test1-boja");
                           $(this).toggleClass("img-test1-boja");
                       }
                   });
                  
//          $(".test5 > div > div").click(function() {
//              $(".test5 > div > div").removeClass("active-test5");
//            $(this).toggleClass("active-test5");      
//          });
$("#rez").click(function(){
    var string2=radioImage_prepareResult;
    string2(2);
})
        });
    </script>
        <script>
      window.onload = function() {
          document.body.className += " loaded";
      }
</script>
<script>
  
<?php
if(isset($stringDrag))
{
echo $stringDrag;

echo $stringDrop;
}
?>
    </script>
</body>

</html>