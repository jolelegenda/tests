<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
   <!-- <link rel="icon" href="favicon.ico">-->
    <meta name="robots" content="noindex, nofollow">
    <title>Holistic</title>
    
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat:300,400,600,800">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Caveat:400,700">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    
      <link rel="stylesheet" href="<?=_WEB_PATH?>views/testsHome/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?=_WEB_PATH?>views/testsHome/style.css">
    <link rel="stylesheet" href="<?=_WEB_PATH?>views/testsHome/selectric.css">
    <link rel="stylesheet" href="<?=_WEB_PATH?>views/testsHome/test.css">
    <link rel="stylesheet" href="<?=_WEB_PATH?>views/testsHome/responsive.css">
     <link rel="stylesheet" href="<?=_WEB_PATH?>views/testsHome/custom.css">

</head>

<body class="test premium results premium-result">
    <!-- Static navbar -->
    <nav class="navbar navbar-default navbar-static-top">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
                <a class="navbar-brand caveat" href="#"><img class="logo" src="<?=_WEB_PATH?>views/testsHome/images/logo.png" alt="Holistic"> <span>English test</span><img class="test-logo hidden-xs" src="<?=_WEB_PATH?>views/testsHome/images/premium-banner.png" alt="Premium test"></a>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="#">HOME</a></li>
                    <li><a href="#about">TESTS</a></li>
                    <li><a href="#contact">FAQ</a></li>
                    <li><a href="#contact">TERMS</a></li>
                    <li><a class="lastA" href="#contact">CONTACT</a></li>
                </ul>
            </div>
            <!--/.nav-collapse -->
        </div>
    </nav>
    <!--/.navigation -->
    <div class="visible-xs test-logo-2"><img src="<?=_WEB_PATH?>views/testsHome/images/premium-banner-2.png"></div>
