<footer class="footer">
      <div class="container">
        <p class="text-center colorWh">Copyright &copy; 2017 T-List. All rights reserved.</p>
      </div>
    </footer>





    <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
    <script src="<?=_WEB_PATH?>views/testsHome/js/bootstrap.min.js"></script>
    <script src="<?=_WEB_PATH?>views/testsHome/js/jquery.selectric.js"></script>
    
    <script>
        $(function() {
          // This will select everything with the class smoothScroll
          // This should prevent problems with carousel, scrollspy, etc...
          $('.smoothScroll').click(function() {
            if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
              var target = $(this.hash);
              target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
              if (target.length) {
                $('html,body').animate({
                  scrollTop: target.offset().top
                }, 1000); // The number here represents the speed of the scroll in milliseconds
                return false;
              }
            }
          });
        });



    </script>
    <script type="text/javascript">
        
        //jQuery time
        var current_fs, next_fs, previous_fs; //fieldsets
        var left, opacity, scale; //fieldset properties which we will animate
        var animating; //flag to prevent quick multi-click glitches

        $(".next").click(function(){
            if(animating) return false;
            animating = true;
            
            current_fs = $(this).parent();
            next_fs = $(this).parent().next();
            
            //activate next step on progressbar using the index of next_fs
            $("#progressbar table tbody tr td").eq($("fieldset").index(next_fs)).addClass("active");
            
            //show the next fieldset
            next_fs.show(); 
            //hide the current fieldset with style
            current_fs.animate({opacity: 0}, {
                step: function(now, mx) {
                    //as the opacity of current_fs reduces to 0 - stored in "now"
                    //1. scale current_fs down to 80%
                    scale = 1 - (1 - now) * 0.2;
                    //2. bring next_fs from the right(50%)
                    left = (now * 50)+"%";
                    //3. increase opacity of next_fs to 1 as it moves in
                    opacity = 1 - now;
                    current_fs.css({
                'transform': 'scale('+scale+')',
                'position': 'absolute'
              });
                    next_fs.css({'left': left, 'opacity': opacity});
                }, 
                duration: 800, 
                complete: function(){
                    current_fs.hide();
                    animating = false;
                }, 
                //this comes from the custom easing plugin
                easing: 'easeInOutBack'
            });
        });

        $(".previous").click(function(){
            if(animating) return false;
            animating = true;
            
            current_fs = $(this).parent();
            previous_fs = $(this).parent().prev();
            
            //de-activate current step on progressbar
            $("#progressbar table tbody tr td").eq($("fieldset").index(current_fs)).removeClass("active");
            
            //show the previous fieldset
            previous_fs.show(); 
            //hide the current fieldset with style
            current_fs.animate({opacity: 0}, {
                step: function(now, mx) {
                    //as the opacity of current_fs reduces to 0 - stored in "now"
                    //1. scale previous_fs from 80% to 100%
                    scale = 0.8 + (1 - now) * 0.2;
                    //2. take current_fs to the right(50%) - from 0%
                    left = ((1-now) * 50)+"%";
                    //3. increase opacity of previous_fs to 1 as it moves in
                    opacity = 1 - now;
                    current_fs.css({'left': left});
                    previous_fs.css({'transform': 'scale('+scale+')', 'opacity': opacity});
                }, 
                duration: 800, 
                complete: function(){
                    current_fs.hide();
                    animating = false;
                }, 
                //this comes from the custom easing plugin
                easing: 'easeInOutBack'
            });
        });

        $('select').selectric();

        $(function() {                       //run when the DOM is ready
          $(".dugme-test1").click(function() {  //use a class, since your ID gets mangled
            $(this).toggleClass("dugme-test1-boja"); 
            
          });      
          //var clicked=false;
         
             function togglePic(ind=1)
             {
                 
                  
                  if(ind==1)
                  {
                       
                 $( ".img-test1" ).each(function( index ) {
             //console.log( index + ": " + $( this ).attr('class') );
                //if(!clicked)
               // {
                if($( this ).attr('class')!='img-test1 img-test1-boja')
                {
                    $(this).off('click');
                    
                }
                 });
                  }
                  else
                  {
                       $( ".img-test1" ).each(function( index ) {
             //console.log( index + ": " + $( this ).attr('class') );
                //if(!clicked)
               // {
                /*if($( this ).attr('class')=='img-test1 img-test1-boja')
                {*/
                    $(this).on('click',function(){
                        $(this).toggleClass("img-test1-boja")
                        
                    });
//                    if($( this ).attr('class')!='img-test1 img-test1-boja')
//                        {
//                         $(this).off('click');
//                    
//                         }
                    
              // }
                 });
                  }
             }
          var clicked=false;
          //console.log(clicked);
          if(clicked===false)
          {
          $(".img-test1").click(function() {  //use a class, since your ID gets mangled
          $(this).toggleClass("img-test1-boja"); 
            

                togglePic(1);
                clicked=true;
         
         
               // }
                
             
               
                
           
//            if(clicked)
//                {
//                    //alert(1222);
//                    var myFunc = function(event){
//            //  event.stopPropagation();
//            $(this).toggleClass("img-test1-boja"); 
//                 }
//           $( ".img-test1" ).each(function( index ) {
//               $(this).bind('click', myFunc);
//              //alert(index)
//           })
//          
//                    clicked=false;
//                    //return false;
//                }
//                else
//                {
//                clicked=true;
//                }
          });
      }
      else
      {
          
          $(".img-test1 img-test1-boja").click(function() {
              alert(0);
          });
          
      }
//          $(".img-test1 img-test1-boja").click(function() {  //use a class, since your ID gets mangled
//            $(this).toggleClass("img-test1-boja"); 
//            $( ".img-test1" ).each(function( index ) {
//             //console.log( index + ": " + $( this ).attr('class') );
//                /*if($( this ).attr('class')=='img-test1')
//                {*/
//                    $(this).bind('click');
//              //  }
//                
//            });
//          });  //run when the DOM is ready
          $(".test5 > div > div").click(function() {  //use a class, since your ID gets mangled
            $(this).toggleClass("active-test5");      //add the class to the clicked element
          });
        });
    </script>
</body>

</html>

