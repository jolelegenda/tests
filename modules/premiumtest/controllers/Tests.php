<?php
namespace Modules\premiumtest;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Tests
 *
 * @author Sonor Smart Force
 */
class Tests extends \Controllers\frontendController
{
    use \Classes\TestsTrait;
     public function __construct() {
        
        $module_name=\Classes\ModuleHelper::getModuleName(dirname(__FILE__));
       // echo dirname(__FILE__);
        parent::__construct($module_name);
       
      //  $this->testTemplate= new \Classes\TestTemplate();
    }
      public function result($name)
    {
        global $_premiumResults,$_additionalUnits,$_finalText;
        //$name=strtolower($name);
        $this->template->setData("interpretations",$_premiumResults);
        $this->template->setData("additional",$_additionalUnits);
        $this->template->setData("finalText",$_finalText);
        $this->template->render('results');
    }
     public function calculating()
    {
         $this->template->setData("body",'');
        $this->template->render('calc');
    }
}
