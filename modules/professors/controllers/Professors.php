<?php
namespace Modules\professors;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Professor
 *
 * @author Sonor Smart Force
 */
class Professors extends \Controllers\frontendController
{
    public function __construct() {
        
        $module_name=\Classes\ModuleHelper::getModuleName(dirname(__FILE__));
        parent::__construct($module_name);
        
    }
    public function index(){}
    public function register()
    {
        $_POST['months'] = (strlen($_POST['months']) == 1) ? "0" . $_POST['months'] : $_POST['months'];
        $_POST['days'] = (strlen($_POST['days']) == 1) ? "0" . $_POST['days'] : $_POST['days'];
        $_POST['dateofbirth'] = $_POST['years'] . "-" . $_POST['months'] . "-" . $_POST['days'] . " 00:00:00";
        $_POST['description'] = $this->filter_input($_POST['description']);
        $instance = new \Modules\register\Register();
        return $instance->register("\\Models\\Professor", "Professor");
    }
    public function listing()
    {
        $all=\Models\Professor::status(1)->orderBy('ID', 'ASC')->get();
       //dump($all);
        $nizProfesoraSaSlikama=array();
        foreach($all as $a)
        {
            //dump($a->surname);
            $a->namelink=$this->url_friendly($a->name);
            $a->surnamelink=$this->url_friendly($a->surname);        
            $nizProfesoraSaSlikama[]=$a;
        }
        return $nizProfesoraSaSlikama;
    }
    public function info($id)
    {
        $prof=\Models\Professor::find($id);
        return $prof;
    }
}
