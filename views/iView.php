<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Views;

/**
 * Description of iView
 *
 * @author WIN 7 PRO
 */
interface iView {
    
    public function render($view_name);
    
}
