<div class="col-sm-12 text-center col-sm-offset-0 col-md-12 col-md-offset-0 col-lg-8 col-lg-offset-2 box-footer">
        <h4 class="upp">WHO ARE WE?</h4>
        <p>T-list is an online English tutoring platform that
can help you improve your English language
skills. From small children, over to students and
businessmen – everyone can find their ideal
tutor with the help of T-list certified
professionals.
Tests on this website are custom made in
compliance with the PETS and CEFR standards.</p>
    </div>

    <footer class="footer">
      <div class="container">
        <p class="text-center"><img src="<?=_WEB_PATH?>views/testsHome/images/logo-fot.png"></p>
        <ul class="nav navbar-nav">
            <li><a href="#">HOME</a></li>
            <li><a href="#about">TESTS</a></li>
            <li><a href="#contact">FAQ</a></li>
            <li><a href="#contact">TERMS</a></li>
            <li><a href="#contact">CONTACT</a></li>
        </ul>
        <p class="text-center colorWh">Copyright &copy; 2017 T-List. All rights reserved.</p>
      </div>
    </footer>






    <!-- Modal -->
    <div id="myModal" class="modal fade" role="dialog">
      <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Why do I need to provide this?</h4>
          </div>
          <div class="modal-body">
            <p>It is necessary that the data you enter in the contact form to be accurate, because you will get tests that are formed on the basis of your age, education, occupation, etc.
                <br><br>
If you are a parent and you want your child to do the test, enter the child's exact data, and you monitor his work on our platform.
</p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div>

      </div>
    </div>
    
      <div id="myModal1" class="modal fade" role="dialog">
      <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title"><?=$_lngPsihoIntroNaslov?></h4>
          </div>
          <div class="modal-body">
            <?=$_lngPsihoIntro?>
          </div>
          <div class="modal-footer">
              <a href="<?=_WEB_PATH?>test/<?=$psiho1?>"><button type="button" class="btn btn-default" >Start personality test</button></a>
          </div>
        </div>

      </div>
    </div>
      <div id="myModalps2" class="modal fade" role="dialog">
      <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title"><?=$_lngPsiho2IntroNaslov?></h4>
          </div>
          <div class="modal-body">
            <?=$_lngPsiho2Intro?>
          </div>
          <div class="modal-footer">
              <a href="<?=_WEB_PATH?>test/<?=$psiho2?>"><button type="button" class="btn btn-default" >Start personality test</button></a>
          </div>
        </div>

      </div>
    </div>

      <div id="myModalpsmladji" class="modal fade" role="dialog">
      <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Personality test</h4>
          </div>
          <div class="modal-body">
           <?=$_lngPsihoMladjiIntro?>
          </div>
          <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal" >Ok</button>
          </div>
        </div>

      </div>
    </div>
    <script src="<?=_WEB_PATH?>views/testsHome/js/jquery-3.2.1.min.js"></script>
    <script src="<?=_WEB_PATH?>views/testsHome/js/bootstrap.min.js"></script>
    <script src="<?=_WEB_PATH?>views/testsHome/js/jquery.selectric.js"></script>
    <script src="<?=_WEB_PATH?>views/testsHome/js/custom.js"></script>
    <script src="<?=_WEB_PATH?>views/testsHome/js/notify.js"></script>
    <script>
        $(function() {
          // This will select everything with the class smoothScroll
          // This should prevent problems with carousel, scrollspy, etc...
          $('.smoothScroll').click(function() {
            if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
              var target = $(this.hash);
              target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
              if (target.length) {
                $('html,body').animate({
                  scrollTop: target.offset().top
                }, 1000); // The number here represents the speed of the scroll in milliseconds
                return false;
              }
            }
          });
        });

        $('select').selectric();
    </script>
</body>

</html>