
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Have fun and test your English knowledge at the same time">
    <meta name="author" content="">
    <link rel="icon" href="favicon.ico">
    <meta name="robots" content="noindex, nofollow">
    <title>T-List - Test your English knowledge</title>

       <?php
     if(isset($og_url))
     {
     ?>
              <meta property="og:url"           content="<?=$og_url?>" />
  <meta property="og:type"          content="<?=$og_type?>" />
  <meta property="og:title"         content="<?=$og_title?>" />
  <meta property="og:description"   content="<?=$og_description?>" />
    <meta property="og:image"         content="<?=$og_image?>" />
  <meta property="fb:app_id" content="1949722975350924" />
  <?php
     }
  ?>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat:300,400,600,800">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Caveat:400,700">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    
    <link rel="stylesheet" href="<?=_WEB_PATH?>views/testsHome/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?=_WEB_PATH?>views/testsHome/style.css">
    <link rel="stylesheet" href="<?=_WEB_PATH?>views/testsHome/selectric.css">
    <link rel="stylesheet" href="<?=_WEB_PATH?>views/testsHome/responsive.css">
    <link href="https://afeld.github.io/emoji-css/emoji.css" rel="stylesheet">
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body class="home">
    <div id="fb-root"></div>
<script>
  // This is called with the results from from FB.getLoginStatus().
  function statusChangeCallback(response) {
    console.log('statusChangeCallback');
    console.log(response);
    // The response object is returned with a status field that lets the
    // app know the current login status of the person.
    // Full docs on the response object can be found in the documentation
    // for FB.getLoginStatus().
    if (response.status === 'connected') {
      // Logged into your app and Facebook.
      testAPI();
    } else {
      // The person is not logged into your app or we are unable to tell.
      document.getElementById('status').innerHTML = 'Please log ' +
        'into this app.';
    }
  }

  // This function is called when someone finishes with the Login
  // Button.  See the onlogin handler attached to it in the sample
  // code below.
  function checkLoginState() {
//    FB.getLoginStatus(function(response) {
//      statusChangeCallback(response);
//    });
   FB.getLoginStatus(function(response) {
  statusChangeCallback(response);
});
  }

  window.fbAsyncInit = function() {
  FB.init({
    appId      : '1949722975350924',
    cookie     : true,  // enable cookies to allow the server to access 
                        // the session
    xfbml      : true,  // parse social plugins on this page
    version    : 'v2.8' // use graph api version 2.8
  });

  // Now that we've initialized the JavaScript SDK, we call 
  // FB.getLoginStatus().  This function gets the state of the
  // person visiting this page and can return one of three states to
  // the callback you provide.  They can be:
  //
  // 1. Logged into your app ('connected')
  // 2. Logged into Facebook, but not your app ('not_authorized')
  // 3. Not logged into Facebook and can't tell if they are logged into
  //    your app or not.
  //
  // These three cases are handled in the callback function.

 /* FB.getLoginStatus(function(response) {
    statusChangeCallback(response);
  });*/

  };

  // Load the SDK asynchronously
  (function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));

  // Here we run a very simple test of the Graph API after login is
  // successful.  See statusChangeCallback() for when this call is made.
  function testAPI() {
  
    console.log('Welcome!  Fetching your information.... ');
    FB.api('/me?fields=id,name,email,gender', function(response) {
      console.log('Successful login for: ' + response.name);
      console.log(response);
     /* document.getElementById('status').innerHTML =
        'Thanks for logging in, ' + response.name + '!';*/
        $("#name").val(response.name);
        $("#email").val(response.email);
        $("#fb_id").val(response.id);
        if(response.gender=="male")
          {
               $("#inlineRadio2").removeAttr("checked");
               $("#inlineRadio1").prop('checked', true);
                            
          }
          else
          {
               $("#inlineRadio1").removeAttr("checked");
               $("#inlineRadio2").prop('checked', true);
          }
        $("#fb_msg").notify(
                       'Popunite polje datum rodjenja \r\n i kliknite na "get started"', "error"
                      );
    });
  }
</script>
    <!-- Static navbar -->
    <nav class="navbar navbar-default navbar-static-top">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
                <a class="navbar-brand" href="#"><img src="<?=_WEB_PATH?>views/testsHome/images/logo.png" alt="Holistic"></a>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="#">HOME</a></li>
                    <li><a href="#about">TESTS</a></li>
                    <li><a href="#contact">FAQ</a></li>
                    <li><a href="#contact">TERMS</a></li>
                    <li><a class="lastA" href="#contact">CONTACT</a></li>
                    <li><a href="<?=_WEB_PATH?>lang/<?=$lng_suffix?>"><img src="<?=$lng_link?>" /></a></li>
                    
                    
                </ul>
            </div>
            <!--/.nav-collapse -->
        </div>
    </nav>
    <!--/.navigation -->
    <div id="themeSlider" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
            <div class="item active">
                <div class="imgOverlay"></div> <img src="<?=_WEB_PATH?>views/testsHome/images/slide1.jpg" alt="First slide"> </div>
            <div class="item">
                <div class="imgOverlay"></div> <img src="<?=_WEB_PATH?>views/testsHome/images/SL_2.jpg" alt="Second slide"> </div>
            <div class="item">
                <div class="imgOverlay"></div> <img src="<?=_WEB_PATH?>views/testsHome/images/SL_3.jpg" alt="Third slide"> </div>
                  <div class="item">
                <div class="imgOverlay"></div> <img src="<?=_WEB_PATH?>views/testsHome/images/SL_4.jpg" alt="Third slide"> </div>
                  <div class="item">
                <div class="imgOverlay"></div> <img src="<?=_WEB_PATH?>views/testsHome/images/SL_5.jpg" alt="Third slide"> </div>
        </div>
        
        <!--
        <a class="left carousel-control" href="#themeSlider" data-slide="prev"> <span class="fa fa-chevron-left">&lt;</span> </a>
        <a class="right carousel-control" href="#themeSlider" data-slide="next"> <span class="fa fa-chevron-right">&gt;</span> </a>
-->