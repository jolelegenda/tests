        <div class="box row main-text">
            <h3 class="text-center caveat colorWh welcome">Hi! Do you want to find out</h3>
            <h1 class="text-center colorWh welcome2">How good is your English?</h1>
       
            <div class="col-xs-12">
                <div class="card card-container">
                    <form id="form-signin">
                        <span id="athc"></span>
                        <div>
                            <h3 class="caveat colorBl text-center">Start today</h3>
                            <!--<div class="row">
                                <div class="col-xs-3"> <p style="font-size: 15px; padding: 0px 10px 10px 1px;">Pol:</p> </div>
                                <div class="col-xs-4">
                                    <div class="radio radio-inline">
                                        <input id="inlineRadio1" value="option1" name="radioInline" checked="" type="radio">
                                        <label for="inlineRadio1"> Muski </label>
                                    </div>
                                </div>
                                <div class="col-xs-4">
                                    <div class="radio radio-inline">
                                        <input id="inlineRadio2" value="option2" name="radioInline" type="radio">
                                        <label for="inlineRadio2"> Zenski </label>
                                    </div>
                                </div>
                            </div>-->
                            
                            <input type="text" id="name" name='name' class="form-control" placeholder="Name and surname" autofocus value="<?=$name?>">
                            <div class="row" style="margin-bottom: 15px; margin-top: -5px;">
                                <div class="col-xs-12"><span style="font-size: 13px; color: rgb(153, 153, 153);">Date of birth:</span></div>
                                <div class="col-xs-3 first-xs"> 
                                    <div id='MonthId'>
                                    <?php 
                                   // echo $birth;
                                    //if($birth!=="")
                                    $birthArr=explode(".",$birth);
                                   // dump($birthArr);
                                    
                                    echo \Classes\FormHelper::monthsCombo(($birth!=="")?$birthArr[1]:""); 
                                    ?>
                                    </div>
                                    <span id="fb_msg"></span>
                                </div>
                                
                                <div class="col-xs-3"> 
                                     <div id='DayId'>
                                    <?php echo \Classes\FormHelper::daysCombo(($birth!=="")?$birthArr[0]:""); ?>
                                     </div>
                                </div>
                                <div class="col-xs-3"> 
                                     <div id='YearId'>
                                    <?php echo \Classes\FormHelper::yearsCombo(($birth!=="")?$birthArr[2]:""); ?>
                                     </div>
                                </div>
                                <div class="col-xs-3 last-xs"> 
                                    <button type="button" class="btn btn-info btn-lg modal-txt" data-toggle="modal" data-target="#myModal"><small style="display: block;">Why do I need to provide this?</small></button>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="row">
                                <div class="col-xs-3 col-sm-4"> <p style="font-size: 15px; padding: 0px 10px 10px 1px;">Gender:</p> </div>
                                <div class="col-xs-4">
                                    <div class="radio radio-inline">
                                        <input id="inlineRadio1" value="m" name="sex" type="radio" <?php echo ($sex=="m" || $sex=="")?"checked":""?>>
                                        <label for="inlineRadio1"> Male </label>
                                    </div>
                                </div>
                                <div class="col-xs-4">
                                    <div class="radio radio-inline">
                                        <input id="inlineRadio2" value="f" name="sex" type="radio" <?php echo ($sex=="f")?"checked":""?>>
                                        <label for="inlineRadio2"> Female </label>
                                    </div>
                                </div>
                            </div>
                            <input type="text" name='email' id="email" class="form-control" placeholder="Email" value="<?=$email?>">
                            <input type="hidden" id='fb_id' name='fb_id' value=""/>
                            <?=$csrf_field?>
                            <button class="btn btn-lg btn-primary btn-block btn-signin" type="submit">GET STARTED!</button>
                            
                            <span id='reg'><span>
                                    <br>
                                    <div style="width:250px;margin:0 auto"><fb:login-button class="fb-login-button" data-max-rows="1" data-size="large" data-button-type="continue_with" data-show-faces="false" data-auto-logout-link="false" data-use-continue-as="false" scope="public_profile,email"  onlogin="checkLoginState();">
                                        </fb:login-button></div>
                   <div id="status">
</div>               
                        </div>
                    </form>
                    <!-- /form -->
                </div>
                <!-- /card-container -->
            </div>
        <p class="text-center anchor clearfix"><a class="smoothScroll" href="#timeline"><img src="<?=_WEB_PATH?>views/testsHome/images/arrow.png"></a></p>
        </div>
    </div>
    <!-- /container -->
    
    
            
    <div class="container" id="timeline">  
        <div class="row">
            <div class="col-md-12 col-lg-10 col-lg-offset-1">
                <h3 class="text-center caveat colorBl">Are you a novice or an expert?</h3>
                <h2 style="margin-top: 0; font-size: 36px; margin-bottom: 110px" class="text-center">Have fun and test your English knowledge at
the same time.</h2>
            </div>
            <div class="col-md-12 col-lg-10 col-lg-offset-1">
                <div class="col-lg-5 col-sm-5 col-sm-offset-0 timeline-line text-center"><img src="<?=_WEB_PATH?>views/testsHome/images/teacher.png"> </div>

                <div class="col-lg-5 col-sm-5 col-sm-offset-2 text-timeline col-lg-offset-2 text-left">
                    <h4>2 IN 1: PERSONALITY &amp; LANGUAGE TESTS</h4>
                    <p>Not only will you find out at which level your English language currently is, but you will also get a personality test. All that for FREE. Advanced test are also available for a small fee.</p>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="col-md-12 col-lg-10 col-lg-offset-1">

                <div class="col-lg-5 col-lg-pull-0 col-sm-push-5 col-sm-5 col-sm-offset-2 col-lg-offset-2 text-center">
                    <img src="<?=_WEB_PATH?>views/testsHome/images/calendar.png" style="padding: 44px 15px;">
                </div>
                <div class="col-lg-5 col-lg-pull-7 col-sm-pull-7 col-sm-5 col-sm-offset-0 text-timeline timeline-line text-right">
                    <h4>ANY PLACE, ANY TIME</h4>
                    <p>You can test your knowledge easily on your smartphone, tablet or laptop/desktop computer. Tests are interactive and take no longer than 10-15 minutes.</p> 
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="col-md-12 col-lg-10 col-lg-offset-1">
                <div class="col-lg-5 col-sm-5 text-center timeline-line"><img src="<?=_WEB_PATH?>views/testsHome/images/girl.png"> </div>

                <div class="col-lg-5 col-sm-5 col-sm-offset-2 text-timeline col-lg-offset-2 text-left">
                    <h4>TAKE THE NEXT STEP</h4>
                    <p>With our custom made English tests, you will
get a clear idea on what you should focus next.
Our English tutors can help you develop your
English knowledge further. Just choose your
tutor and begin your journey!</p>
                </div>
            </div>
        </div>
    </div>
    <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal1" id="psiho-trigger" style='display:none'>Open Modal</button>
    <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModalps2" id="psiho2-trigger" style='display:none'>Open Modal</button>
    <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModalpsmladji" id="psihomladji-trigger" style='display:none'>Open Modal</button>
    <script>
     
    window.onload=function(){
        //alert(123);
     /*  $( "#why" ).bazeModal({
               closeOnOverlayClick:true   
                });*/
        $( "#form-signin" ).on( "submit", function( event ) {
            event.preventDefault();
           // console.log( $( this ).serialize() );
            //var data=$( this ).serialize();
            var fd = new FormData();
          /*  var file_data = $('#images').prop('files')[0];                   
            fd.append('photo', file_data);*/
             var other_data = $( this ).serializeArray();
            $.each(other_data,function(key,input){
                fd.append(input.name,input.value);
            });
          /*  var method = $('#method_id option:selected').val();
            fd.append('method_id',method);
            var category = $('#category_id option:selected').val();
            fd.append('category_id',category);*/
    //console.log(file_data);
    
            ajaxCall(fd,"<?php echo _WEB_PATH."submit-reg"?>",function(data){
                var obj=JSON.parse(data);
                //console.log(obj.hasErrors)
                errorString="";
                if(obj.hasErrors==1)
                {
                for(i in obj.errors)
                {
                    //console.log(obj.errors[i]);
                    for(n in obj.errors[i])
                    {
                        //console.log(obj.errors[i][n]);
                        errorString=errorString+obj.errors[i][n]+"<br/>";
                        
                    }
                    $("#"+i).notify(
                       obj.errors[i][0], "error"
                      );
                }
                
                      /*  $("#msg").removeClass( "alert success").addClass( "alert error" );
                        $("#msg").css("display","block");
                        $("#msg").css("text-align","center");
                        $("#error-msgs").html(errorString);*/
                        //$("#ime").attr("value",obj.errors.name[0]);
                       // $("#email").attr("value",obj.errors.email[0]);
                    
                }
                else
                {
                    console.log(obj);
                    if(obj.message=="Exists")
                    {
                        console.log(obj.data.sex);
                        $("#name").val(obj.data.name);
                        $("#MonthId").html(obj.data.month);
                        $("#YearId").html(obj.data.year);
                        $("#DayId").html(obj.data.day);
                        if(obj.data.sex=="m")
                        {
                            $("#inlineRadio2").removeAttr("checked");
                            $("#inlineRadio1").prop('checked', true);
                            
                        }
                        else
                        {
                            $("#inlineRadio1").removeAttr("checked");
                            $("#inlineRadio2").prop('checked', true);
                        }
                        $('select').selectric();
                    }
                    var godine=obj.data.tstampnow-obj.data.tstamp;
                    godine=Math.round(godine/31556926);
                    if(godine>14 && godine<19)
                    {
                        $("#psiho-trigger").trigger("click");
                    }
                    else if(godine<=14)
                    {
                        $("#psihomladji-trigger").trigger("click");
                    }
                    else
                    {
                        $("#psiho2-trigger").trigger("click");
                    }
                    
                        
                    
//                        $("#msg").removeClass( "alert error").addClass( "alert success" );
//                        $("#msg").css("display","block");
//                        $("#msg").css("text-align","center");
//                        $("#error-msgs").html(obj.message);
                       // alert('wow');
                        //$("#psiho-trigger").trigger("click");
                        
                }
            },'POST',true);
             
        });
       
    }
</script>